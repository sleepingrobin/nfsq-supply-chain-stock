package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:42
 */
@Getter
@AllArgsConstructor
public enum StockResultCode {
    SUCCESS("SUCCESS", "成功"),

    SYSTEM_ERROR("SYSTEM_ERROR", "系统错误"),
    PARAM_ILLEGALITY("PARAM_ILLEGALITY", "参数不合法"),
    PARAM_EMPTY("PARAM_EMPTY", "参数为空"),
    PARAM_LENGTH_ILLEGALITY("PARAM_ILLEGALITY", "参数长度过长"),SAP_EXCEPTION("SAP_EXCEPTION","创建sap入库单异常:"),

    COMPANY_NOT_FOUND("COMPANY_NOT_FOUND","公司不存在"),
    FRANCHISER_NOT_FOUND("FRANCHISER_NOT_FOUND","经销商不存在"),
    STORE_NOT_FOUND("STORE_NOT_FOUND","门店不存在"),
    SKU_NOT_FOUND("SKU_NOT_FOUND","物料不存在");
    /**
     * 错误码
     */
    private final String code;

    /**
     * 描述
     */
    private final String desc;

}
