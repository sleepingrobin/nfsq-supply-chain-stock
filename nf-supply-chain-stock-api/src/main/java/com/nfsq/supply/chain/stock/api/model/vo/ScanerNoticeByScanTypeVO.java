package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.ScanOperDTO;
import com.nfsq.supply.chain.stock.api.model.dto.ScanerNoticeByScanTypeDTO;
import com.nfsq.supply.chain.stock.api.model.dto.ScanerNoticeDetailDTO;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "ReqSwagger2",description = "通知单")
public class ScanerNoticeByScanTypeVO {

    @ApiModelProperty(value = "noticeId",name = "通知单Id",dataType = "Long")
    private Long noticeId;

    @ApiModelProperty(value = "noticeNo",name = "通知单No",dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "custId",name = "客户ID",dataType = "Long")
    private Long custId;

    @ApiModelProperty(value = "custName",name = "客户名称",dataType = "String")
    private String custName;

    @ApiModelProperty(value = "custType",name = "客户类型",dataType = "Integer")
    private Integer custType;

    @ApiModelProperty(value = "repoId",name = "仓库Id",dataType = "Long")
    private Long repoId;

    @ApiModelProperty(value = "bind",name = "绑定",dataType = "String")
    private String bind;

    @ApiModelProperty(value = "taskId",name = "任务单号",dataType = "Long")
    private Long taskId;

    @ApiModelProperty(value = "是否扫码（0 非扫码，1 扫码）",name = "任务单号",dataType = "Integer")
    private Integer scanType;

    @ApiModelProperty(value = "通知单类型",name = "通知单类型",dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "detailList",name = "明细列表",dataType = "List")
    private List<ScanerNoticeByScanTypeDTO> detailList;


    @ApiModelProperty(value = "scanOperList",name = "打印方式列表",dataType = "List")
    private List<ScanOperDTO> scanOperList;

    @ApiModelProperty(value = "processing", name = "是否正在进行", dataType = "Boolean")
    private Boolean processing;

    public Boolean getProcessing() {
        return processing;
    }

    public void setProcessing(Boolean processing) {
        this.processing = processing;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Integer getCustType() {
        return custType;
    }

    public void setCustType(Integer custType) {
        this.custType = custType;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    public String getBind() {
        return bind;
    }

    public void setBind(String bind) {
        this.bind = bind;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getScanType() {
        return scanType;
    }

    public void setScanType(Integer scanType) {
        this.scanType = scanType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<ScanerNoticeByScanTypeDTO> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<ScanerNoticeByScanTypeDTO> detailList) {
        this.detailList = detailList;
    }

    public List<ScanOperDTO> getScanOperList() {
        return scanOperList;
    }

    public void setScanOperList(List<ScanOperDTO> scanOperList) {
        this.scanOperList = scanOperList;
    }
}
