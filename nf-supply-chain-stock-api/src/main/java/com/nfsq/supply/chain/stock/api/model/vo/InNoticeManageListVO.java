package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/7/21 上午10:12
 */
@Getter
@Setter
public class InNoticeManageListVO extends PageParameter {

    @ApiModelProperty(value = "入库通知单List",name = "入库通知单List",dataType = "Long")
    private List<InNoticeManageVO> voList;

}
