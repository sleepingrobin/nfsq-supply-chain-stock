package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/7/21 下午1:10
 */
@Getter
@Setter
public class WmsInInDetailDTO {

    @ApiModelProperty(value = "skuId",name = "skuId",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "sku编号",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "sku名称",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "sku单位",name = "sku单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "批次编号",name = "批次编号",dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "数量",name = "数量",dataType = "Integer")
    private Integer amount;

    @ApiModelProperty(value = "资源拥有者ID",name = "资源拥有者ID",dataType = "Integer")
    private Long owner;

    @ApiModelProperty(value = "资源拥有者名字",name = "资源拥有者名字",dataType = "Integer")
    private String ownerName;

    @ApiModelProperty(value = "资源拥有者类型",name = "资源拥有者类型",dataType = "Integer")
    private Integer ownerType;

}
