package com.nfsq.supply.chain.stock.api.model.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:26
 */
@ApiModel(value = "SkuInfoBean",description = "sku信息")
@Getter
@Setter
public class SkuInfoBean {

    @ApiModelProperty(value = "skuId",name = "物料编码",dataType = "Long",required =true)
    private Long skuId;

    @ApiModelProperty(value = "batchNo",name = "批次信息",dataType = "String",required =true)
    private String batchNo;
    
    @ApiModelProperty(value = "owner",name = "资源拥有者",dataType = "Long",required =true)
    private Long owner;

    @ApiModelProperty(value = "ownerType",name = "资源拥有者类型",dataType = "Integer",required =false)
    private Integer ownerType;

    @ApiModelProperty(value = "amount",name = "数量",dataType = "Integer",required =true)
    private Integer amount;

    @ApiModelProperty(value = "status",name = "状态",dataType = "Integer",required =true)
    private Integer status;

    @ApiModelProperty(value = "ruleGroup",name = "用途组",dataType = "Integer",required =true)
    private Integer ruleGroup;
    
    @ApiModelProperty(value = "ruleType",name = "用途类型",dataType = "Integer",required =true)
    private Integer ruleType;
    
    @ApiModelProperty(value = "ruleType",name = "用途详情",dataType = "Long",required =false)
    private Long ruleDetail;

    @ApiModelProperty(value = "scanType", name = "出入库方式（0 非扫码  1 扫码）", dataType = "Integer", required = true)
    private Integer scanType;

    @ApiModelProperty(value = "targetOwner",name = "目标资源拥有者",dataType = "Long",required =false)
    private Long targetOwner;

    @ApiModelProperty(value = "targetOwnerType",name = "目标资源拥有者类型",dataType = "Integer",required =false)
    private Integer targetOwnerType;
}
