package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xwcai
 * @date 2018/5/22 下午6:34
 */
@Getter
@Setter
public class OutNoticeManageVO {

    @ApiModelProperty(value = "noticeId",notes = "通知单Id", dataType = "Long")
    private Long noticeId;

    @ApiModelProperty(value = "noticeNo",notes = "通知单NO", dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "taskId",notes = "任务ID", dataType = "Long")
    private Long taskId;

    @ApiModelProperty(value = "taskNo",notes = "任务NO", dataType = "String")
    private String taskNo;

    @ApiModelProperty(value = "type", notes = "出库类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "typeDesc", notes = "出库类型", dataType = "String")
    private String typeDesc;

    @ApiModelProperty(value = "destinationStockId",notes = "目的地仓库Id", dataType = "Long")
    private Long destinationStockId;

    @ApiModelProperty(value = "destinationName",notes = "目的地名字", dataType = "String")
    private String destinationName;

    @ApiModelProperty(value = "supplyStockId",notes = "供货方仓库Id", dataType = "Long")
    private Long supplyStockId;

    @ApiModelProperty(value = "supplyName",notes = "供货方名称", dataType = "String")
    private String supplyName;

    @ApiModelProperty(value = "contactName",notes = "联系人", dataType = "String")
    private String contactName;

    @ApiModelProperty(value = "phoneNum",notes = "联系电话", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "address",notes = "地址", dataType = "String")
    private String address;

    @ApiModelProperty(value = "createDate",notes = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "operStatus",notes = "状态", dataType = "Integer")
    private Integer operStatus;

    @ApiModelProperty(value = "operStatusDesc",notes = "状态描述", dataType = "String")
    private String operStatusDesc;

    @ApiModelProperty(value = "transNo",notes = "运单ID", dataType = "String")
    private String transNo;

    @ApiModelProperty(value = "expressNo",notes = "快递单号", dataType = "String")
    private String expressNo;

    @ApiModelProperty(value = "detailManageVO",notes = "详情", dataType = "Object")
    private OutNoticeDetailManagePageVO detailManageVO;

    @ApiModelProperty(value = "printTimes", name = "打印次数",dataType = "Integer")
    private Integer printTimes;

    @ApiModelProperty(value = "isPrint", name = "是否打印过",dataType = "Integer")
    private Integer isPrint;

    @ApiModelProperty(value = "sap交货单单号",notes = "sap交货单单号",dataType = "String")
    private String sapOrderNo;

    @ApiModelProperty(value = "isAllowFinish", name = "是否允许结单",dataType = "Integer")
    private Integer isAllowFinish;

    @ApiModelProperty(value = "systemType", name = "系统类型",dataType = "Integer")
    private Integer systemType;

}
