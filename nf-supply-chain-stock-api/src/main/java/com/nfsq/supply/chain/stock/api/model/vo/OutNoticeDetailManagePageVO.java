package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDetailDTO;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/22 上午11:46
 */
@Getter
@Setter
public class OutNoticeDetailManagePageVO extends PageParameter {

    private List<WmsOutOutNoticeDetailDTO> detailDTOS;

    @ApiModelProperty(value = "totalGrossWeight", name = "总毛重",dataType = "BigDecimal")
    private BigDecimal totalGrossWeight;

}
