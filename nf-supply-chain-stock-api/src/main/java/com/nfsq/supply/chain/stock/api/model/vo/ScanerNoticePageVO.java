package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/19 下午2:54
 */
@Getter
@Setter
@ApiModel(value = "ReqSwagger2",description = "扫码枪通知单显示")
public class ScanerNoticePageVO extends PageParameter{

    @ApiModelProperty(value = "noticeList",name = "通知单列表",dataType = "List")
    private List<ScanerNoticeVO> noticeList;

}
