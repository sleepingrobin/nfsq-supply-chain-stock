package com.nfsq.supply.chain.stock.api.model.qo;

import java.lang.reflect.Field;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 库存查询实体
 * @author  wangk
 * @date 2018年5月16日 上午11:20:28
 */
@ApiModel(value = "WmsStockQO",description = "查询库存")
@Getter
@Setter
public class WmsStockQO extends PageBaseBean {
	@ApiModelProperty(value = "skuId", name = "产品ID", dataType = "Long")
	private Long skuId;

	@ApiModelProperty(value = "id", name = "工厂ID/门店ID/经销商ID", dataType = "Long")
    private Long id;
	
	@ApiModelProperty(value = "batchNo", name = "批次号", dataType = "String")
	private String batchNo;

	@ApiModelProperty(value = "repoId", name = "仓库ID", dataType = "Long")
    private Long repoId;
	
	@ApiModelProperty(value = "machineId", name = "机器ID", dataType = "Long")
    private String machineId;
	
	@ApiModelProperty(value = "type", name = "1工厂/3门店/4经销商/2DC", dataType = "Integer")
	private Integer type;
	
	private List<Long> repoIds;

	/* (non-Javadoc)
	 * @see com.nfsq.supply.chain.utils.common.PageBaseBean#validate()
	 */
	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();	
		if(null!=id&&null==type)
		{
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}

}
