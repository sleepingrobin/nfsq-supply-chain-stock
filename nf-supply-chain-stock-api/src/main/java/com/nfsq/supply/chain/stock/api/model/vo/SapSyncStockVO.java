package com.nfsq.supply.chain.stock.api.model.vo;

import java.util.Map;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author sundi
 *
 */
@ApiModel(value = "ReqSwagger2",description = "返回sap同步库存信息")
public class SapSyncStockVO {
	//key为skuNo,value为总数量
	@ApiModelProperty(value = "skuNumMap",name = "sap同步库存信息",dataType = "Map")
	 private Map<String,Integer> skuNumMap;

    public Map<String, Integer> getSkuNumMap()
    {
        return skuNumMap;
    }

    public void setSkuNumMap(Map<String, Integer> skuNumMap)
    {
        this.skuNumMap = skuNumMap;
    }

}
