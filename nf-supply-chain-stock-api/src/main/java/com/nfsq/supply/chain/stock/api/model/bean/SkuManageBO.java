package com.nfsq.supply.chain.stock.api.model.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/7/1 下午2:48
 */
@Getter
@Setter
public class SkuManageBO {

    @ApiModelProperty(value = "skuId",name = "物料id",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "skuNo",name = "物料编码",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "skuName",name = "物料名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "skuUnit",name = "基本单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "num",name = "数量",dataType = "Integer")
    private Integer num;

    @ApiModelProperty(value = "proportion",name = "百分比",dataType = "BigDecimal")
    private BigDecimal proportion;

}
