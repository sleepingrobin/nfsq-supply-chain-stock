package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/9/24 上午10:57
 */
@Getter
@Setter
public class FinishOutNoticeBean extends BaseBean {

    private Long noticeId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(null == noticeId || noticeId <= 0 ){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }
}
