package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/7/21 上午10:27
 */
@ApiModel(value = "查询入库通知单的qo",description = "入库通知单查询")
@Getter
@Setter
public class InNoticeQO extends PageBaseBean {

    @ApiModelProperty(value = "入库通知单Id",notes = "入库通知单Id",dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "入库通知单编号",notes = "入库通知单编号", dataType = "String")
    private String inNoticeNo;

    @ApiModelProperty(value = "入库通知单状态",notes = "入库通知单状态", dataType = "Integer")
    private List<Integer> operStatuses;

    @ApiModelProperty(value = "入库通知单类型",notes = "入库通知单类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "入库来源Id",notes = "入库来源Id", dataType = "Long")
    private Long inSourceId;

    @ApiModelProperty(value = "开始时间",notes = "开始时间", dataType = "Date")
    private Date startDate;

    @ApiModelProperty(value = "结束时间",notes = "结束时间", dataType = "Date")
    private Date endDate;

    @ApiModelProperty(value = "物流单号",notes = "物流单号", dataType = "String")
    private String expressNo;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
