package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/4/23 下午5:07
 */
@Getter
@AllArgsConstructor
public enum DestinationEnum {
	FACTORY(1,"工厂"),
	DEPOT(2,"仓库"),
	DEALER(3,"经销商"),
	STORE(4,"门店");


    /**
     * code
     */
    private Integer type;

    /**
     * desc 说明
     */
    private String typeDes;


    public static DestinationEnum getByType(Integer type){
        for(DestinationEnum destinationEnum : DestinationEnum.values()){
            if(destinationEnum.getType().equals(type)){
                return destinationEnum;
            }
        }
        return  null;
    }
}
