package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author xwcai
 * @date 2019/2/28 上午9:56
 */
@ApiModel(value = "出库通知单批量查询",description = "出库通知单批量查询")
@Getter
@Setter
public class OutNoticeBatchQO extends BaseBean {

    @ApiModelProperty(value = "outNoticeNos",notes = "出库通知单号",dataType = "String",required =true)
    private List<String> outNoticeNos;

    @Override
    public JsonResult<?> validate() {
        JsonResult jsonResult = new JsonResult();
        if(CollectionUtils.isEmpty(outNoticeNos)){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }
}
