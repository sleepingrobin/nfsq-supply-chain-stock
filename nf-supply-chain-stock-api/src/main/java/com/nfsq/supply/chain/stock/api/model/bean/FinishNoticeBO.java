package com.nfsq.supply.chain.stock.api.model.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/5/21 下午3:13
 */
@Getter
@Setter
public class FinishNoticeBO {

    @ApiModelProperty(value = "noticeId", name = "通知单Id", dataType = "Long")
    private Long noticeId;

    @ApiModelProperty(value = "noticeNO", name = "通知单编号", dataType = "String")
    private String noticeNO;

    @ApiModelProperty(value = "taskId", name = "任务Id", dataType = "Long")
    private Long taskId;

}
