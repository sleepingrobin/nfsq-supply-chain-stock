package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.BillNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.NoticeFinishQO;
import com.nfsq.supply.chain.stock.api.model.vo.BillCountVO;
import com.nfsq.supply.chain.stock.api.model.vo.BillVO;
import com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author xwcai
 * @date 2018/6/19 下午3:00
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/notice",method = RequestMethod.POST)
public interface NoticeProviderApi {

    /**
     * 判断通知单是否全部完成
     *
     * @author xwcai
     * @date 2018/6/19 下午3:01
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     */
    @PostMapping(value = "/query/noticeFinish")
    JsonResult<Boolean> checkNoticeFinish(NoticeFinishQO qo);

    /**
     * 根据任务id 查询任务下所有出库通知单 出库单 入库通知单 入库单  单号
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.BillNoVO>
     * @author xwcai
     * @date 2018/5/31 上午9:45
     */
    @PostMapping(value = "/query/billInfo")
    JsonResult<BillVO> queryBillNOByQO(BillNoQO qo);

    /**
     * 根据时间段 查询单据数量
     *
     * @author xwcai
     * @date 2018/6/21 下午2:39
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.BillCountVO>
     */
    @PostMapping(value = "/query/billCount")
    JsonResult<BillCountVO> queryBillCountByQO(BillCountQO qo);

    /**
     * 查询待办单据数量
     *
     * @author xwcai
     * @date 2018/6/21 下午2:39
     * @param bean
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.BillCountVO>
     */
    @PostMapping(value = "/query/process/billCount")
    JsonResult<ProcessCountVO> queryProcessBillCountByQO(DummyBean bean);
}
