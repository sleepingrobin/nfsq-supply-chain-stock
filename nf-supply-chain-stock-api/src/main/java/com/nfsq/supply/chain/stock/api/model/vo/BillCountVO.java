package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author xwcai
 * @date 2018/6/21 下午2:34
 */
@Getter
@Setter
public class BillCountVO {

    @ApiModelProperty(value = "出库通知单数量",name = "出库通知单数量",dataType = "Integer")
    private Integer outNoticeCount;

    @ApiModelProperty(value = "出库单数量",name = "出库单数量",dataType = "Integer")
    private Integer outOutCount;

    @ApiModelProperty(value = "入库通知单数量",name = "入库通知单数量",dataType = "Integer")
    private Integer inNoticeCount;

    @ApiModelProperty(value = "入库单数量",name = "入库单数量",dataType = "Integer")
    private Integer inInCount;

    @ApiModelProperty(value = "生产入库单数量",name = "生产入库单数量",dataType = "Integer")
    private Integer productInInCount;

    @ApiModelProperty(value = "任务数量",name = "任务数量",dataType = "Integer")
    private Integer taskCount;

    @ApiModelProperty(value = "运单数量",name = "运单数量",dataType = "Integer")
    private Integer wayBillCount;

    @ApiModelProperty(value = "销售订单订单数量",name = "订单数量",dataType = "Integer")
    private Integer saleOrderCount;

    @ApiModelProperty(value = "转储订单订单数量",name = "订单数量",dataType = "Integer")
    private Integer dumpOrderCount;

    @ApiModelProperty(value = "非限制库存 包括百分比",name = "非限制库存 包括百分比",dataType = "List")
    private List<SkuManageBO> unLimitStockAndProp;

    @ApiModelProperty(value = "已经发送库存",name = "已经发送库存",dataType = "List")
    private List<SkuManageBO> sendSkuInfos;

    @ApiModelProperty(value = "未发送库存",name = "未发送库存",dataType = "List")
    private List<SkuManageBO> unSendInfos;

    @ApiModelProperty(value = "审核门店订单数量当月 每天统计",name = "审核门店订单数量当月 每天统计",dataType = "Map")
    private List<Integer> storeOrders;

    @ApiModelProperty(value = "运单数量 当月每天统计",name = "运单数量 当月每天统计",dataType = "Map")
    private List<Integer> wayBills;

    @ApiModelProperty(value = "审批的订单数量",name = "审批的订单数量",dataType = "Integer")
    private Integer checkOrderNum;

    @ApiModelProperty(value = "已发货运单数量",name = "已发货运单数量",dataType = "Integer")
    private Integer sendWayBillNum;

}
