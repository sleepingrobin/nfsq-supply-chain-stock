package com.nfsq.supply.chain.stock.api.model.qo;

import java.lang.reflect.Field;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author sundi
 *
 */
@ApiModel(value = "ReqSwagger2", description = "查询门店库存")
public class StoreStockQO extends BaseBean {
	@ApiModelProperty(value = "repoId", name = "仓库ID", dataType = "Long", required = true)
	private Long repoId;
	
	@ApiModelProperty(value = "storeId", name = "门店ID", dataType = "Long", required = true)
	private Long storeId;
    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
     */
    @Override
    public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		for (Field field : getClass().getDeclaredFields()) {
			ApiModelProperty an = field.getAnnotation(ApiModelProperty.class);
			if(an != null && an.required()){
				ReflectionUtils.makeAccessible(field);
				Object value = ReflectionUtils.getField(field, this);
				if(StringUtils.isEmpty(value)){
					return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
				}
			}
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}

	public Long getRepoId() {
		return repoId;
	}

	public void setRepoId(Long repoId) {
		this.repoId = repoId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	
}
