package com.nfsq.supply.chain.stock.api.model.qo;
import java.util.List;

import org.springframework.util.CollectionUtils;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author sundi
 *
 */
@ApiModel(value = "ReqSwagger2", description = "查询sap同步库存")
public class SapSyncStockQO extends BaseBean {
	@ApiModelProperty(value = "skuNoList", name = "物料编号", dataType = "Long", required = true)
	private List<String> skuNoList;
	@ApiModelProperty(value = "repoId", name = "仓库id", dataType = "Long")
	private Long repoId;
	
    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
     */
    @Override
    public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		if(CollectionUtils.isEmpty(skuNoList))
		{
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}

    public Long getRepoId()
    {
        return repoId;
    }

    public void setRepoId(Long repoId)
    {
        this.repoId = repoId;
    }

    public List<String> getSkuNoList()
    {
        return skuNoList;
    }

    public void setSkuNoList(List<String> skuNoList)
    {
        this.skuNoList = skuNoList;
    }
}
