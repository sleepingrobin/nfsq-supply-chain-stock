package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.bean.FinishOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.OutNoticeListBean;
import com.nfsq.supply.chain.stock.api.model.bean.SendOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeBatchQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticePrintTimesQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xwcai
 * @date 2018/5/9 下午3:46
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/out/notice",method = RequestMethod.POST)
public interface OutNoticeProviderApi {

    /**
     * 批量创建出库通知单
     *
     * @author xwcai
     * @date 2018/5/9 下午4:02
     * @param outNoticeBeans
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     */
    @PostMapping(value = "/list/create")
    JsonResult<Map<String,String>> listOutNoticeCreate(OutNoticeListBean bean);

    /**
     * 查询出库通知单 包含详情
     *
     * @author xwcai
     * @date 2018/5/13 下午3:31
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     */
    @PostMapping(value = "/list/queryWithDetail")
    JsonResult<OutNoticeVOList> queryWithDetailByQo(OutNoticeQo qo);

    /**
     * 根据qo查询  给管理页面用的分页
     *
     * @author xwcai
     * @date 2018/5/22 下午7:04
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     */
    @PostMapping(value = "manage/query/list")
    JsonResult<OutNoticeManageListVO> manageQueryByQO(OutNoticeQo qo) ;

    /**
     * 根据qo查询待办  给管理页面用的分页
     *
     * @author xwcai
     * @date 2018/5/22 下午7:04
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     */
    @PostMapping(value = "manage/query/wait/list")
    JsonResult<OutNoticeManageListVO> manageQueryWaitByQO(OutNoticeQo qo) ;

    /**
     * 根据单号Id获取详情list
     *
     * @author xwcai
     * @date 2018/5/23 上午9:45
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDetailDTO>>
     */
    @PostMapping(value = "manage/detail")
    JsonResult<OutNoticeDetailManagePageVO> manageQuery(OutNoticeQo qo);

    /**
     * 查询未完成
     *
     * @author xwcai
     * @date 2018/5/23 上午10:42
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Long>
     */
    @PostMapping(value = "manage/unfinish/count")
    JsonResult<Long> manageQueryUnFinishCount(DummyBean bean);

    /**
     * 根据qo 查询所有出库通知单
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:56
     */
    @PostMapping(value = "queryByQO")
    JsonResult<OutNoticeVOList> queryByQo(OutNoticeQo qo);

    /**
     * 根据qo 查询所有出库通知单
     *
     * @param bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:56
     */
    @PostMapping(value = "queryAllOutNoticeNos")
    JsonResult<List<String>> queryAllOutNoticeNos(DummyBean bean);

    /**
     * 根据出库单号查询详情 以及主体
     *
     * @author xwcai
     * @date 2018/6/25 下午6:37
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     */
    @PostMapping(value = "queryWithDetailByNO")
    JsonResult<OutNoticeManageVO> queryWithDetailByNO(OutNoticeQo qo) ;

    /**
     * 打印时调用该方法 ，更新打印次数
     * @param outNoticePrintTimesQO
     * @return JsonResult<Boolean>
     *
     */
    @PostMapping(value = "updatePrintTimes")
    JsonResult<Boolean> updatePrintTimes(OutNoticePrintTimesQO outNoticePrintTimesQO);

    /**
     * 查询所有出库的产品信息 根据出库通知单编号
     *
     * @author xwcai
     * @date 2018/9/4 下午3:47
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    @PostMapping(value = "query/all/stock/out/sku")
    JsonResult<List<StockOutSkuInfoVO>> selectStockOutSkuInfo(StockOutSkuInfoQO qo);

    /**
     * 查询所有出库的产品信息 根据出库通知单编号 以通知单号的维度
     *
     * @author xwcai
     * @date 2018/9/4 下午3:47
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    @PostMapping(value = "query/all/stock/out/skuByMap")
    JsonResult<Map<String,List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO qo);

    /**
     * 根据出库通知单单号 查询出库通知单明细的出库方式 true 就是扫码  false 非扫码
     *
     * @author xwcai
     * @date 2018/9/17 下午1:54
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     */
    @PostMapping(value = "check/stock/out/scan")
    JsonResult<Boolean> checkOutNoticeStockOutScan(OutNoticeQo qo);

    /**
     * 结单操作
     *
     * @author xwcai
     * @date 2018/9/24 上午11:00
     * @param bean
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     */
    @PostMapping(value = "manage/finish")
    JsonResult<Long> manageFinishOutNotice(FinishOutNoticeBean bean);

    /**
     * 查询所有出库的产品信息 根据出库通知单编号
     *
     * @author xwcai
     * @date 2018/9/4 下午3:47
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    @PostMapping(value = "query/all/stock/out/sap")
    JsonResult<List<OutOutSapDetailVO>> selectStockOutSapInfo(StockOutSkuInfoQO qo);

    /**
     * 发送出库消息
     *
     * @author xwcai
     * @date 2018/9/4 下午3:47
     * @param bean
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    @PostMapping(value = "send/out/msg")
    JsonResult<Boolean> sendStockOutMsg(SendOutNoticeBean bean);

    /**
     * 批量根据出库单号查询详情 以及主体
     *
     * @param qo 查询 qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     * @author xwcai
     * @date 2018/6/25 下午6:37
     */
    @PostMapping(value = "manage/queryWithDetailByNOs")
    JsonResult<List<OutNoticeManageVO>> batchQueryWithDetailByNOs(OutNoticeBatchQO qo);

    /**
     * 批量根据出库单号查询是否已经被接单
     * @param qo
     * @return
     */
    @PostMapping(value = "query/stock/out/orderState")
    JsonResult<Boolean> checkAcceptOrderStatus(OutNoticeBatchQO qo);

    /**
     * 批量根据出库单号删除出库通知单和出库通知详情单
     * @param qo
     * @return
     */
    @PostMapping(value = "stock/out/deleteNoticeAndDetail")
    JsonResult<Boolean> deleteNoticeAndDetail(OutNoticeBatchQO qo);
}
