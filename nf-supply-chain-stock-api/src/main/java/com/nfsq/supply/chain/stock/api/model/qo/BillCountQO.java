package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/21 下午2:32
 */
@Getter
@Setter
public class BillCountQO extends BaseBean {

    @ApiModelProperty(value = "startDate", name = "开始时间", dataType = "Date", required = true)
    private Date startDate;

    @ApiModelProperty(value = "endDate", name = "结束时间", dataType = "Date", required = true)
    private Date endDate;

    @ApiModelProperty(value = "outNoticeType", name = "出库通知单类型", dataType = "Date", required = true)
    private Integer outNoticeType;

    @ApiModelProperty(value = "inNoticeType", name = "入库通知单类型", dataType = "Date", required = true)
    private Integer inNoticeType;

    @ApiModelProperty(value = "orderType", name = "订单类型", dataType = "Date", required = true)
    private Integer orderType;
    
    private List<Long> repoIdList;
    

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
