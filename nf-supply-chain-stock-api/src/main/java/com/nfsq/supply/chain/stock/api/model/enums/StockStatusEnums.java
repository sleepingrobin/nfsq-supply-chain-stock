package com.nfsq.supply.chain.stock.api.model.enums;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StockStatusEnums {
	WMSINVENTORYSTOCK_QUALITY(0, "质检"),
	WMSINVENTORYSTOCK_QUALITY_COMPLETE(1,"占用"),
	WMSINVENTORYSTOCK_FROZEN(2,"冻结"),
	WMSINVENTORYSTOCK_NOT_LIMIT(3,"非限制"),
	WMSINVENTORYSTOCK_ON_THE_WAY(4,"在途"),
	WMSINVENTORYSTOCK_SCRAP(5,"报废"),
	WMSINVENTORYSTOCK_SALEED(6,"已售卖");
	private Integer code;
    private String name;
    
    public static List<Integer> getCodes(){
    	List<Integer> codes = new ArrayList<>();
    	for(StockStatusEnums enums:StockStatusEnums.values()){
    		codes.add(enums.getCode());
    	}
    	return codes;
    }
    
    public static StockStatusEnums getEnum(Integer code){
    	for(StockStatusEnums enums:StockStatusEnums.values()){
    		if(enums.code.equals(code)){
    			return enums;
    		}
    	}
    	return null;
    }
    
    public static List<Integer> getAvaliableStatus(){
    	return Lists.newArrayList(WMSINVENTORYSTOCK_NOT_LIMIT.code);
    }
    
    public static List<Integer> getStoreStatus(){
    	return Lists.newArrayList(WMSINVENTORYSTOCK_NOT_LIMIT.code,WMSINVENTORYSTOCK_ON_THE_WAY.code);
    }
}
