package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.ScanerNoticeDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/19 下午3:28
 */
@Getter
@Setter
@ApiModel(value = "ReqSwagger2",description = "通知单")
public class ScanerNoticeVO {

    @ApiModelProperty(value = "noticeId",name = "通知单Id",dataType = "Long")
    private Long noticeId;

    @ApiModelProperty(value = "noticeNo",name = "通知单No",dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "custId",name = "客户ID",dataType = "Long")
    private Long custId;

    @ApiModelProperty(value = "custName",name = "客户名称",dataType = "String")
    private String custName;

    @ApiModelProperty(value = "custType",name = "客户类型",dataType = "Integer")
    private Integer custType;

    @ApiModelProperty(value = "repoId",name = "仓库Id",dataType = "Long")
    private Long repoId;

    @ApiModelProperty(value = "bind",name = "绑定",dataType = "String")
    private String bind;

    @ApiModelProperty(value = "taskId",name = "任务单号",dataType = "Long")
    private Long taskId;

    @ApiModelProperty(value = "通知单类型",name = "通知单类型",dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "系统类型", name = "系统类型", dataType = "Integer")
    private Integer systemType;

    @ApiModelProperty(value = "detailList",name = "明细列表",dataType = "String")
    private List<ScanerNoticeDetailDTO> detailList;

}
