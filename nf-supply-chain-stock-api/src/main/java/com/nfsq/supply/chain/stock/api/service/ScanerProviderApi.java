package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.bean.BindOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticePageQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerUnLimitStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticePageByScanTypeVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVO;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/13 下午3:44
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "/provider/scaner",method = RequestMethod.POST)
public interface ScanerProviderApi {

    /**
     * 根据单号查询出库通知单详情
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    @PostMapping(value = "/outNotice/detailByNoticeNo")
    JsonResult<ScanerNoticeVO> queryWithScaner(ScanerNoticeQo qo);

    /**
     * 根据机器码分页查询出库通知单详情（任务：绑定了机器码的就是任务）
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    @PostMapping(value = "/outNotice/list")
    JsonResult<ScanerNoticePageByScanTypeVO> queryWithScanerPage(ScanerNoticePageQo qo);

    /**
     * 绑定出库通知单
     *
     * @param bindOutNoticeBean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author lyao
     * @date 2018/5/16 下午3:16
     */
    @PostMapping(value = "/outNotice/bind")
    JsonResult<Boolean> bindOutOutNotice(BindOutNoticeBean bindOutNoticeBean);

    /**
     * 根据单号查询出库通知单详情
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    @PostMapping(value = "/outNotice/detailByQO")
    JsonResult<ScanerNoticeVO> queryWithScanerQO(ScanerNoticeQo qo);

    /**
     * 查询可用库存
     *
     * @author xwcai
     * @date 2018/7/27 下午1:40
     * @param qo 根据单号，机器码查询
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO>
     */
    @PostMapping(value = "outNotice/queryNotLimitStock")
    JsonResult<List<ScanerNotLimitStockVO>> queryNotLimitStock(ScanerUnLimitStockQO qo) ;

    /**
     * 根据通知单NO 获取通知单信息
     *
     * @author xwcai
     * @date 2019/1/8 上午8:58
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO>
     */
    @PostMapping(value = "outNotice/queryWithScanerShow")
    JsonResult<ScanerNoticeByScanTypeVO> queryWithScanerShow(ScanerNoticeQo qo);
}
