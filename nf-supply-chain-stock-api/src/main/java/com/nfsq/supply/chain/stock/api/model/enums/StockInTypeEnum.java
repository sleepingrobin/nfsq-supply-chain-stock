package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/4/12 上午10:16
 */
@Getter
@AllArgsConstructor
public enum StockInTypeEnum {
	PRODUCTIONIN(1,"手工入库"),
	DUMPIN(2,"转储入库"),
    OUT_NOTICE(3,"销售入库"),
    SAP_DELIVERY_IN(4,"SAP交货单入库");

    /**
     * code
     */
    private Integer type;

    /**
     * desc 说明
     */
    private String typeDesc;

    public static StockInTypeEnum getByType(Integer type){
        for(StockInTypeEnum stockInTypeEnum : StockInTypeEnum.values()){
            if(stockInTypeEnum.getType().equals(type)){
                return stockInTypeEnum;
            }
        }
        return  null;
    }
}
