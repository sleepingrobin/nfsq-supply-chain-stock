package com.nfsq.supply.chain.stock.api.model.dto;

public class WmsInventoryStockDTO {
    private Long skuId;

    private String batchNo;

    private String statusNums;

    private Long repoId;
    
    private String repoName;
    
    private String repoOwnerName;
    
    private String skuName;
    
    private Integer skuType;
    
    private String skuTypeName;
    
    private String skuNo;
    
    private String skuUnit;
    
    private Integer unlimit = 0;
    
    private Integer quality = 0;
    
    private Integer frozen = 0;
    
    private Integer onTheWay = 0;
    
    private Integer scrap = 0;
    
    private Long owner;
    
    private Integer ownerType;

    public Long getOwner()
    {
        return owner;
    }

    public void setOwner(Long owner)
    {
        this.owner = owner;
    }

    public Integer getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType)
    {
        this.ownerType = ownerType;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getStatusNums() {
		return statusNums;
	}

	public void setStatusNums(String statusNums) {
		this.statusNums = statusNums;
	}

	public String getRepoName() {
		return repoName;
	}

	public void setRepoName(String repoName) {
		this.repoName = repoName;
	}

	public String getRepoOwnerName() {
		return repoOwnerName;
	}

	public void setRepoOwnerName(String repoOwnerName) {
		this.repoOwnerName = repoOwnerName;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSkuType() {
		return skuType;
	}

	public void setSkuType(Integer skuType) {
		this.skuType = skuType;
	}

	public String getSkuUnit() {
		return skuUnit;
	}

	public void setSkuUnit(String skuUnit) {
		this.skuUnit = skuUnit;
	}

	public Integer getUnlimit() {
		return unlimit;
	}

	public void setUnlimit(Integer unlimit) {
		this.unlimit = unlimit;
	}

	public Integer getQuality() {
		return quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Integer getFrozen() {
		return frozen;
	}

	public void setFrozen(Integer frozen) {
		this.frozen = frozen;
	}

	public Integer getOnTheWay() {
		return onTheWay;
	}

	public void setOnTheWay(Integer onTheWay) {
		this.onTheWay = onTheWay;
	}

	public Integer getScrap() {
		return scrap;
	}

	public void setScrap(Integer scrap) {
		this.scrap = scrap;
	}

	public String getSkuNo() {
		return skuNo;
	}

	public void setSkuNo(String skuNo) {
		this.skuNo = skuNo;
	}

	public String getSkuTypeName() {
		return skuTypeName;
	}

	public void setSkuTypeName(String skuTypeName) {
		this.skuTypeName = skuTypeName;
	}

}