package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/5/18 下午4:48
 */
@Getter
@Setter
public class ConfirmBO extends BaseBean{

    private Long taskId;

    private Long repoId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        if(null == taskId || null == repoId){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
