package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author xwcai
 * @date 2018/7/21 下午1:02
 */
@Getter
@Setter
public class InManageVO {

    @ApiModelProperty(value = "入库通知单Id",notes = "入库通知单Id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "入库通知单编号",notes = "入库通知单编号", dataType = "String")
    private String inNoticeNo;

    @ApiModelProperty(value = "入库单编号",notes = "入库单编号", dataType = "String")
    private String inNo;

    @ApiModelProperty(value = "type", notes = "出库类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "typeDesc", notes = "出库类型", dataType = "String")
    private String typeDesc;

    @ApiModelProperty(value = "仓库Id",notes = "仓库Id", dataType = "Long")
    private Long repoId;

    @ApiModelProperty(value = "任务Id",notes = "任务Id", dataType = "Long")
    private Long inSourceId;

    @ApiModelProperty(value = "仓库名称",notes = "仓库名称", dataType = "String")
    private String repoName;

    @ApiModelProperty(value = "创建时间",notes = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "操作人",notes = "操作人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "联系人",notes = "联系人", dataType = "String")
    private String contactName;

    @ApiModelProperty(value = "联系电话",notes = "联系电话", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "地址",notes = "地址", dataType = "String")
    private String address;

    private InDetailManageListVO detailList;

}
