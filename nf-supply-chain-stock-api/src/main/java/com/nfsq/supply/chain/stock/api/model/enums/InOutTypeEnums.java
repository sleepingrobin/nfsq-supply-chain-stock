package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum InOutTypeEnums {
	OUT_STOCK(0, "出库"),
	IN_STOCK(1,"入库"),IN_NOTICE_STOCK(2,"入库通知"),
	BA_OUT_NOTICE_STOCK(3,"BA售卖"),
    OUT_NOTICE_STOCK(4,"出库通知"),
    BA_OUT_ROLLBACK_STOCK(5,"BA售卖回滚"),
	MACHINE_REPLENISH_STOCK(6,"机器仓库补货"),
	MACHINE_REJECTED_STOCK(7,"机器仓库退货"),
	MACHINE_SELL_STOCK(8,"机器仓库售卖"),
	MACHINE_SOLDOUT_STOCK(9,"机器仓库下架");
	private Integer code;
    private String name;
    
    public static List<Integer> getCodes(){
    	List<Integer> codes = new ArrayList<>();
    	for(InOutTypeEnums enums:InOutTypeEnums.values()){
    		codes.add(enums.getCode());
    	}
    	return codes;
    }
}
