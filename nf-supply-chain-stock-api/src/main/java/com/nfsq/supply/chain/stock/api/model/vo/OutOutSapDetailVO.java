package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/9/26 下午4:19
 */
@Getter
@Setter
@ApiModel(value = "根据出库通知单查询",description = "sap交货单实际出货详情")
public class OutOutSapDetailVO {

    @ApiModelProperty(value = "物料Id",name = "物料Id",dataType = "Long",required =true)
    private Long skuId;

    @ApiModelProperty(value = "sku编号",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "sku名称",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "批次信息",name = "批次信息",dataType = "String",required =true)
    private String batchNo;

    @ApiModelProperty(value = "数量",name = "数量",dataType = "Integer",required =true)
    private Integer amount;

    @ApiModelProperty(value = "单位",name = "单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "重量单位",name = "重量单位",dataType = "String")
    private String weightUnit;

    @ApiModelProperty(value = "毛重",name = "毛重",dataType = "BigDecimal")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "净重",name = "净重",dataType = "BigDecimal")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "sap发货工厂",name = "sap发货工厂",dataType = "String")
    private String sendFactory;

    @ApiModelProperty(value = "sap发货地",name = "sap发货地",dataType = "String")
    private String sendLocation;

    @ApiModelProperty(value = "资产拥有者",name = "资产拥有者",dataType = "Long")
    private Long owner;

    @ApiModelProperty(value = "资产拥有者类型",name = "资产拥有者类型",dataType = "Integer")
    private Integer ownerType;

    @ApiModelProperty(value = "仓库Id",name = "仓库Id",dataType = "Integer")
    private Long repoId;

    @ApiModelProperty(value = "供货工厂",name = "供货工厂",dataType = "Integer")
    private String repoName;
}
