package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/31 上午9:41
 */
@Getter
@Setter
public class BillVO {

    @ApiModelProperty(value = "outNoticeNOs",name = "出库通知单单号List",dataType = "List")
    private List<BillDTO> outNoticeNOs;

    @ApiModelProperty(value = "outOutNOs",name = "出库单单号List",dataType = "List")
    private List<BillDTO> outOutNOs;

    @ApiModelProperty(value = "inNoticeNOs",name = "入库通知单单号List",dataType = "List")
    private List<BillDTO> inNoticeNOs;

    @ApiModelProperty(value = "inInNOs",name = "入库单单号List",dataType = "List")
    private List<BillDTO> inInNOs;

}
