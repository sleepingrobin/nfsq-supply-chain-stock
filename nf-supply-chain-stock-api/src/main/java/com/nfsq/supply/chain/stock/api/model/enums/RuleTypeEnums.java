package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum RuleTypeEnums {
	COMMON(0, "通用"),
	KA(1,"KA"),
	CHANNEL(2,"渠道"),
	EXCLUSIVE_DISTRIBUTOR(3,"专属经销商"),
	DUMP(4,"转储");
	private Integer code;
    private String name;

    public static List<Integer> getCodes(){
    	List<Integer> codes = new ArrayList<>();
    	for(RuleTypeEnums enums:RuleTypeEnums.values()){
    		codes.add(enums.getCode());
    	}
    	return codes;
    }


}
