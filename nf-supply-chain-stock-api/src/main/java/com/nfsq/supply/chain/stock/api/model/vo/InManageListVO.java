package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/7/21 下午1:11
 */
@Getter
@Setter
public class InManageListVO extends PageParameter {

    private List<InManageVO> voList;

}
