package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author xwcai
 * @date 2018/5/23 上午9:48
 */
@Getter
@Setter
public class OutManageVO {

    @ApiModelProperty(value = "id",notes = "单Id",dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "no",notes = "单NO",dataType = "String")
    private String no;

    @ApiModelProperty(value = "taskId",notes = "任务单Id",dataType = "Long")
    private Long taskId;

    @ApiModelProperty(value = "type",notes = "出库类型",dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "typeDesc",notes = "出库类型",dataType = "String")
    private String typeDesc;

    @ApiModelProperty(value = "noticeId",notes = "通知单Id",dataType = "Long")
    private Long noticeId;

    @ApiModelProperty(value = "noticeNo",notes = "通知单NO",dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "destinationStockId",notes = "目的地仓库Id",dataType = "Long")
    private Long destinationStockId;

    @ApiModelProperty(value = "destinationName",notes = "目的地名字",dataType = "String")
    private String destinationName;

    @ApiModelProperty(value = "supplyStockId",notes = "供货方仓库Id",dataType = "Long")
    private Long supplyStockId;

    @ApiModelProperty(value = "supplyName",notes = "供货方名称",dataType = "String")
    private String supplyName;

    @ApiModelProperty(value = "contactName",notes = "联系人",dataType = "String")
    private String contactName;

    @ApiModelProperty(value = "phoneNum",notes = "联系电话",dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "address",notes = "地址",dataType = "String")
    private String address;

    @ApiModelProperty(value = "outDate",notes = "出库时间",dataType = "Date")
    private Date outDate;

    @ApiModelProperty(value = "modifyUser",notes = "操作人",dataType = "String")
    private String modifyUser;

    @ApiModelProperty(value = "operStatus",notes = "状态",dataType = "Integer")
    private Integer operStatus;

    @ApiModelProperty(value = "operStatusDesc",notes = "状态描述",dataType = "String")
    private String operStatusDesc;

    @ApiModelProperty(value = "detailManageVO",notes = "详情", dataType = "Object")
    private OutDetailManagePageVO detailManageVO;

}
