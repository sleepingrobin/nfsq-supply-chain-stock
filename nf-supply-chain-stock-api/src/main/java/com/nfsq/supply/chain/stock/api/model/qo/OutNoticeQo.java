package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;


/**
 * @author xwcai
 * @date 2018/4/14 下午5:29
 */
@ApiModel(value = "出库通知单查询",description = "出库通知单查询")
@Getter
@Setter
public class OutNoticeQo extends PageBaseBean{

    @ApiModelProperty(value = "id",notes = "主键",dataType = "Long",required =false)
    private Long id;

    @ApiModelProperty(value = "outNoticeNo",notes = "出库通知单号",dataType = "String",required =false)
    private String outNoticeNo;

    @ApiModelProperty(value = "type", notes = "出库类型", dataType = "Integer", required = true)
    private Integer type;

    @ApiModelProperty(value = "outSourceId", notes = "出库类型对应来源id", dataType = "Long", required = true)
    private Long outSourceId;

    @ApiModelProperty(value = "destinationId", notes = "目的地", dataType = "Long", required = true)
    private Long destinationId;

    @ApiModelProperty(value = "destinationType", notes = "目的地类型", dataType = "Integer", required = true)
    private Integer destinationType;

    @ApiModelProperty(value = "repoId", notes = "仓库Id", dataType = "Long", required = false)
    private Long repoId;

    @ApiModelProperty(value = "bind", notes = "绑定人", dataType = "String", required = false)
    private String bind;

    @ApiModelProperty(value = "operStatus", notes = "状态", dataType = "List", required = false)
    private List<Integer> operStatus;

    @ApiModelProperty(value = "expressNo", notes = "快递单号", dataType = "String", required = false)
    private String expressNo;

    @ApiModelProperty(value = "startDate", notes = "开始时间", dataType = "Date")
    private Date startDate;

    @ApiModelProperty(value = "endDate", notes = "结束时间", dataType = "Date")
    private Date endDate;

    @ApiModelProperty(value = "noneStatus", notes = "非状态", dataType = "Integer")
    private Integer noneStatus;

    @ApiModelProperty(value = "isPage", notes = "是否分业0 分页  1 不分页", dataType = "Integer")
    private Integer isPage = 0;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
