package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/13 下午5:12
 */
@ApiModel(value = "OutNoticeBean", description = "出库通知单")@Getter
@Setter

public class OutNoticeBean extends BaseBean {

    @ApiModelProperty(value = "outNoticeId", name = "出库通知单ID", dataType = "Long", required = false)
    private Long outNoticeId;

    @ApiModelProperty(value = "type", name = "出库类型", dataType = "Integer", required = true)
    private Integer type;

    @ApiModelProperty(value = "outSourceId", name = "任务单号", dataType = "String", required = true)
    private Long outSourceId;

    @ApiModelProperty(value = "repoId", name = "存储仓库ID", dataType = "Long", required = true)
    private Long repoId;

    @ApiModelProperty(value = "planOutDate", name = "计划出库时间", dataType = "Date", required = true)
    private Date planOutDate;

    @ApiModelProperty(value = "destinationId", name = "目的地", dataType = "Long", required = true)
    private Long destinationId;

    @ApiModelProperty(value = "destinationType", name = "目的地类型", dataType = "Integer", required = true)
    private Integer destinationType;

    @ApiModelProperty(value = "returnKeyName",name = "返回map的kay",dataType = "Integer",required =false)
    private String returnKeyName;

    @ApiModelProperty(value = "系统类型", name = "系统类型", dataType = "Integer", required = false)
    private Integer systemType;

    @ApiModelProperty(value = "skuInfoList", name = "出库sku详情", dataType = "List", required = true)
    private List<SkuInfoBean> skuInfoList;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        //参数是否为空检查
        if (null == type || null == outSourceId || null == repoId || repoId < 0 || null == skuInfoList || skuInfoList.isEmpty() || null == destinationId || null == destinationType || null == this.getUserId() || null == systemType) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        //判断传入的出库通知单状态
        if (null == StockOutTypeEnum.getByType(type)) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        //检查所有sku内参数是否正确
        for (SkuInfoBean skuInfo : skuInfoList) {
            //检查内部sku参数是否为空
            if (null == skuInfo.getAmount() || null == skuInfo.getSkuId() || null == skuInfo.getTargetOwner() || null == skuInfo.getTargetOwnerType()) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查内部订单Id 订单类型 用途组
            if(null == skuInfo.getRuleGroup() || null == skuInfo.getRuleType()) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查内部sku数量、重量不能小于0
            if (skuInfo.getAmount() <= 0) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
