package com.nfsq.supply.chain.stock.api.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class WmsOutOutNoticeDTO {
    private Long id;

    private String outNoticeNo;

    private Integer type;

    private String typeDes;

    private String outSourceId;

    private Long repoId;

    private Date planOutDate;

    private Long destinationId;

    private Integer destinationType;

    private Integer operStatus;
    
    private String bind;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

}