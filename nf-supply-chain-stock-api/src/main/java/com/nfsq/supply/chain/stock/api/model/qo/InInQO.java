package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author xwcai
 * @date 2018/7/21 下午12:59
 */
@Getter
@Setter
public class InInQO extends PageBaseBean {

    @ApiModelProperty(value = "主键",notes = "入库单主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "入库单编号",notes = "入库单编号", dataType = "Integer")
    private String inNo;

    @ApiModelProperty(value = "入库单类型",notes = "入库单类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "开始时间",notes = "开始时间", dataType = "Date")
    private Date startDate;

    @ApiModelProperty(value = "结束时间",notes = "结束时间", dataType = "Date")
    private Date endDate;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
