package com.nfsq.supply.chain.stock.api.model.vo;

import java.util.Map;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author sundi
 *
 */
@ApiModel(value = "ReqSwagger2",description = "返回门店库存信息")
public class StoreStockVO {
	//key为skuId,value为总数量
	@ApiModelProperty(value = "skuNumMap",name = "门店库存信息",dataType = "Map")
	 private Map<Long,Integer> skuNumMap;

	public Map<Long, Integer> getSkuNumMap() {
		return skuNumMap;
	}

	public void setSkuNumMap(Map<Long, Integer> skuNumMap) {
		this.skuNumMap = skuNumMap;
	}
}
