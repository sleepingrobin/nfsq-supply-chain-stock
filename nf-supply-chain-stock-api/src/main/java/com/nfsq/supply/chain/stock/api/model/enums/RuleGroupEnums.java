package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum RuleGroupEnums {
	CURRENCY(0, "通用"),
	SALE(1,"销售"),
	DUMP(2,"转储");
	private Integer code;
    private String name;

    public static List<Integer> getCodes(){
    	List<Integer> codes = new ArrayList<>();
    	for(RuleGroupEnums enums:RuleGroupEnums.values()){
    		codes.add(enums.getCode());
    	}
    	return codes;
    }

    public static RuleGroupEnums getCode(Integer code){
        for(RuleGroupEnums enums : RuleGroupEnums.values()){
            if(enums.getCode().equals(code)){
                return enums;
            }
        }
        return  null;
    }
}
