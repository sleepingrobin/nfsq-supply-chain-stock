package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeByOutBean;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xwcai
 * @date 2018/5/9 下午3:45
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/in/notice",method = RequestMethod.POST)
public interface InNoticeProviderApi {

    /**
     * 创建入库通知单
     *
     * @param inNoticeBean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/9 下午5:29
     */
    @PostMapping(value = "/create")
    JsonResult<InNoticeCreateVO> createInNotice(InNoticeBean inNoticeBean);

    /**
     * 根据出库通知单创建入库通知单
     *
     * @param [inNoticeBean]
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/16 上午11:28
     */
    @PostMapping(value = "/createByOutNotice")
    JsonResult<List<InNoticeCreateVO>> createInInNoticeByOut(InNoticeByOutBean nNoticeByOutBean);

    /**
     * 根据qo 查询入库通知单列表
     *
     * @author xwcai
     * @date 2018/7/21 下午2:08
     * @param inNoticeQo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageListVO>
     */
    @PostMapping(value = "manage/query/list")
    JsonResult<InNoticeManageListVO> queryByInInNoticeQo(InNoticeQO inNoticeQo) ;

    /**
     * 根据入库通知单Id获取入库通知详情
     *
     * @param qo 通知单id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeDetailListVO>
     * @author xwcai
     * @date 2018/4/14 下午5:17
     */
    @PostMapping(value = "manage/detail")
    JsonResult<InNoticeDetailListVO> getDetailByNoticeId(InNoticeQO qo);

    /**
     * 根据qo(入库通知单单号) 管理页面查询入库通知单包括详情
     *
     * @author xwcai
     * @date 2018/7/21 上午11:25
     * @param qo 查询qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageVO>
     */
    @PostMapping(value = "manage/queryWithDetailByNO")
    JsonResult<InNoticeManageVO> selectManageWithDetailByQO(InNoticeQO qo) ;

    /**
     * 查询所有的入库通知单编号
     *
     * @author xwcai
     * @date 2018/7/21 下午2:04
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<java.lang.String>>
     */
    @PostMapping(value = "manage/queryAllInNoticeNos")
    JsonResult<List<String>> selectAllNOs(DummyBean bean) ;

    /**
     * 根据入库通知单号，查询入库批次
     *
     * @author xwcai
     * @date 2018/9/13 下午2:40
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.Set<java.lang.String>>
     */
    @PostMapping(value = "/query/selectAllBatchNoStockIn")
    JsonResult<Set<String>> selectAllBatchNoStockIn(InNoticeQO qo);

    /**
     * 查询所有出库的产品信息 根据出库通知单编号 以通知单号的维度
     *
     * @author xwcai
     * @date 2018/9/4 下午3:47
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    @PostMapping(value = "query/all/stock/out/skuByMap")
    JsonResult<Map<String,List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO qo);

}
