package com.nfsq.supply.chain.stock.api.model.qo;

import java.lang.reflect.Field;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月28日 上午11:14:39
 */
@Getter
@Setter
public class RepoQO extends BaseBean {
	@ApiModelProperty(value = "id", name = "工厂ID/门店ID/经销商ID", dataType = "Long")
    private Long id;
	
	@ApiModelProperty(value = "repoId", name = "仓库ID", dataType = "Long")
    private Long repoId;
	
	@ApiModelProperty(value = "type", name = "1工厂/2DC/3门店/4经销商", dataType = "Integer", required = true)
	private Integer type;

	/* (non-Javadoc)
	 * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
	 */
	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		for (Field field : getClass().getDeclaredFields()) {
			ApiModelProperty an = field.getAnnotation(ApiModelProperty.class);
			if(an != null && an.required()){
				ReflectionUtils.makeAccessible(field);
				Object value = ReflectionUtils.getField(field, this);
				if(StringUtils.isEmpty(value)){
					return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
				}
			}
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}

}
