package com.nfsq.supply.chain.stock.api.model.qo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import com.nfsq.supply.chain.stock.api.model.bean.WmsStockBO;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月22日 上午10:33:56
 */
@ApiModel(value = "ReqSwagger2",description = "更改库存")
@Getter
@Setter
public class ModifyStockQO extends BaseBean {
	@ApiModelProperty(value = "wmsStockBOs", name = "待更改库存实体", dataType = "List", required = true)
	private List<WmsStockBO> wmsStockBOs;
	/* (non-Javadoc)
	 * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
	 */
	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		if(CollectionUtils.isEmpty(wmsStockBOs)){
			return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
		}
		for (WmsStockBO wmsStockBO : wmsStockBOs) {
			JsonResult<?> result = wmsStockBO.validate();
			if(!result.isSuccess()){
				return result;
			}
		}
		jsonResult.setSuccess(Boolean.TRUE);
		return jsonResult;
	}
}
