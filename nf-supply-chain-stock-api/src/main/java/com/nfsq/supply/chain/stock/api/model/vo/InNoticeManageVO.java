package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author xwcai
 * @date 2018/7/21 上午9:55
 */
@Getter
@Setter
public class InNoticeManageVO {

    @ApiModelProperty(value = "入库通知单Id",notes = "入库通知单Id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "入库通知单编号",notes = "入库通知单编号", dataType = "String")
    private String inNoticeNo;

    @ApiModelProperty(value = "运单ID",notes = "运单ID", dataType = "String")
    private String transNo;

    @ApiModelProperty(value = "快递单号",notes = "快递单号", dataType = "String")
    private String expressNo;

    @ApiModelProperty(value = "任务ID",notes = "任务ID", dataType = "Long")
    private Long inSourceId;

    @ApiModelProperty(value = "入库通知单类型",notes = "入库通知单类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "入库通知单类型说明",notes = "入库通知单类型说明", dataType = "String")
    private String typeDesc;

    @ApiModelProperty(value = "收货仓库Id",notes = "收货仓库Id", dataType = "Integer")
    private Long repoId;

    @ApiModelProperty(value = "收货仓库名称",notes = "收货仓库名称", dataType = "String")
    private String repoName;

    @ApiModelProperty(value = "入库通知单状态",notes = "入库通知单状态", dataType = "Integer")
    private Integer operStatus;

    @ApiModelProperty(value = "收货人",notes = "收货人", dataType = "Integer")
    private String createUser;

    @ApiModelProperty(value = "入库通知单状态说明",notes = "入库通知单状态说明", dataType = "String")
    private String operStatusDesc;

    @ApiModelProperty(value = "入库通知单创建时间",notes = "入库通知单创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "承运商",notes = "承运商", dataType = "String")
    private String carrierName;

    @ApiModelProperty(value = "contactName",notes = "联系人", dataType = "String")
    private String contactName;

    @ApiModelProperty(value = "phoneNum",notes = "联系电话", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "详情列表",notes = "详情列表", dataType = "Object")
    private InNoticeDetailListVO detailListVO;

}
