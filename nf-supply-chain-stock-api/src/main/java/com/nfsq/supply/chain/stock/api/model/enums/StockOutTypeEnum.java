package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/4/23 下午5:04
 */
@Getter
@AllArgsConstructor
public enum StockOutTypeEnum {
    SELLOUT(1,"销售出库"),
	DUMPOUT(2,"转储出库"),
    SAP_DELIVERY_OUT(3,"SAP交货单出库"),;

    /**
     * code
     */
    private Integer type;

    /**
     * desc 说明
     */
    private String typeDesc;

    public static StockOutTypeEnum getByType(Integer type){
        for(StockOutTypeEnum stockOutTypeEnum : StockOutTypeEnum.values()){
            if(stockOutTypeEnum.getType().equals(type)){
                return stockOutTypeEnum;
            }
        }
        return  null;
    }
}
