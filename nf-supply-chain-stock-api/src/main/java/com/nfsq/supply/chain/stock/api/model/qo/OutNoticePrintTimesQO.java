package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * @author chenhongwei
 * @date 2018/7/20
 * @description
 */
@Getter
@Setter
public class OutNoticePrintTimesQO extends BaseBean {

    @ApiModelProperty(value = "outNoticeNo", name = "出库通知单号", dataType = "String", required = true)
    String outNoticeNo;

    @Override
    public JsonResult<?> validate() {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        if (!StringUtils.isEmpty(this.outNoticeNo)) {
            result.setSuccess(true);
        }
        return result;
    }
}
