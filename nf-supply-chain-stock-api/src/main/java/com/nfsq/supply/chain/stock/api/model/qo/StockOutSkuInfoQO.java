package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/9/4 下午3:19
 */
@ApiModel(value = "根据通知单查询所有出库的sku信息",description = "通知单查询")
@Getter
@Setter
public class StockOutSkuInfoQO extends BaseBean {

    @ApiModelProperty(value = "出库通知单号list", notes = "出库通知单号list", dataType = "List")
    private List<String> noticeNoList;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(CollectionUtils.isEmpty(noticeNoList)){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
