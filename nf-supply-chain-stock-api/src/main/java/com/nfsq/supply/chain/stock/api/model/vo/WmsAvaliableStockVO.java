package com.nfsq.supply.chain.stock.api.model.vo;

import java.util.List;

import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月16日 上午11:42:50
 */
@ApiModel(value = "ReqSwagger2",description = "返回可用库存信息")
public class WmsAvaliableStockVO {
	@ApiModelProperty(value = "wmsAvaliableStockBOs",name = "可用库存实体信息",dataType = "List")
	List<WmsAvaliableStockBO> wmsAvaliableStockBOs;

	public List<WmsAvaliableStockBO> getWmsAvaliableStockBOs() {
		return wmsAvaliableStockBOs;
	}

	public void setWmsAvaliableStockBOs(List<WmsAvaliableStockBO> wmsAvaliableStockBOs) {
		this.wmsAvaliableStockBOs = wmsAvaliableStockBOs;
	}
}
