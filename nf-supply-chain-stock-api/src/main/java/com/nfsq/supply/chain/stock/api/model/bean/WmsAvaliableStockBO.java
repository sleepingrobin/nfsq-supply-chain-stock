package com.nfsq.supply.chain.stock.api.model.bean;

import java.lang.reflect.Field;
import java.util.List;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月10日 下午6:56:42
 */
@ApiModel(value = "ReqSwagger2",description = "可用库存实体")
public class WmsAvaliableStockBO extends BaseBean {
	@ApiModelProperty(value = "skuId", name = "产品ID", dataType = "Long", required = true)
	private Long skuId;
	
	@ApiModelProperty(value = "repoId", name = "仓库ID", dataType = "Long", required = true)
	private Long repoId;
	
	@ApiModelProperty(value = "statuses", name = "资产状态", dataType = "List", required = true)
	private List<Integer> statuses;
	
	@ApiModelProperty(value = "ruleGroup", name = "规则组", dataType = "Integer", required = true)
	private Integer ruleGroup;
	
	@ApiModelProperty(value = "ruleType", name = "规则类型", dataType = "Integer", required = true)
    private Integer ruleType;
	
    /** 实际库存-通知单中占用库存 **/
    private Long nums;
    
    @ApiModelProperty(value = "dealerIds", name = "专属经销商ID", dataType = "List")
    private List<Long> dealerIds;
    
	public Long getSkuId() {
		return skuId;
	}
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	public Long getRepoId() {
		return repoId;
	}
	public void setRepoId(Long repoId) {
		this.repoId = repoId;
	}
	public Integer getRuleGroup() {
		return ruleGroup;
	}
	public void setRuleGroup(Integer ruleGroup) {
		this.ruleGroup = ruleGroup;
	}
	public Integer getRuleType() {
		return ruleType;
	}
	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}
    public List<Integer> getStatuses()
    {
        return statuses;
    }
    public void setStatuses(List<Integer> statuses)
    {
        this.statuses = statuses;
    }
    public List<Long> getDealerIds()
    {
        return dealerIds;
    }
    public void setDealerIds(List<Long> dealerIds)
    {
        this.dealerIds = dealerIds;
    }
    public Long getNums() {
		return nums;
	}
	public void setNums(Long nums) {
		this.nums = nums;
	}

	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		for (Field field : getClass().getDeclaredFields()) {
			ApiModelProperty an = field.getAnnotation(ApiModelProperty.class);
			if(an != null && an.required()){
				ReflectionUtils.makeAccessible(field);
				Object value = ReflectionUtils.getField(field, this);
				if(StringUtils.isEmpty(value)){
					return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
				}
			}
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}
}
