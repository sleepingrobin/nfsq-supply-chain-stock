package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * @author xwcai
 * @date 2018/5/19 上午10:53
 */
@Getter
@Setter
public class ScanerNoticePageQo extends PageBaseBean{

    @ApiModelProperty(value = "machineCode", name = "机器码", dataType = "String")
    private String machineCode;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
//        if(StringUtils.isBlank(machineCode.trim())){
//            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
//            return jsonResult;
//        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
