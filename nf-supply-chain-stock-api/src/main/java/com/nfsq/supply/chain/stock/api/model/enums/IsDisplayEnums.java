package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/7/24 下午1:24
 */
@Getter
@AllArgsConstructor
public enum IsDisplayEnums {
    HIDE(0,"隐藏"),
    DISPLAY(1,"显示");

    private Integer code;

    private String desc;

}
