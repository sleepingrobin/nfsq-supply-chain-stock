package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/7/26 下午2:33
 */
@Getter
@Setter
public class ProcessCountVO {

    @ApiModelProperty(value = "运单待办数量",name = "运单待办数量",dataType = "Integer")
    private Integer wayBillCount;

    @ApiModelProperty(value = "销售订单待发数量",name = "销售订单待发数量",dataType = "Integer")
    private Integer saleTaskCount;

    @ApiModelProperty(value = "转储订单待发数量",name = "转储订单待发数量",dataType = "Integer")
    private Integer dumpTaskCount;

    @ApiModelProperty(value = "转储收货任务",name = "转储收货任务",dataType = "Integer")
    private Integer dumpReceiveCount;

    @ApiModelProperty(value = "转储单审核",name = "转储单审核",dataType = "Integer")
    private Integer dumpBillApproveCount;

    @ApiModelProperty(value = "sap单审核",name = "sap单审核",dataType = "Integer")
    private Integer sapBillApproveCount;

    @ApiModelProperty(value = "sap单待发数量",name = "sap单待发数量",dataType = "Integer")
    private Integer sapTaskCount;

    @ApiModelProperty(value = "sap待过账",name = "sap单待发数量",dataType = "Integer")
    private Integer sapUnPostOrder;

}
