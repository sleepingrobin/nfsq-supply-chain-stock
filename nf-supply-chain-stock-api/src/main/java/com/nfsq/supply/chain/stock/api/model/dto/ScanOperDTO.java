package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;

public class ScanOperDTO {

    @ApiModelProperty(value = "scanOperCode",name = "编号",dataType = "Integer")
    private Integer scanOperCode;

    @ApiModelProperty(value = "scanOperName",name = "名称",dataType = "String")
    private String scanOperName;

    @ApiModelProperty(value = "scanOperFlag",name = "默认标识",dataType = "Integer")
    private Integer scanOperFlag;

    public Integer getScanOperCode() {
        return scanOperCode;
    }

    public void setScanOperCode(Integer scanOperCode) {
        this.scanOperCode = scanOperCode;
    }

    public String getScanOperName() {
        return scanOperName;
    }

    public void setScanOperName(String scanOperName) {
        this.scanOperName = scanOperName;
    }

    public Integer getScanOperFlag() {
        return scanOperFlag;
    }

    public void setScanOperFlag(Integer scanOperFlag) {
        this.scanOperFlag = scanOperFlag;
    }
}
