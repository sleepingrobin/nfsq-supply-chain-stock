package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.datacenter.api.response.NcpStore;
import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.datacenter.api.response.SaleMachine;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 
 * @author sundi
 *
 */
@ApiModel(value = "ReqSwagger2",description = "返回仓库或门店列表")
@Getter
@Setter
public class RepoOrStockVO {
	@ApiModelProperty(value = "repoList",name = "仓库列表信息",dataType = "List")
	 private List<Repo> repoList;
	
	@ApiModelProperty(value = "storeList",name = "门店列表信息",dataType = "List")
    private List<NcpStore> storeList;

	@ApiModelProperty(value = "saleMachineList",name = "机器列表信息",dataType = "List")
    private List<SaleMachine> saleMachineList;
}
