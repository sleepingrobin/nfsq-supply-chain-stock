package com.nfsq.supply.chain.stock.api.model.enums;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RiceSkuEnum {
	HALF_KG("000000011110101667","000000011110101454",24),
	ONE_AND_HALF_KG("000000011110101668","000000011110101455",10),
	TWO_AND_HALF_KG("000000011110101669","000000011110101456",6);
	
	private String bag;
    private String box;
    private int rate;
    
    public static List<String> getBags(){
    	List<String> skuNos= new ArrayList<>();
    	for(RiceSkuEnum enums:RiceSkuEnum.values()){
    	    skuNos.add(enums.getBag());
    	}
    	return skuNos;
    }
    
    public static List<String> getBoxes(){
        List<String> skuNos= new ArrayList<>();
        for(RiceSkuEnum enums:RiceSkuEnum.values()){
            skuNos.add(enums.getBox());
        }
        return skuNos;
    }
    
    public static RiceSkuEnum getEnumByBag(String bag){
    	for(RiceSkuEnum enums:RiceSkuEnum.values()){
    		if(enums.getBag().equals(bag)){
    			return enums;
    		}
    	}
    	return null;
    }
    
    public static RiceSkuEnum getEnumByBox(String box){
        for(RiceSkuEnum enums:RiceSkuEnum.values()){
            if(enums.getBox().equals(box)){
                return enums;
            }
        }
        return null;
    }
}
