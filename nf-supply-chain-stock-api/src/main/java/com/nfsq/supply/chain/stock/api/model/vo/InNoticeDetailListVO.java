package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.WmsInNoticeDetailDTO;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/7/21 上午11:02
 */
@Getter
@Setter
public class InNoticeDetailListVO extends PageParameter {

    @ApiModelProperty(value = "详情List",dataType = "List")
    private List<WmsInNoticeDetailDTO> detailDTOS;

}
