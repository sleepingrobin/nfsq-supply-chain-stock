package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InDetailManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InNoticeDetailListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageListVO;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/9 下午3:34
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/in/in",method = RequestMethod.POST)
public interface InInProviderApi {

    /**
     * 创建入库单
     *
     * @param stockInBean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/9 下午5:29
     */
    @PostMapping(value = "/create")
    JsonResult<String> createInIn(StockInBean stockInBean);

    /**
     * 根据订单号收货 初期是全部收货  不支持多订单情况
     *
     * @param bo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/18 下午4:50
     */
    @PostMapping(value = "/confirm")
    JsonResult<Boolean> confirmInIn(ConfirmBO bo);

    /**
     * 根据qo 查询入库通知单列表
     *
     * @author xwcai
     * @date 2018/7/21 下午2:08
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageListVO>
     */
    @PostMapping(value = "manage/query/list")
    JsonResult<InManageListVO> queryInInByQo(InInQO qo) ;

    /**
     * 根据入库通知单Id获取入库通知详情
     *
     * @param qo 通知单id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeDetailListVO>
     * @author xwcai
     * @date 2018/4/14 下午5:17
     */
    @PostMapping(value = "manage/detail")
    JsonResult<InDetailManageListVO> getDetailByQO(InInQO qo);

    /**
     * 查询所有的入库通知单编号
     *
     * @author xwcai
     * @date 2018/7/21 下午2:04
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<java.lang.String>>
     */
    @PostMapping(value = "manage/queryAllInNos")
    JsonResult<List<String>> selectAllNOs(DummyBean bean) ;
}
