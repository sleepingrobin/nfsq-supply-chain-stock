package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/6/21 上午9:21
 */
@Getter
@Setter
public class InNoticeCreateVO {

    @ApiModelProperty(value = "repoId",name = "入库仓库",dataType = "Long")
    private Long repoId;

    @ApiModelProperty(value = "noticeNo",name = "通知单编号",dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "noticeId",name = "通知单ID",dataType = "Long")
    private Long noticeId;

}
