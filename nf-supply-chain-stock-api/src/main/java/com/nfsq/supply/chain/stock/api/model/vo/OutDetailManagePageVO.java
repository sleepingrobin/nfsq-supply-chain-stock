package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/22 上午11:44
 */
@Getter
@Setter
public class OutDetailManagePageVO extends PageParameter {

    private List<OutDetailManageVO> voList;

}
