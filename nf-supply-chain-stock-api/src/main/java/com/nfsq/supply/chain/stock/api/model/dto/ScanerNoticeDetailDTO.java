package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/5/19 上午10:44
 */
@Getter
@Setter
public class ScanerNoticeDetailDTO {

    @ApiModelProperty(value = "noticeDetailId",name = "明细ID",dataType = "Long")
    private Long noticeDetailId;

    @ApiModelProperty(value = "skuId",name = "SkuId",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "skuNo",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "skuName",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "amount",name = "需求数量",dataType = "BigDecimal")
    private BigDecimal amount;

    @ApiModelProperty(value = "batchNo",name = "批次",dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "status",name = "状态",dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "unFinishAmount",name = "未扫描数量",dataType = "BigDecimal")
    private BigDecimal unFinishAmount;

    @ApiModelProperty(value = "finishAmount",name = "已经完成数量",dataType = "BigDecimal")
    private BigDecimal finishAmount;

    @ApiModelProperty(value = "skuUnit",name = "单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "scanType", name = "出入库方式（0 非扫码  1 扫码）", dataType = "Integer", required = true)
    private Integer scanType;

    @ApiModelProperty(value = "owner",name = "资源拥有者",dataType = "Long",required =false)
    private Long owner;

    @ApiModelProperty(value = "ownerType",name = "资源拥有者类型",dataType = "Integer",required =false)
    private Integer ownerType;

    @ApiModelProperty(value = "targetOwner",name = "目标资源拥有者",dataType = "Long",required =false)
    private Long targetOwner;

    @ApiModelProperty(value = "targetOwnerType",name = "目标资源拥有者类型",dataType = "Integer",required =false)
    private Integer targetOwnerType;

}
