package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/6/19 下午2:57
 */
@Getter
@Setter
public class NoticeFinishQO extends BaseBean {

    @ApiModelProperty(value = "taskId", name = "任务id", dataType = "Long", required = true)
    private Long taskId;

    @ApiModelProperty(value = "queryType", name = "查询类型", dataType = "List", required = true)
    private Integer queryType;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(null == taskId || null == queryType){
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
        }
        if(!queryType.equals(InOutTypeEnums.IN_NOTICE_STOCK.getCode()) && !queryType.equals(InOutTypeEnums.OUT_NOTICE_STOCK.getCode())){
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
