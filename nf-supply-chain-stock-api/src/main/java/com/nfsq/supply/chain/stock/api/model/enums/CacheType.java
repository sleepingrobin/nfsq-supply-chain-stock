package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author  wangk
 * @date 2017年12月7日 下午2:54:09
 */
@Getter
@AllArgsConstructor
public enum CacheType {
	FIFTEEN_SECONDS("FIFTEEN_SECONDS",15L),THIRTY_SECONDS("THIRTY_SECONDS",30L),ONE_MINUTE("ONE_MINUTE",60L),FIVE_MINUTES("FIVE_MINUTES",300L),FIFTEEN_MINUTES("FIFTEEN_MINUTES",900L),HALF_HOUR("HALF_HOUR",1800L),ONE_HOUR("ONE_HOUR",3600L),EIGHT_HOURS("EIGHT_HOURS",28800L),ONE_DAY("ONE_DAY",86400L);
	
	private String cacheName;
	private Long expire;
	
	public static class ExpireTime{
		public static final String FIFTEEN_SECONDS = "FIFTEEN_SECONDS";
		public static final String THIRTY_SECONDS = "THIRTY_SECONDS";
		public static final String ONE_MINUTE = "ONE_MINUTE";
		public static final String FIVE_MINUTES = "FIVE_MINUTES";
		public static final String FIFTEEN_MINUTES = "FIFTEEN_MINUTES";
		public static final String HALF_HOUR = "HALF_HOUR";
		public static final String ONE_HOUR = "ONE_HOUR";
		public static final String EIGHT_HOURS = "EIGHT_HOURS";
		public static final String ONE_DAY = "ONE_DAY";
	}
}
