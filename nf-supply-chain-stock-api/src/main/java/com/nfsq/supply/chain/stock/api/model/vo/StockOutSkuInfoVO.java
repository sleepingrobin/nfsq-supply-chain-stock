package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/9/4 下午3:38
 */
@ApiModel(value = "根据出库通知单",description = "出库通知单查询")
@Getter
@Setter
public class StockOutSkuInfoVO {
    @ApiModelProperty(value = "订单号",name = "订单号",dataType = "String",required =false)
    private String orderNo;

    @ApiModelProperty(value = "物料Id",name = "物料Id",dataType = "Long",required =true)
    private Long skuId;

    @ApiModelProperty(value = "sku编号",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "sku名称",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "批次信息",name = "批次信息",dataType = "String",required =true)
    private String batchNo;

    @ApiModelProperty(value = "数量",name = "数量",dataType = "Integer",required =true)
    private Integer amount;

    @ApiModelProperty(value = "单位",name = "单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "重量单位",name = "重量单位",dataType = "String")
    private String weightUnit;

    @ApiModelProperty(value = "毛重",name = "毛重",dataType = "BigDecimal")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "通知单号",name = "通知单号",dataType = "String",required =false)
    private String noticeNo;
}
