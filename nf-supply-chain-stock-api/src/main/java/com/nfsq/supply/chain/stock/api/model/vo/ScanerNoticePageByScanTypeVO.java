package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;


@ApiModel(value = "ReqSwagger2",description = "扫码枪通知单显示")
public class ScanerNoticePageByScanTypeVO extends PageParameter  {

    @ApiModelProperty(value = "noticeList",name = "通知单列表",dataType = "List")
    private List<ScanerNoticeByScanTypeVO> noticeList;

    public List<ScanerNoticeByScanTypeVO> getNoticeList() {
        return noticeList;
    }

    public void setNoticeList(List<ScanerNoticeByScanTypeVO> noticeList) {
        this.noticeList = noticeList;
    }
}
