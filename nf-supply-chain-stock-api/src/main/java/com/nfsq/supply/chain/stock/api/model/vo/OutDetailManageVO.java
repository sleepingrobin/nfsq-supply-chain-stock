package com.nfsq.supply.chain.stock.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xwcai
 * @date 2018/5/23 上午9:55
 */
@Getter
@Setter
public class OutDetailManageVO {

    @ApiModelProperty(value = "id",name = "单Id",dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "outId",name = "出库单Id",dataType = "Long")
    private Long outId;

    @ApiModelProperty(value = "noticeDetailId",name = "出库通知单Id",dataType = "Long")
    private Long noticeDetailId;

    @ApiModelProperty(value = "skuId",name = "skuId",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "skuNo",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "skuName",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "skuUnit",name = "sku单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "batchNo",name = "批次",dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "amount",name = "数量",dataType = "Integer")
    private Integer amount;

    @ApiModelProperty(value = "grossWeight",name = "净重",dataType = "BigDecimal")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "netWeight",name = "毛重",dataType = "BigDecimal")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "grossWeightStr",name = "净重",dataType = "BigDecimal")
    private String grossWeightStr;

    @ApiModelProperty(value = "netWeightStr",name = "毛重",dataType = "BigDecimal")
    private String netWeightStr;

    @ApiModelProperty(value = "weightUnit",name = "重量单位",dataType = "String")
    private String weightUnit;

    @ApiModelProperty(value = "scanType",name = "扫描类型",dataType = "Integer")
    private Integer scanType;

    @ApiModelProperty(value = "scanTypeDesc",name = "扫描类型描述",dataType = "String")
    private String scanTypeDesc;

    @ApiModelProperty(value = "资源拥有者ID",name = "资源拥有者ID",dataType = "Integer")
    private Long owner;

    @ApiModelProperty(value = "资源拥有者名字",name = "资源拥有者名字",dataType = "Integer")
    private String ownerName;

    @ApiModelProperty(value = "资源拥有者类型",name = "资源拥有者类型",dataType = "Integer")
    private Integer ownerType;

}
