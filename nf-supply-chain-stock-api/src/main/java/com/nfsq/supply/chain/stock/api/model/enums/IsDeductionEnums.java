package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/7/24 下午1:26
 */
@Getter
@AllArgsConstructor
public enum IsDeductionEnums {
    NOT_DEDUCTION(0,"不扣减库存"),
    DEDUCTION(1,"扣减库存");

    private Integer code;

    private String desc;
}
