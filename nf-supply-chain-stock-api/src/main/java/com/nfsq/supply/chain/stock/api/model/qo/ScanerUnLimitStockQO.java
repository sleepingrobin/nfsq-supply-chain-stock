package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * @author xwcai
 * @date 2018/7/27 下午1:27
 */
@Getter
@Setter
public class ScanerUnLimitStockQO extends BaseBean {

    @ApiModelProperty(value = "machineCode", notes = "机器码", dataType = "String")
    private String machineCode;

    @ApiModelProperty(value = "noticeId", notes = "通知单ID", dataType = "Long")
    private Long noticeId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(StringUtils.isEmpty(machineCode) || null == noticeId || noticeId < 0){
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(),StockResultCode.SYSTEM_ERROR.getDesc());
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
