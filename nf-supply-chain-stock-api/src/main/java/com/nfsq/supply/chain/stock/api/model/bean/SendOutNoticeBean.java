package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * @author xwcai
 * @date 2018/10/10 下午2:44
 */
@Getter
@Setter
public class SendOutNoticeBean extends BaseBean {

    @ApiModelProperty(value = "outNo",name = "出库单编号",dataType = "String",required =true)
    private String outNo;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(StringUtils.isBlank(outNo)){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
