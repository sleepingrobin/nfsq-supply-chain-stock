package com.nfsq.supply.chain.stock.api.common.constants;

/**
 * @author xwcai
 * @date 2018/5/9 下午3:37
 */
public class StockConstant {

    public static final String APPLICATION_NAME = "nf-supply-chain-stock";

    public static final String BASE_PACKAGE = "com.nfsq.supply.chain.stock.api.service";

}
