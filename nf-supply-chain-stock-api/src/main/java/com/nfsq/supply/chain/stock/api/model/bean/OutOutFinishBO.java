package com.nfsq.supply.chain.stock.api.model.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author xwcai
 * @date 2018/6/28 上午9:17
 */
@Getter
@Setter
public class OutOutFinishBO {

    @ApiModelProperty(value = "outNoticeNo", name = "出库通知单编号", dataType = "String", required = true)
    private String outNoticeNo;

    @ApiModelProperty(value = "taskId", name = "任务id", dataType = "Long", required = true)
    private Long taskId;

    @ApiModelProperty(value = "skuAmountMap", name = "sku数量", dataType = "map", required = true)
    private Map<Long,Integer> skuAmountMap;

    @ApiModelProperty(value = "isAutoCommit", name = "是否自动提交", dataType = "Boolean")
    private Boolean isAutoCommit;

}
