package com.nfsq.supply.chain.stock.api.model.bean;

import java.util.Date;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author lyao
 * @date 2018/5/16 下午1:24
 */
@ApiModel(value = "InNoticeByOutBean", description = "根据出库通知单创建入库通知单的入参")
@Getter
@Setter
public class InNoticeByOutBean extends BaseBean  {

    @ApiModelProperty(value = "taskId", name = "任务Id", dataType = "Long", required = true)
    private Long taskId;

    @ApiModelProperty(value = "planInDate", name = "计划入库时间", dataType = "Date", required = true)
    private Date planInDate;

    @ApiModelProperty(value = "systemType", name = "系统类型", dataType = "Integer", required = true)
    private Integer systemType;

	@Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        //参数是否为空检查
        if (null == taskId || null == systemType) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
