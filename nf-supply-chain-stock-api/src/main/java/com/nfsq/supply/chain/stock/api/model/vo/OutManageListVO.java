package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/23 上午10:33
 */
@Getter
@Setter
public class OutManageListVO extends PageParameter{

    private List<OutManageVO> voList;

}
