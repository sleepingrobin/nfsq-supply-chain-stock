package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/22 下午7:04
 */
@Getter
@Setter
public class OutNoticeManageListVO extends PageParameter{

    private List<OutNoticeManageVO> noticeManageVOS;

}
