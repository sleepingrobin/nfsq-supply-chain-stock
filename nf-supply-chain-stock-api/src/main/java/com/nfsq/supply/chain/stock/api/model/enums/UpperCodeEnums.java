package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/7/24 下午2:50
 */
@Getter
@AllArgsConstructor
public enum UpperCodeEnums {
    WAIT_FOR_CODE(-2L,"等待"),
    TOP_CODE(-1L,"没有上层code");

    private Long code;

    private String desc;

}
