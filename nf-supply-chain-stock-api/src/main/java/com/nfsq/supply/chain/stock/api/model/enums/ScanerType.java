package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/5/18 上午10:15
 */
@Getter
@AllArgsConstructor
public enum ScanerType {
    NOT_SCAN(0,"非扫码"),
    SCAN(1,"扫码"),
    FILL_A_VACANCY(2,"数量补缺");


    private Integer type;

    private String desc;

    public static ScanerType getByType(Integer type){
        for(ScanerType scanerType : ScanerType.values()){
            if(scanerType.getType().equals(type)){
                return scanerType;
            }
        }
        return  null;
    }

    public static Boolean scanEquals(Integer noticeScanType,Integer newScanType){
        if(noticeScanType.equals(newScanType)){
            return Boolean.TRUE;
        }else if(SCAN.getType().equals(noticeScanType) && FILL_A_VACANCY.getType().equals(newScanType)){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }
}
