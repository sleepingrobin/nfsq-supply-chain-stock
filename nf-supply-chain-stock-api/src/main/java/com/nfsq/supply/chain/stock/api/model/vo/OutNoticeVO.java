package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDTO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 上午11:18
 */
@Getter
@Setter
@ApiModel(value = "ReqSwagger2",description = "出库通知单")
public class OutNoticeVO {

    @ApiModelProperty(value = "wmsOutOutNotice",name = "出库通知单",dataType = "Object")
    private WmsOutOutNoticeDTO wmsOutOutNotice;

    @ApiModelProperty(value = "wmsOutOutNoticeDetails",name = "出库通知详情单",dataType = "List")
    private List<WmsOutOutNoticeDetailDTO> wmsOutOutNoticeDetails;

}
