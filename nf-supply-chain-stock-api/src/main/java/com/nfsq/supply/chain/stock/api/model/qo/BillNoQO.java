package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/5/31 上午9:40
 */
@Getter
@Setter
public class BillNoQO extends BaseBean{

    @ApiModelProperty(value = "taskId", name = "任务ID", dataType = "Long", required = true)
    private Long taskId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(null == taskId){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
