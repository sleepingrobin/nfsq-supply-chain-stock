package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/13 下午3:29
 */
@Getter
@Setter
@ApiModel(value = "ReqSwagger2",description = "出库通知单列表（含有详情）")
public class OutNoticeVOList extends PageParameter {

    @ApiModelProperty(value = "outNoticeVos",name = "出库通知单列表（含有详情）",dataType = "List")
    private List<OutNoticeVO> outNoticeVos;

}
