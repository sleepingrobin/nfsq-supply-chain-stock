package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class WmsOutOutNoticeDetailDTO {
    private Long id;

    private Long outNoticeId;
    
    private Long skuId;

    private String skuNo;

    private String skuName;

    private String skuUnit;

    private String batchNo;

    private Long batchId;

    private Long owner;

    private Integer ownerType;

    private Integer amount;

    private Integer realAmount;

    private String grossWeightStr;

    private String netWeightStr;

    private BigDecimal grossWeight;

    private BigDecimal netWeight;

    private String weightUnit;
    
    private Integer status;

    private Integer ruleGroup;
    
    private Integer ruleType;

    private Long ruleDetail;

    @ApiModelProperty(value = "scanType",name = "扫描类型",dataType = "Integer")
    private Integer scanType;

    @ApiModelProperty(value = "scanTypeDesc",name = "扫描类型描述",dataType = "String")
    private String scanTypeDesc;


    @ApiModelProperty(value = "targetOwner",name = "目标资源拥有者",dataType = "Long",required =false)
    private Long targetOwner;

    @ApiModelProperty(value = "targetOwnerType",name = "目标资源拥有者类型",dataType = "Integer",required =false)
    private Integer targetOwnerType;
    
    private Integer operStatus;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    @ApiModelProperty(value = "sap发货工程",name = "sap发货工程",dataType = "Integer",required =false)
    private String sendFactory;

    @ApiModelProperty(value = "sap发货地",name = "sap发货地",dataType = "Integer",required =false)
    private String sendLocation;

}