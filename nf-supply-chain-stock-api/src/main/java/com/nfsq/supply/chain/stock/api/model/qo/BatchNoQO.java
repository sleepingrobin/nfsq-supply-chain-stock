package com.nfsq.supply.chain.stock.api.model.qo;

import lombok.Getter;
import lombok.Setter;

import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月28日 上午11:14:39
 */
@Getter
@Setter
public class BatchNoQO extends BaseBean {
	@ApiModelProperty(value = "batchNo", name = "批次号", dataType = "String")
    private String batchNo;

	/* (non-Javadoc)
	 * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
	 */
	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		return jsonResult;
	}

}
