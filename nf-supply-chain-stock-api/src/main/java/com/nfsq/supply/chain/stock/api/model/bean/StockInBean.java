package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/13 上午9:53
 */
@ApiModel(value = "StockInBean",description = "创建入库单")
@Getter
@Setter
public class StockInBean extends BaseBean{

    @ApiModelProperty(value = "inNoticeId",name = "入库通知单ID",dataType = "Long",required =true)
    private Long inNoticeId;

    @ApiModelProperty(value = "inDate",name = "入库实际时间",dataType = "Date",required =true)
    private Date inDate;

    @ApiModelProperty(value = "repoId",name = "仓库Id",dataType = "Long",required =true)
    private Long repoId;

    @ApiModelProperty(value = "skuInfoList",name = "入库sku详情",dataType = "List",required =true)
    private List<SkuInfoBean> skuInfoList;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        //参数检查 是否为空
        if(null == inNoticeId || inNoticeId < 0 || null == repoId || repoId < 0 || null == skuInfoList || skuInfoList.isEmpty()){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        //检查所有sku内参数是否正确
        for(SkuInfoBean skuInfo : skuInfoList){
            //检查内部sku参数是否为空
            if(null == skuInfo.getAmount() || StringUtils.isBlank(skuInfo.getBatchNo()) || null == skuInfo.getSkuId()){
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查内部sku数量、重量不能小于0
            if(skuInfo.getAmount() <= 0){
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
