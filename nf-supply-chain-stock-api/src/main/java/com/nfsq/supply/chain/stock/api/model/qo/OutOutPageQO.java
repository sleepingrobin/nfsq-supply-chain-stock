package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author xwcai
 * @date 2018/5/23 上午9:38
 */
@Getter
@Setter
public class OutOutPageQO extends PageBaseBean {

    @ApiModelProperty(value = "id",notes = "单号", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "outNo",notes = "单编号", dataType = "String")
    private String outNo;

    @ApiModelProperty(value = "outNoticeNo",notes = "出库通知单编号", dataType = "String")
    private String outNoticeNo;

    @ApiModelProperty(value = "startDate",notes = "开始时间", dataType = "Date")
    private Date startDate;

    @ApiModelProperty(value = "endDate",notes = "结束时间", dataType = "Date")
    private Date endDate;

    @ApiModelProperty(value = "noneStatus",notes = "非状态", dataType = "Integer")
    private Integer noneStatus;

    @ApiModelProperty(value = "type",notes = "出库类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "isPage",notes = "是否分业0 分页  1 不分页", dataType = "Integer")
    private Integer isPage = 0;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
