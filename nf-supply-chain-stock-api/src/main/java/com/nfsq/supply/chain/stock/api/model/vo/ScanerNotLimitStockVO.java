package com.nfsq.supply.chain.stock.api.model.vo;

import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/7/27 下午1:33
 */
@Getter
@Setter
public class ScanerNotLimitStockVO {

    @ApiModelProperty(value = "skuId", notes = "skuID", dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "skuNo", notes = "sku编号", dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "skuName", notes = "sku名称", dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "skuUnit", notes = "sku单位", dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "requireAmount", notes = "需求数量", dataType = "BigDecimal")
    private BigDecimal requireAmount;

    @ApiModelProperty(value = "unfinishAmount", notes = "未完成数量", dataType = "BigDecimal")
    private BigDecimal unfinishAmount;

    @ApiModelProperty(value = "repoId", notes = "仓库Id", dataType = "BigDecimal")
    private Long repoId;

    @ApiModelProperty(value = "status", notes = "状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "statusDesc", notes = "状态说明", dataType = "String")
    private String statusDesc;

    @ApiModelProperty(value = "numOptions", notes = "可用数量选项", dataType = "List")
    private List<SkuNumsBO> numOptions;

    @ApiModelProperty(value = "批次", notes = "批次", dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "owner", notes = "owner", dataType = "Long")
    private Long owner;

    @ApiModelProperty(value = "ownerType", notes = "owner类型", dataType = "Integer")
    private Integer ownerType;

}
