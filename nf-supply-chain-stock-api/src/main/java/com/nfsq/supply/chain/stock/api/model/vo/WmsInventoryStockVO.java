package com.nfsq.supply.chain.stock.api.model.vo;

import java.util.List;

import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.utils.common.PageParameter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author  wangk
 * @date 2018年5月16日 上午10:56:48
 */
@ApiModel(value = "ReqSwagger2",description = "返回库存信息")
public class WmsInventoryStockVO extends PageParameter {
	@ApiModelProperty(value = "wmsInventoryStockDTOs",name = "库存实体信息",dataType = "List")
	List<WmsInventoryStockDTO> wmsInventoryStockDTOs;

	public List<WmsInventoryStockDTO> getWmsInventoryStockDTOs() {
		return wmsInventoryStockDTOs;
	}

	public void setWmsInventoryStockDTOs(List<WmsInventoryStockDTO> wmsInventoryStockDTOs) {
		this.wmsInventoryStockDTOs = wmsInventoryStockDTOs;
	}
}
