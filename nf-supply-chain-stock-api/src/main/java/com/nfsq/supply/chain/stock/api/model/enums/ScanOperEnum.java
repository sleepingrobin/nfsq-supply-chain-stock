package com.nfsq.supply.chain.stock.api.model.enums;

import com.nfsq.supply.chain.stock.api.model.dto.ScanOperDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum ScanOperEnum {
    PACKING(1,"按包装",1),
    SUPPRT(2,"按托",1),
    SINGLEBOX(3,"按单箱",0);


    /**
     * code
     */
    private Integer type;

    /**
     * desc 说明
     */
    private String typeDes;

    /**
     * 默认标识
     */
    private Integer flag;

    public static ScanOperEnum getByType(Integer type){
        for(ScanOperEnum scanOperEnum : ScanOperEnum.values()){
            if(scanOperEnum.getType().equals(type)){
                return scanOperEnum;
            }
        }
        return  null;
    }


}
