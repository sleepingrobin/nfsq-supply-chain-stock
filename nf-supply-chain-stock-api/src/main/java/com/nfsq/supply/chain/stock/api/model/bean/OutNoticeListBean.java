package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/22 上午11:01
 */
@Getter
@Setter
public class OutNoticeListBean extends BaseBean{

    @ApiModelProperty(value = "noticeBeanList", name = "出库通知单List", dataType = "List", required = true)
    private List<OutNoticeBean> noticeBeanList ;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        for(OutNoticeBean bean : noticeBeanList){
            //参数是否为空检查
            if (null == bean.getType() || null == bean.getOutSourceId() || null == bean.getRepoId() || bean.getRepoId() < 0 || null == bean.getSkuInfoList() || bean.getSkuInfoList().isEmpty() || null == bean.getDestinationId() || null == bean.getDestinationType()  || null == bean.getReturnKeyName() || null == bean.getSystemType()) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //判断传入的出库通知单状态
            if (null == StockOutTypeEnum.getByType(bean.getType())) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查所有sku内参数是否正确
            for (SkuInfoBean skuInfo : bean.getSkuInfoList()) {
                //检查内部sku参数是否为空
                if (null == skuInfo.getAmount() || null == skuInfo.getSkuId() || null == skuInfo.getTargetOwner() || null == skuInfo.getTargetOwnerType()) {
                    jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                    return jsonResult;
                }
                //检查内部订单Id 订单类型 用途组
                if(null == skuInfo.getRuleGroup() || null == skuInfo.getRuleType()) {
                    jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                    return jsonResult;
                }
                //检查内部sku数量、重量不能小于0
                if (skuInfo.getAmount() <= 0) {
                    jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                    return jsonResult;
                }
            }
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
