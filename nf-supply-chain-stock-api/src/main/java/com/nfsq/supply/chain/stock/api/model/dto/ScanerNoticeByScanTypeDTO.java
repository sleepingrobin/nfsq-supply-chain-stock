package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ScanerNoticeByScanTypeDTO {


    @ApiModelProperty(value = "scanType",name = "编号",dataType = "Integer")
    private Integer scanType;

    @ApiModelProperty(value = "scanName",name = "名称",dataType = "String")
    private String scanName;

    @ApiModelProperty(value = "skuList",name = "明细列表",dataType = "String")
    private List<ScanerNoticeDetailDTO> skuList;

    public Integer getScanType() {
        return scanType;
    }

    public void setScanType(Integer scanType) {
        this.scanType = scanType;
    }

    public String getScanName() {
        return scanName;
    }

    public void setScanName(String scanName) {
        this.scanName = scanName;
    }

    public List<ScanerNoticeDetailDTO> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<ScanerNoticeDetailDTO> skuList) {
        this.skuList = skuList;
    }
}
