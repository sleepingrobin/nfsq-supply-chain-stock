package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * @author xfeng6
 * @created 2018年4月11日 上午10:44:09
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/stock",method = RequestMethod.POST)
public interface StockProviderApi {

    /**
     * 查询库存
     *
     * @param stock
     *
     * @return JsonResult<WmsInventoryStockVO>
     * @author wangk
     * @date 2018年4月27日 上午9:42:39
     */
    @PostMapping(value = "/queryStock")
    public JsonResult<WmsInventoryStockVO> queryStock(WmsStockQO stock);

    /**
     * 经销商直销查询库存
     *
     * @param stock
     */
    @PostMapping(value = "/queryStockByFanAndStore")
    public JsonResult<List<FanAndStoreStockInfoVO>> queryStockByFanAndStore(@RequestBody WmsStockQO stock);

    /**
     * 更新库存
     *
     * @param wmsStockBOs
     *
     * @return JsonResult<Boolean>
     * @throws ChainStockException
     * @author wangk
     * @date 2018年4月27日 上午9:43:58
     */
    @PostMapping(value = "/updateStock")
    public JsonResult<Boolean> updateStock(ModifyStockQO modifyStockQO);

    /**
     * 查询可用库存
     *
     * @param wmsAvaliableStockBOs
     *
     * @return JsonResult<WmsAvaliableStockVO>
     * @author wangk
     * @date 2018年5月11日 下午1:54:23
     */
    @PostMapping(value = "/queryAvaliableStock")
    public JsonResult<WmsAvaliableStockVO> queryAvaliableStock(AvaliableStockQO avaliableStockQO);
    
    /**
     * 查询门店库存
     * @param storeStockQO
     * @return
     */
    @PostMapping(value = "/queryStoreStock")
    public JsonResult<StoreStockVO> queryStoreStock(StoreStockQO storeStockQO);
    
    /**
     * 
     * 查询sap同步库存
     *
     * @author sundi
     * @param sapSyncStockQO
     * @return
     */
    @PostMapping("/querySapSyncStock")
    JsonResult<SapSyncStockVO> querySapSyncStock(SapSyncStockQO sapSyncStockQO);
    
    @PostMapping("/queryRepos")
    public JsonResult<RepoOrStockVO> queryRepos(RepoQO bean);
    
    @PostMapping("/queryBatchNos")
    public JsonResult<List<String>> queryBatchNos(BatchNoQO qo);   
}
