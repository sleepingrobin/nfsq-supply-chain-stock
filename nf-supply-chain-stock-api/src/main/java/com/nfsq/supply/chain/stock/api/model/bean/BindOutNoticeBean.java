package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * @author lyao
 * @date 2018/5/16 下午3:11
 */
@ApiModel(value = "BindOutNoticeBean", description = "绑定通知单")
@Getter
@Setter
public class BindOutNoticeBean  extends BaseBean {

    @ApiModelProperty(value = "noticeId", name = "通知单ID", dataType = "Long", required = true)
    private Long noticeId;

    @ApiModelProperty(value = "machineCode", name = "机器码", dataType = "String", required = true)
    private String machineCode;

	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
        //参数是否为空检查
        if (null == noticeId || StringUtils.isBlank(machineCode)) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
	}

}
