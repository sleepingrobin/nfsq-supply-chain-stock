package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/7/21 上午10:52
 */
@Getter
@Setter
public class WmsInNoticeDetailDTO {

    @ApiModelProperty(value = "skuId",name = "skuId",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "sku编号",name = "sku编号",dataType = "String")
    private String skuNo;

    @ApiModelProperty(value = "sku名称",name = "sku名称",dataType = "String")
    private String skuName;

    @ApiModelProperty(value = "sku单位",name = "sku单位",dataType = "String")
    private String skuUnit;

    @ApiModelProperty(value = "批次编号",name = "批次编号",dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "需求数量",name = "需求数量",dataType = "Integer")
    private Integer amount;

    @ApiModelProperty(value = "实际数量",name = "实际数量",dataType = "Integer")
    private Integer realAmount;

    @ApiModelProperty(value = "毛重",name = "毛重",dataType = "String")
    private String grossWeightStr;

    @ApiModelProperty(value = "净重",name = "净重",dataType = "String")
    private String netWeightStr;

    @ApiModelProperty(value = "毛重无单位",name = "毛重无单位",dataType = "BigDecimal")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "净重无单位",name = "净重无单位",dataType = "BigDecimal")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "重量单位",name = "重量单位",dataType = "String")
    private String weightUnit;

    @ApiModelProperty(value = "资源拥有者ID",name = "资源拥有者ID",dataType = "Integer")
    private Long owner;

    @ApiModelProperty(value = "资源拥有者名字",name = "资源拥有者名字",dataType = "Integer")
    private String ownerName;

    @ApiModelProperty(value = "资源拥有者类型",name = "资源拥有者类型",dataType = "Integer")
    private Integer ownerType;

    @ApiModelProperty(value = "库存状态",name = "库存状态",dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "库存状态说明",name = "库存状态说明",dataType = "Integer")
    private String statusDesc;
    
}
