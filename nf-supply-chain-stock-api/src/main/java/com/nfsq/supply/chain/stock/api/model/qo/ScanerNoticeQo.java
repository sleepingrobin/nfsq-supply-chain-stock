package com.nfsq.supply.chain.stock.api.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

/**
 * @author xwcai
 * @date 2018/5/19 上午10:48
 */
@Getter
@Setter
public class ScanerNoticeQo extends BaseBean{

    @ApiModelProperty(value = "machineCode", name = "机器码", dataType = "String")
    private String machineCode;

    @ApiModelProperty(value = "noticeNo", name = "通知单编号", dataType = "String")
    private String noticeNo;

    @ApiModelProperty(value = "noticeId", name = "通知单ID", dataType = "Long")
    private Long noticeId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(StringUtils.isBlank(machineCode)){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        if(null == noticeId && StringUtils.isBlank(noticeNo)){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
