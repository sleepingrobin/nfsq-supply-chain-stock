package com.nfsq.supply.chain.stock.api.service;

import com.nfsq.supply.chain.stock.api.common.constants.StockConstant;
import com.nfsq.supply.chain.stock.api.model.bean.RollBackBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockOutBean;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.OutDetailManagePageVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutDetailManageVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageVO;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/9 下午3:46
 */
@FeignClient(value = StockConstant.APPLICATION_NAME)
@RequestMapping(value = "provider/out/out",method = RequestMethod.POST)
public interface OutOutProviderApi {

    /**
     * 创建出库通知单 返回出库单单号
     *
     * @param stockOutBean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/9 下午5:29
     */
    @PostMapping(value = "/create")
    JsonResult<String> createOutOut(StockOutBean stockOutBean);

    /**
     * 查询
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutManageListVO>
     * @author xwcai
     * @date 2018/5/23 上午10:35
     */
    @PostMapping(value = "/manage/query/list")
    JsonResult<OutManageListVO> manageQueryByQo(OutOutPageQO qo);

    /**
     * 根据Id 查询详情
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<com.nfsq.supply.chain.stock.api.model.vo.OutDetailManageVO>>
     * @author xwcai
     * @date 2018/5/23 上午10:40
     */
    @PostMapping(value = "/manage/query/detail")
    JsonResult<OutManageVO> manageDetailQueryByQo(OutOutPageQO qo);

    /**
     * 根据qo 查询所有出库通知单
     *
     * @param [qo]
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:56
     */
    @PostMapping(value = "queryAllOutNos")
    JsonResult<List<String>> queryAllOutNos(DummyBean bean);

    /**
     * 根据Id 查询详情
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<com.nfsq.supply.chain.stock.api.model.vo.OutDetailManageVO>>
     * @author xwcai
     * @date 2018/5/23 上午10:40
     */
    @PostMapping(value = "/qr/code/manage/query/detail")
    JsonResult<OutManageVO> qrCodeDetailQueryByOutNo(OutOutPageQO qo);
}
