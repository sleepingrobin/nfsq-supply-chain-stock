package com.nfsq.supply.chain.stock.api.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author chenhongwei
 * @date 2019-02-28
 * @description 经销商直销库存查询
 */
public class FanAndStoreStockInfoVO {

    @JsonIgnoreProperties
    private Long skuId;
    private String skuNo;
    private String skuName;
    private String skuTypeName;
    private String skuUnit;
    private String notLimitNum;
    private String onTheWayNum;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getSkuNo() {
        return skuNo;
    }

    public void setSkuNo(String skuNo) {
        this.skuNo = skuNo;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuTypeName() {
        return skuTypeName;
    }

    public void setSkuTypeName(String skuTypeName) {
        this.skuTypeName = skuTypeName;
    }

    public String getSkuUnit() {
        return skuUnit;
    }

    public void setSkuUnit(String skuUnit) {
        this.skuUnit = skuUnit;
    }

    public String getNotLimitNum() {
        return notLimitNum;
    }

    public void setNotLimitNum(String notLimitNum) {
        this.notLimitNum = notLimitNum;
    }

    public String getOnTheWayNum() {
        return onTheWayNum;
    }

    public void setOnTheWayNum(String onTheWayNum) {
        this.onTheWayNum = onTheWayNum;
    }
}
