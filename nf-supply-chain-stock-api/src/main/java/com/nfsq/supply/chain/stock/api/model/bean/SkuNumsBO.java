package com.nfsq.supply.chain.stock.api.model.bean;

import io.swagger.annotations.ApiModelProperty;
/**
 * @author sundi
 * @version 2018年7月19日
 * @since SCM
 */
public class SkuNumsBO {

    @ApiModelProperty(value = "skuId",name = "物料id",dataType = "Long")
    private Long skuId;

    @ApiModelProperty(value = "batchNo",name = "批次号",dataType = "String")
    private String batchNo;

    @ApiModelProperty(value = "owner",name = "资产拥有者id",dataType = "Long")
    private Long owner;
    
    @ApiModelProperty(value = "ownerType",name = "资产拥有者类型",dataType = "Integer")
    private Integer ownerType;
    
    @ApiModelProperty(value = "ownerName",name = "资产拥有者名称",dataType = "String")
    private String ownerName;

    @ApiModelProperty(value = "totalNums",name = "总数量数量",dataType = "Integer")
    private Integer totalNums;

    @ApiModelProperty(value = "availableQuantity",name = "可用数量",dataType = "Integer")
    private Integer availableQuantity;

    public Long getSkuId()
    {
        return skuId;
    }

    public void setSkuId(Long skuId)
    {
        this.skuId = skuId;
    }

    public String getBatchNo()
    {
        return batchNo;
    }

    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public Long getOwner()
    {
        return owner;
    }

    public void setOwner(Long owner)
    {
        this.owner = owner;
    }

    public Integer getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType)
    {
        this.ownerType = ownerType;
    }


    public String getOwnerName()
    {
        return ownerName;
    }

    public void setOwnerName(String ownerName)
    {
        this.ownerName = ownerName;
    }

    public Integer getTotalNums()
    {
        return totalNums;
    }

    public void setTotalNums(Integer totalNums)
    {
        this.totalNums = totalNums;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @Override
    public String toString()
    {
        return "SkuNumsBO [skuId=" + skuId + ", batchNo=" + batchNo + ", owner=" + owner + ", ownerType=" + ownerType
            + ", ownerName=" + ownerName + ", totalNums=" + totalNums + "]";
    }
}
