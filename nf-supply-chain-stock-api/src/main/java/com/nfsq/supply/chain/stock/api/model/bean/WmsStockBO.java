package com.nfsq.supply.chain.stock.api.model.bean;

import java.lang.reflect.Field;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 出入库参数bean
 * @author  xfeng6
 * @created 2018年4月19日 下午2:25:53
 */
@ApiModel(value = "ReqSwagger2",description = "库存更改")
public class WmsStockBO extends BaseBean {
	@ApiModelProperty(value = "skuId", name = "产品ID", dataType = "Long", required = true)
    private Long skuId;

	@ApiModelProperty(value = "batchId", name = "批次ID", dataType = "Long")
    private Long batchId;

	@ApiModelProperty(value = "batchNo", name = "批次号", dataType = "String", required = true)
    private String batchNo;

	@ApiModelProperty(value = "nums", name = "数量", dataType = "Integer", required = true)
    private Integer nums;

	@ApiModelProperty(value = "status", name = "库存状态", dataType = "Integer")
    private Integer status;

	@ApiModelProperty(value = "repoId", name = "仓库ID", dataType = "Long", required = true)
    private Long repoId;

	@ApiModelProperty(value = "owner", name = "资产归属", dataType = "Long", required = true)
    private Long owner;
	
	@ApiModelProperty(value = "ownerType", name = "资产归属类型", dataType = "Integer", required = true)
    private Integer ownerType;

    @ApiModelProperty(value = "ruleGroup", name = "规则组", dataType = "Integer", required = true)
    private Integer ruleGroup;

    @ApiModelProperty(value = "ruleType", name = "规则类型", dataType = "Integer", required = true)
    private Integer ruleType;

    @ApiModelProperty(value = "targetStatus", name = "目标状态", dataType = "Integer")
	private Integer targetStatus;
	
    @ApiModelProperty(value = "dealerId", name = "专属经销商ID", dataType = "Long")
	private Long dealerId;
    
    @ApiModelProperty(value = "orderDetailId", name = "出入库详情单ID", dataType = "Long")
	private Long orderDetailId;
    
    @ApiModelProperty(value = "type", name = "0出库,1入库,2入库通知单,3BA售卖", dataType = "Integer", required = true)
	private Integer type;

	public Integer getTargetStatus() {
		return targetStatus;
	}

	public void setTargetStatus(Integer targetStatus) {
		this.targetStatus = targetStatus;
	}

	public Long getDealerId() {
		return dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	public Long getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Integer getNums() {
		return nums;
	}

	public void setNums(Integer nums) {
		this.nums = nums;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getRepoId() {
		return repoId;
	}

	public void setRepoId(Long repoId) {
		this.repoId = repoId;
	}

	public Long getOwner() {
		return owner;
	}

	public void setOwner(Long owner) {
		this.owner = owner;
	}

	public Integer getRuleGroup() {
		return ruleGroup;
	}

	public void setRuleGroup(Integer ruleGroup) {
		this.ruleGroup = ruleGroup;
	}

	public Integer getRuleType() {
		return ruleType;
	}

	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}
	
	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

	@Override
	public JsonResult<?> validate() {
		JsonResult<?> jsonResult = new JsonResult<>();
		for (Field field : getClass().getDeclaredFields()) {
			ApiModelProperty an = field.getAnnotation(ApiModelProperty.class);
			if(an != null && an.required()){
				ReflectionUtils.makeAccessible(field);
				Object value = ReflectionUtils.getField(field, this);
				if(StringUtils.isEmpty(value)){
					return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
				}
			}
		}
		jsonResult.setSuccess(true);
		return jsonResult;
	}

}
