package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author  wangk
 * @date 2018年5月24日 上午10:56:30
 */
@Getter
@AllArgsConstructor
public enum RepoOwnerEnum {
	FACTORY(1,"工厂"),
	DC(2,"DC"),
	STORE(3,"门店"),
	DEALER(4,"经销商"),
	MACHINE(6,"机器");


    /**
     * code
     */
    private Integer type;

    /**
     * desc 说明
     */
    private String typeDes;

    public static RepoOwnerEnum getByType(Integer type){
        for(RepoOwnerEnum repoOwnerEnum : RepoOwnerEnum.values()){
            if(repoOwnerEnum.getType().equals(type)){
                return repoOwnerEnum;
            }
        }
        return  null;
    }
}
