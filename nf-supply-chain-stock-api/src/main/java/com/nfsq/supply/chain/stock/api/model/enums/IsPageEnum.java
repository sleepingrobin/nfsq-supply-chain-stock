package com.nfsq.supply.chain.stock.api.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/7/30 下午7:28
 */
@Getter
@AllArgsConstructor
public enum IsPageEnum {
    IS_PAGE(0,"分页"),
    IS_NOT_PAGE(1,"不分页");

    private Integer code;

    private String desc;

}
