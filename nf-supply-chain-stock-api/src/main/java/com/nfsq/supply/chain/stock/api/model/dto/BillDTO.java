package com.nfsq.supply.chain.stock.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/6/20 下午4:57
 */
@Getter
@Setter
public class BillDTO {

    @ApiModelProperty(value = "billNo", name = "单号", dataType = "Long", required = true)
    private String billNo;

    @ApiModelProperty(value = "inRepoId", name = "入库仓库ID", dataType = "Long", required = true)
    private Long inRepoId;

    @ApiModelProperty(value = "outRepoId", name = "出库仓库ID", dataType = "Long", required = true)
    private Long outRepoId;
}
