package com.nfsq.supply.chain.stock.api.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockInTypeEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:08
 */
@Getter
@Setter
@ApiModel(value = "InNoticeBean", description = "创建入库通知单的入参")
public class InNoticeBean extends BaseBean {

    @ApiModelProperty(value = "type", name = "入库类型", dataType = "Integer", required = true)
    private Integer type;

    @ApiModelProperty(value = "inSourceId", name = "任务单号", dataType = "Long", required = true)
    private Long inSourceId;

    @ApiModelProperty(value = "repoId", name = "存储仓库ID", dataType = "Long", required = true)
    private Long repoId;

    @ApiModelProperty(value = "planInDate", name = "计划入库时间", dataType = "Date", required = true)
    private Date planInDate;

    @ApiModelProperty(value = "是否显示 0 不显示 1 显示", name = "是否显示 0 不显示 1 显示", dataType = "Integer", required = false)
    private Integer isDisplay = 1;

    @ApiModelProperty(value = "是否扣减库存 0 不扣减 1 扣减", name = "是否扣减库存 0 不扣减 1 扣减", dataType = "Integer", required = false)
    private Integer isDeduction = 1;

    @ApiModelProperty(value = "上级code  默认为-1", name = "上级code  默认为-1", dataType = "Long", required = false)
    private Long upperCode = -1L;

    @ApiModelProperty(value = "系统类型", name = "系统类型", dataType = "Integer", required = false)
    private Integer systemType;
    
    @ApiModelProperty(value = "skuInfoList", name = "入库sku详情", dataType = "List", required = true)
    private List<SkuInfoBean> skuInfoList;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        //参数是否为空检查
        if (null == type || null == inSourceId || null == repoId || repoId <= 0 || null == skuInfoList || skuInfoList.isEmpty() || null == systemType) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        if (null == StockInTypeEnum.getByType(type)) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        //检查所有sku内参数是否正确
        for (SkuInfoBean skuInfo : skuInfoList) {
            //检查内部sku参数是否为空
            if (null == skuInfo.getAmount() ||  StringUtils.isBlank(skuInfo.getBatchNo()) || null == skuInfo.getSkuId()) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查内部sku数量、重量不能小于0
            if (skuInfo.getAmount() <= 0) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
