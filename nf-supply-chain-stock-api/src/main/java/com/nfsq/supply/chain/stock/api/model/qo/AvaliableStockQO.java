package com.nfsq.supply.chain.stock.api.model.qo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author wangk
 * @date 2018年5月22日 上午10:28:59
 */
@ApiModel(value = "ReqSwagger2", description = "查询可用库存")
@Getter
@Setter
public class AvaliableStockQO extends BaseBean {
    @ApiModelProperty(value = "wmsAvaliableStockBOs", name = "待查询的可用库存实体", dataType = "List", required = true)
    private List<WmsAvaliableStockBO> wmsAvaliableStockBOs;

    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.utils.common.BaseBean#validate()
     */
    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if (CollectionUtils.isEmpty(wmsAvaliableStockBOs)) {
            return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
        }
        for (WmsAvaliableStockBO wmsAvaliableStockBO : wmsAvaliableStockBOs) {
			JsonResult<?> result = wmsAvaliableStockBO.validate();
			if(!result.isSuccess()){
				return result;
			}
		}
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

}
