package com.nfsq.supply.chain.stock.microservice.in.model.vo;

import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/12 下午1:49
 */
@ApiModel(value = "ReqSwagger2",description = "入库通知单")
@Getter
@Setter
public class InNoticeVo{

    @ApiModelProperty(value = "inInNotice",name = "入库通知单",dataType = "Object")
    private WmsInInNotice inInNotice;

    @ApiModelProperty(value = "inInNoticeDetails",name = "入库通知详情单",dataType = "List")
    private List<WmsInInNoticeDetail> inInNoticeDetails;

}
