package com.nfsq.supply.chain.stock.microservice.out.service.impl;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutRule;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutRuleMapper;
import com.nfsq.supply.chain.stock.microservice.out.service.OutRuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xwcai
 * @date 2018/9/2 下午1:46
 */
@Component
@Slf4j
public class OutRuleServiceImpl implements OutRuleService {

    @Resource
    WmsOutRuleMapper wmsOutRuleMapper;

    @Override
    public WmsOutRule selectBySystemType(Integer systemType) {
        return wmsOutRuleMapper.selectBySystemType(systemType);
    }
}
