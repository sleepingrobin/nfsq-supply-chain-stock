package com.nfsq.supply.chain.stock.microservice.in.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageVO;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface WmsInInNoticeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInInNotice record);

    int insertSelective(WmsInInNotice record);

    WmsInInNotice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInInNotice record);

    int updateByPrimaryKey(WmsInInNotice record);

    InNoticeVo selectDetailByNoticeId(@Param("id") Long id);

    List<InNoticeVo> selectDetailByTaskIdAndRepoId(@Param("taskId") Long taskId,@Param("repoId")Long repoId);

    List<WmsInInNotice> selectUnFinishByTaskId(@Param("taskId") Long taskId);

    List<WmsInInNotice> selectByTaskId(@Param("taskId") Long taskId);

    @DataAuthority(tableAlias="n")
    List<InNoticeManageVO> selectManageByQoPage(InNoticeQO inNoticeQo);

    @DataAuthority(tableAlias="n")
    InNoticeManageVO selectManageByNO(@Param("inNoticeNo") String inNoticeNo);

    List<WmsInInNotice> selectByTypeAndInSourceId(@Param("type") Integer type,@Param("inSourceId") Long inSourceId,@Param("repoId") Long repoId);

    int updateByNoticeNoSelective(WmsInInNotice record);

    WmsInInNotice selectByRepoIdAndSourceIdAndType(@Param("repoId") Long repoId,@Param("inSourceId") Long inSourceId,@Param("type") Integer type);

    List<BillDTO> queryBillNOsByQO(@Param("inSourceId") Long inSourceId);

    List<String> selectAllNOs();

    @DataAuthority
    Integer selectCountByQO(BillCountQO qo);

    int updateUpperCode(WmsInInNotice record);

    @DataAuthority
    Integer selectCountProcessing(@Param("type") Integer type);

    List<WmsInInNoticeDetail> getAllStoreRecord(@Param("ownerType") Integer code);

    void repairAllStoreRecord(@Param("list") List<WmsInInNoticeDetail> inInNoticeDetails);

    Set<String> selectAllBatchNoStockIn(@Param("inNoticeNo") String inNoticeNo);

}