package com.nfsq.supply.chain.stock.microservice.out.model.vo;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 下午5:53
 */
@ApiModel(value = "ReqSwagger2",description = "出库通知单列表")
@Getter
@Setter
public class OutOutNoticeListVo extends PageParameter {

    @ApiModelProperty(value = "outOutNotices",name = "出库通知单列表",dataType = "List")
    private List<WmsOutOutNotice> outOutNotices;
}
