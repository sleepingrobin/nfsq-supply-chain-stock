package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.FinishOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.OutNoticeListBean;
import com.nfsq.supply.chain.stock.api.model.bean.SendOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeBatchQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticePrintTimesQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.api.service.OutNoticeProviderApi;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author xwcai
 * @date 2018/5/13 下午4:01
 */
@RestController
@Slf4j
public class OutNoticeController implements OutNoticeProviderApi {

    @Autowired
    OutNoticeService outNoticeService;

    @Override
    public JsonResult<Map<String, String>> listOutNoticeCreate(@RequestBody OutNoticeListBean bean) {
        JsonResult<Map<String, String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.listCreateOutOutNotice(bean);
        } catch (ChainStockException e) {
            log.warn("批量创建出库通知单失败：");
            log.warn(e.getMessage(), e);
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(Boolean.FALSE);
        } catch (Exception e) {
            log.error("批量创建出库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeVOList> queryWithDetailByQo(@RequestBody OutNoticeQo qo) {
        JsonResult<OutNoticeVOList> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithDetailByQo(qo);
        } catch (ChainStockException e) {
            log.warn("查询出库通知单包括详情失败：");
            log.warn(e.getMessage(), e);
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(Boolean.FALSE);
        } catch (Exception e) {
            log.error("查询出库通知单包括详情失败：");
            log.error(e.getMessage(), e);
            jsonResult.setSuccess(Boolean.FALSE);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageListVO> manageQueryByQO(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<OutNoticeManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.manageQueryByQO(outNoticeQo);
        } catch (Exception e) {
            log.error("查询出库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageListVO> manageQueryWaitByQO(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<OutNoticeManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.manageQueryWaitByQO(outNoticeQo);
        } catch (Exception e) {
            log.error("查询待办出库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeDetailManagePageVO> manageQuery(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<OutNoticeDetailManagePageVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.manageDetailQuery(outNoticeQo);
        } catch (Exception e) {
            log.error("查询出库通知详情单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Long> manageQueryUnFinishCount(@RequestBody DummyBean bean) {
        JsonResult<Long> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.manageQueryUnFinishCount();
        } catch (Exception e) {
            log.error("查询未完成出库通过单单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeVOList> queryByQo(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<OutNoticeVOList> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryByQo(outNoticeQo);
        } catch (Exception e) {
            log.error("根据qo 查询出库通知单：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> queryAllOutNoticeNos(@RequestBody DummyBean dummyBean) {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryAllOutNoticeNos();
        } catch (Exception e) {
            log.error("查询所有出库通知单单号失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageVO> queryWithDetailByNO(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<OutNoticeManageVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithDetailByNO(outNoticeQo);
        } catch (Exception e) {
            log.error("根据出库单号查询出库通知详情失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> updatePrintTimes(@RequestBody OutNoticePrintTimesQO outNoticePrintTimesQO) {
        JsonResult<Boolean> result = new JsonResult<>();
        result.setData(true);
        try {
            int res = outNoticeService.updatePrintTimesByOutNoticeNo(outNoticePrintTimesQO.getOutNoticeNo());
            if (res == 1) {
                result.setData(false);
            }
        } catch (Exception e) {
            result.setSuccess(false);
            String errmsg = "更新打印次数异常";
            log.error(errmsg, e);
            result.setMsg(errmsg);
            return result;
        }
        result.setSuccess(true);
        return result;
    }

    @Override
    public JsonResult<List<StockOutSkuInfoVO>> selectStockOutSkuInfo(@RequestBody StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<List<StockOutSkuInfoVO>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.selectStockOutSkuInfo(stockOutSkuInfoQO);
        } catch (Exception e) {
            log.error("根据出库通知单列表，查询所有出库产品失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(@RequestBody StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.selectStockOutSkuInfoByMap(stockOutSkuInfoQO);
        } catch (Exception e) {
            log.error("根据出库通知单列表，查询所有出库产品失败，以通知单维度");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> checkOutNoticeStockOutScan(@RequestBody OutNoticeQo outNoticeQo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.checkOutNoticeStockOutScan(outNoticeQo);
        } catch (Exception e) {
            log.error("根据出库通知单单号查询该通知单是否扫码出库失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Long> manageFinishOutNotice(@RequestBody FinishOutNoticeBean finishOutNoticeBean) {
        JsonResult<Long> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.manageFinishOutNotice(finishOutNoticeBean);
        } catch (Exception e) {
            log.error("根据出库通知单单号，结单失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<OutOutSapDetailVO>> selectStockOutSapInfo(@RequestBody StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<List<OutOutSapDetailVO>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.selectStockOutSapInfo(stockOutSkuInfoQO);
        } catch (Exception e) {
            log.error("sap交货单显示，根据出库通知单列表，查询所有出库产品失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> sendStockOutMsg(@RequestBody SendOutNoticeBean sendOutNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.sendStockOutMsg(sendOutNoticeBean);
        } catch (Exception e) {
            log.error("根据出库单单号，发送出库完成消息失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<OutNoticeManageVO>> batchQueryWithDetailByNOs(@RequestBody OutNoticeBatchQO outNoticeBatchQO) {
        JsonResult<List<OutNoticeManageVO>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.batchQueryWithDetailByNOs(outNoticeBatchQO);
        } catch (Exception e) {
            log.error("根据通知单单号列表 查询对应的明细");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> checkAcceptOrderStatus(@RequestBody OutNoticeBatchQO outNoticeBatchQO) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try{
            jsonResult = outNoticeService.checkAcceptOrderStatus(outNoticeBatchQO);
        }catch(Exception e){
            log.error("根据出库通知单单号查询该通知单是否已被接单失败");
            log.error(e.getMessage(),e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(),StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> deleteNoticeAndDetail(@RequestBody OutNoticeBatchQO outNoticeBatchQO) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try{
            jsonResult = outNoticeService.deleteNoticeAndDetail(outNoticeBatchQO);
        }catch(Exception e){
            log.error("根据出库通知单单号删除通知单和通知单详情失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
            return jsonResult;
        }
        return jsonResult;
    }
}
