package com.nfsq.supply.chain.stock.microservice.in.model.vo;

import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 下午5:07
 */
@ApiModel(value = "ReqSwagger2",description = "入库详情单列表")
@Getter
@Setter
public class InNoticeListVo extends PageParameter {

    @ApiModelProperty(value = "wmsInInNotices",name = "入库详情单列表",dataType = "List")
    private List<WmsInInNotice> wmsInInNotices;
}
