package com.nfsq.supply.chain.stock.microservice.common.constant;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/5/19 下午5:08
 */
public class InOutConstants {

    /**
     * 修改通知单的
     */
    public static final String UPDATE_NOTICE = "updateNotice";

    /**
     * 新增实际单的
     */
    public static final String INSERT_ORDER = "insertOrder";

    /**
     * 修改库存的
     */
    public static final String UPDATE_STOCK = "updateStock";

    /**
     * topic 出库通知单完成
     */
    public static final String FINISH_OUT_NOTICE = "outNoticeFinish";

    /**
     * topic 入库通知单完成
     */
    public static final String FINISH_IN_NOTICE = "inNoticeFinish";

    /**
     * topic 出库单结束
     */
    public static final String FINISH_OUT_OUT = "outOutFinish";

    /**
     * topic sap交货
     */
    public static final String SAP_DELIVERY = "scm.sap.dump.delivery";


    /**
     * topic sap收货
     */
    public static final String SAP_RECEIPT = "scm.sap.dump.recipient";


    /**
     * 随机数最大
     */
    public static final Integer RANDOM_MAX_NUM = 99999;

    /**
     * 出库通知单
     */
    public static final String OUT_NOTICE_NO_STR = "cktzd";

    /**
     * 出库单
     */
    public static final String OUT_OUT_NO_STR = "ckd";

    /**
     * 入库通知单
     */
    public static final String IN_NOTICE_NO_STR = "rktzd";

    /**
     * 入库单
     */
    public static final String IN_IN_NO_STR = "rkd";

    /**
     * 默认数据库批次
     */
    public static final String DEFAULT_BATCH_NO = "default";


    /**
     * 默认批次
     */
    public static final String DEFAULT_BATCH_NO_CHINESE = "无";

    /**
     * 重量换算率
     */
    public static final BigDecimal RATE_OF_WEIGHT = new BigDecimal("1000");

    public static final BigDecimal ZERO = new BigDecimal("0");
}
