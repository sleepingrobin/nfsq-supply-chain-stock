package com.nfsq.supply.chain.stock.microservice.out.model.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/8/7 下午6:43
 */
@Getter
@Setter
public class SkuInfoWithSkuNoBean {

    @ApiModelProperty(value = "skuId",name = "物料编码",dataType = "Long",required =true)
    private Long skuId;

    @ApiModelProperty(value = "skuNo",name = "物料编码",dataType = "Long",required =true)
    private String skuNo;

    @ApiModelProperty(value = "skuUnit",name = "物料单位",dataType = "Long",required =true)
    private String skuUnit;

    @ApiModelProperty(value = "batchNo",name = "批次信息",dataType = "String",required =true)
    private String batchNo;

    @ApiModelProperty(value = "amount",name = "数量",dataType = "Integer",required =true)
    private Integer amount;

    public Long getSkuId() {
        return skuId;
    }
}
