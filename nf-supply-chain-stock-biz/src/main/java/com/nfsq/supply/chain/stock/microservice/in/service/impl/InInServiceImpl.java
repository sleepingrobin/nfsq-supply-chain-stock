package com.nfsq.supply.chain.stock.microservice.in.service.impl;

import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInInDetailDTO;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleGroupEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InDetailManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageVO;
import com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.in.dal.mapper.WmsInInDetailMapper;
import com.nfsq.supply.chain.stock.microservice.in.dal.mapper.WmsInInMapper;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InInVo;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo;
import com.nfsq.supply.chain.stock.microservice.in.service.InInService;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.DataCenterService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.stock.microservice.out.service.AsyncService;
import com.nfsq.supply.chain.utils.common.BigDecimalUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:03
 */
@Component
@Slf4j
public class InInServiceImpl implements InInService {

    @Autowired
    InNoticeService inNoticeService;

    @Autowired
    WmsInInMapper wmsInInMapper;

    @Autowired
    WmsInInDetailMapper wmsInInDetailMapper;

    @Autowired
    WmsInventoryStockService wmsInventoryStockService;

    @Autowired
    BaseInfoService baseInfoService;

    @Autowired
    DataCenterService dataCenterService;

    @Autowired
    AsyncService asyncService;

    @Override
    public JsonResult<String> createInInWithSap(StockInBean inBean) {
        return ((InInServiceImpl) AopContext.currentProxy()).createInIn(inBean);
    }

    @Override
    public JsonResult<String> createInIn(StockInBean inBean) {
        JsonResult<String> jsonResult = ((InInServiceImpl) AopContext.currentProxy()).createInInWithOutSendMsg(inBean);
        if(jsonResult.isSuccess() && StringUtils.isNotEmpty(jsonResult.getData())){
            //发送kafka消息
            asyncService.sendInNoticeMessage(inBean);
        }
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<String> createInInWithOutSendMsg(StockInBean inBean){
        JsonResult<String> jsonResult = new JsonResult<>();
        //检查入库通知单单号是否正确
        InNoticeVo inNoticeVo = inNoticeService.getByInInNoticeId(inBean.getInNoticeId());
        //判断入库通知单状态
        JsonResult<Boolean> result = inNoticeService.checkInInNotice(inNoticeVo);
        if (!result.isSuccess()) {
            jsonResult.setMsg(result.getMsg());
            return jsonResult;
        }

        //校验入库通知单 并构建入库详细单 并且会重新给需要更新的入库通知详情单  入库通知单完成状态赋值
        JsonResult<Map<String, Object>> detailResult = this.getInDetail(inBean, inNoticeVo);
        if (!detailResult.isSuccess()) {
            jsonResult.generateCodeAndMsgInfo(detailResult.getCode(), detailResult.getMsg());
            return jsonResult;
        }
        if (null == detailResult.getData() || detailResult.getData().isEmpty()) {
            jsonResult.setMsg("生成入库详情单失败");
            return jsonResult;
        }

        //生成入库单
        String no = this.createInInOrder(inBean, detailResult.getData(),inNoticeVo.getInInNotice().getSystemType());
        jsonResult.setSuccess(true);
        jsonResult.setData(no);
        return jsonResult;
    }

    @Override
    public JsonResult<InManageListVO> queryInInByQo(InInQO inInQo) {
        JsonResult<InManageListVO> jsonResult = new JsonResult<>();
        InManageListVO vo = new InManageListVO();
        jsonResult.setSuccess(true);
        List<InManageVO> manageVOS = wmsInInMapper.selectByQoPage(inInQo);
        BeanUtils.copyProperties(inInQo, vo);
        if (null == manageVOS || manageVOS.isEmpty()) {
            jsonResult.setData(vo);
            return jsonResult;
        }
        //获取仓库名称，联系人 联系电话 等
        for(InManageVO manageVO : manageVOS){
            this.getRepoInfoByRepoId(manageVO);
        }
        vo.setVoList(manageVOS);
        jsonResult.setData(vo);
        return jsonResult;
    }

    @Override
    public InInVo getByInInNO(String inInNo) {
        InInVo inInVo = wmsInInMapper.selectDetailByInNo(inInNo);
        //判断查询出来的内容是否正确
        if (null == inInVo || null == inInVo.getWmsInIn() || null == inInVo.getWmsInInDetails() || inInVo.getWmsInInDetails().isEmpty()) {
            return null;
        }
        return inInVo;
    }

    @Override
    public List<WmsInIn> getByInNoticeId(Long inNoticeId) {
        return wmsInInMapper.selectByInNoticeId(inNoticeId);
    }

    @Override
    public JsonResult<InDetailManageListVO> getDetailByQO(InInQO qo) {
        JsonResult<InDetailManageListVO> jsonResult = new JsonResult<>();
        InDetailManageListVO vo = new InDetailManageListVO();
        jsonResult.setSuccess(true);
        List<WmsInInDetailDTO> detailDTOS = wmsInInDetailMapper.selectByQOPage(qo);
        BeanUtils.copyProperties(qo,vo);
        if (CollectionUtils.isEmpty(detailDTOS)) {
            jsonResult.setData(vo);
            return jsonResult;
        }
        detailDTOS.forEach(detail -> {
            //去除0
            detail.setSkuNo(detail.getSkuNo().replaceAll("^(0+)", ""));
            //获取owner 名称
            String ownerName = dataCenterService.findOwnerNameByIdAndType(detail.getOwner(),detail.getOwnerType());
            if(org.apache.commons.lang.StringUtils.isNotEmpty(ownerName)){
                detail.setOwnerName(ownerName);
            }
        });
        vo.setDetailDTOS(detailDTOS);
        jsonResult.setData(vo);
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> confirmInIn(ConfirmBO confirmBO) {

        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //根据任务ID 获取所有未完成的入库通知单
        List<InNoticeVo> inNoticeVos = inNoticeService.getDetailByTaskIdAndRepoId(confirmBO);
        if (null == inNoticeVos || inNoticeVos.isEmpty()) {
            jsonResult.setMsg("根据任务单号查询不到入库通知单");
            return jsonResult;
        }

        //根据所有未完成的入库通知单 创建对应的入库单
        List<StockInBean> beans = this.buildListStockInBean(inNoticeVos);
        //循环创建入库单
        this.listConfirmInIn(confirmBO,beans);
        //发送sap消息 确认收货消息 没有异常 就是成功
        for(StockInBean inBean : beans){
            //发送kafka消息
            asyncService.sendInNoticeMessage(inBean);
        }

        jsonResult.setData(true);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void listConfirmInIn(ConfirmBO confirmBO,List<StockInBean> beans){
        //循环创建入库单
        for (StockInBean bean : beans) {
            //创建人
            bean.setModifyId(confirmBO.getUserId());
            JsonResult<String> result = ((InInServiceImpl) AopContext.currentProxy()).createInInWithOutSendMsg(bean);
            if (!result.isSuccess()) {
                log.warn("批量创建入库单失败" + result.getMsg());
                throw new ChainStockException("批量创建入库单失败");
            }
        }
    }

    @Override
    public List<BillDTO> queryNOsByTaskId(Long taskId) {
        return wmsInInMapper.queryBillNOsByQO(taskId);
    }

    @Override
    public JsonResult<List<String>> selectAllNOs() {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        List<String> inNOs = wmsInInMapper.selectAllNOs();
        if(!CollectionUtils.isEmpty(inNOs)){
            jsonResult.setData(inNOs);
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public Integer queryInInCount(BillCountQO qo) {
        return wmsInInMapper.selectCountByQO(qo);
    }

    /**
     * 获取仓库信息
     *
     * @author xwcai
     * @date 2018/7/21 下午1:29
     * @param manageVO
     * @return void
     */
    private void getRepoInfoByRepoId(InManageVO manageVO){
        Repo repo = dataCenterService.findRepoDetailWithOutException(manageVO.getRepoId());
        if(null != repo){
            manageVO.setRepoName(repo.getRepoName());
        }
    }

    /**
     * 构建入库的bean
     *
     * @param inNoticeVos 入库通知单包括明细
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.bean.StockInBean>
     * @author xwcai
     * @date 2018/5/21 下午4:08
     */
    private List<StockInBean> buildListStockInBean(List<InNoticeVo> inNoticeVos) {
        List<StockInBean> beans = new ArrayList<>();
        Date now = new Date();
        for (InNoticeVo vo : inNoticeVos) {
            StockInBean bean = new StockInBean();
            bean.setInDate(now);
            bean.setInNoticeId(vo.getInInNotice().getId());
            bean.setRepoId(vo.getInInNotice().getRepoId());
            List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
            for (WmsInInNoticeDetail detail : vo.getInInNoticeDetails()) {
                SkuInfoBean skuInfoBean = new SkuInfoBean();
                BeanUtils.copyProperties(detail, skuInfoBean);
                //出库数量 是需要出库数量 减去已经出库数量
                skuInfoBean.setAmount((new BigDecimal(detail.getAmount()).subtract(new BigDecimal(detail.getRealAmount()))).intValue());
                skuInfoBeanList.add(skuInfoBean);
            }
            bean.setSkuInfoList(skuInfoBeanList);
            beans.add(bean);

        }
        return beans;
    }

    /**
     * 创建入库单
     *
     * @param inBean 入库的bean
     * @param map    构建好的入库详情单 修改的入库通知详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/13 下午2:27
     */
    @SuppressWarnings("unchecked")
    private String createInInOrder(StockInBean inBean, Map<String, Object> map,Integer systemType) {

        //新建入库单
        WmsInIn wmsInIn = this.buildWmsInIn(inBean,systemType);
        wmsInInMapper.insert(wmsInIn);

        //构建入库详情单
        List<WmsInInDetail> inInDetails = (List<WmsInInDetail>) map.get(InOutConstants.INSERT_ORDER);

        //构建入库详单
        wmsInInDetailMapper.insertBatch(inInDetails, wmsInIn);

        //库存变化
        List<WmsStockBean> wmsStockBeans = this.buildWithInList(inInDetails, inBean,systemType);
        wmsInventoryStockService.updateStock(wmsStockBeans);

        List<WmsInInNoticeDetail> inNoticeDetails = (List<WmsInInNoticeDetail>) map.get(InOutConstants.UPDATE_NOTICE);

        //更新入库通知单状态 入库通知详情单真实数量
        inNoticeService.updateInNoticeDetailWithRealAmount(inNoticeDetails, inBean);
        return wmsInIn.getInNo();
    }

    /**
     * 构建入库单实体
     *
     * @param inBean 入库的实体
     *
     * @return void
     * @author xwcai
     * @date 2018/4/13 下午2:16
     */
    private WmsInIn buildWmsInIn(StockInBean inBean,Integer systemType) {
        WmsInIn wmsInIn = new WmsInIn();
        Date now = new Date();
        //构建创建人 创建时间 修改人 修改时间
        wmsInIn.setCreateDate(now);
        wmsInIn.setModifiedDate(now);
        wmsInIn.setCreateUser(inBean.getUserId());
        wmsInIn.setModifiedUser(inBean.getUserId());

        //构建参数 入库时间 就是当前时间
        wmsInIn.setInDate(now);

        wmsInIn.setOperStatus(StockOperStatusEnum.FINISH.getType());

        //仓库Id
        wmsInIn.setRepoId(inBean.getRepoId());
        //入库单跟入库通知单关联
        wmsInIn.setInNoticeId(inBean.getInNoticeId());
        //入库单单号
        wmsInIn.setInNo(this.getInInId());
        //系统类型
        wmsInIn.setSystemType(systemType);
        return wmsInIn;
    }

    /**
     * 入库单ID 生成
     *
     * @return java.lang.String
     * @author xwcai
     * @date 2018/4/13 下午3:40
     */
    private String getInInId() {
        return baseInfoService.getNOByCharacter(InOutConstants.IN_IN_NO_STR);
    }

    /**
     * 入库参数校验
     *
     * @param inBean     入库的bean
     * @param inNoticeVo 入库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 下午1:51
     */
    private JsonResult<Map<String, Object>> getInDetail(StockInBean inBean, InNoticeVo inNoticeVo) {
        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        //先判断入库单信息
        JsonResult<Boolean> infoResult = this.checkNoticeInfo(inBean, inNoticeVo);
        if (!infoResult.isSuccess()) {
            jsonResult.generateCodeAndMsgInfo(infoResult.getCode(), infoResult.getMsg());
            return jsonResult;
        }
        //判断入库单详细sku信息
        jsonResult = this.getSku(inBean, inNoticeVo);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 入库信息 和 入库通知单信息  校验
     *
     * @param inBean     入库的bean
     * @param inNoticeVo 入库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 下午1:46
     */
    private JsonResult<Boolean> checkNoticeInfo(StockInBean inBean, InNoticeVo inNoticeVo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //判断获取到的参数是否正确
        if (null == inNoticeVo || null == inNoticeVo.getInInNotice() || null == inNoticeVo.getInInNoticeDetails() || inNoticeVo.getInInNoticeDetails().isEmpty() || inNoticeVo.getInInNotice().getOperStatus().equals(StockOperStatusEnum.INVAILD.getType())) {
            log.info("该入库通知单不存在 入库通知单ID：" + inBean.getInNoticeId());
            jsonResult.setMsg("该入库通知单不存在");
            return jsonResult;
        }
        //判断是否是同一个仓库
        if (!inBean.getRepoId().equals(inNoticeVo.getInInNotice().getRepoId())) {
            log.info("该入库通知单 仓库 不正确 仓库ID：" + inBean.getRepoId());
            jsonResult.setMsg("该入库通知单 仓库 不正确");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 入库详情 同入库通知单详情校验
     *
     * @param inBean     入库的bean
     * @param inNoticeVo 入库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 下午1:44
     */
    private JsonResult<Map<String, Object>> getSku(StockInBean inBean, InNoticeVo inNoticeVo) {
        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();
        List<SkuInfoBean> scanList = inBean.getSkuInfoList();
        if (null == inNoticeVo || null == inNoticeVo.getInInNoticeDetails() || inNoticeVo.getInInNoticeDetails().isEmpty()) {
            log.info("入库通知单详情不存在" + inBean.getInNoticeId());
            jsonResult.setMsg("入库通知单详情不存在：");
            return jsonResult;
        }

        List<WmsInInNoticeDetail> noticeDetailList = inNoticeVo.getInInNoticeDetails();

        //对比通知单详情sku种类和入库种类多少 如果不相同 直接返回 认为入库失败
        if (scanList.size() > noticeDetailList.size()) {
            log.info("该扫码结果sku种类超过入库通知单未完成的种类：{}" , scanList.size(), "入库通知单ID：{}" , inBean.getInNoticeId());
            jsonResult.setMsg("该扫码结果sku种类超过入库通知单未完成的种类：" + scanList.size());
            return jsonResult;
        }
        return this.getParam(inBean, scanList, noticeDetailList);

    }

    /**
     * 实际入库校验 获取入库详情单、需要更新的入库单
     *
     * @param inBean           入库的bean
     * @param scanList         扫码的sku统计结果
     * @param noticeDetailList 入库通知单详情列表
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.Map>
     * @author xwcai
     * @date 2018/4/20 下午4:53
     */
    private JsonResult<Map<String, Object>> getParam(StockInBean inBean, List<SkuInfoBean> scanList, List<WmsInInNoticeDetail> noticeDetailList) {
        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();
        Map<String, Object> map = new HashMap<>();
        List<WmsInInDetail> inInDetails = new ArrayList<>();
        //构建更新的入库通知详情单
        List<WmsInInNoticeDetail> updateNoticeDetailList = new ArrayList<>();
        //对比具体的sku详情 skuId 批次  并且去修改入库单数量状态
        for (SkuInfoBean skuInfoBean : scanList) {
            //设置入库扫码的sku 是否存在于入库通知详情单中 初始化为不存在
            Boolean isContains = false;
            for (WmsInInNoticeDetail detail : noticeDetailList) {
                //判断如果skuId  批次相同 状态相同 就认为是同一个
                if (detail.getSkuId().equals(skuInfoBean.getSkuId()) && detail.getBatchNo().equals(skuInfoBean.getBatchNo()) && detail.getStatus().equals(skuInfoBean.getStatus()) && detail.getScanType().equals(skuInfoBean.getScanType())) {
                    //包含
                    isContains = true;
                    //构建修改的bean
                    detail.setModifiedUser(inBean.getUserId());
                    detail.setModifiedDate(new Date());
                    //需要入库数量
                    BigDecimal amount = new BigDecimal(detail.getAmount().toString());
                    //实际已经入库数量
                    BigDecimal realAmount = new BigDecimal(detail.getRealAmount().toString());

                    //设置拥有者
                    detail.setOwner(skuInfoBean.getOwner());

                    //判断新入库的数量 是否小于 还可以入库的数量（需要入库数量 - 实际已经入库数量）
                    Integer compareFlag = new BigDecimal(skuInfoBean.getAmount().toString()).compareTo(amount.subtract(realAmount));
                    //入库数量的比对 如果小于等于 证明 可以入库 ；大于不能入库
                    if (compareFlag < 0) {
                        //设置真实入库数量
                        detail.setRealAmount(skuInfoBean.getAmount());
                        detail.setOperStatus(StockOperStatusEnum.STARTING.getType());
                    } else if (compareFlag == 0) {
                        //设置真实入库数量
                        detail.setRealAmount(skuInfoBean.getAmount());
                        detail.setOperStatus(StockOperStatusEnum.FINISH.getType());
                    } else {
                        jsonResult.setSuccess(false);
                        jsonResult.setMsg("入库数量超出限制");
                        return jsonResult;
                    }

                    WmsInInDetail inDetail = this.buildWmsInInDetail(detail, skuInfoBean);

                    //设置入库单重量判断重量
                    if (null != detail.getGrossWeight() && null != detail.getNetWeight() && StringUtils.isNotEmpty(detail.getWeightUnit())) {
                        //设置重量 要先计算单位重量 然后给出库单详情计算重量 然后再计算出库通知单本次增加的重量
                        BigDecimal grossWeightPro = BigDecimalUtils.div(detail.getGrossWeight(),detail.getAmount(),3);
                        BigDecimal netWeightPro = BigDecimalUtils.div(detail.getNetWeight(),detail.getAmount(), 3);
                        inDetail.setGrossWeight(BigDecimalUtils.mul(grossWeightPro,detail.getAmount(),3));
                        inDetail.setNetWeight(BigDecimalUtils.mul(netWeightPro,detail.getAmount(),3));
                        inDetail.setWeightUnit(detail.getWeightUnit());
                    }

                    inInDetails.add(inDetail);
                    updateNoticeDetailList.add(detail);
                }
            }
            //入库扫码的sku 有在入库通知单中不存在的
            if (!isContains) {
                jsonResult.setSuccess(false);
                jsonResult.setMsg("入库扫码出现不匹配的sku：{}" + skuInfoBean.getSkuId() + "批次信息" + skuInfoBean.getBatchNo());
                return jsonResult;
            }

        }
        map.put(InOutConstants.UPDATE_NOTICE, updateNoticeDetailList);
        map.put(InOutConstants.INSERT_ORDER, inInDetails);
        jsonResult.setData(map);
        jsonResult.setSuccess(true);

        return jsonResult;
    }

    /**
     * 构建入库详细单
     *
     * @param detail      入库通知单详情
     * @param skuInfoBean 扫码入库的sku详情
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail
     * @author xwcai
     * @date 2018/4/13 下午2:57
     */
    private WmsInInDetail buildWmsInInDetail(WmsInInNoticeDetail detail, SkuInfoBean skuInfoBean) {
        WmsInInDetail inInDetail = new WmsInInDetail();
        Date now = new Date();
        //创建人 创建时间 修改人 修改时间
        inInDetail.setCreateDate(now);
        inInDetail.setModifiedDate(now);
        inInDetail.setCreateUser(detail.getModifiedUser());
        inInDetail.setModifiedUser(detail.getModifiedUser());
        inInDetail.setStatus(detail.getStatus());

        //数量
        inInDetail.setAmount(skuInfoBean.getAmount());
        //批次
        inInDetail.setBatchNo(skuInfoBean.getBatchNo());
        //资产拥有者
        inInDetail.setOwner(skuInfoBean.getOwner());
        inInDetail.setOwnerType(skuInfoBean.getOwnerType());
        //sku信息
        inInDetail.setSkuId(skuInfoBean.getSkuId());
        inInDetail.setSkuNo(detail.getSkuNo());

        //设置扫码方式
        inInDetail.setScanType(skuInfoBean.getScanType());

        //构建基础的数据
        inInDetail.setSkuName(detail.getSkuName());
        inInDetail.setSkuUnit(detail.getSkuUnit());
        inInDetail.setInNoticeDetailId(detail.getId());

        return inInDetail;
    }

    /**
     * 构建扣减库存的bean
     *
     * @param detailList 入库单详情
     * @param inBean     构建修改库存bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean
     * @author xwcai
     * @date 2018/5/19 下午5:02
     */
    private List<WmsStockBean> buildWithInList(List<WmsInInDetail> detailList, StockInBean inBean,Integer systemType) {
        List<WmsStockBean> wmsStockBeans = new ArrayList<>();
        for (WmsInInDetail detail : detailList) {
            WmsStockBean bean = this.buildWithIn(detail, inBean,systemType);
            wmsStockBeans.add(bean);
        }
        return wmsStockBeans;
    }

    /**
     * 构建修改库存的beanbean
     *
     * @param detail 入库单详情
     * @param inBean 入库的bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean
     * @author xwcai
     * @date 2018/6/6 上午9:34
     */
    private WmsStockBean buildWithIn(WmsInInDetail detail, StockInBean inBean,Integer systemType) {
        WmsStockBean bean = new WmsStockBean();
        //构建sku相关
        bean.setStatus(detail.getStatus());
        bean.setBatchNo(detail.getBatchNo());
        bean.setNums(detail.getAmount());
        bean.setSkuId(detail.getSkuId());
        bean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());

        //构建创建时间 修改时间 创建人 修改人
        bean.setModifiedUser(detail.getModifiedUser());
        bean.setCreatedUser(detail.getModifiedUser());
        bean.setModifiedDate(detail.getModifiedDate());
        bean.setCreatedDate(detail.getModifiedDate());

        //构建资源拥有
        bean.setOwner(detail.getOwner());
        bean.setOwnerType(detail.getOwnerType());
        //构建用途 入库都是通用
        bean.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
        bean.setRuleType(RuleTypeEnums.COMMON.getCode());

        //构建仓库
        bean.setRepoId(inBean.getRepoId());
        //设置入库单Id
        bean.setOrderDetailId(detail.getId());
        //设置操作类型
        bean.setType(InOutTypeEnums.IN_STOCK.getCode());
        //目标状态
        bean.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        //系统类型
        bean.setSystemType(systemType);
        return bean;
    }

    @Override
    public List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(List<String> inNoticeNos) {
        return wmsInInDetailMapper.selectStockOutSkuInfoByMap(inNoticeNos);
    }
}
