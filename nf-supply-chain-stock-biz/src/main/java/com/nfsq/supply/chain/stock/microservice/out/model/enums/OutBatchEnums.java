package com.nfsq.supply.chain.stock.microservice.out.model.enums;

import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/9/2 下午1:58
 */
@Getter
public enum OutBatchEnums {

    BATCH(1,"分批"),
    NOT_BATCH(0,"不分批");

    private Integer code;

    private String desc;

    OutBatchEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
