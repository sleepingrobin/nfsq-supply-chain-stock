package com.nfsq.supply.chain.stock.common.kafka;

import com.nfsq.supply.chain.stock.api.model.bean.SendOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;



/**
 * @author peter
 * @date 2017/12/31 16:42
 */
@Component
public class ConsumeMessageService {

    private static final Logger logger = LoggerFactory.getLogger(ConsumeMessageService.class);

    @Autowired
    OutNoticeService outNoticeService;

    @KafkaListener(topics = "sendMessageToStock")
    public JsonResult<Boolean> outOfStockMsg(String content){
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try{
            logger.info("接收出库消息:{}",content);
            SendOutNoticeBean sendOutNoticeBean = JsonUtils.toObject(content,SendOutNoticeBean.class);
            jsonResult = outNoticeService.sendStockOutMsg(sendOutNoticeBean);
        }catch (Exception e){
            logger.error("sendMessageToStock接收消息发生异常:{}",e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }


}