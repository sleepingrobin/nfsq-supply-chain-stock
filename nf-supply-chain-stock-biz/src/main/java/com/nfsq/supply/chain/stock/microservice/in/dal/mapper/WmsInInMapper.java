package com.nfsq.supply.chain.stock.microservice.in.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageVO;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InInVo;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsInInMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInIn record);

    int insertSelective(WmsInIn record);

    WmsInIn selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInIn record);

    int updateByPrimaryKey(WmsInIn record);

    InInVo selectDetailByInNo(@Param("inInNo") String inInNo);

    List<WmsInIn> selectByInNoticeId(@Param("inNoticeId") Long inNoticeId);

    @DataAuthority(tableAlias="i")
    List<InManageVO> selectByQoPage(InInQO qo);

    List<BillDTO> queryBillNOsByQO(@Param("inSourceId") Long inSourceId);

    List<String> selectAllNOs();

    Integer selectCountByQO(BillCountQO qo);
}