package com.nfsq.supply.chain.stock.common.idempotency;

import com.nfsq.supply.chain.stock.common.idempotency.mapper.IdempotencyMapper;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import com.nfsq.supply.chain.utils.common.ObjectUtil;
import com.nfsq.supply.chain.utils.common.ResultCode;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.lang.reflect.Method;

/**
 * @author liuxinxing
 * @date 2018年3月29日 上午10:05:32
 */
@Aspect //AOP 切面
@Component
@EnableAspectJAutoProxy(exposeProxy = true)
public class IdempotencyCheckAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(IdempotencyCheckAspect.class);
    @Autowired
    private IdempotencyMapper idempotencyMapper;

    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    private final String UNIQUEKEYSPLIT = "-";

    /**
     * 在方法执行前后
     *
     * @param joinPoint
     * @param idempotencyCheck
     * @return
     */
    @Around(value = "@annotation(com.nfsq.supply.chain.stock.common.idempotency.IdempotencyCheck) && @annotation(idempotencyCheck)")
    public Object around(ProceedingJoinPoint joinPoint, IdempotencyCheck idempotencyCheck) throws Exception {
        //最终返回数据
        JsonResult<?> mainResult = new JsonResult<>();
        //拦截的方法
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        LOGGER.info("幂等方法:" + method + "++++around方法++++ 执行");
        //获取参数封装uniqueKey
        IdempotencyDO idempotencyDO = null;
        try {
            idempotencyDO = createUniqueKey(joinPoint,mainResult,idempotencyCheck);
        } catch (RuntimeException e) {
            return mainResult;
        }
        //插入记录表操作
        Boolean insertFlag = proceedAfterInsert(idempotencyDO, joinPoint);
        //执行业务方法
        return doProceed(joinPoint, mainResult, method, idempotencyDO, insertFlag);
    }

    /**
     * 执行业务方法
     * @param joinPoint
     * @param mainResult
     * @param method
     * @param idempotencyDO
     * @param insertFlag
     * @return
     */
    private Object doProceed(ProceedingJoinPoint joinPoint, JsonResult<?> mainResult, Method method, IdempotencyDO idempotencyDO, Boolean insertFlag) {
        try {
            if (insertFlag) {
                //执行业务方法
                JsonResult<?> jsonResult = (JsonResult<?>) joinPoint.proceed();
                if (jsonResult.isSuccess()) {
                    //业务执行成功 将结果添加至记录表中；返回执行结果；
                    idempotencyDO.setResult(JsonUtils.toJson(jsonResult));
                    idempotencyMapper.updateByUniqueKey(idempotencyDO);
                } else {
                    //业务执行失败，将之前记录删除并返回
                    idempotencyMapper.deleteByUniqueKey(idempotencyDO);
                }
                return jsonResult;
            } else {
                //防止插入失败且查询有问题；
                try {
                    LOGGER.warn("幂等情况发生，方法：",method,"参数为：", joinPoint.getArgs());
                    IdempotencyDO idempotency = idempotencyMapper.selectByUniqueKey(idempotencyDO);
                    String result = idempotency.getResult();
                    if (StringUtils.isNotBlank(result)) {
                        //如果有result值直接返回
                        return JsonUtils.toObject(result, JsonResult.class);
                    } else {
                        //没有的result则返回当前系统业务正在执行，
                        return mainResult.generateCodeAndMsgInfo(ResultCode.ERROR, "当前系统业务正在执行");
                    }
                } catch (Exception e) {
                    LOGGER.warn("系统异常！",e);
                    return mainResult.generateCodeAndMsgInfo(ResultCode.ERROR, "系统异常！");
                }
            }
        } catch (Throwable throwable) {
            //业务代码执行报错；已成功插入数据库，回滚之前记录 获取当前异常并返回；
            LOGGER.info("代码执行报错{}",throwable);
            try {
                //delete可能在insert失败时也失败
                idempotencyMapper.deleteByUniqueKey(idempotencyDO);
                return mainResult.generateCodeAndMsgInfo(ResultCode.ERROR,"系统异常,代码执行报错");

            } catch (Exception e) {
                LOGGER.info("除记录库同时失败删{}",e);
                return mainResult.generateCodeAndMsgInfo(ResultCode.ERROR,"系统异常,删除记录库同时失败");
            }
        }
    }

    /**
     * 获取参数并封装uniqueKey
     * @param joinPoint
     * @param mainResult
     * @param idempotencyCheck
     * @return
     * @throws Exception
     */
    private IdempotencyDO createUniqueKey(ProceedingJoinPoint joinPoint, JsonResult<?> mainResult,IdempotencyCheck idempotencyCheck) throws RuntimeException{
        StringBuffer uniqueKey = new StringBuffer();
        String[] bizUniqueKeys = idempotencyCheck.bizUniqueKey();
        IdempotencyDO idempotencyDO =new IdempotencyDO();
        Object[] args = joinPoint.getArgs();
        if(args == null || args.length>1){
            mainResult.generateCodeAndMsgInfo(ResultCode.ERROR, "使用@idempotencyCheck注解时，方法参数必须为一个");
            LOGGER.error("使用@idempotencyCheck注解时，方法参数必须为一个");
            throw new RuntimeException("使用@idempotencyCheck注解时，方法参数必须为一个");
        }
        Object arg = args[0];
        for (String key :bizUniqueKeys) {
            try {
                Object oneOfUniqueKey = ObjectUtil.getValue(key,arg);
                uniqueKey.append(oneOfUniqueKey.toString()).append(UNIQUEKEYSPLIT);
            }catch (Exception e){
                LOGGER.error(key+"属性在参数对象中不存在，请确认",e);
                mainResult.generateCodeAndMsgInfo(ResultCode.ERROR,key+"属性在参数对象中不存在，请确认");
                throw new RuntimeException("属性在参数对象中不存在，请确认");
            }
        }
        idempotencyDO.setBizUniqueKey(uniqueKey.substring(0,uniqueKey.length()-1));
        return idempotencyDO;
    }

    /**
     * 插入操作记录
     * @param idempotencyDO
     * @param joinPoint
     * @return
     */
    public Boolean proceedAfterInsert(IdempotencyDO idempotencyDO, ProceedingJoinPoint joinPoint) {
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = dataSourceTransactionManager.getTransaction(def);
        try {
            //唯一索引，所以插入失败必然抛错；
            idempotencyMapper.insert(idempotencyDO);
            dataSourceTransactionManager.commit(status);
            return true;
        } catch (Exception e) {
            LOGGER.error("插入记录表失败{}",e);
            dataSourceTransactionManager.rollback(status);
            return false;
        }
    }
}
