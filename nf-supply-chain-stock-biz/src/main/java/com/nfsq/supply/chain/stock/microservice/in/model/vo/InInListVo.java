package com.nfsq.supply.chain.stock.microservice.in.model.vo;

import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.utils.common.PageParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 下午5:24
 */
@ApiModel(value = "ReqSwagger2",description = "入库单列表")
@Getter
@Setter
public class InInListVo extends PageParameter {

    @ApiModelProperty(value = "wmsInInList",name = "入库单列表",dataType = "List")
    private List<WmsInIn> wmsInInList;
}
