package com.nfsq.supply.chain.stock.microservice.in.dal.domain;

import java.util.Date;

public class WmsInInNotice {
    private Long id;

    private String inNoticeNo;

    private Integer type;

    private String typeDes;

    private Long inSourceId;

    private Long repoId;

    private Date planInDate;

    private Integer operStatus;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    private Integer isDisplay;

    private Integer isDeduction;

    private Long upperCode;

    private Integer systemType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInNoticeNo() {
        return inNoticeNo;
    }

    public void setInNoticeNo(String inNoticeNo) {
        this.inNoticeNo = inNoticeNo == null ? null : inNoticeNo.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeDes() {
        return typeDes;
    }

    public void setTypeDes(String typeDes) {
        this.typeDes = typeDes == null ? null : typeDes.trim();
    }

    public Long getInSourceId() {
        return inSourceId;
    }

    public void setInSourceId(Long inSourceId) {
        this.inSourceId = inSourceId;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    public Date getPlanInDate() {
        return planInDate;
    }

    public void setPlanInDate(Date planInDate) {
        this.planInDate = planInDate;
    }

    public Integer getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(Integer operStatus) {
        this.operStatus = operStatus;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(Integer isDisplay) {
        this.isDisplay = isDisplay;
    }

    public Integer getIsDeduction() {
        return isDeduction;
    }

    public void setIsDeduction(Integer isDeduction) {
        this.isDeduction = isDeduction;
    }

    public Long getUpperCode() {
        return upperCode;
    }

    public void setUpperCode(Long upperCode) {
        this.upperCode = upperCode;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }
}