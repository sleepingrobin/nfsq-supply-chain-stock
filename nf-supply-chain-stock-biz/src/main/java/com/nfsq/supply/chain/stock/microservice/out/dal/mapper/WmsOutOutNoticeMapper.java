package com.nfsq.supply.chain.stock.microservice.out.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticePageQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticeQo;
import com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVO;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityConstants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public interface WmsOutOutNoticeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsOutOutNotice record);

    int insertSelective(WmsOutOutNotice record);

    WmsOutOutNotice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsOutOutNotice record);

    int updateByPrimaryKey(WmsOutOutNotice record);

    OutOutNoticeVo selectDetailByOutNoticeId(@Param("id") Long id);

    OutOutNoticeVo selectDetailAndNotFinishByOutNoticeId(@Param("id") Long id);

    List<WmsOutOutNotice> selectByQoPage(OutNoticeQo qo);

    List<WmsOutOutNotice> selectByTypeAndInSourceIdAndRepoIdAndDestinationId(OutNoticeQo qo);

    int updateByNoticeNoSelective(WmsOutOutNotice record);

    List<OutOutNoticeVo> selectWithDetailByQo(OutNoticeQo qo);

    List<OutOutNoticeVo> selectWithDetailByQoPage(OutNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    ScanerNoticeVO queryWithScaner(@Param("qo") ScanerNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    ScanerNoticeVO queryWithScanerQO(@Param("qo") ScanerNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<ScanerNoticeVO> queryWithScanerPage(ScanerNoticePageQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    ScanerNoticeVO queryWithScanerShow(ScanerNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    Integer selectCountProcessing(@Param("type") Integer type);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<OutNoticeManageVO> selectManageByQoPage(OutNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<OutNoticeManageVO> selectManageByWaitPage(OutNoticeQo qo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    OutNoticeManageVO selectManageByNO(@Param("outNoticeNo") String outNoticeNo);

    @DataAuthority(tableAlias="n",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<OutNoticeManageVO> selectManageByNOs(@Param("outNoticeNos") List<String> outNoticeNos);

    List<BillDTO> queryBillNOsByQO(@Param("outSourceId") Long outSourceId);

    List<WmsOutOutNotice> selectUnFinishByTaskId(@Param("taskId") Long taskId);

    List<WmsOutOutNotice> selectByTaskId(@Param("taskId") Long taskId);

    @DataAuthority(fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    Integer selectCountByQO(BillCountQO qo);

    List<String> selectAllOutNoticeNos();

    int updateBindByMachineCode(WmsOutOutNotice record);

    int updatePrintTimesByOutNoticeNo(@Param("outNoticeNo") String outNoticeNo);

    Integer selectPrintTimesByNo(@Param("outNoticeNo") String outNoticeNo);

    Long selectByNO(@Param("outNoticeNo") String outNoticeNo);

    List<Long> selectByOutNos(@Param("outNoticeNos") List<String> outNoticeNos);

    Integer selectOrderState(@Param("outNoticeNos") List<String> outNoticeNos);

    int deleteBatchNos(@Param("noticeIds") List<Long> noticeIds);

    int updatePrintTimesByIds(@Param("noticeIds") Set<Long> noticeIds);

}