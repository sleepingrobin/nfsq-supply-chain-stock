package com.nfsq.supply.chain.stock.microservice.out.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.OutDetailManageVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutOutSapDetailVO;
import com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithDestinationBean;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithSkuNoBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityConstants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsOutOutDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsOutOutDetail record);

    int insertSelective(WmsOutOutDetail record);

    WmsOutOutDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsOutOutDetail record);

    int updateByPrimaryKey(WmsOutOutDetail record);

    void insertBatch(@Param("list") List<WmsOutOutDetail> list, @Param("out")WmsOutOut outOut);

    List<WmsOutOutDetail> selectByOutId(@Param("outId") Long outId);

    List<OutDetailManageVO> selectManageByQoPage(OutOutPageQO qo);

    List<OutDetailManageVO> selectManageByQo(OutOutPageQO qo);

    List<SkuInfoWithDestinationBean> selectSkuInfoByTaskId(@Param("taskId") Long taskId);

    List<SkuInfoWithSkuNoBean> selectSkuInfoByOutNoticeId(@Param("outNoticeId") Long outNoticeId);

    @DataAuthority(tableAlias="o",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<SkuManageBO> selectSkuSnedCount(@Param("list") List<Long> longs);

    List<StockOutSkuInfoVO> selectStockOutSkuInfo(@Param("list") List<String> list);

    List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(@Param("list") List<String> list);

    List<OutOutSapDetailVO> selectStockOutSapInfo(@Param("list") List<String> list);

    List<Integer> selectScanTypeByNoticeId(@Param("outNoticeId") Long outNoticeId);
}