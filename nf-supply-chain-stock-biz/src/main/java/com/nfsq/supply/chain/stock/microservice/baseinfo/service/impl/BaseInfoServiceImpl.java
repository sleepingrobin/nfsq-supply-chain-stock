package com.nfsq.supply.chain.stock.microservice.baseinfo.service.impl;

import com.nfsq.supply.chain.datacenter.api.SkuApi;
import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.utils.common.DateUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author xwcai
 * @date 2018/4/14 上午10:47
 */
@Component
@Slf4j
public class BaseInfoServiceImpl implements BaseInfoService {

    @Autowired
    private SkuApi skuApi;

    @Override
    public List<Sku> getSkuDetailBySkuId(List<Long> skuIdList) {
        //判断传入的skuset是否为空
        if (null == skuIdList || skuIdList.isEmpty()) {
            throw new ChainStockException("获取sku详情失败，skuId 不能为空：");
        }
        JsonResult<List<Sku>> jsonResult = skuApi.getBySkuIdList(skuIdList);
        if (!jsonResult.isSuccess() || CollectionUtils.isEmpty(jsonResult.getData())) {
            throw new ChainStockException("获取sku详情失败：" + jsonResult.getMsg());
        }

        return jsonResult.getData();
    }

    @Override
    public Sku getBeanBySkuId(List<Sku> beans, Long skuId) {
        for (Sku sku : beans) {
            if (skuId.equals(sku.getId())) {
                return sku;
            }
        }
        return null;
    }

    @Override
    public StockOperStatusEnum getStockOperStatus(List<Integer> operStatus) {
        StockOperStatusEnum operStatusEnum;
        //如果包含进行中的 直接将主单设置为进行中
        if (operStatus.contains(StockOperStatusEnum.STARTING.getType())) {
            operStatusEnum = StockOperStatusEnum.STARTING;
        } else if (operStatus.contains(StockOperStatusEnum.UN_START.getType())) {
            //如果包含未完成的 直接将主单设置为未完成
            operStatusEnum = StockOperStatusEnum.STARTING;
        } else if (operStatus.contains(StockOperStatusEnum.MORE.getType())) {
            //如果包含超出的 直接将主单设置为未完成
            operStatusEnum = StockOperStatusEnum.STARTING;
        } else if (operStatus.contains(StockOperStatusEnum.FINISH.getType())) {
            //如果包含完成 直接将主单设置为完成
            operStatusEnum = StockOperStatusEnum.FINISH;
        } else {
            //其他的判断都是异常情况
            return null;
        }
        return operStatusEnum;
    }

    @Override
    public String getNOByCharacter(String str) {
        String date = DateUtils.getYyyyMMddHHmmss(new Date());
        Random random = new Random();
        int num = random.nextInt(InOutConstants.RANDOM_MAX_NUM);
        DecimalFormat decimalFormat = new DecimalFormat("00000");
        String numStr = decimalFormat.format(num);
        return str + date + numStr;
    }
}
