package com.nfsq.supply.chain.stock.common.idempotency;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author liuxinxing
 * @date 2018年3月29日 上午10:03:51
 * 注解实现幂登性校验
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface IdempotencyCheck {
    @AliasFor("bizUniqueKey")
    String[] bizUniqueKey() default {};
}