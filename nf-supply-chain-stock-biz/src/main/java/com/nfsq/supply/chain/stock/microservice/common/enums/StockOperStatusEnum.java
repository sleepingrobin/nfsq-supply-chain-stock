package com.nfsq.supply.chain.stock.microservice.common.enums;


import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/4/18 下午7:15
 */
public enum StockOperStatusEnum {
    UN_START(0,"已创建"),
    STARTING(1,"进行中"),
    FINISH(2,"已完成"),
    MORE(3,"超出"),
    INVAILD(-1,"取消");

    /**
     * code
     */
    @Getter
    @Setter
    private Integer type;

    /**
     * desc 说明
     */
    @Getter
    @Setter
    private String typeDes;

    StockOperStatusEnum(Integer type, String typeDes) {
        this.type = type;
        this.typeDes = typeDes;
    }

    public static StockOperStatusEnum getByType(Integer type){
        for(StockOperStatusEnum statusEnum : StockOperStatusEnum.values()){
            if(statusEnum.getType().equals(type)){
                return statusEnum;
            }
        }
        return  null;
    }
}
