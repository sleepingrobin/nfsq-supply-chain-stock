package com.nfsq.supply.chain.stock.microservice.common.enums;

import com.nfsq.supply.chain.stock.api.model.enums.StockInTypeEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/7/26 下午2:45
 */
public enum InAndOutTypeRelationEnum {
    SALE(StockInTypeEnum.OUT_NOTICE,StockOutTypeEnum.SELLOUT),
    DUMP(StockInTypeEnum.DUMPIN,StockOutTypeEnum.DUMPOUT),
    SAP_DELIVERY(StockInTypeEnum.SAP_DELIVERY_IN,StockOutTypeEnum.SAP_DELIVERY_OUT),;

    @Getter
    @Setter
    private StockInTypeEnum inTypeEnum;

    @Getter
    @Setter
    private StockOutTypeEnum outTypeEnum;

    InAndOutTypeRelationEnum(StockInTypeEnum inTypeEnum, StockOutTypeEnum outTypeEnum) {
        this.inTypeEnum = inTypeEnum;
        this.outTypeEnum = outTypeEnum;
    }

    /**
     * 根据出库类型 获取对应的入库类型
     *
     * @author xwcai
     * @date 2018/7/26 下午3:08
     * @param outTypeEnum
     * @return com.nfsq.supply.chain.stock.api.model.enums.StockInTypeEnum
     */
    public static StockInTypeEnum getInTypeEnum(StockOutTypeEnum outTypeEnum){
        for(InAndOutTypeRelationEnum inAndOutTypeRelationEnum : InAndOutTypeRelationEnum.values()){
            if(inAndOutTypeRelationEnum.getOutTypeEnum().equals(outTypeEnum)){
                return inAndOutTypeRelationEnum.getInTypeEnum();
            }
        }
        return  null;
    }

    /**
     * 根据入库类型 获取对应的出库类型
     *
     * @author xwcai
     * @date 2018/7/26 下午3:08
     * @param inTypeEnum
     * @return com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum
     */
    public static StockOutTypeEnum getOutTypeEnum(StockInTypeEnum inTypeEnum){
        for(InAndOutTypeRelationEnum inAndOutTypeRelationEnum : InAndOutTypeRelationEnum.values()){
            if(inAndOutTypeRelationEnum.getInTypeEnum().equals(inTypeEnum)){
                return inAndOutTypeRelationEnum.getOutTypeEnum();
            }
        }
        return  null;
    }
}
