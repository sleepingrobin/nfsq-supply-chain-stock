package com.nfsq.supply.chain.stock.microservice.baseinfo.service;

import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 上午10:46
 */
public interface BaseInfoService {

    /**
     * 获取SkuDetailInfo
     *
     * @param skuIdList skuId
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.model.bean.SkuDetailBean>
     * @author xwcai
     * @date 2018/4/14 上午10:50
     */
    List<Sku> getSkuDetailBySkuId(List<Long> skuIdList);

    /**
     * 根据skuId 获取具体的skuDetail
     *
     * @param beans sku的信息
     * @param skuId sku的id
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.model.bean.SkuDetailBean
     * @author xwcai
     * @date 2018/4/14 上午10:54
     */
    Sku getBeanBySkuId(List<Sku> beans, Long skuId);

    /**
     * 根据类型返回状态啊
     *
     * @param operStatus 单据状态
     *
     * @return com.nfsq.supply.chain.stock.microservice.enums.StockOperStatusEnum
     * @author xwcai
     * @date 2018/4/23 下午1:12
     */
    StockOperStatusEnum getStockOperStatus(List<Integer> operStatus);

    /**
     * 生成各单据编号
     *
     * @param str 编号前缀
     *
     * @return java.lang.String
     * @author xwcai
     * @date 2018/5/28 上午8:54
     */
    String getNOByCharacter(String str);
}
