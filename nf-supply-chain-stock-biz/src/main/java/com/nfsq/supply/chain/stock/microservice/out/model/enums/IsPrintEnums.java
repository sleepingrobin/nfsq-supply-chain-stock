package com.nfsq.supply.chain.stock.microservice.out.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/9/6 下午1:05
 */
@Getter
@AllArgsConstructor
public enum  IsPrintEnums {
    NOT_PRINT(0,"未打印过"),
    PRINT(1,"打印过");

    private Integer code;

    private String desc;
}
