package com.nfsq.supply.chain.stock.microservice.in.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/4/12 下午7:14
 */
@ApiModel(value = "ReqSwagger2",description = "取消入库通知单的入参")
@Getter
@Setter
public class CancelInNoticeBean extends BaseBean{

    @ApiModelProperty(value = "inNoticeId",name = "入库通知单号",dataType = "Long",required =true)
    private Long inNoticeId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        if(null == inNoticeId || inNoticeId < 0){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
