package com.nfsq.supply.chain.stock.microservice.baseinfo.model.bean;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author xwcai
 * @date 2018/4/12 上午9:37
 */
@Getter
@Setter
public class SkuDetailBean {

    private Long id;

    private String skuId;

    private String skuName;

    private String skuUnit;

    private String batchNo;

    private BigDecimal grossWeight;
    
    private BigDecimal netWeight;
    
    private String weightUnit;
}
