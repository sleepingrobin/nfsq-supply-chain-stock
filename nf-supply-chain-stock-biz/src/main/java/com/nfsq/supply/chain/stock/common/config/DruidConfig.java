package com.nfsq.supply.chain.stock.common.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import com.google.common.collect.Lists;

@Configuration
public class DruidConfig {
	public static final Logger logger = LoggerFactory.getLogger(DruidConfig.class);

	@Value("${spring.datasource.scm.db1.url}")
	private String dbUrl;

	@Value("${spring.datasource.scm.db1.username}")
	private String username;

	@Value("${spring.datasource.scm.db1.password}")
	private String password;

	@Value("${spring.datasource.scm.db1.driverClassName}")
	private String driverClassName;

	@Value("${spring.datasource.scm.db1.initialSize}")
	private int initialSize;

	@Value("${spring.datasource.scm.db1.minIdle}")
	private int minIdle;

	@Value("${spring.datasource.scm.db1.maxActive}")
	private int maxActive;

	@Value("${spring.datasource.scm.db1.maxWait}")
	private int maxWait;

	@Value("${spring.datasource.scm.db1.timeBetweenEvictionRunsMillis}")
	private int timeBetweenEvictionRunsMillis;

	@Value("${spring.datasource.scm.db1.minEvictableIdleTimeMillis}")
	private int minEvictableIdleTimeMillis;

	@Value("${spring.datasource.scm.db1.validationQuery}")
	private String validationQuery;

	@Value("${spring.datasource.scm.db1.testWhileIdle}")
	private boolean testWhileIdle;

	@Value("${spring.datasource.scm.db1.testOnBorrow}")
	private boolean testOnBorrow;

	@Value("${spring.datasource.scm.db1.testOnReturn}")
	private boolean testOnReturn;

	@Value("${spring.datasource.scm.db1.poolPreparedStatements}")
	private boolean poolPreparedStatements;

	@Value("${spring.datasource.scm.db1.maxPoolPreparedStatementPerConnectionSize}")
	private int maxPoolPreparedStatementPerConnectionSize;

	@Value("${spring.datasource.scm.db1.filters}")
	private String filters;

	@Value("{spring.datasource.scm.db1.connectionProperties}")
	private String connectionProperties;


	@Bean //声明其为Bean实例
	@Primary //在同样的DataSource中，首先使用被标注的DataSource
	public DataSource dataSource() {
		DruidDataSource datasource = new DruidDataSource();

		datasource.setUrl(this.dbUrl);
		datasource.setUsername(username);
		datasource.setPassword(password);
		datasource.setDriverClassName(driverClassName);

		//configuration
		datasource.setInitialSize(initialSize);
		datasource.setMinIdle(minIdle);
		datasource.setMaxActive(maxActive);
		datasource.setMaxWait(maxWait);
		datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
		datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
		datasource.setValidationQuery(validationQuery);
		datasource.setTestWhileIdle(testWhileIdle);
		datasource.setTestOnBorrow(testOnBorrow);
		datasource.setTestOnReturn(testOnReturn);
		datasource.setPoolPreparedStatements(poolPreparedStatements);
		datasource.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
		try {
			datasource.setFilters(filters);
		} catch (SQLException e) {
			logger.error("druid configuration initialization filter", e);
		}
		datasource.setConnectionProperties(connectionProperties);
		WallConfig wc = new WallConfig ();
        wc.setMultiStatementAllow(true);
        WallFilter wfilter = new WallFilter ();
        wfilter.setConfig(wc);
		datasource.setProxyFilters(Lists.newArrayList(wfilter));
		return datasource;
	}
	
	@Bean
	public ServletRegistrationBean druidServlet() {
		return new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean fb = new FilterRegistrationBean();
		fb.setFilter(new WebStatFilter());
		fb.addUrlPatterns("/*");
		fb.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return fb;
	}
	@Bean
	public DataSourceTransactionManager dataSourceTransactionManager() {
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(dataSource());
		return dataSourceTransactionManager;
	}
}
