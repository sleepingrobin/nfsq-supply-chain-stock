package com.nfsq.supply.chain.stock.common;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;

/**
 * @author suma
 * @date 2017/12/4 上午11:28
 */
public class ChainStockException extends RuntimeException {

	/**
	 * Description: 
	 */
	private static final long serialVersionUID = 1L;
	private String errorCode;

	public ChainStockException() {
		super();
	}

	public ChainStockException(StockResultCode stockResultCode){
        super(stockResultCode.getDesc());
        this.setErrorCode(stockResultCode.getCode());
    }
	
	public ChainStockException(String message) {
		super(message);
	}

	public ChainStockException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChainStockException(Throwable cause) {
		super(cause);
	}

	protected ChainStockException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ChainStockException(String errorCode, String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
