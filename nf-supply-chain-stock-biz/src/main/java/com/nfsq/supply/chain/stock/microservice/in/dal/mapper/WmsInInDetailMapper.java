package com.nfsq.supply.chain.stock.microservice.in.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.WmsInInDetailDTO;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsInInDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInInDetail record);

    int insertSelective(WmsInInDetail record);

    WmsInInDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInInDetail record);

    int updateByPrimaryKey(WmsInInDetail record);

    void insertBatch(@Param("list") List<WmsInInDetail> list, @Param("in")WmsInIn in);

    List<WmsInInDetailDTO> selectByQOPage(InInQO qo);

    List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(@Param("list") List<String> list);
}