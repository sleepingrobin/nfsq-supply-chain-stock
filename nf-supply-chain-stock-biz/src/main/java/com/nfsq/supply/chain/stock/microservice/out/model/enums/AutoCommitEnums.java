package com.nfsq.supply.chain.stock.microservice.out.model.enums;

import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/9/2 下午1:49
 */
@Getter
public enum AutoCommitEnums {

    COMMIT(1,"自动提交"),
    NOT_COMMIT(0,"不自动提交");

    private Integer code;

    private String desc;

    AutoCommitEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
