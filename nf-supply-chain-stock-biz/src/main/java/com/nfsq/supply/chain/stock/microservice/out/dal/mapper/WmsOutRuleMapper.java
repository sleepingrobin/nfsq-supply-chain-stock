package com.nfsq.supply.chain.stock.microservice.out.dal.mapper;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutRule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WmsOutRuleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsOutRule record);

    int insertSelective(WmsOutRule record);

    WmsOutRule selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsOutRule record);

    int updateByPrimaryKey(WmsOutRule record);

    WmsOutRule selectBySystemType(@Param("systemType") Integer systemType);
}