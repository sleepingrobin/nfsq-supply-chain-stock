package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.SapSyncStock;

public interface SapSyncStockMapper
{
    int insertSapStock(SapSyncStock sapSyncStock);
    
    int addSapStock(SapSyncStock sapSyncStock);
    
    int reduceSapStock(SapSyncStock sapSyncStock);
    
    int queryStock(SapSyncStock sapSyncStock);
    
    SapSyncStock querySapStock(@Param("skuNo") String skuNo, @Param("factoryNo") String factoryNo, @Param("repoNo") String repoNo);
}
