package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.InNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeByOutBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.api.service.InNoticeProviderApi;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xwcai
 * @date 2018/5/21 下午3:27
 */
@RestController
@Slf4j
public class InNoticeController implements InNoticeProviderApi {

    @Autowired
    InNoticeService inNoticeService;

    @Override
    public JsonResult<InNoticeCreateVO> createInNotice(@RequestBody InNoticeBean inNoticeBean) {
        JsonResult<InNoticeCreateVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.createInInNotice(inNoticeBean);
        } catch (ChainStockException e) {
            log.warn("创建入库通知单失败：");
            log.warn(e.getMessage(), e);
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(Boolean.FALSE);
        } catch (Exception e) {
            log.error("创建入库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<InNoticeCreateVO>> createInInNoticeByOut(@RequestBody InNoticeByOutBean inNoticeByOutBean) {
        JsonResult<List<InNoticeCreateVO>> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.createInInNoticeByOut(inNoticeByOutBean);
        } catch (ChainStockException e) {
            log.warn("根据出库通知单创建入库通知单失败：");
            log.warn(e.getMessage(), e);
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(Boolean.FALSE);
        } catch (Exception e) {
            log.error("根据出库通知单创建入库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
        }
        return jsonResult;
    }

    @Override
    public JsonResult<InNoticeManageListVO> queryByInInNoticeQo(@RequestBody InNoticeQO inNoticeQo) {
        JsonResult<InNoticeManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.queryByInInNoticeQo(inNoticeQo);
        } catch (Exception e) {
            log.error("管理页面查询入库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<InNoticeDetailListVO> getDetailByNoticeId(@RequestBody InNoticeQO qo) {
        JsonResult<InNoticeDetailListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.getDetailByNoticeId(qo);
        } catch (Exception e) {
            log.error("管理页面查询入库通知单详情失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<InNoticeManageVO> selectManageWithDetailByQO(@RequestBody InNoticeQO qo) {
        JsonResult<InNoticeManageVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.selectManageWithDetailByQO(qo);
        } catch (Exception e) {
            log.error("管理页面查询入库通知单包括详情失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> selectAllNOs(@RequestBody DummyBean bean) {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.selectAllNOs();
        } catch (Exception e) {
            log.error("管理页面查询入库通知单编号失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Set<String>> selectAllBatchNoStockIn(@RequestBody InNoticeQO inNoticeQO) {
        JsonResult<Set<String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.selectAllBatchNoStockIn(inNoticeQO);
        } catch (Exception e) {
            log.error("查询入库通知单批次失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(@RequestBody StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult = new JsonResult<>();
        try {
            jsonResult = inNoticeService.selectStockOutSkuInfoByMap(stockOutSkuInfoQO);
        } catch (Exception e) {
            log.error("根据入库通知单列表，查询所有出库产品失败，以通知单维度");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }
}
