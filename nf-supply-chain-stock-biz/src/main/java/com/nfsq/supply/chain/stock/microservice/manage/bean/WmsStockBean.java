package com.nfsq.supply.chain.stock.microservice.manage.bean;

import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;

/**
 * 出入库参数bean
 * @author  xfeng6
 * @created 2018年4月19日 下午2:25:53
 */
public class WmsStockBean extends WmsInventoryStock {
    
	/** 目标状态 **/
	private Integer targetStatus;
	
	/** 专属经销商ID **/
	private Long dealerId;
	/** 出入库详情单ID **/
	private Long orderDetailId;
	/** 0出库 1入库  2入库通知单 3BA售卖**/
	private Integer type;

	public Integer getTargetStatus() {
		return targetStatus;
	}

	public void setTargetStatus(Integer targetStatus) {
		this.targetStatus = targetStatus;
	}

	public Long getDealerId() {
		return dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	public Long getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
