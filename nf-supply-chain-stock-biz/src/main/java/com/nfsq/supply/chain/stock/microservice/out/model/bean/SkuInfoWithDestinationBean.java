package com.nfsq.supply.chain.stock.microservice.out.model.bean;

import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/21 上午11:14
 */
@Getter
@Setter
public class SkuInfoWithDestinationBean {

    private Long destinationId;

    private Integer outType;

    private List<SkuInfoBean> infoBeans;


}
