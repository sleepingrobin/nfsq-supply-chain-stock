package com.nfsq.supply.chain.stock.microservice.out.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 用于返回占用的sku数量的bean
 * @author xwcai
 * @date 2018/5/11 上午9:53
 */
@Getter
@Setter
public class SkuOccupyVo {

    /**
     * skuId
     */
    private Long skuId;

    /**
     * 占用的数量
     */
    private Long occpyNo;
}
