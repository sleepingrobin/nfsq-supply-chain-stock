package com.nfsq.supply.chain.stock;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.nfsq.supply.chain.dataAuthority.api.common.constants.DataAuthorityConstant;
import com.nfsq.supply.chain.datacenter.api.common.constants.DataCenterConstants;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.serviceregistry.EurekaAutoServiceRegistration;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableKafka
@EnableSwagger2
@EnableAsync
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = {DataCenterConstants.DATA_CENTER_API_PACKAGE_NAME,DataAuthorityConstant.BASE_PACKAGE})
@MapperScan(basePackages = "com.nfsq.supply.chain.**.mapper")
@EnableApolloConfig
public class NfSupplyChainStockBizApplication {
	public static void main(String[] args) {
		SpringApplication.run(NfSupplyChainStockBizApplication.class, args);
	}
	@Autowired
	private EurekaAutoServiceRegistration eurekaAutoServiceRegistration;
}
