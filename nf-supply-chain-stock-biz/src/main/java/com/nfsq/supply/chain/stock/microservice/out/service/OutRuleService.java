package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutRule;

/**
 * @author xwcai
 * @date 2018/9/2 下午1:46
 */
public interface OutRuleService {

    /**
     * 根据系统类型 查询出库规则
     *
     * @author xwcai
     * @date 2018/9/2 下午1:47
     * @param systemType 系统类型
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutRule
     */
    WmsOutRule selectBySystemType(Integer systemType) ;
}
