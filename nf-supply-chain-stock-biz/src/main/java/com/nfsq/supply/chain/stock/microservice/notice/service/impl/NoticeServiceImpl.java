package com.nfsq.supply.chain.stock.microservice.notice.service.impl;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.StockInTypeEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.BillNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.NoticeFinishQO;
import com.nfsq.supply.chain.stock.api.model.vo.BillCountVO;
import com.nfsq.supply.chain.stock.api.model.vo.BillVO;
import com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO;
import com.nfsq.supply.chain.stock.common.config.StockConfig;
import com.nfsq.supply.chain.stock.microservice.in.service.InInService;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.stock.microservice.notice.service.NoticeService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutOutService;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/19 下午3:05
 */
@Component
@Slf4j
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    OutNoticeService outNoticeService;

    @Autowired
    InNoticeService inNoticeService;

    @Autowired
    InInService inInService;

    @Autowired
    OutOutService outOutService;

    @Autowired
    WmsInventoryStockService wmsInventoryStockService;

    @Autowired
    StockConfig stockConfig;

    @Override
    public JsonResult<Boolean> checkNoticeFinish(NoticeFinishQO qo) {
        //判断如果是查询入库通知单
        if (qo.getQueryType().equals(InOutTypeEnums.IN_NOTICE_STOCK.getCode())) {
            return inNoticeService.checkInNoticeFinish(qo.getTaskId());
        }
        //判断如果是查询出库通知单
        else if (qo.getQueryType().equals(InOutTypeEnums.OUT_NOTICE_STOCK.getCode())) {
            return outNoticeService.checkOutNoticeFinish(qo.getTaskId());
        }
        log.error("查询通知单是否完成，输入的类型id不正确，任务ID：" + qo.getTaskId() + "类型：" + qo.getQueryType());
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        jsonResult.setMsg("输入的类型id不正确");
        jsonResult.setSuccess(false);
        jsonResult.setSuccess(false);
        return jsonResult;
    }

    @Override
    public JsonResult<BillVO> queryBillNOByQO(BillNoQO qo) {
        JsonResult<BillVO> jsonResult = new JsonResult<>();
        BillVO vo = new BillVO();
        //出库通知单
        List<BillDTO> outNoticeNos = outNoticeService.queryBillNOsByQO(qo.getTaskId());
        if (null != outNoticeNos && !outNoticeNos.isEmpty()) {
            vo.setOutNoticeNOs(outNoticeNos);
        }
        //出库单
        List<BillDTO> outOutNos = outOutService.queryNOsByTaskId(qo.getTaskId());
        if (null != outOutNos && !outOutNos.isEmpty()) {
            vo.setOutOutNOs(outOutNos);
        }
        //入库通知单
        List<BillDTO> inNoticeNos = inNoticeService.queryNOsByTaskId(qo.getTaskId());
        if (null != inNoticeNos && !inNoticeNos.isEmpty()) {
            vo.setInNoticeNOs(inNoticeNos);
        }
        //入库单
        List<BillDTO> inInNos = inInService.queryNOsByTaskId(qo.getTaskId());
        if (null != inInNos && !inInNos.isEmpty()) {
            vo.setInInNOs(inInNos);
        }
        jsonResult.setData(vo);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<BillCountVO> queryBillCountByQO(BillCountQO qo) {
        JsonResult<BillCountVO> jsonResult = new JsonResult<>();
        BillCountVO vo = new BillCountVO();

        if(SystemTypeEnum.MAQUILLAGE.getCode().equals(qo.getCurrentSystemType())){
            //已发货根据sku统计
            vo.setUnSendInfos(outNoticeService.selectSkuUnSnedCount());
            //未发货根据sku统计
            vo.setSendSkuInfos(outOutService.selectSkuSnedCount());
            //非限制库存数量 百分比 统计
            vo.setUnLimitStockAndProp(wmsInventoryStockService.selectUnLimitStockByRepoId(stockConfig.getDefaultRepoId()));
            //出入库通知单类型
            qo.setOutNoticeType(StockOutTypeEnum.SELLOUT.getType());
            qo.setInNoticeType(StockInTypeEnum.OUT_NOTICE.getType());
        }
        if(SystemTypeEnum.WATER.getCode().equals(qo.getCurrentSystemType()) || SystemTypeEnum.APPLE.getCode().equals(qo.getCurrentSystemType())){
            //出入库通知单类型
            qo.setOutNoticeType(StockOutTypeEnum.SAP_DELIVERY_OUT.getType());
            qo.setInNoticeType(StockInTypeEnum.SAP_DELIVERY_IN.getType());
        }
        if(SystemTypeEnum.RICE.getCode().equals(qo.getCurrentSystemType())){
            //出入库通知单类型
            qo.setOutNoticeType(StockOutTypeEnum.SELLOUT.getType());
            qo.setInNoticeType(StockInTypeEnum.OUT_NOTICE.getType());
        }
        //出库通知单数量 销售
        Integer outNoticeCount = outNoticeService.queryOutNoticeCount(qo);
        vo.setOutNoticeCount(outNoticeCount);
        //入库通知单数量  销售
        vo.setInNoticeCount(inNoticeService.queryInNoticeCount(qo));
        jsonResult.setSuccess(true);
        jsonResult.setData(vo);
        return jsonResult;
    }

    @Override
    public JsonResult<ProcessCountVO> queryProcessCount() {
        JsonResult<ProcessCountVO> jsonResult = new JsonResult<>();
        ProcessCountVO vo = new ProcessCountVO();
        //销售待办任务
        vo.setSaleTaskCount(outNoticeService.manageQueryProcessCount(StockOutTypeEnum.SELLOUT));
        //转储待办任务
        vo.setDumpTaskCount(outNoticeService.manageQueryProcessCount(StockOutTypeEnum.DUMPOUT));
        //转储收货任务
        vo.setDumpReceiveCount(inNoticeService.manageQueryProcessCount(StockInTypeEnum.DUMPIN));
        //sap待办任务
        vo.setSapTaskCount(outNoticeService.manageQueryProcessCount(StockOutTypeEnum.SAP_DELIVERY_OUT));
        jsonResult.setSuccess(true);
        jsonResult.setData(vo);
        return jsonResult;
    }
}
