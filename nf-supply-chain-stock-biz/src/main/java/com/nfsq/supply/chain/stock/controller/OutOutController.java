package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.StockOutBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageVO;
import com.nfsq.supply.chain.stock.api.service.OutOutProviderApi;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.out.service.OutOutService;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/21 下午3:16
 */
@RestController
@Slf4j
public class OutOutController implements OutOutProviderApi {

    @Autowired
    OutOutService outOutService;

    @Override
    public JsonResult<String> createOutOut(@RequestBody StockOutBean stockOutBean) {
        JsonResult<String> jsonResult = new JsonResult<>();
        try {
            jsonResult = outOutService.createOutOutWithSap(stockOutBean);
        } catch (ChainStockException e) {
            log.warn("创建出库单失败：");
            log.warn(e.getMessage(), e);
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(Boolean.FALSE);
        } catch (Exception e) {
            log.error("创建出库单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(Boolean.FALSE);
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutManageListVO> manageQueryByQo(@RequestBody OutOutPageQO outOutPageQO) {
        JsonResult<OutManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outOutService.manageQueryByQo(outOutPageQO);
        } catch (Exception e) {
            log.error("查询出库单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutManageVO> manageDetailQueryByQo(@RequestBody OutOutPageQO outOutPageQO) {
        JsonResult<OutManageVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outOutService.manageDetailQueryByQo(outOutPageQO);
        } catch (Exception e) {
            log.error("查询出库详情单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> queryAllOutNos(@RequestBody DummyBean dummyBean) {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outOutService.queryAllOutNos();
        } catch (Exception e) {
            log.error("查询所有出库单号失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<OutManageVO> qrCodeDetailQueryByOutNo(@RequestBody OutOutPageQO outOutPageQO) {
        JsonResult<OutManageVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outOutService.manageDetailQueryByQo(outOutPageQO);
        } catch (Exception e) {
            log.error("追溯查询出库详情单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }
}
