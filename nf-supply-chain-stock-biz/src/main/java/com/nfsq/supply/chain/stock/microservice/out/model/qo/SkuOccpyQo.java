package com.nfsq.supply.chain.stock.microservice.out.model.qo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 用于查询占用库存的bean
 * @author xwcai
 * @date 2018/5/11 上午9:55
 */
@Getter
@Setter
public class SkuOccpyQo {

    /**
     * skuId
     */
    private Long skuId;

    /**
     * 计算可用库存的状态
     */
    private List<Integer> statuses;

    /**
     * 仓库Id
     */
    private Long repoId;

    /**
     * 用途组：销售 转储
     */
    private Integer ruleGroup;

    /**
     * 用途类型：ka 专属 通用
     */
    private Integer ruleType;

    /**
     * 经销商Id
     */
    private List<Long> dealerId;
}
