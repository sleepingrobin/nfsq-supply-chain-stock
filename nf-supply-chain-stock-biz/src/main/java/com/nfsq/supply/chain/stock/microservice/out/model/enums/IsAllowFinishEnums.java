package com.nfsq.supply.chain.stock.microservice.out.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/9/25 下午3:33
 */
@Getter
@AllArgsConstructor
public enum IsAllowFinishEnums {
    ALLOW_FINISH(1,"允许自动提交"),
    NOT_ALLOW_FINISH(0,"不自动提交");

    private Integer code;

    private String desc;
}
