package com.nfsq.supply.chain.stock.microservice.in.service;

import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeByOutBean;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.enums.StockInTypeEnum;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.CancelInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.ModifyInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo;
import com.nfsq.supply.chain.utils.common.JsonResult;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:01
 */
public interface InNoticeService {

    /**
     * 创建入库通知单
     *
     * @param inNoticeBean 入库通知单创建bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/11 下午4:55
     */
    JsonResult<InNoticeCreateVO> createInInNotice(InNoticeBean inNoticeBean);

    /**
     * 根据出库通知单创建入库通知单
     *
     * @param bean 根据条件创建入库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/16 上午11:28
     */
    JsonResult<List<InNoticeCreateVO>> createInInNoticeByOut(InNoticeByOutBean bean);

    /**
     * 根据qo查询入库通知单列表
     *
     * @param inNoticeQo 入库通知单查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:03
     */
    JsonResult<InNoticeManageListVO> queryByInInNoticeQo(InNoticeQO inNoticeQo);

    /**
     * 取消入库通知单
     *
     * @param cancelInNoticeBean 取消bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/12 下午7:16
     */
    JsonResult<Boolean> cancelInInNotice(CancelInNoticeBean cancelInNoticeBean);

    /**
     * 根据入库通知单单号查询
     *
     * @param noticeId 通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo
     * @author xwcai
     * @date 2018/4/14 上午11:29
     */
    InNoticeVo getByInInNoticeId(Long noticeId);

    /**
     * 修改入库通知单
     *
     * @param modifyBean 修改bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/12 上午11:27
     */
    JsonResult<Boolean> updateInInNotice(ModifyInNoticeBean modifyBean);

    /**
     * 判断入库通知单状态 判断是否存在 是否已经入库
     *
     * @param inNoticeVo 入库通知单包括详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午3:41
     */
    JsonResult<Boolean> checkInInNotice(InNoticeVo inNoticeVo);

    /**
     * 根据入库通知单Id获取入库通知详情
     *
     * @param noticeId 通知单id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List   <   com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail>>
     * @author xwcai
     * @date 2018/4/14 下午5:17
     */
    JsonResult<InNoticeDetailListVO> getDetailByNoticeId(InNoticeQO qo);

    /**
     * 批量更新数量
     *
     * @param detailList 更新的list
     * @param inBean     入库的bean
     *
     * @return void
     * @author xwcai
     * @date 2018/4/18 下午6:57
     */
    void updateInNoticeDetailWithRealAmount(List<WmsInInNoticeDetail> detailList, StockInBean inBean);

    /**
     * 根据任务Id 查询未完成的入库通知单
     *
     * @param confirmBO 确认收获的bean
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo>
     * @author xwcai
     * @date 2018/5/21 下午3:53
     */
    List<InNoticeVo> getDetailByTaskIdAndRepoId(ConfirmBO confirmBO);

    /**
     * 查询单号 通过taskId
     *
     * @param taskId 任务id
     *
     * @return java.util.List<java.lang.String>
     * @author xwcai
     * @date 2018/5/31 上午9:59
     */
    List<BillDTO> queryNOsByTaskId(Long taskId);

    /**
     * 查询是否所有入库通知单全部完成
     *
     * @param taskId 任务Id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/6/19 下午3:06
     */
    JsonResult<Boolean> checkInNoticeFinish(Long taskId);

    /**
     * 根据任务单号查询 所有未完成的入库通知单 并且不是取消
     *
     * @param taskId 任务id
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice>
     * @author xwcai
     * @date 2018/5/21 下午5:26
     */
    List<WmsInInNotice> getInUnFinishNotice(Long taskId);

    /**
     * 根据任务单号查询 入库通知单 并且不是取消
     *
     * @param taskId 任务id
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice>
     * @author xwcai
     * @date 2018/5/21 下午5:26
     */
    List<WmsInInNotice> getInNoticeByTaskId(Long taskId);

    /**
     * 根据id 查询入库通知单
     *
     * @param id 入库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice
     * @author xwcai
     * @date 2018/6/28 下午2:46
     */
    WmsInInNotice getInNoticeById(Long id);

    /**
     * 根据qo(入库通知单单号) 管理页面查询入库通知单包括详情
     *
     * @author xwcai
     * @date 2018/7/21 上午11:25
     * @param qo 查询qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeManageVO>
     */
    JsonResult<InNoticeManageVO> selectManageWithDetailByQO(InNoticeQO qo) ;

    /**
     * 查询所有的入库通知单编号
     *
     * @author xwcai
     * @date 2018/7/21 下午2:04
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<java.lang.String>>
     */
    JsonResult<List<String>> selectAllNOs() ;

    /**
     * 查询入库通知单 数量
     *
     * @author xwcai
     * @date 2018/7/23 上午11:01
     * @param qo 查询qo
     * @return java.lang.Integer
     */
    Integer queryInNoticeCount(BillCountQO qo) ;

    /**
     * 查询未完成收货任务
     *
     * @author xwcai
     * @date 2018/7/26 下午3:47
     * @param
     * @return java.lang.Integer
     */
    Integer manageQueryProcessCount(StockInTypeEnum inTypeEnum) ;

    /**
     * 根据入库通知单号，查询入库批次
     *
     * @author xwcai
     * @date 2018/9/13 下午2:40
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.Set<java.lang.String>>
     */
    JsonResult<Set<String>> selectAllBatchNoStockIn(InNoticeQO qo);

    /**
     * 根据入库通知单单号list  查询对应所有的出库产品信息
     *
     * @author xwcai
     * @date 2018/9/4 下午4:00
     * @param stockOutSkuInfoQO 查询的qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO stockOutSkuInfoQO) ;

}
