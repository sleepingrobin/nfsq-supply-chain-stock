package com.nfsq.supply.chain.stock.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/21 上午10:02
 */
@Configuration
@Getter
@Setter
public class StockConfig {

    @Value("${scm.default.repo}")
    private Long defaultRepoId;

    @Value("#{'${scm.unlimit.stock.sku}'.split(',')}")
    private List<Long> unLimitSkuIds;

    @Value("${sap.sync.default.factoryno}")
    private String factoryNo;
    
    @Value("${sap.sync.default.repono}")
    private String repoNo;
}
