package com.nfsq.supply.chain.stock.microservice.manage.persistservice;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RiceSkuEnum;
import com.nfsq.supply.chain.stock.api.model.enums.RuleGroupEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleTypeEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BatchNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.FanAndStoreStockInfoVO;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStockDetail;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.WmsInventoryStockDetailMapper;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.WmsInventoryStockMapper;
import com.nfsq.supply.chain.stock.microservice.manage.enums.OperStatusEnums;
import com.nfsq.supply.chain.stock.microservice.out.service.impl.AsyncServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 库存表操作底层
 * @author  xfeng6
 * @created 2018年4月19日 下午3:16:45
 */
@Component
public class WmsStockPersistService {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WmsInventoryStockMapper wmsInventoryStockMapper;

    @Autowired
    private WmsInventoryStockDetailMapper wmsInventoryStockDetailMapper;
    
    @Autowired
    private DataCenterService dataCenterService;
    
    @Autowired
    private AsyncServiceImpl asyncService;
      
    public List<WmsInventoryStockDTO> queryStock(WmsStockQO stock){
        return wmsInventoryStockMapper.queryStockPage(stock);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void updateStock(WmsStockBean wmsStockBean) {
        ((WmsStockPersistService)AopContext.currentProxy()).reduceStock(wmsStockBean);
        ((WmsStockPersistService)AopContext.currentProxy()).addStock(wmsStockBean);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void addStock(WmsStockBean wmsStockBean) {
        //所有的入库用途都变为通用
        wmsStockBean.setRuleType(RuleTypeEnums.COMMON.getCode());
        WmsInventoryStock stock = new WmsInventoryStock();
        BeanUtils.copyProperties(wmsStockBean, stock);
        stock.setStatus(wmsStockBean.getTargetStatus());
        int re = wmsInventoryStockMapper.queryStock(stock);
        if(re>1){
            throw new ChainStockException("多条库存记录，增加库存失败");
        }
        if(re == 1){
            int j = wmsInventoryStockMapper.addStock(wmsStockBean);
            if(j == 0){
                throw new ChainStockException("增加库存失败");
            }
        }else{
            int k = wmsInventoryStockMapper.insert(stock);
            if(k == 0){
                throw new ChainStockException("插入初始库存失败");
            }       
        }
        wmsStockBean.setStatus(wmsStockBean.getTargetStatus());
        addStockDetail(wmsStockBean);
        if (wmsStockBean.getRuleGroup() == RuleGroupEnums.CURRENCY.getCode()
            && wmsStockBean.getRuleType() == RuleTypeEnums.COMMON.getCode())
        {
           //删除通用库存缓存
            asyncService.deleteCommonStockRedis(wmsStockBean);
        }
        else
        {
          //删除规则库存缓存
            asyncService.deleteRuleStockRedis(wmsStockBean);
        }     
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void reduceStock(WmsStockBean bean) {
        try
        {
            //先做个深拷贝，避免影响原始bean
            WmsStockBean wmsStockBean = new WmsStockBean();
            BeanUtils.copyProperties(bean, wmsStockBean);
            Long stock = wmsInventoryStockMapper.checkStock(wmsStockBean);
            boolean isEnough = stock>=wmsStockBean.getNums();
            if(isEnough){
                if(RuleTypeEnums.EXCLUSIVE_DISTRIBUTOR.getCode().equals(wmsStockBean.getRuleType())){
                    boolean isExist = wmsInventoryStockMapper.checkRuleStock(wmsStockBean)>0;
                    if(!isExist){
                        reduceCommonStock(wmsStockBean);
                        //删除通用库存缓存
                        asyncService.deleteCommonStockRedis(wmsStockBean);
                        return;
                    }
                }
                //通用库存
                if(bean.getRuleGroup()==RuleGroupEnums.CURRENCY.getCode()&&bean.getRuleType()==RuleTypeEnums.COMMON.getCode())
                {
                    reduceCommonStock(wmsStockBean);
                    //删除通用库存缓存
                    asyncService.deleteCommonStockRedis(wmsStockBean);
                }
                //规则库存
                else
                {
                    //先扣减规则库存
                    wmsInventoryStockMapper.reduceRuleStock(wmsStockBean);
                    int sourceNum = wmsStockBean.getNums();
                    int remainNum = wmsInventoryStockMapper.queryRemainNum(wmsStockBean);
                    if(remainNum<0){
                        wmsStockBean.setNums(0);
                        //抹平负数，使库存归零
                        wmsInventoryStockMapper.updateStock(wmsStockBean);
                        //设置实际扣减库存
                        wmsStockBean.setNums(sourceNum + remainNum);
                        //加入详情日志
                        addStockDetail(wmsStockBean);
                        //专属库存不够，再扣减通用库存
                        wmsStockBean.setNums(Math.abs(remainNum));                        
                        reduceCommonStock(wmsStockBean);
                        //删除通用库存缓存
                        asyncService.deleteCommonStockRedis(wmsStockBean);
                    }
                    else{
                        addStockDetail(wmsStockBean);                       
                    }     
                    //删除规则库存缓存
                    asyncService.deleteRuleStockRedis(bean);
                }   
            }else{
                throw new ChainStockException("库存不足");
            }       
        }
        catch(Exception e)
        {
            Sku sku=dataCenterService.findBySkuId(bean.getSkuId());
            throw new ChainStockException("物料 "+(sku==null?"":sku.getSkuName())+" 库存不足");
        }   
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void specifyReduceStock(WmsStockBean bean) {
        //获取指定物料的物料编号
        String bagSkuNo=dataCenterService.getRiceSkuNo(bean.getSkuId());
        Long bagSkuId=bean.getSkuId();
        RiceSkuEnum riceSkuEnum=RiceSkuEnum.getEnumByBag(bagSkuNo);
        String boxSkuNo=riceSkuEnum.getBox();
        Long boxSkuId=dataCenterService.getRiceSkuId(boxSkuNo);
        int rate=riceSkuEnum.getRate();
        List<Long> skuIdList=new ArrayList<>();
        skuIdList.add(bagSkuId);   
        skuIdList.add(boxSkuId);
        //获取大米箱包库存数据并锁住
        List<WmsInventoryStock> stocks=wmsInventoryStockMapper.queryStocks(bean, skuIdList);
        int totalNum=0;
        int bagNum=0;
        //计算总包数
        for(WmsInventoryStock stock:stocks)
        {
            if(stock.getSkuId().equals(boxSkuId))
            {
                totalNum=totalNum+stock.getNums()*rate;
            }
            else
            {
                bagNum=stock.getNums();
                totalNum=totalNum+stock.getNums();
            }
        }
        //判断库存是否足够
        if(totalNum>=bean.getNums().intValue())
        {
            try
            {
                //判断是否需要拆箱
                if(bagNum<bean.getNums().intValue())
                {
                    //不够的包数
                    int remianNum=bean.getNums().intValue()-bagNum;
                    //拆箱数量
                    int unpackNum=remianNum/rate+1;
                    //拆箱完补充的包数
                    int addNum=unpackNum*rate-bean.getNums().intValue();
                    //扣减箱数
                    WmsStockBean boxWmsStockBean = new WmsStockBean();
                    BeanUtils.copyProperties(bean, boxWmsStockBean);  
                    boxWmsStockBean.setNums(unpackNum);
                    boxWmsStockBean.setSkuId(boxSkuId);
                    reduceCommonStock(boxWmsStockBean);
                    //先做个深拷贝，避免影响原始bean
                    WmsStockBean wmsStockBean = new WmsStockBean();
                    BeanUtils.copyProperties(bean, wmsStockBean);         
                    wmsStockBean.setTargetStatus(wmsStockBean.getStatus());
                    wmsStockBean.setStatus(null);
                    wmsStockBean.setNums(addNum);
                    //补充拆箱的包数
                    ((WmsStockPersistService)AopContext.currentProxy()).addStock(wmsStockBean);      
                }
                else
                {
                    //扣减需要的包数
                    reduceCommonStock(bean);
                }     
            }
            catch(Exception e)
            {
                logger.debug("大米库存变更失败,e={}",e);
                Sku sku=dataCenterService.findBySkuId(bean.getSkuId());
                throw new ChainStockException("物料 "+(sku==null?"":sku.getSkuName())+"扣减库存失败");
            }       
        }
        else
        {
            Sku sku=dataCenterService.findBySkuId(bean.getSkuId());
            throw new ChainStockException("物料 "+(sku==null?"":sku.getSkuName())+" 库存不足");
        }
    }

    /**
     * 
     * 判断是否是米业有箱包转换操作变更
     *
     * @author sundi
     * @param wmsStockBean
     * @return
     */
    public boolean isSpecify(WmsStockBean wmsStockBean)
    {
        Integer systemType = wmsStockBean.getSystemType();
        //米业且是指定物料且非工厂的库存扣减时需要走特殊扣减逻辑
        if (systemType == SystemTypeEnum.RICE.getCode()
            && (wmsStockBean.getType() == InOutTypeEnums.MACHINE_REPLENISH_STOCK.getCode()
                || wmsStockBean.getType() == InOutTypeEnums.MACHINE_REJECTED_STOCK.getCode()))
         {       
            String skuNo=dataCenterService.getRiceSkuNo(wmsStockBean.getSkuId());
           if(!StringUtils.isEmpty(skuNo))
           {
               return true;
           }
           return false;
         }
        return false;    
    }
    
    private void reduceCommonStock(WmsStockBean wmsStockBean) {
        wmsStockBean.setRuleType(RuleTypeEnums.COMMON.getCode());
        wmsStockBean.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
        int commonRe = wmsInventoryStockMapper.reduceCommonStock(wmsStockBean);
        if(commonRe != 1){
            throw new ChainStockException("扣减通用库存失败");
        }else{
            addStockDetail(wmsStockBean);
        }
    }

    public Long queryCommonStock(Long skuId, Integer status, Long repoId) {
        return wmsInventoryStockMapper.queryCommonStock(skuId,status,repoId);
    }
    
    public Long queryRuleStock(Long skuId, Integer status, Long repoId,Integer ruleType,Integer ruleGroup, List<Long> dealerIds) {
        return wmsInventoryStockMapper.queryRuleStock(skuId,status,repoId,ruleType,ruleGroup,dealerIds);
    }

    public List<Long> queryDealerIdsByStockIds(Long skuId, List<Integer> statuses, Long repoId, Integer ruleGroup, Integer ruleType, List<Long> dealerIds, Long owner, Integer ownerType) {
        return wmsInventoryStockMapper.queryDealerIdsByStockIds(skuId,statuses,repoId,ruleGroup,ruleType,dealerIds,owner,ownerType);
    }

    public List<String> queryBatchNos(BatchNoQO qo) {
        return wmsInventoryStockMapper.queryBatchNos(qo);
    }

    public List<WmsInventoryStock> queryStoreStock(Long repoId, List<Integer> statuses, Long storeId) {
        return wmsInventoryStockMapper.queryStoreStock(repoId,statuses,storeId);
    }

    public void addStockDetail(WmsStockBean wmsStockBean) {
        Long stockId = wmsInventoryStockMapper.queryStockId(wmsStockBean);
        WmsInventoryStockDetail record = new WmsInventoryStockDetail();
        BeanUtils.copyProperties(wmsStockBean, record);
        record.setStockId(stockId);
        record.setOperStatus(OperStatusEnums.NORMAL.getCode());
        int re = wmsInventoryStockDetailMapper.insertSelective(record);
        if(re != 1){
            throw new ChainStockException("插入库存详情日志失败");
        }
    }

    
    /**
     * 根据仓库id 查询非限制的sku 数量 百分比
     *
     * @author xwcai
     * @date 2018/7/1 下午3:39
     * @param repoId
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO>
     */
    public List<SkuManageBO> selectUnLimitStockByRepoId(Long repoId){
        List<SkuManageBO> boList =  wmsInventoryStockMapper.selectUnLimitStockByRepoId(repoId);
        //去除0
        boList.forEach(bo -> bo.setSkuNo(bo.getSkuNo().replaceAll("^(0+)", "")));
        return boList;
    }

    /**
     * 
     * 根据物料id,仓库id和状态查询sku总数量列表
     *
     * @author sundi
     * @param skuId
     * @param repoId
     * @param status
     * @return
     */
    public List<SkuNumsBO> selectUnlimitStockByCondition(Long skuId,Long repoId,Integer status,String batchNo,Long owner,Integer ownerType){
        return wmsInventoryStockMapper.selectUnlimitStockByCondition(skuId, repoId, status,batchNo,owner,ownerType);
    }

    /**
     * @param wmsStockQO
     * @return
     */
    public List<FanAndStoreStockInfoVO> queryFranAndStoreStockInfo(WmsStockQO wmsStockQO) {
        List<FanAndStoreStockInfoVO> fanAndStoreStockInfoVOS = wmsInventoryStockMapper.queryFranAndStoreStoreInfo(wmsStockQO);
        return fanAndStoreStockInfoVOS;
    }

}
