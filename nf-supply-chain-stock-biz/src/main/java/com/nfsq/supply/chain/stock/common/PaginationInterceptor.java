package com.nfsq.supply.chain.stock.common;

import com.google.common.collect.Lists;
import com.nfsq.supply.chain.dataAuthority.api.model.enums.RoleTypeEnum;
import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.common.config.SpringContextHolder;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.utils.common.PageParameter;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityUtils;

import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * created by tom on 15/10/08 通过拦截<code>StatementHandler</code>的
 * <code>prepare</code>方法，重写sql语句实现物理分页。 老规矩，签名里要拦截的类型只能是接口。
 *
 */
@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class,Integer.class }) })
public class PaginationInterceptor implements Interceptor {
    private static final Logger logger = LoggerFactory.getLogger(PaginationInterceptor.class);
	private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
	private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
	private static final ReflectorFactory DEFAULT_REFLECTOR_FACTORY = new DefaultReflectorFactory();
	private static final String DEFAULT_DIALECT = "mysql";
	private static final String DEFAULT_PAGE_SQLID = ".*Page$";
	private String dialect;
	private String pageSqlId;

    @Override
	public Object intercept(Invocation invocation) throws Throwable {
		StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
		MetaObject metaStatementHandler = MetaObject.forObject(statementHandler, DEFAULT_OBJECT_FACTORY,
				DEFAULT_OBJECT_WRAPPER_FACTORY,DEFAULT_REFLECTOR_FACTORY);
		// 分离代理对象链(由于目标类可能被多个拦截器拦截，从而形成多次代理，通过下面的两次循环
		// 可以分离出最原始的的目标类)
		while (metaStatementHandler.hasGetter("h")) {
			Object object = metaStatementHandler.getValue("h");
			metaStatementHandler = MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY,DEFAULT_REFLECTOR_FACTORY);
		}
		// 分离最后一个代理对象的目标类
		while (metaStatementHandler.hasGetter("target")) {
			Object object = metaStatementHandler.getValue("target");
			metaStatementHandler = MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY,DEFAULT_REFLECTOR_FACTORY);
		}
		if (null == dialect || "".equals(dialect)) {
			logger.warn("Property dialect is not setted,use default 'mysql' ");
			dialect = DEFAULT_DIALECT;
		}
		if (null == pageSqlId || "".equals(pageSqlId)) {
			logger.warn("Property pageSqlId is not setted,use default '.*Page$' ");
			pageSqlId = DEFAULT_PAGE_SQLID;
		}
		MappedStatement mappedStatement = (MappedStatement) metaStatementHandler.getValue("delegate.mappedStatement");
		 // ******************************数据权限相关处理****************************//
        //获取数据权限注解
        DataAuthority dataAuthority=DataAuthorityUtils.getDataAuthority(metaStatementHandler);
        if(null!=dataAuthority)
        {
          //获取数据权限相关信息
            DataAuthorityBean dataAuthorityBean=DataAuthorityUtils.getCurrentDataAuthorityBean();
            //信息为空则不处理
            if (null != dataAuthorityBean && !StringUtils.isEmpty(dataAuthorityBean.getLoginId())
                && null != dataAuthorityBean.getCurrentSystemType())
            {
                Map<String,List<String>> map=prepareAuthorityData(dataAuthorityBean);
                if(CollectionUtils.isEmpty(map))
                {
                    DataAuthorityUtils.reBuildNoneSql(metaStatementHandler,dataAuthority);
                    return invocation.proceed();
                }
                //重写sql
                Long time1=System.currentTimeMillis();
                DataAuthorityUtils.newReBuildWhere(metaStatementHandler, dataAuthority, map);
                Long time2=System.currentTimeMillis();
                logger.info("数据权限处理耗时：{}ms,sqlid={}" , (time2 - time1),mappedStatement.getId()); 
            }
            else
            {
                logger.info("数据权限处理信息不全：dataAuthorityBean={}",dataAuthorityBean);  
                DataAuthorityUtils.reBuildNoneSql(metaStatementHandler,dataAuthority);
                return invocation.proceed();
            }
        }
         // ******************************数据权限相关处理****************************//
        
		PageParameter page = null;
		if(mappedStatement.getId().matches(pageSqlId)){
			Object type = metaStatementHandler.getValue("delegate.boundSql.parameterObject");
			if (type == null || !PageParameter.class.isInstance(type)) {
				throw new RuntimeException(mappedStatement.getId() + ":参数为空or参数不是PageParameter的子类！");
			}
			page = (PageParameter) type;
		}
		
		// 只重写需要分页的sql语句。通过MappedStatement的ID匹配，默认重写以Page结尾的
		// MappedStatement的sql
		if (page != null && page.getCurrentPage() > 0
				&& page.getPageSize() > 0) {
			BoundSql boundSql = (BoundSql) metaStatementHandler.getValue("delegate.boundSql");
			// 分页参数作为参数对象parameterObject的一个属性
			String sql = boundSql.getSql();
			// 重写sql
			String pageSql = buildPageSql(sql, page, dialect);
			metaStatementHandler.setValue("delegate.boundSql.sql", pageSql);
			// 采用物理分页后，就不需要mybatis的内存分页了，所以重置下面的两个参数
			metaStatementHandler.setValue("delegate.rowBounds.offset", RowBounds.NO_ROW_OFFSET);
			metaStatementHandler.setValue("delegate.rowBounds.limit", RowBounds.NO_ROW_LIMIT);
			Connection connection = (Connection) invocation.getArgs()[0];
			// 重设分页参数里的总页数等
			setPageParameter(sql, connection, mappedStatement, boundSql, page);
		}
		// 将执行权交给下一个拦截器
		return invocation.proceed();
	}

	private Map<String,List<String>> prepareAuthorityData(DataAuthorityBean dataAuthorityBean)
    {
        Date date1=new Date();
        Map<String,List<String>> map=new HashMap<>();
        RedisManagerService redisManagerService=(RedisManagerService)SpringContextHolder.getBean("redisManagerService");
        if(null!=redisManagerService)
        {
            Map<String,List<String>> authorityMap =redisManagerService.getDataAuthorityValues(dataAuthorityBean.getLoginId()); 
            if (CollectionUtils.isEmpty(authorityMap)
                || CollectionUtils.isEmpty(authorityMap.get(RoleTypeEnum.SYSTEMTYPE.getTitle())))
            {
                logger.info("未获取{}的数据权限",dataAuthorityBean.getLoginId()); 
                return map;
            }
            if(authorityMap.get(RoleTypeEnum.SYSTEMTYPE.getTitle()).contains(dataAuthorityBean.getCurrentSystemType().toString()))
            {
                //获取当前系统权限
                map.put(RoleTypeEnum.SYSTEMTYPE.getTitle(), Lists.newArrayList(String.valueOf(dataAuthorityBean.getCurrentSystemType())));
                //获取当前系统权限下的仓库权限
                if(!CollectionUtils.isEmpty(authorityMap.get(SystemTypeEnum.getDesc(dataAuthorityBean.getCurrentSystemType()))))
                {
                    map.put(RoleTypeEnum.REPOTYPE.getTitle(), authorityMap.get(SystemTypeEnum.getDesc(dataAuthorityBean.getCurrentSystemType())));
                }
            }   
        }   
        Date date2=new Date();
        logger.info("数据权限预处理耗时：{}ms,dataAuthorityMap={}" ,(date2.getTime() - date1.getTime()),map);   
        return map;
    }
	
	private String buildPageSql(String sql, PageParameter page, String dialect) {
		if (page != null) {
			StringBuilder pageSql = new StringBuilder();
			if ("mysql".equals(dialect)) {
				pageSql = buildPageSqlForMysql(sql, page);
			} else if ("oracle".equals(dialect)) {
				pageSql = buildPageSqlForOracle(sql, page);
			} else {
				return sql;
			}
			return pageSql.toString();
		} else {
			return sql;
		}
	}

	@Override
	public Object plugin(Object target) {
		// 当目标类是StatementHandler类型时，才包装目标类，否者直接返回目标本身,减少目标被代理的次数
		if (target instanceof StatementHandler) {
			return Plugin.wrap(target, this);
		} else {
			return target;
		}
	}

	@Override
	public void setProperties(Properties properties) {
		// To change body of implemented methods use File | Settings | File
		// Templates.
	}

	public StringBuilder buildPageSqlForMysql(String sql, PageParameter page) {
		StringBuilder pageSql = new StringBuilder(100);
		String beginrow = String.valueOf((page.getCurrentPage() - 1) * page.getPageSize());
		pageSql.append(sql);
		pageSql.append(" limit " + beginrow + "," + page.getPageSize());
		return pageSql;
	}

	public StringBuilder buildPageSqlForOracle(String sql, PageParameter page) {
		StringBuilder pageSql = new StringBuilder(100);
		String beginrow = String.valueOf((page.getCurrentPage() - 1) * page.getPageSize());
		String endrow = String.valueOf(page.getCurrentPage() * page.getPageSize());
		pageSql.append("select * from ( select temp.*, rownum row_id from ( ");
		pageSql.append(sql);
		pageSql.append(" ) temp where rownum <= ").append(endrow);
		pageSql.append(") where row_id > ").append(beginrow);
		return pageSql;
	}

	/**
	 * 从数据库里查询总的记录数并计算总页数，回写进分页参数<code>PageParameter</code>,这样调用 者就可用通过 分页参数
	 * <code>PageParameter</code>获得相关信息。
	 * 
	 * @param sql
	 * @param connection
	 * @param mappedStatement
	 * @param boundSql
	 * @param page
	 * @throws SQLException 
	 */
	private void setPageParameter(String sql, Connection connection, MappedStatement mappedStatement, BoundSql boundSql,
			PageParameter page) throws SQLException {
		// 记录总记录数
		String countSql = "select count(0) as total from (" + sql + ")  tt";
		PreparedStatement countStmt = null;
		ResultSet rs = null;
		try {
			countStmt = connection.prepareStatement(countSql);
			setParameters(countStmt, mappedStatement, boundSql);
			rs = countStmt.executeQuery();
			int totalCount = 0;
			if (rs.next()) {
				totalCount = rs.getInt(1);
			}
			page.setTotalElements(totalCount);
			int totalPage = totalCount / page.getPageSize() + ((totalCount % page.getPageSize() == 0) ? 0 : 1);
			page.setTotalPage(totalPage);
		} catch (SQLException e) {
			logger.error("Ignore this exception", e);
			throw e;
		} finally {
			try {
				if(rs != null){
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Ignore this exception", e);
			}
			try {
				if(countStmt != null){
					countStmt.close();
				}
			} catch (SQLException e) {
				logger.error("Ignore this exception", e);
			}
		}
	}

	/**
	 * 对SQL参数(?)设值
	 * 
	 * @param ps
	 * @param mappedStatement
	 * @param boundSql
	 * @param parameterObject
	 * @throws SQLException
	 */
	private void setParameters(PreparedStatement ps, MappedStatement mappedStatement, BoundSql boundSql)
			throws SQLException {

		ParameterHandler parameterHandler = mappedStatement.getConfiguration().newParameterHandler(mappedStatement,
				boundSql.getParameterObject(), boundSql);
		parameterHandler.setParameters(ps);

	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public void setPageSqlId(String pageSqlId) {
		this.pageSqlId = pageSqlId;
	}

}