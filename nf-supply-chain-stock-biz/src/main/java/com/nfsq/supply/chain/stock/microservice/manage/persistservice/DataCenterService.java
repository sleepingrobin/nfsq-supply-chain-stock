package com.nfsq.supply.chain.stock.microservice.manage.persistservice;

import com.nfsq.supply.chain.datacenter.api.*;
import com.nfsq.supply.chain.datacenter.api.parameter.*;
import com.nfsq.supply.chain.datacenter.api.response.*;
import com.nfsq.supply.chain.stock.api.model.enums.RiceSkuEnum;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.out.service.AsyncService;
import com.nfsq.supply.chain.datacenter.api.common.enums.MachineTypeEnum;
import com.nfsq.supply.chain.datacenter.api.common.enums.OwnerTypeEnum;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * com.nfsq.supply.chain.dispatch.center.web.service
 *
 * @Author : Gaohf
 * @Description :
 * @Date : 2018/5/10
 */
@Service
public class DataCenterService implements InitializingBean
{
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private SkuApi skuApi;
    
    @Autowired
    private FranchiserApi franchiserApi;
    
    @Autowired
    private RepoAPI repoAPI;
    
    @Autowired
    private StoreApi storeApi;
    
    @Autowired
    private CompanyApi companyApi;
    
    @Autowired
    private AsyncService asyncService;
    
    private Map<Long,String> riceSkuMap=new HashMap<Long, String>();
    
    /**
     * 
     * 根据owner和类型获取ownerName
     *
     * @author sundi
     * @param owner
     * @param type
     * @return
     */
    public String findOwnerNameByIdAndType(Long owner, Integer type)
    {
        String ownerName = null;
        try
        {
            if (type == OwnerTypeEnum.COMPANY.getCode())
            {
                ownerName = this.findCompanyById(owner).getCompanyName();
            }
            else if (type == OwnerTypeEnum.FRANCHISER.getCode())
            {
                ownerName = this.findFranchiserByScmId(owner).getName();
            }
            else if (type == OwnerTypeEnum.STORE.getCode())
            {
                ownerName = this.findStoreByScmId(owner).getName();
            }
        }
        catch (ChainStockException e)
        {
            logger.info("查询ownerName失败");
        }
        
        return ownerName;
    }
    
    /**
     * 
     * 通过公司id获取公司信息
     *
     * @author sundi
     * @param companyId
     * @return
     */
    public Company findCompanyById(Long companyId)
    {
        CompanyParam companyParam = new CompanyParam();
        companyParam.setId(companyId);
        JsonResult<Company> result = companyApi.getCompanyDetail(companyParam);
        if (result != null && result.isSuccess()&&null!=result.getData())
        {
            return result.getData();
        }
        logger.info("查询公司失败：companyId={}",companyId);
        throw new ChainStockException(StockResultCode.COMPANY_NOT_FOUND);
    }
    
    /**
     * 
     * 通过经销商id获取经销商信息
     *
     * @author sundi
     * @param franchiserId
     * @return
     */
    public NcpFranchiser findFranchiserById(Long franchiserId)
    {
        JsonResult<NcpFranchiser> result = franchiserApi.getByFranchiserId(franchiserId);
        if (result != null && result.isSuccess() && null != result.getData())
        {
            return result.getData();
        }
        logger.info("查询经销商失败：franchiserId={}",franchiserId);
        throw new ChainStockException(StockResultCode.FRANCHISER_NOT_FOUND);
    }

    /**
     *
     * 通过SCM经销商id获取经销商信息
     *
     * @author sundi
     * @param franchiserId
     * @return
     */
    public SystemBaseFranchiserNew findFranchiserByScmId(Long franchiserId)
    {
        JsonResult<SystemBaseFranchiserNew> result = franchiserApi.getByScmFranchiserId(franchiserId);
        if (result != null && result.isSuccess() && null != result.getData())
        {
            return result.getData();
        }
        logger.info("查询经销商失败：franchiserId={}",franchiserId);
        throw new ChainStockException(StockResultCode.FRANCHISER_NOT_FOUND);
    }
    
    /**
     * 
     * 通过门店id获取门店信息
     *
     * @author sundi
     * @param storeId
     * @return
     */
    public NcpStore findStoreById(Long storeId)
    {
        JsonResult<NcpStore> result = storeApi.getByStoreId(storeId);
        if (result != null && result.isSuccess() && null != result.getData())
        {
            return result.getData();
        }
        logger.info("查询门店失败：storeId={}",storeId);
        throw new ChainStockException(StockResultCode.STORE_NOT_FOUND);
    }

    /**
     *
     * 通过Scm门店id获取门店信息
     *
     * @author sundi
     * @param storeId
     * @return
     */
    public NcpStore findStoreByScmId(Long storeId)
    {
        JsonResult<NcpStore> result = storeApi.getNcpStoreByScmStoreId(storeId);
        if (result != null && result.isSuccess() && null != result.getData())
        {
            return result.getData();
        }
        logger.info("查询门店失败：storeId={}",storeId);
        throw new ChainStockException(StockResultCode.STORE_NOT_FOUND);
    }
    
    /**
     * 
     * 通过门店id获取门店信息
     *
     * @author sundi
     * @param franchiserId
     * @return
     */
    public List<NcpStore> findStoreByFranchiserId(Long franchiserId)
    {
        JsonResult<List<NcpStore>> result = storeApi.getByFranchiserId(franchiserId);
        if (result != null && result.isSuccess() && null != result.getData())
        {
            return result.getData();
        }
        logger.info("查询门店失败：storeId={}",franchiserId);
        throw new ChainStockException(StockResultCode.STORE_NOT_FOUND);
    }
    
    /**
     * 
     * 通过经销商id获取机器信息
     *
     * @author sundi
     * @param franchiserId
     * @return
     */
    public List<SaleMachine> findMachineByFranchiserId(Long franchiserId)
    {
        JsonResult<List<SaleMachine>> result = franchiserApi.getMachineByFranchiserId(String.valueOf(franchiserId));
        List<SaleMachine> list=new ArrayList<>();
        if (result != null && result.isSuccess() && null != result.getData())
        {
            result.getData().forEach(machine->{
                if(MachineTypeEnum.getAllMachineType().contains(machine.getMachineType()))
                {
                    machine.setMachineName(MachineTypeEnum.getEnumByCode(machine.getMachineType()).getDesc()+" "+machine.getId());
                    list.add(machine);
                }
            });
            return list;
        }
        logger.info("查询机器失败：sapFranchiserId={}",franchiserId);
        return null;
    }
    
    /**
     * 
     * 获取所有仓库信息
     *
     * @author sundi
     * @param id
     * @param type
     * @return
     */
    public List<Repo> getRepoList(Long id, Integer type)
    {
        RepoParam param = new RepoParam();
        param.setExtendsId(id);
        param.setExtendsType(type);
        //可用状态 可用：1 不可用：0 停用：-1
        param.setState(1);
        JsonResult<List<Repo>> result = repoAPI.getByConditions(param);
        if (result == null || !result.isSuccess() || null == result.getData())
        {
            throw new ChainStockException("查询仓库失败：" + JsonUtils.toJson(param));
        }
        return result.getData();
    }

    /**
     *
     * 根据ecode获取仓库
     *
     * @author sundi
     * @param ecode
     * @param type
     * @return
     */
    public List<Repo> getRepoByExtendsNo(String ecode, Integer type)
    {
        RepoParam param = new RepoParam();
        param.setExtendsNo(ecode);
        param.setExtendsType(type);
        //可用状态 可用：1 不可用：0 停用：-1
        param.setState(1);
        JsonResult<List<Repo>> result = repoAPI.getByConditions(param);
        if (result == null || !result.isSuccess() || null == result.getData())
        {
            throw new ChainStockException("查询仓库失败：" + JsonUtils.toJson(param));
        }
        return result.getData();
    }
    
    /**
     * 
     * 根据skuId查询sku信息
     *
     * @author sundi
     * @param skuId
     * @return
     */
    public Sku findBySkuId(Long skuId)
    {
        SkuParam skuParam = new SkuParam();
        skuParam.setId(skuId);
        JsonResult<Sku> result = skuApi.getSkuDetail(skuParam);
        if (result == null || !result.isSuccess() || null == result.getData())
        {
            logger.info("查询物料信息失败，skuId："+skuId);
            return null;
        }
        return result.getData();     
    }
    
    /**
     * 
     * 通过repoId获取仓库信息
     *
     * @author sundi
     * @param repoId
     * @return
     */
    public Repo findRepoDetail(Long repoId)
    {
        RepoParam repoParam = new RepoParam();
        repoParam.setId(repoId);
        JsonResult<Repo> result = repoAPI.getRepoDetail(repoParam);
        if (result == null || !result.isSuccess() || null == result.getData())
        {
            throw new ChainStockException("查询仓库失败");
        }
        return result.getData();
    }
    
    /**
     * 
     * 通过repoId获取仓库信息
     *
     * @author sundi
     * @param repoId
     * @return
     */
    public Repo findRepoDetailWithOutException(Long repoId)
    {
        JsonResult<Repo> result =null;
        try
        {
            RepoParam repoParam = new RepoParam();
            repoParam.setId(repoId);
            result = repoAPI.getRepoDetail(repoParam);
            if (result == null || !result.isSuccess() || null == result.getData())
            {
                logger.info("查询仓库信息失败：repoId={}",repoId);
               return null;
            }
        }
        catch(Exception e)
        {
            logger.info("查询仓库信息失败：repoId={},e={}",repoId,e);
        }
        
        return result.getData();
    }
    
    public String getRiceSkuNo(Long skuId)
    {
        String skuNo=null;
        //启动加载的map正确则直接从map中获取对应的skuNo
        if(riceSkuMap.size()==(RiceSkuEnum.getBags().size()+RiceSkuEnum.getBoxes().size()))
        {
            skuNo=riceSkuMap.get(skuId);
        }
        //否则去数据中心查sku信息
        else
        {
            Sku sku=findBySkuId(skuId);
            skuNo=sku.getSkuNo(); 
        }
        //特殊扣减库存只处理袋装数据
        if(!RiceSkuEnum.getBags().contains(skuNo))
        {
            skuNo=null;
        }
        return skuNo;
    }
  
    
    public Long getRiceSkuId(String skuNo)
    {
        Long skuId=null;
        for (Long id : riceSkuMap.keySet()) { 
            if(skuNo.equals(riceSkuMap.get(id)))
            {
                skuId=id;
            }
        }
        if(skuId==null)
        {
            SkuParam skuParam=new SkuParam();
            skuParam.setSkuNo(skuNo);
            JsonResult<Sku> result=skuApi.getSkuDetail(skuParam);
            if(null!=result&&result.isSuccess()&&null!=result.getData())
            {
                skuId=result.getData().getId();
            }
            else
            {
                throw new ChainStockException("查询大米箱包对应物料信息失败");
            }
        }
        return skuId;
    }
    public void prepareSkuByParams( List<SkuParam> skuParams)
    {
        List<Sku> skuList=new ArrayList<>();
        try
        {
            JsonResult<List<Sku>> result=skuApi.getSkusDetail(skuParams);
            if(null!=result&&result.isSuccess()&&null!=result.getData())
            {
                skuList=result.getData();
            }
        }
        catch(Exception e)
        {
            logger.info("查询物料信息失败：skuParams={},e={}",skuParams,e);
        }
        if(CollectionUtils.isEmpty(skuList))
        {
            logger.error("启动时获取大米指定物料信息失败");
        }
        else
        {
            skuList.forEach(sku->
            {
                riceSkuMap.put(sku.getId(), sku.getSkuNo());
            });
        }    
    }

    @Override
    public void afterPropertiesSet()
        throws Exception
    {
        List<SkuParam> skuParams=new ArrayList<SkuParam>();
        RiceSkuEnum.getBags().forEach(skuNo->{
            SkuParam skuParam=new SkuParam();
            skuParam.setSkuNo(skuNo);
            skuParams.add(skuParam);
        });
        RiceSkuEnum.getBoxes().forEach(skuNo->{
            SkuParam skuParam=new SkuParam();
            skuParam.setSkuNo(skuNo);
            skuParams.add(skuParam);
        });    
        asyncService.prepareSkuByParams(skuParams);
    }
}
