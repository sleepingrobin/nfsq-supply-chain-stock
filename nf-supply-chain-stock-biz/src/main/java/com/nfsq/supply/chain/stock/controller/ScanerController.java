package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.BindOutNoticeBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticePageQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerUnLimitStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticePageByScanTypeVO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVO;
import com.nfsq.supply.chain.stock.api.service.ScanerProviderApi;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/13 下午3:51
 */
@RestController
@Slf4j
public class ScanerController implements ScanerProviderApi {

    @Autowired
    OutNoticeService outNoticeService;

    @Override
    public JsonResult<ScanerNoticeVO> queryWithScaner(@RequestBody ScanerNoticeQo scanerNoticeQo) {
        JsonResult<ScanerNoticeVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithScaner(scanerNoticeQo);
        } catch (Exception e) {
            log.error("扫码枪查询出库通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<ScanerNoticePageByScanTypeVO> queryWithScanerPage(@RequestBody ScanerNoticePageQo scanerNoticePageQo) {
        JsonResult<ScanerNoticePageByScanTypeVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithScanersPage(scanerNoticePageQo);
        } catch (Exception e) {
            log.error("扫码枪查询任务列表失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> bindOutOutNotice(@RequestBody BindOutNoticeBean bindOutNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.bindOutOutNotice(bindOutNoticeBean);
        } catch (Exception e) {
            log.error("扫码枪绑定通知单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<ScanerNoticeVO> queryWithScanerQO(@RequestBody ScanerNoticeQo qo) {
        JsonResult<ScanerNoticeVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithScanerQO(qo);
        } catch (Exception e) {
            log.error("扫码枪查询出库通知单详情失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }


    @Override
    public JsonResult<List<ScanerNotLimitStockVO>> queryNotLimitStock(@RequestBody ScanerUnLimitStockQO scanerUnLimitStockQO) {
        JsonResult<List<ScanerNotLimitStockVO>> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryNotLimitStock(scanerUnLimitStockQO);
        } catch (Exception e) {
            log.error("扫码枪无码出库查询可用库存失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    /**
     * 根据通知单NO 获取通知单信息
     *
     * @author xwcai
     * @date 2019/1/8 上午8:58
     * @param qo
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO>
     */
    @Override
    public JsonResult<ScanerNoticeByScanTypeVO> queryWithScanerShow(@RequestBody ScanerNoticeQo qo){
        JsonResult<ScanerNoticeByScanTypeVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = outNoticeService.queryWithScanerShow(qo);
        } catch (Exception e) {
            log.error("扫码枪查询通知单失败");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }
}
