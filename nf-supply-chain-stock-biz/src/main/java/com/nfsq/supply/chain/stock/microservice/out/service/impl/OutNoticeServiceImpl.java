package com.nfsq.supply.chain.stock.microservice.out.service.impl;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.datacenter.api.RepoAPI;
import com.nfsq.supply.chain.datacenter.api.common.enums.WeightUnitEnum;
import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.datacenter.api.response.RepoAndExtendInfo;
import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.api.model.bean.*;
import com.nfsq.supply.chain.stock.api.model.dto.*;
import com.nfsq.supply.chain.stock.api.model.enums.*;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.common.config.StockConfig;
import com.nfsq.supply.chain.stock.common.kafka.ProduceMessageService;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.InAndOutTypeRelationEnum;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.*;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutNoticeDetailMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutNoticeMapper;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.CancelOutNoticeBean;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.AutoCommitEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.IsAllowFinishEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.IsPrintEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeListVo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutOutService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutRuleService;
import com.nfsq.supply.chain.utils.common.BigDecimalUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xwcai
 * @date 2018/4/13 下午5:09
 */
@Component
@Slf4j
public class OutNoticeServiceImpl implements OutNoticeService {

    @Autowired
    WmsOutOutNoticeMapper wmsOutOutNoticeMapper;

    @Autowired
    WmsOutOutNoticeDetailMapper wmsOutOutNoticeDetailMapper;

    @Autowired
    WmsInventoryStockService wmsInventoryStockService;

    @Autowired
    BaseInfoService baseInfoService;

    @Lazy
    @Autowired
    OutOutService outOutService;

    @Autowired
    RepoAPI repoAPI;

    @Autowired
    AsyncServiceImpl asyncService;

    @Autowired
    StockConfig stockConfig;

    @Autowired
    RedisManagerService redisManagerService;

    @Lazy
    @Autowired
    OutRuleService outRuleService;

    @Lazy
    @Autowired
    ProduceMessageService produceMessageService;

    @Lazy
    @Autowired
    InNoticeService inNoticeService;

    private static final String REPO_AUTH_SPLIT = "-";
    private static final String REPO_AUTH_START = "f";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    //TODO 批量创建出库通知单
    public JsonResult<Map<String, String>> listCreateOutOutNotice(OutNoticeListBean bean) {
        JsonResult<Map<String, String>> jsonResult = new JsonResult<>();
        Map<String, String> map = new HashMap<>();
        for (OutNoticeBean noticeBean : bean.getNoticeBeanList()) {
            //设置修改人
            noticeBean.setModifyId(bean.getUserId());
            JsonResult<String> result = this.createOutOutNotice(noticeBean);
            if (!result.isSuccess()) {
                throw new ChainStockException(result.getMsg());
            }
            map.put(noticeBean.getReturnKeyName(), result.getData());
        }
        jsonResult.setData(map);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<String> outOutNoticeCreate(OutNoticeBean outNoticeBean) {
        return this.createOutOutNotice(outNoticeBean);
    }

    /**
     * 批量创建出库通知单
     *
     * @param outNoticeBean 创建出库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/13 下午4:05
     */
    private JsonResult<String> createOutOutNotice(OutNoticeBean outNoticeBean) {
        JsonResult<String> jsonResult = new JsonResult<>();
        //创建前参数校验
        JsonResult<Boolean> checkResult = this.checkBeforeCreate(outNoticeBean);
        if (!checkResult.isSuccess()) {
            jsonResult.setMsg(checkResult.getMsg());
            return jsonResult;
        }

        //构建出库通知单
        WmsOutOutNotice wmsOutOutNotice = this.buildWmsOutNoticeByCreate(outNoticeBean);

        //生成出库通知单
        wmsOutOutNoticeMapper.insert(wmsOutOutNotice);

        //插入出库通知详情单
        this.insertBatchOutNoticeDetail(outNoticeBean, wmsOutOutNotice);

        jsonResult.setSuccess(true);
        //放入编号
        jsonResult.setData(wmsOutOutNotice.getOutNoticeNo());
        return jsonResult;
    }

    /**
     * 构造创建的出库通知单bean
     *
     * @param outNoticeBean 创建出库通知单的bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice
     * @author xwcai
     * @date 2018/10/23 下午2:39
     */
    private WmsOutOutNotice buildWmsOutNoticeByCreate(OutNoticeBean outNoticeBean) {
        //构建出库通知单
        WmsOutOutNotice wmsOutOutNotice = this.buildWmsOutNotice(outNoticeBean);

        //构建 创建时间 创建人
        wmsOutOutNotice.setCreateDate(new Date());
        wmsOutOutNotice.setCreateUser(outNoticeBean.getUserId());
        //系统类型
        wmsOutOutNotice.setSystemType(outNoticeBean.getSystemType());

        // 只有大米的需要做 仓库 数据权限 新增水业的也需要仓库权限 2019-04-15
        if (SystemTypeEnum.RICE.getCode().equals(wmsOutOutNotice.getSystemType()) || SystemTypeEnum.WATER.getCode().equals(wmsOutOutNotice.getSystemType())) {
            this.setWholeRepoNoWithRepo(wmsOutOutNotice);
        }

        return wmsOutOutNotice;
    }

    /**
     * 设置仓库权限
     *
     * @param wmsOutOutNotice
     *
     * @return void
     * @author xwcai
     * @date 2018/10/23 下午3:39
     */
    private void setWholeRepoNoWithRepo(WmsOutOutNotice wmsOutOutNotice) {
        //设置数据权限字段  仓库相关
        JsonResult<Repo> repoJsonResult = repoAPI.getById(wmsOutOutNotice.getRepoId());
        if (null == repoJsonResult || !repoJsonResult.isSuccess() || null == repoJsonResult.getData()) {
            log.error("创建出库通知单时，获取仓库信息失败,仓库ID：", wmsOutOutNotice.getRepoId());
            throw new ChainStockException("创建出库通知单时，获取仓库信息失败");
        }
        wmsOutOutNotice.setWholeRepoNo(this.getWholeRepoNoByRepo(repoJsonResult.getData()).toString());
    }

    /**
     * 获取数据权限 仓库字段
     *
     * @param repo 仓库
     *
     * @return java.lang.StringBuilder
     * @author xwcai
     * @date 2018/10/23 下午3:00
     */
    private StringBuilder getWholeRepoNoByRepo(Repo repo) {
        return new StringBuilder().append(REPO_AUTH_START).append(REPO_AUTH_SPLIT).append(repo.getExtendsId()).append(REPO_AUTH_SPLIT).append(repo.getId());
    }

    /**
     * 创建出库通知单前参数校验
     *
     * @param outNoticeBean 创建出库单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/17 上午10:20
     */
    private JsonResult<Boolean> checkBeforeCreate(OutNoticeBean outNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();

        //判断是否存在出库通知单编号 如果存在 直接返回
        if (null == outNoticeBean.getOutSourceId()) {
            log.info("构建出库通知单时，出库来源ID 不能为NULL" + outNoticeBean.getOutSourceId());
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;

        //现在不做重复校验
        //判断外部传来id 是否已经生成出库单
//        List<WmsOutOutNotice> outOutNotices = this.queryByTypeAndOutSourceId(outNoticeBean.getType(), outNoticeBean.getOutSourceId(), outNoticeBean.getRepoId(), outNoticeBean.getDestinationId(), outNoticeBean.getDestinationType());
//        if (null == outOutNotices || outOutNotices.isEmpty()) {
//            jsonResult.setSuccess(true);
//            return jsonResult;
//        } else {
//            log.info("该出库通知单已经生成，请勿重复生成。" + outNoticeBean.getOutSourceId());
//            jsonResult.setMsg("该出库通知单已经生成，请勿重复生成。" + outNoticeBean.getOutSourceId());
//            return jsonResult;
//        }
    }

    /**
     * 根据type outSourceId 获取所有出库通知单
     *
     * @param type            出库通知单类型
     * @param outSourceId     来源id  == 任务id
     * @param repoId          仓库id
     * @param destinationId   目的地id
     * @param destinationType 目的地类型
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice>
     * @author xwcai
     * @date 2018/4/17 上午10:17
     */
    private List<WmsOutOutNotice> queryByTypeAndOutSourceId(Integer type, Long outSourceId, Long repoId, Long destinationId, Integer destinationType) {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutSourceId(outSourceId);
        qo.setDestinationId(destinationId);
        qo.setType(type);
        qo.setRepoId(repoId);
        qo.setDestinationType(destinationType);
        return wmsOutOutNoticeMapper.selectByTypeAndInSourceIdAndRepoIdAndDestinationId(qo);
    }

    /**
     * 构建出库通知单
     *
     * @param outNoticeBean 创建 出库通知单的bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice
     * @author xwcai
     * @date 2018/4/13 下午5:35
     */
    private WmsOutOutNotice buildWmsOutNotice(OutNoticeBean outNoticeBean) {
        WmsOutOutNotice wmsOutOutNotice = new WmsOutOutNotice();

        //设置修改人 修改时间
        wmsOutOutNotice.setModifiedDate(new Date());
        wmsOutOutNotice.setModifiedUser(outNoticeBean.getUserId());

        //构建参数
        //设置目的地信息
        wmsOutOutNotice.setDestinationId(outNoticeBean.getDestinationId());
        wmsOutOutNotice.setDestinationType(outNoticeBean.getDestinationType());
        //设置外部来源单信息
        wmsOutOutNotice.setOutSourceId(outNoticeBean.getOutSourceId());
        wmsOutOutNotice.setType(outNoticeBean.getType());
        wmsOutOutNotice.setTypeDes(StockOutTypeEnum.getByType(outNoticeBean.getType()).getTypeDesc());

        //设置计划出库时间
        wmsOutOutNotice.setPlanOutDate(outNoticeBean.getPlanOutDate());

        //设置仓库信息
        wmsOutOutNotice.setRepoId(outNoticeBean.getRepoId());

        //设置状态为 0 已创建
        wmsOutOutNotice.setOperStatus(StockOperStatusEnum.UN_START.getType());

        //构建出库通知单单号
        wmsOutOutNotice.setOutNoticeNo(this.getOutNoticeId());

        return wmsOutOutNotice;
    }


    /**
     * 构建出库通知单单号
     *
     * @return java.lang.String
     * @author xwcai
     * @date 2018/4/13 下午5:35
     */
    private String getOutNoticeId() {
        return baseInfoService.getNOByCharacter(InOutConstants.OUT_NOTICE_NO_STR);
    }

    /**
     * 批量插入出库通知详情单
     *
     * @param outNoticeBean   创建出库通知单的bean
     * @param wmsOutOutNotice 出库通知单实体
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午2:19
     */
    private void insertBatchOutNoticeDetail(OutNoticeBean outNoticeBean, WmsOutOutNotice wmsOutOutNotice) {

        //构建出库通知单list
        List<WmsOutOutNoticeDetail> detailList = this.buildOutNoticeDetailList(outNoticeBean, wmsOutOutNotice);

        //插入出库通知详情单
        wmsOutOutNoticeDetailMapper.insertBatch(detailList);

        //库存变化 出库通知单不需要库存变化 没有占用状态
    }

    /**
     * 构建出库通知详情单list
     *
     * @param outNoticeBean   创建出库通知单的bean
     * @param wmsOutOutNotice 出库通知单实体
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail>
     * @author xwcai
     * @date 2018/4/14 上午11:05
     */
    private List<WmsOutOutNoticeDetail> buildOutNoticeDetailList(OutNoticeBean outNoticeBean, WmsOutOutNotice wmsOutOutNotice) {
        List<WmsOutOutNoticeDetail> details = new ArrayList<>();

        Set<Long> skuIdSet = new HashSet<>();
        //根据skuId获取sku详细信息
        for (SkuInfoBean skuInfo : outNoticeBean.getSkuInfoList()) {
            skuIdSet.add(skuInfo.getSkuId());
        }

        //根据skuIdList获取sku的详细信息
        List<Sku> skus = baseInfoService.getSkuDetailBySkuId(new ArrayList<>(skuIdSet));

        //构建WmsInInNoticeDetail
        for (SkuInfoBean skuInfo : outNoticeBean.getSkuInfoList()) {
            Sku sku = baseInfoService.getBeanBySkuId(skus, skuInfo.getSkuId());
            if (null == sku) {
                log.info("该sku信息没有获取到：" + skuInfo.getSkuId());
                continue;
            }
            WmsOutOutNoticeDetail wmsOutOutNoticeDetail = this.buildOutNoticeDetail(wmsOutOutNotice, sku, skuInfo);
            details.add(wmsOutOutNoticeDetail);
        }
        return details;
    }

    /**
     * 构建出库通知详情单
     *
     * @param wmsOutOutNotice 出库通知单实体
     * @param sku             查询返回的sku
     * @param bean            创建出库通知单中的sku
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail
     * @author xwcai
     * @date 2018/4/14 上午11:05
     */
    private WmsOutOutNoticeDetail buildOutNoticeDetail(WmsOutOutNotice wmsOutOutNotice, Sku sku, SkuInfoBean bean) {
        WmsOutOutNoticeDetail noticeDetail = new WmsOutOutNoticeDetail();
        Date now = new Date();
        //创建人 创建时间 修改人 修改时间
        noticeDetail.setCreateDate(now);
        noticeDetail.setModifiedDate(now);
        noticeDetail.setCreateUser(wmsOutOutNotice.getModifiedUser());
        noticeDetail.setModifiedUser(wmsOutOutNotice.getModifiedUser());

        //设置总数量
        noticeDetail.setAmount(bean.getAmount());

        //设置出库方式 根据sku 本身是否有码来判定是否需要扫码出库 0 无码 1 有码
        noticeDetail.setScanType(sku.getHasCode());

        //设置出库通知单编号
        noticeDetail.setOutNoticeId(wmsOutOutNotice.getId());
        //批次
        noticeDetail.setBatchNo(bean.getBatchNo());
        //设置重量
        if (null != sku.getGrossWeight() && null != sku.getNetWeight() && StringUtils.isNotEmpty(sku.getWeightUnit())) {
            noticeDetail.setGrossWeight(BigDecimalUtils.mul(sku.getGrossWeight(), bean.getAmount(), 3));
            noticeDetail.setNetWeight(BigDecimalUtils.mul(sku.getNetWeight(), bean.getAmount(), 3));
            noticeDetail.setWeightUnit(sku.getWeightUnit());
        }
        //设置sku信息
        noticeDetail.setSkuId(bean.getSkuId());
        noticeDetail.setSkuNo(sku.getSkuNo());
        noticeDetail.setSkuName(sku.getSkuName());
        noticeDetail.setSkuUnit(sku.getSkuUnit());
        noticeDetail.setStatus(bean.getStatus());
        noticeDetail.setRealAmount(0);
        //资产拥有者
        noticeDetail.setOwner(bean.getOwner());
        noticeDetail.setOwnerType(bean.getOwnerType());

        //设置用途信息
        noticeDetail.setRuleGroup(bean.getRuleGroup());
        noticeDetail.setRuleType(bean.getRuleType());
        noticeDetail.setRuleDetail(bean.getRuleDetail());

        //设置目标资产拥有者
        noticeDetail.setTargetOwner(bean.getTargetOwner());
        noticeDetail.setTargetOwnerType(bean.getTargetOwnerType());

        //设置状态有效
        noticeDetail.setOperStatus(StockOperStatusEnum.UN_START.getType());
        return noticeDetail;
    }

    @Override
    public OutOutNoticeVo getByOutOutNoticeId(Long outNoticeId) {
        OutOutNoticeVo outNoticeVo = wmsOutOutNoticeMapper.selectDetailByOutNoticeId(outNoticeId);
        //判断查询结果是否正确
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNotice() || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty()) {
            return null;
        }
        return outNoticeVo;
    }

    @Override
    public OutOutNoticeVo selectDetailAndNotFinishByOutNoticeId(Long outNoticeId) {
        OutOutNoticeVo outNoticeVo = wmsOutOutNoticeMapper.selectDetailAndNotFinishByOutNoticeId(outNoticeId);
        //判断查询结果是否正确
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNotice() || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty()) {
            return null;
        }
        return outNoticeVo;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<Boolean> cancelOutOutNotice(CancelOutNoticeBean cancelBean) {

        //判断传入的通知单单号是否正确
        WmsOutOutNotice outNotice = wmsOutOutNoticeMapper.selectByPrimaryKey(cancelBean.getOutNoticeId());
        //检查取消的出库通知单是否允许
        JsonResult<Boolean> jsonResult = this.checkCancelOutNotice(outNotice);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        //取消出库通知单 包括出库通知单详情
        this.cancelOutNotice(cancelBean);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 检查取消的出库通知单是否允许
     *
     * @param outNotice 出库通知单
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/8/24 下午2:15
     */
    private JsonResult<Boolean> checkCancelOutNotice(WmsOutOutNotice outNotice) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        if (null == outNotice) {
            jsonResult.setMsg("该出库通知单不存在");
            return jsonResult;
        }
        //只有未开始的才能够取消
        if (StockOperStatusEnum.INVAILD.getType().equals(outNotice.getOperStatus())) {
            jsonResult.setSuccess(true);
            jsonResult.setMsg("该出库通知单已经取消");
            return jsonResult;
        }
        //除了刚创建的状态的都允许 取消
        if (!StockOperStatusEnum.UN_START.getType().equals(outNotice.getOperStatus())) {
            jsonResult.setMsg("该出库通知单正在出库中，不能取消");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 取消出库通知单逻辑
     *
     * @param cancelBean 出库通知单 取消的bean
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午2:03
     */
    private void cancelOutNotice(CancelOutNoticeBean cancelBean) {

        //将出库通知单状态修改为取消
        this.cancelMainOutNotice(cancelBean);

        //将出库通知详情单 状态修改为无效 并且 修改库存
        this.cancelOutNoticeDetail(cancelBean);
    }

    /**
     * 将出库通知单 状态修改为无效
     *
     * @param cancelBean 出库通知单 取消的bean
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午1:49
     */
    private void cancelMainOutNotice(CancelOutNoticeBean cancelBean) {

        //将出库通知单状态修改为取消
        WmsOutOutNotice wmsOutOutNotice = new WmsOutOutNotice();
        wmsOutOutNotice.setModifiedUser(cancelBean.getUserId());
        wmsOutOutNotice.setModifiedDate(new Date());
        wmsOutOutNotice.setOperStatus(StockOperStatusEnum.INVAILD.getType());
        wmsOutOutNotice.setId(cancelBean.getOutNoticeId());

        wmsOutOutNoticeMapper.updateByPrimaryKeySelective(wmsOutOutNotice);
    }

    /**
     * 取消出库通知详情单
     *
     * @param cancelBean 修改人 修改的单号
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午1:51
     */
    private void cancelOutNoticeDetail(CancelOutNoticeBean cancelBean) {
        WmsOutOutNoticeDetail cancelDetail = new WmsOutOutNoticeDetail();

        //设置修改时间 修改人
        cancelDetail.setModifiedDate(new Date());
        cancelDetail.setModifiedUser(cancelBean.getUserId());

        //设置状态
        cancelDetail.setOperStatus(StockOperStatusEnum.INVAILD.getType());
        //设置出库通知单单号
        cancelDetail.setOutNoticeId(cancelBean.getOutNoticeId());
        //取消出库通知详情单
        wmsOutOutNoticeDetailMapper.updateByPrimaryKeySelective(cancelDetail);
    }

    @Override
    public JsonResult<Boolean> updateOutOutNotice(OutNoticeBean outNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //修改 如果没有通知单编号 返回
        if (null == outNoticeBean.getOutSourceId()) {
            log.info("出库通知单单号不能为空");
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }

        //判断传入的通知单单号是否正确 并且获取出库通知单详情
        OutOutNoticeVo outNoticeVo = this.getByOutOutNoticeId(outNoticeBean.getOutNoticeId());
        jsonResult = this.checkOutOutNoticeByModify(outNoticeVo);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }

        //修改出库通知单逻辑
        this.updateOutNotice(outNoticeBean, outNoticeVo);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改删除前的判断
     *
     * @param outNoticeVo 出库通知单 包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/20 下午1:42
     */
    private JsonResult<Boolean> checkOutOutNoticeByModify(OutOutNoticeVo outNoticeVo) {
        //检查是否存在
        JsonResult<Boolean> jsonResult = this.checkOutOutNotice(outNoticeVo);

        List<WmsOutOut> list = outOutService.getByOutNoticeId(outNoticeVo.getWmsOutOutNotice().getId());
        //判断对应出库单是否生成
        if (CollectionUtils.isEmpty(list)) {
            log.info("该通知单已经出库 不能操作 出库单单号：" + outNoticeVo.getWmsOutOutNotice().getOutNoticeNo());
            jsonResult.setMsg("该通知单已经出库 不能操作 出库单单号：" + outNoticeVo.getWmsOutOutNotice().getOutNoticeNo());
            return jsonResult;
        }

        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 更新出库通知单
     *
     * @param outNoticeBean 更新出库通知单的bean
     * @param outNoticeVo   出库通知单包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午2:21
     */
    private void updateOutNotice(OutNoticeBean outNoticeBean, OutOutNoticeVo outNoticeVo) {

        //先修改出库通知单主单
        WmsOutOutNotice updateNotice = this.buildWmsOutNotice(outNoticeBean);
        updateNotice.setId(outNoticeVo.getWmsOutOutNotice().getId());
        wmsOutOutNoticeMapper.updateByPrimaryKey(updateNotice);

        //重新创建出库通知详情单
        this.insertBatchOutNoticeDetail(outNoticeBean, updateNotice);
    }

    @Override
    public JsonResult<Boolean> checkOutOutNotice(OutOutNoticeVo outNoticeVo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();

        //判断获取到的参数是否为空
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNotice() || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty() || StockOperStatusEnum.INVAILD.getType().equals(outNoticeVo.getWmsOutOutNotice().getOperStatus())) {
            log.info("该出库通知单不存在：");
            jsonResult.setMsg("该通知单不存在");
            return jsonResult;
        }

        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<OutOutNoticeListVo> queryByQoPage(OutNoticeQo qo) {
        JsonResult<OutOutNoticeListVo> jsonResult = new JsonResult<>();
        OutOutNoticeListVo noticeListVo = new OutOutNoticeListVo();
        List<WmsOutOutNotice> noticeList = wmsOutOutNoticeMapper.selectByQoPage(qo);
        BeanUtils.copyProperties(qo, noticeListVo);
        jsonResult.setSuccess(true);
        //如果查询没有记录 直接返回
        if (null == noticeList || noticeList.isEmpty()) {
            return jsonResult;
        }
        noticeListVo.setOutOutNotices(noticeList);
        jsonResult.setData(noticeListVo);
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeVOList> queryByQo(OutNoticeQo qo) {
        JsonResult<OutNoticeVOList> jsonResult = new JsonResult<>();
        OutNoticeVOList outNoticeVoList = new OutNoticeVOList();
        jsonResult.setSuccess(true);
        List<OutOutNoticeVo> outNoticeVoLists = wmsOutOutNoticeMapper.selectWithDetailByQo(qo);
        BeanUtils.copyProperties(outNoticeVoList, qo);
        if (null == outNoticeVoLists || outNoticeVoLists.isEmpty()) {
            jsonResult.setData(outNoticeVoList);
            return jsonResult;
        }
        List<OutNoticeVO> dtos = new ArrayList<>();
        for (OutOutNoticeVo vo : outNoticeVoLists) {
            WmsOutOutNoticeDTO wmsOutOutNotice = new WmsOutOutNoticeDTO();
            OutNoticeVO dto = new OutNoticeVO();
            BeanUtils.copyProperties(vo.getWmsOutOutNotice(), wmsOutOutNotice);
            BeanUtils.copyProperties(vo, dto);
            dto.setWmsOutOutNotice(wmsOutOutNotice);
            dtos.add(dto);
        }
        outNoticeVoList.setOutNoticeVos(dtos);
        jsonResult.setData(outNoticeVoList);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<List<WmsOutOutNoticeDetail>> getDetailByNoticeNo(Long noticeId) {
        JsonResult<List<WmsOutOutNoticeDetail>> jsonResult = new JsonResult<>();
        List<WmsOutOutNoticeDetail> outOutNoticeDetails = wmsOutOutNoticeDetailMapper.selectByNoticeId(noticeId);
        jsonResult.setSuccess(true);
        //如果查询没有记录 直接返回
        if (null == outOutNoticeDetails || outOutNoticeDetails.isEmpty()) {
            return jsonResult;
        }
        jsonResult.setData(outOutNoticeDetails);
        return jsonResult;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void updateOutNoticeDetailWithRealAmount(List<WmsOutOutNoticeDetail> detailList, OutOutNoticeVo outNoticeVo, String modifyUser, WmsOutRule outRule) {
        //更新实际出库数量
        wmsOutOutNoticeDetailMapper.updateRealAmountBatch(detailList);

        //更新出库通知单状态
        WmsOutOutNotice outOutNotice = new WmsOutOutNotice();
        outOutNotice.setId(outNoticeVo.getWmsOutOutNotice().getId());
        outOutNotice.setModifiedDate(new Date());
        outOutNotice.setModifiedUser(modifyUser);

        //如果是自动提交的
        if (AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit())) {
            this.updateNoticeStatus(outOutNotice);
        }

    }

    /**
     * 更新出库通知单详情
     *
     * @param outOutNotice 出库通知单
     *
     * @return void
     * @author xwcai
     * @date 2018/4/23 下午1:06
     */
    private void updateNoticeStatus(WmsOutOutNotice outOutNotice) {
        List<Integer> operStatus = wmsOutOutNoticeDetailMapper.selectOperStatusGroupByInNoticeNo(outOutNotice.getId());

        //判断查询到的所有状态list 是否正确
        if (null == operStatus || operStatus.isEmpty()) {
            throw new ChainStockException("查询到的出库通知详情单状态不正确" + outOutNotice.getId());
        }

        //判断当前的主单状态
        StockOperStatusEnum operStatusEnum = baseInfoService.getStockOperStatus(operStatus);
        if (null == operStatusEnum) {
            throw new ChainStockException("查询到的出库通知详情单状态不正确，ID：" + outOutNotice.getId());
        }
        //设置主单状态
        outOutNotice.setOperStatus(operStatusEnum.getType());
        //更新主单状态
        wmsOutOutNoticeMapper.updateByNoticeNoSelective(outOutNotice);
    }

    @Override
    public Long queryOccpyStock(WmsAvaliableStockBO bo) {
        Long num = wmsOutOutNoticeDetailMapper.selectOccpyStock(bo);
        return null == num ? 0 : num;
    }

    @Override
    public JsonResult<OutNoticeVOList> queryWithDetailByQo(OutNoticeQo qo) {
        JsonResult<OutNoticeVOList> jsonResult = new JsonResult<>();
        OutNoticeVOList outNoticeVoList = new OutNoticeVOList();
        jsonResult.setSuccess(true);
        List<OutOutNoticeVo> outNoticeVoLists = wmsOutOutNoticeMapper.selectWithDetailByQoPage(qo);
        BeanUtils.copyProperties(outNoticeVoList, qo);
        if (null == outNoticeVoLists || outNoticeVoLists.isEmpty()) {
            jsonResult.setData(outNoticeVoList);
            return jsonResult;
        }
        List<OutNoticeVO> dtos = new ArrayList<>();
        for (OutOutNoticeVo vo : outNoticeVoLists) {
            WmsOutOutNoticeDTO wmsOutOutNotice = new WmsOutOutNoticeDTO();
            OutNoticeVO dto = new OutNoticeVO();
            BeanUtils.copyProperties(vo.getWmsOutOutNotice(), wmsOutOutNotice);
            BeanUtils.copyProperties(vo, dto);
            dto.setWmsOutOutNotice(wmsOutOutNotice);
            dtos.add(dto);
        }
        outNoticeVoList.setOutNoticeVos(dtos);
        jsonResult.setData(outNoticeVoList);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> bindOutOutNotice(BindOutNoticeBean bindOutNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();

        //判断传入的通知单单号是否正确 并且获取出库通知单详情
        OutOutNoticeVo outNoticeVo = this.getByOutOutNoticeId(bindOutNoticeBean.getNoticeId());
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNotice() || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty() || StockOperStatusEnum.INVAILD.getType().equals(outNoticeVo.getWmsOutOutNotice().getOperStatus())) {
            log.info("该通知单不存在：");
            jsonResult.setMsg("该通知单不存在");
            return jsonResult;
        }

        if (StringUtils.isNotEmpty(outNoticeVo.getWmsOutOutNotice().getBind())) {
            log.info("该通知单已被绑定：");
            jsonResult.setMsg("该通知单已被绑定");
            return jsonResult;
        }

        //绑定出库通知单
        WmsOutOutNotice updateNotice = new WmsOutOutNotice();
        updateNotice.setId(bindOutNoticeBean.getNoticeId());
        updateNotice.setBind(bindOutNoticeBean.getMachineCode());
        updateNotice.setOperStatus(StockOperStatusEnum.STARTING.getType());
        updateNotice.setModifiedUser(bindOutNoticeBean.getMachineCode());
        updateNotice.setModifiedDate(new Date());
        wmsOutOutNoticeMapper.updateBindByMachineCode(updateNotice);
        jsonResult.setSuccess(true);
        jsonResult.setData(true);
        return jsonResult;
    }

    @Override
    public JsonResult<ScanerNoticeVO> queryWithScaner(ScanerNoticeQo qo) {
        JsonResult<ScanerNoticeVO> jsonResult = new JsonResult<>();
        //查询出库通知单
        ScanerNoticeVO scaner = wmsOutOutNoticeMapper.queryWithScaner(qo);
        //如果查不到证明不存在 直接返回
        if (null == scaner) {
            jsonResult.setMsg("该出库通知单不存在或已经被接单，请查证后再试");
            return jsonResult;
        }
        //查询详情
        List<ScanerNoticeDetailDTO> detailDTOS = this.deleteUnfinishAmountZero(scaner.getNoticeId(), scaner.getSystemType());
        if (!CollectionUtils.isEmpty(detailDTOS)) {
            scaner.setDetailList(detailDTOS);
            //展示去0
            this.replaceZero(scaner);
        }
        jsonResult.setData(scaner);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 过滤需求数量为0的
     *
     * @param noticeId
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.dto.ScanerNoticeDetailDTO>
     * @author xwcai
     * @date 2018/9/20 上午11:06
     */
    private List<ScanerNoticeDetailDTO> deleteUnfinishAmountZero(Long noticeId, Integer systemType) {
        WmsOutRule outRule = outRuleService.selectBySystemType(systemType);
        if (null == outRule) {
            log.warn("业务来源对应出库规则不存在,{}", systemType);
            throw new ChainStockException("业务来源对应出库规则不存在");
        }
        //如果是不是水业的，就需要将不同批次的合并，如果是水业的，就不需要
        List<ScanerNoticeDetailDTO> detailDTOS = new ArrayList<>();
        if(SystemTypeEnum.WATER.getCode().equals(systemType)){
            detailDTOS = wmsOutOutNoticeDetailMapper.selectScanerByNoticeIdWithWater(noticeId);
        }else{
            detailDTOS = wmsOutOutNoticeDetailMapper.selectScanerByNoticeId(noticeId);
        }
        detailDTOS = detailDTOS.stream().filter(detail -> {
                    if (AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit())) {
                        return detail.getUnFinishAmount().compareTo(InOutConstants.ZERO) > 0;
                    }
                    return Boolean.TRUE;
                })
                .collect(Collectors.toList());
        detailDTOS.forEach(detail -> {
            if (detail.getUnFinishAmount().compareTo(InOutConstants.ZERO) < 0) {
                detail.setUnFinishAmount(InOutConstants.ZERO);
            }
        });
        return detailDTOS;
    }

    /**
     * 扫码枪展示去0
     *
     * @param scaner
     *
     * @return void
     * @author xwcai
     * @date 2018/9/6 下午4:23
     */
    private void replaceZero(ScanerNoticeVO scaner) {
        scaner.getDetailList().forEach(detail -> detail.setSkuNo(detail.getSkuNo().replaceAll("^(0+)", "")));
    }

    @Override
    public JsonResult<ScanerNoticePageByScanTypeVO> queryWithScanersPage(ScanerNoticePageQo qo) {

        JsonResult<ScanerNoticePageByScanTypeVO> jsonResult = new JsonResult<>();

        ScanerNoticePageByScanTypeVO typeVO = new ScanerNoticePageByScanTypeVO();
        List<ScanerNoticeByScanTypeVO> scanerNoticeByScanTypeVO = new ArrayList<>();

        //查询
        List<ScanerNoticeVO> voList = wmsOutOutNoticeMapper.queryWithScanerPage(qo);
        //复制分页信息
        BeanUtils.copyProperties(qo, typeVO);
        //判断是否有结果
        if (null == voList || voList.isEmpty()) {
            log.info("查询无结果");
            jsonResult.setData(typeVO);
            jsonResult.setSuccess(true);
            return jsonResult;
        }
        //查询如果有结果 就分别去获取各个详情
        voList.forEach(notice -> {
            ScanerNoticeByScanTypeVO vo = this.buildScanerWithDetailList(notice);
            if(null != vo){
                scanerNoticeByScanTypeVO.add(vo);
                //展示去0
                this.replaceZero(notice);
            }
        });
        typeVO.setNoticeList(scanerNoticeByScanTypeVO);
        jsonResult.setData(typeVO);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    private ScanerNoticeByScanTypeVO buildScanerWithDetailList(ScanerNoticeVO notice) {
        //获取所有详情
        List<ScanerNoticeDetailDTO> detailDTOS = this.deleteUnfinishAmountZero(notice.getNoticeId(), notice.getSystemType());
        if(CollectionUtils.isEmpty(detailDTOS)){
            return null;
        }

        //根据扫码方式 区分
        Map<Integer, List<ScanerNoticeDetailDTO>> scanMap = detailDTOS.stream().collect(Collectors.groupingBy(ScanerNoticeDetailDTO::getScanType, Collectors.toList()));

        List<ScanerNoticeDetailDTO> notScanDetailS = scanMap.get(ScanerType.NOT_SCAN.getType());
        List<ScanerNoticeDetailDTO> scanDetailS = scanMap.get(ScanerType.SCAN.getType());

        ScanerNoticeByScanTypeVO scanerNoticeByScanTypeVO = new ScanerNoticeByScanTypeVO();

        BeanUtils.copyProperties(notice, scanerNoticeByScanTypeVO);
        List<ScanerNoticeByScanTypeDTO> scanTypeDTOS = new ArrayList<>();
        //构建 无码的返回
        if (!CollectionUtils.isEmpty(notScanDetailS)) {
            ScanerNoticeByScanTypeDTO scanerNoticeByScanTypeDTO = new ScanerNoticeByScanTypeDTO();
            scanerNoticeByScanTypeDTO.setScanName(ScanerType.NOT_SCAN.getDesc());
            scanerNoticeByScanTypeDTO.setScanType(ScanerType.NOT_SCAN.getType());
            scanerNoticeByScanTypeDTO.setSkuList(notScanDetailS);
            scanTypeDTOS.add(scanerNoticeByScanTypeDTO);
        }
        //构建 有码的返回
        if (!CollectionUtils.isEmpty(scanDetailS)) {
            ScanerNoticeByScanTypeDTO scanerNoticeByScanTypeDTO = new ScanerNoticeByScanTypeDTO();
            scanerNoticeByScanTypeDTO.setScanName(ScanerType.SCAN.getDesc());
            scanerNoticeByScanTypeDTO.setScanType(ScanerType.SCAN.getType());
            scanerNoticeByScanTypeDTO.setSkuList(scanDetailS);
            scanTypeDTOS.add(scanerNoticeByScanTypeDTO);
        }

        scanerNoticeByScanTypeVO.setDetailList(scanTypeDTOS);

        //扫码方式列表
        List<ScanOperDTO> scanOperList = this.buildScanOperList(notice);
        scanerNoticeByScanTypeVO.setScanOperList(scanOperList);
        return scanerNoticeByScanTypeVO;
    }

    //组合扫码方式列表
    private List<ScanOperDTO> buildScanOperList(ScanerNoticeVO notice) {
        List<ScanOperEnum> list = new ArrayList<>();
        if (SystemTypeEnum.MAQUILLAGE.getCode().equals(notice.getSystemType())) {
            list = Arrays.asList(ScanOperEnum.PACKING);
        } else if (SystemTypeEnum.WATER.getCode().equals(notice.getSystemType()) || SystemTypeEnum.APPLE.getCode().equals(notice.getSystemType()) || SystemTypeEnum.RICE.getCode().equals(notice.getSystemType())) {
            list = Arrays.asList(ScanOperEnum.SUPPRT, ScanOperEnum.SINGLEBOX);
        } else {
            log.error("没有对应的扫码方式，{}", notice.getSystemType());
            throw new ChainStockException("没有对应的扫码方式");
        }

        List<ScanOperDTO> scanOperDTOS = new ArrayList<>();
        list.forEach(scan -> {
            for (ScanOperEnum scanOperEnum : ScanOperEnum.values()) {
                if (scanOperEnum.getType().equals(scan.getType())) {
                    ScanOperDTO scanOperDTO = new ScanOperDTO();
                    scanOperDTO.setScanOperName(scanOperEnum.getTypeDes());
                    scanOperDTO.setScanOperCode(scanOperEnum.getType());
                    scanOperDTO.setScanOperFlag(scanOperEnum.getFlag());
                    scanOperDTOS.add(scanOperDTO);
                }
            }
        });
        return scanOperDTOS;
    }

    @Override
    public JsonResult<ScanerNoticeVO> queryWithScanerQO(ScanerNoticeQo qo) {
        JsonResult<ScanerNoticeVO> jsonResult = new JsonResult<>();
        //查询出库通知单
        ScanerNoticeVO scaner = wmsOutOutNoticeMapper.queryWithScanerQO(qo);
        //如果查不到证明不存在 直接返回
        if (null == scaner || null == scaner.getDetailList() || scaner.getDetailList().isEmpty()) {
            jsonResult.setMsg("该出库通知单不存在或已经被接单，请查证后再试");
            return jsonResult;
        }
        //展示去0
        this.replaceZero(scaner);
        jsonResult.setData(scaner);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageListVO> manageQueryByQO(OutNoticeQo qo) {
        JsonResult<OutNoticeManageListVO> jsonResult = new JsonResult<>();
        OutNoticeManageListVO listVO = new OutNoticeManageListVO();
        //查询
        List<OutNoticeManageVO> manageVOs = wmsOutOutNoticeMapper.selectManageByQoPage(qo);
        //copy 分页参数
        BeanUtils.copyProperties(qo, listVO);
        if (null == manageVOs || manageVOs.isEmpty()) {
            jsonResult.setSuccess(true);
            jsonResult.setData(listVO);
            return jsonResult;
        }

        for (OutNoticeManageVO vo : manageVOs) {
            this.getNameByChinese(vo);
        }
        listVO.setNoticeManageVOS(manageVOs);
        jsonResult.setData(listVO);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageListVO> manageQueryWaitByQO(OutNoticeQo qo) {
        JsonResult<OutNoticeManageListVO> jsonResult = new JsonResult<>();
        OutNoticeManageListVO listVO = new OutNoticeManageListVO();
        //查询
        List<OutNoticeManageVO> manageVOs = wmsOutOutNoticeMapper.selectManageByWaitPage(qo);
        //copy 分页参数
        BeanUtils.copyProperties(qo, listVO);
        if (null == manageVOs || manageVOs.isEmpty()) {
            jsonResult.setSuccess(true);
            jsonResult.setData(listVO);
            return jsonResult;
        }

        for (OutNoticeManageVO vo : manageVOs) {
            //是否可以结单的判断 苹果的才允许点击
            if (SystemTypeEnum.APPLE.getCode().equals(vo.getSystemType())) {
                vo.setIsAllowFinish(IsAllowFinishEnums.ALLOW_FINISH.getCode());
            } else {
                vo.setIsAllowFinish(IsAllowFinishEnums.NOT_ALLOW_FINISH.getCode());
            }
            this.getNameByChinese(vo);
        }

        listVO.setNoticeManageVOS(manageVOs);
        jsonResult.setData(listVO);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 翻译一些参数
     *
     * @param vo 出库通知单管理页面的返回
     *
     * @return void
     * @author xwcai
     * @date 2018/6/25 下午6:48
     */
    private void getNameByChinese(OutNoticeManageVO vo) {
        //获取状态名成
        StockOperStatusEnum statusEnum = StockOperStatusEnum.getByType(vo.getOperStatus());
        vo.setOperStatusDesc(statusEnum.getTypeDes());
        //判断是否有打印过
        if (null != vo.getPrintTimes()) {
            if (vo.getPrintTimes() > 0) {
                vo.setIsPrint(IsPrintEnums.PRINT.getCode());
            } else {
                vo.setIsPrint(IsPrintEnums.NOT_PRINT.getCode());
            }
        }

        //获取目的地 供货方名称
        JsonResult<Map<Long, RepoAndExtendInfo>> repoResult = repoAPI.getExtendsInfoById(vo.getSupplyStockId(), vo.getDestinationStockId());
        //如果请求不成功
        if (!repoResult.isSuccess() || null == repoResult.getData() || repoResult.getData().isEmpty()) {
            log.warn("获取供货方 送达方 名称信息失败" + repoResult.getMsg());
        } else {
            Map<Long, RepoAndExtendInfo> map = repoResult.getData();
            //供货方仓库
            RepoAndExtendInfo suppluStock = map.get(vo.getSupplyStockId());
            if (null != suppluStock) {
                vo.setSupplyName(suppluStock.getRepoName());
            }
            //目的地仓库
            RepoAndExtendInfo destionationStock = map.get(vo.getDestinationStockId());
            if (null != destionationStock) {
                //目的地 供货方名称 联系电话
                vo.setAddress(destionationStock.getRepoAddr());
                vo.setContactName(destionationStock.getRepoConsignee());
                vo.setPhoneNum(destionationStock.getRepoConsigneeTel());
                vo.setDestinationName(destionationStock.getRepoName());
            }
        }
    }

    @Override
    public JsonResult<OutNoticeDetailManagePageVO> manageDetailQuery(OutNoticeQo qo) {
        JsonResult<OutNoticeDetailManagePageVO> jsonResult = new JsonResult<>();

        OutNoticeDetailManagePageVO pageVO = new OutNoticeDetailManagePageVO();
        List<WmsOutOutNoticeDetailDTO> details = new ArrayList<>();
        //判断是否需要分页 不分页 默认为不聚合的
        if (qo.getIsPage().equals(IsPageEnum.IS_PAGE.getCode())) {
            //获取详情list
            details = wmsOutOutNoticeDetailMapper.selectManageByQOPage(qo);
            BeanUtils.copyProperties(qo, pageVO);
            if (null == details || details.isEmpty()) {
                jsonResult.setSuccess(true);
                return jsonResult;
            }
        } else {
            details = wmsOutOutNoticeDetailMapper.selectManageByQO(qo);
        }
        //获取中文，以及总重量
        pageVO.setDetailDTOS(details);
        this.outOutNoticeGetChinese(pageVO);
        jsonResult.setData(pageVO);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     *
     * @auther: xwcai
     * @Date: 2019/2/28 上午11:04
     *
     * @param : [pageVO]
     * @return: void
     *
     * @Description: 获取出库通知单明细的中文 以及总毛重
     */
    private void outOutNoticeGetChinese(OutNoticeDetailManagePageVO pageVO) {
        BigDecimal totalGrossWeight = new BigDecimal("0");
        Boolean weightUnitFlag = Boolean.TRUE;
        for (WmsOutOutNoticeDetailDTO detail : pageVO.getDetailDTOS()) {
            //获取扫码类型名称
            ScanerType type = ScanerType.getByType(detail.getScanType());
            if (null != type) {
                detail.setScanTypeDesc(type.getDesc());
            }
            //去除0
            detail.setSkuNo(detail.getSkuNo().replaceAll("^(0+)", ""));
            //设置净重 毛重
            if (null == detail.getGrossWeight() || detail.getGrossWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(detail.getWeightUnit())) {
                detail.setGrossWeightStr("");
            } else {
                totalGrossWeight = totalGrossWeight.add(this.rateOfWeightTotal(detail));
                detail.setGrossWeightStr(detail.getGrossWeight().toString() + detail.getWeightUnit());
            }
            if (null == detail.getNetWeight() || detail.getNetWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(detail.getWeightUnit())) {
                detail.setNetWeightStr("");
            } else {
                detail.setNetWeightStr(detail.getNetWeight().toString() + detail.getWeightUnit());
            }
            if (StringUtils.isEmpty(detail.getWeightUnit())) {
                weightUnitFlag = Boolean.FALSE;
            }
        }
        totalGrossWeight = weightUnitFlag ? totalGrossWeight : new BigDecimal("0");
        pageVO.setTotalGrossWeight(totalGrossWeight);
    }

    /**
     * 换算重量单位
     *
     * @param detail
     *
     * @return java.math.BigDecimal
     * @author xwcai
     * @date 2018/9/19 下午6:26
     */
    private BigDecimal rateOfWeightTotal(WmsOutOutNoticeDetailDTO detail) {
        BigDecimal totalGrossWeight = new BigDecimal("0");
        if (WeightUnitEnum.TON.name().equalsIgnoreCase(detail.getWeightUnit())) {
            totalGrossWeight = totalGrossWeight.add(detail.getGrossWeight().multiply(InOutConstants.RATE_OF_WEIGHT));
        }
        if (WeightUnitEnum.G.name().equalsIgnoreCase(detail.getWeightUnit())) {
            BigDecimal grossWeightKg = detail.getGrossWeight().divide(InOutConstants.RATE_OF_WEIGHT, 3, BigDecimal.ROUND_HALF_UP);
            totalGrossWeight = totalGrossWeight.add(grossWeightKg);
        }
        if (WeightUnitEnum.KG.name().equalsIgnoreCase(detail.getWeightUnit())) {
            totalGrossWeight = totalGrossWeight.add(detail.getGrossWeight());
        }
        return totalGrossWeight;
    }

    @Override
    public JsonResult<Long> manageQueryUnFinishCount() {
        JsonResult<Long> jsonResult = new JsonResult<>();
        Long num = wmsOutOutNoticeMapper.selectCountProcessing(null).longValue();
        jsonResult.setSuccess(true);
        if (null == num) {
            jsonResult.setData(0L);
            return jsonResult;
        }
        jsonResult.setData(num);
        return jsonResult;
    }

    @Override
    public Integer manageQueryProcessCount(StockOutTypeEnum outTypeEnum) {
        Integer num = wmsOutOutNoticeMapper.selectCountProcessing(outTypeEnum.getType());
        return num == null ? 0 : num;
    }

    @Override
    public JsonResult<Boolean> checkOutNoticeFinish(Long taskId) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();

        List<WmsOutOutNotice> list = wmsOutOutNoticeMapper.selectByTaskId(taskId);
        if (null == list || list.isEmpty()) {
            log.info("该任务没有对应出库通知单");
            jsonResult.setMsg("该任务没有对应出库通知单");
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        //根据任务号 查询是否有未完成入库通知单
        List<WmsOutOutNotice> unFinishList = wmsOutOutNoticeMapper.selectUnFinishByTaskId(taskId);
        //如果查询出来不是空 就认为false
        if (null == unFinishList || unFinishList.isEmpty()) {
            jsonResult.setSuccess(Boolean.TRUE);
            jsonResult.setData(Boolean.TRUE);
        } else {
            jsonResult.setSuccess(Boolean.TRUE);
            jsonResult.setData(Boolean.FALSE);
        }
        return jsonResult;
    }

    @Override
    public List<BillDTO> queryBillNOsByQO(Long taskId) {
        return wmsOutOutNoticeMapper.queryBillNOsByQO(taskId);
    }

    @Override
    public Integer queryOutNoticeCount(BillCountQO qo) {
        return wmsOutOutNoticeMapper.selectCountByQO(qo);
    }

    @Override
    public JsonResult<List<String>> queryAllOutNoticeNos() {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        List<String> outNoticeNos = wmsOutOutNoticeMapper.selectAllOutNoticeNos();
        if (null != outNoticeNos && !outNoticeNos.isEmpty()) {
            jsonResult.setData(outNoticeNos);
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<OutNoticeManageVO> queryWithDetailByNO(OutNoticeQo qo) {
        JsonResult<OutNoticeManageVO> jsonResult = new JsonResult<>();
        if (StringUtils.isBlank(qo.getOutNoticeNo())) {
            log.info("查询出库通知单，单号不能为空");
            jsonResult.setMsg("单号不能为空");
            return jsonResult;
        }
        OutNoticeManageVO manageVO = wmsOutOutNoticeMapper.selectManageByNO(qo.getOutNoticeNo());
        if (null == manageVO) {
            jsonResult.setSuccess(true);
            return jsonResult;
        }
        qo.setId(manageVO.getNoticeId());
        this.getNameByChinese(manageVO);
        //查询明细 计算总毛重
        JsonResult<OutNoticeDetailManagePageVO> detailResult = this.manageDetailQuery(qo);
        if (null == detailResult || !detailResult.isSuccess() || null == detailResult.getData()) {
            jsonResult.setData(manageVO);
        } else {
            manageVO.setDetailManageVO(detailResult.getData());
            jsonResult.setData(manageVO);
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public WmsOutOutNotice getOutNoticeById(Long id) {
        return wmsOutOutNoticeMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SkuManageBO> selectSkuUnSnedCount() {
        return wmsOutOutNoticeDetailMapper.selectSkuUnSnedCount(stockConfig.getUnLimitSkuIds());
    }

    @Override
    public JsonResult<List<ScanerNotLimitStockVO>> queryNotLimitStock(ScanerUnLimitStockQO qo) {
        JsonResult<List<ScanerNotLimitStockVO>> jsonResult = new JsonResult<>();
        //根据id  以及 机器码 查询出库通知单详情
        List<ScanerNotLimitStockVO> notLimitStockVOS = wmsOutOutNoticeDetailMapper.selectScanerSku(qo);
        if (CollectionUtils.isEmpty(notLimitStockVOS)) {
            jsonResult.setMsg("该出库通知单已完成或不存在");
            return jsonResult;
        }
        WmsOutOutNotice notice = wmsOutOutNoticeMapper.selectByPrimaryKey(qo.getNoticeId());
        if (null == notice) {
            jsonResult.setMsg("该出库通知单已完成或不存在");
            return jsonResult;
        }
        WmsOutRule outRule = outRuleService.selectBySystemType(notice.getSystemType());
        if (null == outRule) {
            log.error("该系统类型不存在，请排查,{}", notice.getSystemType());
            jsonResult.setMsg("该出库通知单业务来源不正确");
            return jsonResult;
        }
        List<ScanerNotLimitStockVO> notLimitStock = this.getNotLimitStockByNotScan(notLimitStockVOS, outRule);

        if (!CollectionUtils.isEmpty(notLimitStock)) {
            jsonResult.setData(notLimitStock);
            jsonResult.setSuccess(true);
        } else {
            jsonResult.setSuccess(Boolean.FALSE);
            jsonResult.setMsg("无可用库存");
        }

        return jsonResult;
    }

    /**
     * 查询可用库存 并且 过滤为0的情况
     *
     * @param notLimitStockVOS
     * @param outRule
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO>
     * @author xwcai
     * @date 2018/9/20 下午3:50
     */
    private List<ScanerNotLimitStockVO> getNotLimitStockByNotScan(List<ScanerNotLimitStockVO> notLimitStockVOS, WmsOutRule outRule) {
        List<ScanerNotLimitStockVO> notLimitStock = new ArrayList<>();
        //去获取非限制数量 并构建返回
        notLimitStockVOS.forEach(detail -> {
            if (AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit()) && detail.getUnfinishAmount().compareTo(InOutConstants.ZERO) <= 0) {
                return;
            }
            if (AutoCommitEnums.NOT_COMMIT.getCode().equals(outRule.getIsAutoCommit()) && detail.getUnfinishAmount().compareTo(InOutConstants.ZERO) <= 0) {
                detail.setUnfinishAmount(InOutConstants.ZERO);
            }
            //设置库存状态中文说明
            this.setStockStatusDesc(detail);
            //获取可用非扫码库存
            this.getUnLimitStock(notLimitStock, detail);
        });
        return notLimitStock;
    }

    /**
     * 设置库存状态中文说明
     *
     * @param detail
     *
     * @return void
     * @author xwcai
     * @date 2018/12/26 下午2:32
     */
    private void setStockStatusDesc(ScanerNotLimitStockVO detail) {
        StockStatusEnums statusEnum = StockStatusEnums.getEnum(detail.getStatus());
        if (null == statusEnum) {
            log.error("错误的库存状态code，状态，{}", detail.getStatus());
        } else {
            detail.setStatusDesc(statusEnum.getName());
        }
    }

    /**
     * 获取可用非扫码库存
     *
     * @param notLimitStock
     * @param detail
     *
     * @return void
     * @author xwcai
     * @date 2018/12/26 下午2:34
     */
    private void getUnLimitStock(List<ScanerNotLimitStockVO> notLimitStock, ScanerNotLimitStockVO detail) {
        //查询可用库存
        List<SkuNumsBO> unLimitStock = wmsInventoryStockService.selectUnlimitStockByCondition(detail.getSkuId(), detail.getRepoId(), detail.getStatus(), detail.getBatchNo(), detail.getOwner(), detail.getOwnerType());
        //设置可用库存批次中文说明
        unLimitStock.forEach(skuNumsBO -> {
            if (skuNumsBO.getBatchNo().equals(InOutConstants.DEFAULT_BATCH_NO)) {
                skuNumsBO.setBatchNo(InOutConstants.DEFAULT_BATCH_NO_CHINESE);
            }
        });
        //判断是否可用库存为null 如果不是就设置相关数据
        if (!CollectionUtils.isEmpty(unLimitStock)) {
            //如果sku查询出来的库存刚好只有一个
            if (unLimitStock.size() == 1 && InOutConstants.DEFAULT_BATCH_NO_CHINESE.equals(unLimitStock.get(0).getBatchNo())) {
                this.setAvailableQuantity(unLimitStock, detail);
                detail.setUnfinishAmount(BigDecimal.ZERO);
            }
            detail.setNumOptions(unLimitStock);
            notLimitStock.add(detail);
        }
    }

    /**
     * 非扫码出库 填写实际数量
     *
     * @param unLimitStock 可用库存明细
     * @param detail       通知单明细
     *
     * @return void
     * @author xwcai
     * @date 2018/12/26 下午2:28
     */
    private void setAvailableQuantity(List<SkuNumsBO> unLimitStock, ScanerNotLimitStockVO detail) {
        SkuNumsBO numBO = unLimitStock.get(0);
        numBO.setAvailableQuantity(detail.getUnfinishAmount().intValue());
    }

    @Override
    public int updatePrintTimesByOutNoticeNo(String outNoticeNo) {
        //先去更新
        wmsOutOutNoticeMapper.updatePrintTimesByOutNoticeNo(outNoticeNo);
        //然后去查询一共有多少次数
        Integer printTimes = wmsOutOutNoticeMapper.selectPrintTimesByNo(outNoticeNo);
        return printTimes == null ? 0 : printTimes;
    }

    @Override
    public JsonResult<List<StockOutSkuInfoVO>> selectStockOutSkuInfo(StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<List<StockOutSkuInfoVO>> jsonResult = new JsonResult<>();
        //查询后 聚合所有的 没有noticeNo
        List<StockOutSkuInfoVO> voList = outOutService.selectStockOutSkuInfo(stockOutSkuInfoQO.getNoticeNoList());
        if (!CollectionUtils.isEmpty(voList)) {
            jsonResult.setData(voList);
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO stockOutSkuInfoQO) {

        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult = new JsonResult<>();
        //查询后 返回outNoticeNo 并分组
        List<StockOutSkuInfoVO> voList = outOutService.selectStockOutSkuInfoByMap(stockOutSkuInfoQO.getNoticeNoList());
        if (!CollectionUtils.isEmpty(voList)) {
            jsonResult.setData(voList.stream().collect(Collectors.groupingBy(StockOutSkuInfoVO::getNoticeNo)));
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> checkOutNoticeStockOutScan(OutNoticeQo qo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //先根据通知单单号判断通知单 是否存在
        if (StringUtils.isEmpty(qo.getOutNoticeNo())) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
            return jsonResult;
        }

        Long noticeId = wmsOutOutNoticeMapper.selectByNO(qo.getOutNoticeNo());
        if (null == noticeId) {
            log.info("该出库通知单不存在，单号，{}", qo.getOutNoticeNo());
            jsonResult.setSuccess(Boolean.FALSE);
            jsonResult.setData(Boolean.FALSE);
            jsonResult.setMsg("该出库通知单不存在");
            return jsonResult;
        }

        //再根据实际出库方式判断该出库通知单的出库方式
        List<Integer> scanTypes = outOutService.selectScanTypeByNoticeId(noticeId);
        Boolean scanFlag = scanTypes.contains(ScanerType.SCAN.getType());
        jsonResult.setData(scanFlag);
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<Long> manageFinishOutNotice(FinishOutNoticeBean bean) {

        //先判断能不能结单
        JsonResult<Long> jsonResult = this.checkCanFinishOutNotice(bean.getNoticeId());
        if (!jsonResult.isSuccess() || null == jsonResult.getData()) {
            return jsonResult;
        }
        //然后结单
        Integer num = wmsOutOutNoticeDetailMapper.updateByFinishOutNotice(StockOperStatusEnum.FINISH.getType(), bean.getUserId(), bean.getNoticeId());
        if (null == num || num <= 0) {
            log.warn("该通知详情单，没有可以完成的");
            jsonResult.setSuccess(Boolean.FALSE);
            jsonResult.setMsg("该通知详情单，没有可以完成的");
            return jsonResult;
        }
        WmsOutOutNotice outOutNotice = new WmsOutOutNotice();
        outOutNotice.setId(bean.getNoticeId());
        outOutNotice.setModifiedDate(new Date());
        outOutNotice.setModifiedUser(bean.getUserId());
        this.updateNoticeStatus(outOutNotice);
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<List<OutOutSapDetailVO>> selectStockOutSapInfo(StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<List<OutOutSapDetailVO>> jsonResult = new JsonResult<>();
        //查询后 聚合所有的 没有noticeNo
        List<OutOutSapDetailVO> voList = outOutService.selectStockOutSapInfo(stockOutSkuInfoQO.getNoticeNoList());
        if (!CollectionUtils.isEmpty(voList)) {
            voList.forEach(detail -> detail.setSkuNo(detail.getSkuNo().replaceAll("^(0+)", "")));
            Map<Long, List<OutOutSapDetailVO>> repoIdSapMap = voList.stream().collect(Collectors.groupingBy(OutOutSapDetailVO::getRepoId, Collectors.toList()));
            Set<Long> repoIds = voList.stream().map(OutOutSapDetailVO::getRepoId).collect(Collectors.toSet());
            JsonResult<List<Repo>> repoResult = repoAPI.getReposByIds(new ArrayList<>(repoIds));
            if (repoResult.isSuccess() && !CollectionUtils.isEmpty(repoResult.getData())) {
                Map<Long, Repo> repoMap = repoResult.getData().stream().collect(Collectors.toMap(Repo::getId, a -> a, (k1, k2) -> k1));
                repoIdSapMap.forEach((key, value) -> {
                    Repo repo = repoMap.get(key);
                    if (null != repo) {
                        value.forEach(detail -> {
                            //设置仓库名称
                            detail.setRepoName(repo.getRepoName());
                        });
                    }
                });
            }

            jsonResult.setData(voList);
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> sendStockOutMsg(SendOutNoticeBean sendOutNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        jsonResult.setData(Boolean.FALSE);
        WmsOutOut wmsOutOut = outOutService.selectOutBillByOutNo(sendOutNoticeBean.getOutNo());
        if (null == wmsOutOut || StockOperStatusEnum.INVAILD.getType().equals(wmsOutOut.getOperStatus())) {
            log.warn("查询不到出库单，NO为：{}", sendOutNoticeBean.getOutNo());
            jsonResult.setMsg("查询不到出库单");
            return jsonResult;
        }
        WmsOutOutNotice outNotice = wmsOutOutNoticeMapper.selectByPrimaryKey(wmsOutOut.getOutNoticeId());
        if (null == outNotice || StockOperStatusEnum.INVAILD.getType().equals(outNotice.getOperStatus())) {
            log.warn("查询不到出库通知单单，NO为：{}", sendOutNoticeBean.getOutNo());
            jsonResult.setMsg("查询不到出库通知单单");
            return jsonResult;
        }
        List<WmsOutOutDetail> outOutDetails = outOutService.selectOutDetailByOutId(wmsOutOut.getId());
        if (CollectionUtils.isEmpty(outOutDetails)) {
            log.warn("查询不到出库单详情，NO为：{}", sendOutNoticeBean.getOutNo());
            jsonResult.setMsg("查询不到出库单详情");
            return jsonResult;
        }

        StockOutTypeEnum outTypeEnum = StockOutTypeEnum.getByType(outNotice.getType());
        //先判断获取到的出库类型
        if (null != outTypeEnum) {
            //然后获取对应的入库类型 如果为空  就不生成入库通知单
            StockInTypeEnum inTypeEnum = InAndOutTypeRelationEnum.getInTypeEnum(outTypeEnum);
            if (null != inTypeEnum) {
                this.buildInNotice(outNotice, outOutDetails, sendOutNoticeBean.getModifyId(), inTypeEnum);
            }
        }
        this.sendOutMsg(outNotice, outOutDetails);
        jsonResult.setSuccess(Boolean.TRUE);
        jsonResult.setData(Boolean.TRUE);
        return jsonResult;
    }

    /**
     * 发送消息
     *
     * @param outNotice
     * @param outOutDetails
     *
     * @return void
     * @author xwcai
     * @date 2018/10/10 下午3:05
     */
    private void sendOutMsg(WmsOutOutNotice outNotice, List<WmsOutOutDetail> outOutDetails) {
        Boolean flag = Boolean.TRUE;
        WmsOutRule outRule = outRuleService.selectBySystemType(outNotice.getSystemType());
        if (null == outRule) {
            log.error("规则不存在，请查看，{}", outNotice.getSystemType());
        } else {
            flag = AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit());
        }
        //出库完成消息
        OutOutFinishBO outOutFinishBO = new OutOutFinishBO();
        outOutFinishBO.setOutNoticeNo(outNotice.getOutNoticeNo());
        outOutFinishBO.setTaskId(outNotice.getOutSourceId());
        outOutFinishBO.setIsAutoCommit(flag);
        Map<Long, Integer> map = outOutDetails.stream().collect(Collectors.toMap(WmsOutOutDetail::getSkuId, WmsOutOutDetail::getAmount, (k1, k2) -> BigDecimalUtils.add(k1, k2).intValue()));
        outOutFinishBO.setSkuAmountMap(map);
        //直接发送出库单完成消息
        produceMessageService.sendMessage(InOutConstants.FINISH_OUT_OUT, JsonUtils.toJson(outOutFinishBO));

    }

    /**
     * 生成入库通知单
     *
     * @param outOutNotice  出库通知单
     * @param outOutDetails 出库通知单详情
     * @param modifyUser    修改人
     * @param inTypeEnum    入库类型
     *
     * @return void
     * @author xwcai
     * @date 2018/7/26 下午2:56
     */
    private void buildInNotice(WmsOutOutNotice outOutNotice, List<WmsOutOutDetail> outOutDetails, String modifyUser, StockInTypeEnum inTypeEnum) {
        //先生成对应的不显示的入库入库通知单
        InNoticeBean inBean = new InNoticeBean();
        inBean.setUpperCode(UpperCodeEnums.WAIT_FOR_CODE.getCode());
        inBean.setIsDisplay(IsDisplayEnums.HIDE.getCode());
        inBean.setIsDeduction(IsDeductionEnums.DEDUCTION.getCode());
        inBean.setInSourceId(outOutNotice.getOutSourceId());
        inBean.setType(inTypeEnum.getType());
        inBean.setRepoId(outOutNotice.getDestinationId());
        inBean.setModifyId(modifyUser);
        inBean.setSystemType(outOutNotice.getSystemType());

        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();

        outOutDetails.forEach(outDetail -> {

            SkuInfoBean bean = new SkuInfoBean();
            bean.setScanType(ScanerType.NOT_SCAN.getType());
            bean.setAmount(outDetail.getAmount());
            bean.setOwnerType(outDetail.getOwnerType());
            bean.setOwner(outDetail.getOwner());
            bean.setBatchNo(outDetail.getBatchNo());
            bean.setSkuId(outDetail.getSkuId());
            bean.setStatus(outDetail.getStatus());
            bean.setTargetOwner(outDetail.getTargetOwner());
            bean.setTargetOwnerType(outDetail.getTargetOwnerType());
            skuInfoBeanList.add(bean);
        });

        //设置入库通知单sku
        inBean.setSkuInfoList(skuInfoBeanList);

        //生成入库通知单
        inNoticeService.createInInNotice(inBean);
    }

    /**
     * 检查要结单的出库通知单是否能够结单
     *
     * @param noticeId 通知单Id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/9/24 上午11:18
     */
    private JsonResult<Long> checkCanFinishOutNotice(Long noticeId) {
        JsonResult<Long> jsonResult = new JsonResult<>();
        //先判断出库通知单是否存在
        WmsOutOutNotice outOutNotice = wmsOutOutNoticeMapper.selectByPrimaryKey(noticeId);
        if (null == outOutNotice || StockOperStatusEnum.FINISH.getType().equals(outOutNotice.getOperStatus()) || StockOperStatusEnum.INVAILD.getType().equals(outOutNotice.getOperStatus())) {
            jsonResult.setMsg("出库通知单不存在或已经完成");
            return jsonResult;
        }
        //判断出库通知单是否允许手动结单
        WmsOutRule outRule = outRuleService.selectBySystemType(outOutNotice.getSystemType());
        if (null == outRule) {
            log.error("该业务类型对应的出库规则 不存在，请检查,{}", outOutNotice.getSystemType());
            jsonResult.setMsg("该业务类型对应的出库规则 不存在");
            return jsonResult;
        }
        if (AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit())) {
            jsonResult.setMsg("该业务类型不允许手动出库");
            return jsonResult;
        }
        //判断详情单是否存在
        List<WmsOutOutNoticeDetail> detailList = wmsOutOutNoticeDetailMapper.selectByNoticeId(noticeId);
        if (CollectionUtils.isEmpty(detailList)) {
            jsonResult.setMsg("出库通知详情单不存在或已经完成");
            return jsonResult;
        }
        //检查所有详情单 是否全部可以结单 如果全部为0 就不能结单 如果全部完成就要提示已经结过单
        detailList = detailList.stream().filter(detail -> {
            if (StockOperStatusEnum.STARTING.getType().equals(detail.getOperStatus())) {
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(detailList)) {
            jsonResult.setMsg("不允许未操作的通知单结单");
            return jsonResult;
        }
        jsonResult.setSuccess(Boolean.TRUE);
        jsonResult.setData(outOutNotice.getOutSourceId());
        return jsonResult;
    }

    @Override
    public JsonResult<ScanerNoticeByScanTypeVO> queryWithScanerShow(ScanerNoticeQo qo) {

        JsonResult<ScanerNoticeByScanTypeVO> jsonResult = new JsonResult<>();
        //查询
        ScanerNoticeVO vo = wmsOutOutNoticeMapper.queryWithScanerShow(qo);
        //判断是否有结果
        if (null == vo) {
            log.info("查询无结果");
            jsonResult.setSuccess(Boolean.TRUE);
            return jsonResult;
        }

        List<ScanerNoticeDetailDTO> detailDTOS = vo.getDetailList().stream().filter(detail -> ScanerType.SCAN.getType().equals(detail.getScanType())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(detailDTOS)) {
            log.info("查询无结果");
            jsonResult.setSuccess(Boolean.TRUE);
            return jsonResult;
        }

        //已完成数量设置为0
        detailDTOS.forEach(detail -> detail.setFinishAmount(BigDecimal.ZERO));
        //展示去0
        this.replaceZero(vo);
        //构建返回值
        ScanerNoticeByScanTypeVO scanerNoticeByScanTypeVO = this.buildScannerShowResponse(vo, detailDTOS);
        jsonResult.setData(scanerNoticeByScanTypeVO);
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<List<OutNoticeManageVO>> batchQueryWithDetailByNOs(OutNoticeBatchQO qo) {
        JsonResult<List<OutNoticeManageVO>> jsonResult = new JsonResult<>();

        //获取所有出库通知单主体信息
        List<OutNoticeManageVO> noticeManageVOS = wmsOutOutNoticeMapper.selectManageByNOs(qo.getOutNoticeNos());
        if(CollectionUtils.isEmpty(noticeManageVOS)){
            log.info("本次查询无结果");
            jsonResult.setSuccess(Boolean.TRUE);
            return jsonResult;
        }
        //根据获取的通知单主体信息查询 通知单明细 1、先获取所有通知单ID
        Set<Long> noticeIds = noticeManageVOS.stream().map(OutNoticeManageVO::getNoticeId).collect(Collectors.toSet());Collectors.toSet();

        //2、获取所有明细
        List<WmsOutOutNoticeDetailDTO> detailDTOS = wmsOutOutNoticeDetailMapper.selectManageByNoticeIds(noticeIds);

        //3、根据id给明细分组
        Map<Long,List<WmsOutOutNoticeDetailDTO>> detailMaps = detailDTOS.stream().collect(Collectors.groupingBy(WmsOutOutNoticeDetailDTO::getOutNoticeId, Collectors.toList()));

        //4、封装返回参数
        noticeManageVOS.forEach(noticeManageVO -> {
            //查询一些中文
            this.getNameByChinese(noticeManageVO);
            OutNoticeDetailManagePageVO detailManageVO = new OutNoticeDetailManagePageVO();
            detailManageVO.setDetailDTOS(detailMaps.get(noticeManageVO.getNoticeId()));
            //获取总毛重 以及明细中的名词转义
            this.outOutNoticeGetChinese(detailManageVO);
            noticeManageVO.setDetailManageVO(detailManageVO);
        });

        //5、修改所有通知单的修改次数
        wmsOutOutNoticeMapper.updatePrintTimesByIds(noticeIds);

        jsonResult.setData(noticeManageVOS);
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    /**
     * 构建返回值
     *
     * @param vo
     * @param detailDTOS
     *
     * @return com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO
     * @author xwcai
     * @date 2019/1/9 上午10:49
     */
    private ScanerNoticeByScanTypeVO buildScannerShowResponse(ScanerNoticeVO vo, List<ScanerNoticeDetailDTO> detailDTOS) {
        ScanerNoticeByScanTypeVO scanerNoticeByScanTypeVO = new ScanerNoticeByScanTypeVO();
        BeanUtils.copyProperties(vo, scanerNoticeByScanTypeVO);
        List<ScanerNoticeByScanTypeDTO> detailList = new ArrayList<>();

        ScanerNoticeByScanTypeDTO dto = new ScanerNoticeByScanTypeDTO();
        dto.setScanName(ScanerType.SCAN.getDesc());
        dto.setScanType(ScanerType.SCAN.getType());
        dto.setSkuList(detailDTOS);
        detailList.add(dto);
        scanerNoticeByScanTypeVO.setDetailList(detailList);
        //获取扫码方式
        List<ScanOperDTO> scanOperList = this.buildScanOperList(vo);
        scanerNoticeByScanTypeVO.setScanOperList(scanOperList);
        return scanerNoticeByScanTypeVO;
    }

    @Override
    public JsonResult<Boolean> checkAcceptOrderStatus(OutNoticeBatchQO qo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //false为没有接单，true为存在被接单的
        Boolean isAccept = false;

        //根据出库通知单号查询是否被接单
        Integer bind = wmsOutOutNoticeMapper.selectOrderState(qo.getOutNoticeNos());
        if (bind > 0){
           isAccept = true;
        }
        jsonResult.setSuccess(Boolean.TRUE);
        jsonResult.setData(isAccept);
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<Boolean> deleteNoticeAndDetail(OutNoticeBatchQO qo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //要删除的是未接单的出库通知单和出库通知详情单，删除之前确认是否有已接单的
        JsonResult<Boolean> result = this.checkAcceptOrderStatus(qo);

        if (!result.getData()){
            //根据通知单号查出id,再根据id 删除通知单
            List<Long> list = wmsOutOutNoticeMapper.selectByOutNos(qo.getOutNoticeNos());
            if (CollectionUtils.isEmpty(list)){
                jsonResult.setSuccess(Boolean.FALSE);
                jsonResult.setData(Boolean.FALSE);
                jsonResult.setMsg("未查询到对应的出库通知单ID");
                return jsonResult;
            }
            wmsOutOutNoticeDetailMapper.deleteByNoticeIds(list);
            wmsOutOutNoticeMapper.deleteBatchNos(list);
        }
        jsonResult.setSuccess(Boolean.TRUE);
        jsonResult.setData(Boolean.TRUE);
        return jsonResult;
    }

}
