package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStockDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsInventoryStockDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInventoryStockDetail record);

    int insertSelective(WmsInventoryStockDetail record);

    WmsInventoryStockDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInventoryStockDetail record);

    int updateByPrimaryKey(WmsInventoryStockDetail record);

	int addStockDetail(WmsStockBean wmsStockBean);
}