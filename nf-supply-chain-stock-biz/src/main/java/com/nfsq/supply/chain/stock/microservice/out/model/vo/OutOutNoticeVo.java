package com.nfsq.supply.chain.stock.microservice.out.model.vo;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/14 上午9:47
 */
@ApiModel(value = "ReqSwagger2",description = "出库通知单")
@Getter
@Setter
public class OutOutNoticeVo {

    @ApiModelProperty(value = "wmsOutOutNotice",name = "出库通知单",dataType = "Object")
    private WmsOutOutNotice wmsOutOutNotice;

    @ApiModelProperty(value = "wmsOutOutNoticeDetails",name = "出库通知详情单",dataType = "List")
    private List<WmsOutOutNoticeDetail> wmsOutOutNoticeDetails;
}
