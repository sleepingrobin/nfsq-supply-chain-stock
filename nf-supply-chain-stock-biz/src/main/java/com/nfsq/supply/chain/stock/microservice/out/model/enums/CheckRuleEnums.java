package com.nfsq.supply.chain.stock.microservice.out.model.enums;

import lombok.Getter;

/**
 * @author xwcai
 * @date 2018/9/2 下午1:51
 */
@Getter
public enum CheckRuleEnums {
    LESS_THAN(1,"小于"),
    LESS_THAN_AND_EQUAL(2,"小于等于"),
    EQUAL(3,"等于"),
    MORE_THAN_AND_EQUAL(4,"大于等于"),
    MORE_THAN(5,"大于"),
    NOT_CHECK(-1,"不检查");

    private Integer code;

    private String desc;

    CheckRuleEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static Boolean checkAllowOutLessThan(Integer code){
        return CheckRuleEnums.LESS_THAN.getCode().equals(code) || CheckRuleEnums.LESS_THAN_AND_EQUAL.getCode().equals(code) || CheckRuleEnums.NOT_CHECK.getCode().equals(code);
    }

    public static Boolean checkAllowOutMoreThan(Integer code){
        return CheckRuleEnums.MORE_THAN.getCode().equals(code) || CheckRuleEnums.MORE_THAN_AND_EQUAL.getCode().equals(code) || CheckRuleEnums.NOT_CHECK.getCode().equals(code);
    }

    public static Boolean checkAllowOutEqual(Integer code){
        return CheckRuleEnums.LESS_THAN_AND_EQUAL.getCode().equals(code) || CheckRuleEnums.EQUAL.getCode().equals(code) || CheckRuleEnums.MORE_THAN_AND_EQUAL.getCode().equals(code) || CheckRuleEnums.NOT_CHECK.getCode().equals(code);
    }
}
