package com.nfsq.supply.chain.stock.microservice.in.service.impl;

import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.api.model.bean.*;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInNoticeDetailDTO;
import com.nfsq.supply.chain.stock.api.model.enums.*;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.common.kafka.ProduceMessageService;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.InAndOutTypeRelationEnum;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.in.dal.mapper.WmsInInNoticeDetailMapper;
import com.nfsq.supply.chain.stock.microservice.in.dal.mapper.WmsInInNoticeMapper;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.CancelInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.ModifyInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.ModifyNoticeSkuBean;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo;
import com.nfsq.supply.chain.stock.microservice.in.service.InInService;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.DataCenterService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutDetailMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutNoticeDetailMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutNoticeMapper;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithDestinationBean;
import com.nfsq.supply.chain.stock.microservice.out.service.AsyncService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.utils.common.BigDecimalUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:01
 */
@Component
@Slf4j
public class InNoticeServiceImpl implements InNoticeService {

    @Autowired
    WmsInInNoticeMapper wmsInInNoticeMapper;

    @Autowired
    WmsInInNoticeDetailMapper wmsInInNoticeDetailMapper;

    @Autowired
    WmsInventoryStockService wmsInventoryStockService;

    @Autowired
    WmsOutOutNoticeMapper wmsOutOutNoticeMapper;

    @Autowired
    WmsOutOutNoticeDetailMapper wmsOutOutNoticeDetailMapper;

    @Autowired
    @Lazy
    InInService inInService;

    @Autowired
    BaseInfoService baseInfoService;

    @Autowired
    OutNoticeService outNoticeService;

    @Autowired
    ProduceMessageService produceMessageService;

    @Autowired
    WmsOutOutDetailMapper wmsOutOutDetailMapper;

    @Autowired
    AsyncService asyncService;

    @Autowired
    DataCenterService dataCenterService;

    private static final Long USE_SOURCE_OWMNER = -1L;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<InNoticeCreateVO> createInInNotice(InNoticeBean inNoticeBean) {
        JsonResult<InNoticeCreateVO> jsonResult = new JsonResult<>();
        //数据校验
        JsonResult<Long> checkResult = this.checkBeforeCreate(inNoticeBean);
        if (!checkResult.isSuccess()) {
            jsonResult.setMsg(checkResult.getMsg());
            return jsonResult;
        }

        //构建入库通知单 初始化创建时间 创建人
        WmsInInNotice wmsInInNotice = this.buildInNotice(inNoticeBean);
        //构建入库通知单单号
        wmsInInNotice.setInNoticeNo(this.getInNoticeId());
        //构建创建人 创建时间
        wmsInInNotice.setCreateDate(new Date());
        wmsInInNotice.setCreateUser(inNoticeBean.getUserId());
        //系统类型
        wmsInInNotice.setSystemType(inNoticeBean.getSystemType());

        wmsInInNoticeMapper.insert(wmsInInNotice);
        //批量插入入库通知单详情 并且 库存变化
        //获取通知单详情list
        List<WmsInInNoticeDetail> noticeDetails = this.buildNoticeDetailList(inNoticeBean, wmsInInNotice);
        this.insertBatchNoticeDetail(noticeDetails, wmsInInNotice);

        InNoticeCreateVO vo = new InNoticeCreateVO();
        vo.setNoticeNo(wmsInInNotice.getInNoticeNo());
        vo.setNoticeId(wmsInInNotice.getId());
        vo.setRepoId(inNoticeBean.getRepoId());
        jsonResult.setData(vo);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<List<InNoticeCreateVO>> createInInNoticeByOut(InNoticeByOutBean bean) {
        JsonResult<List<InNoticeCreateVO>> jsonResult = new JsonResult<>();

        List<InNoticeCreateVO> createVOS = new ArrayList<>();
        //先判断是否有未完成的出库通知单
        JsonResult<Boolean> finishCheckResult = outNoticeService.checkOutNoticeFinish(bean.getTaskId());
        if (!finishCheckResult.isSuccess() || !finishCheckResult.getData()) {
            log.info("该任务下有未完成的出库通知单");
            jsonResult.setMsg("该任务下有未完成的出库通知单");
            return jsonResult;
        }

        //构建已经出库的sku信息列表 包含目的地
        List<SkuInfoWithDestinationBean> skuInfoList = wmsOutOutDetailMapper.selectSkuInfoByTaskId(bean.getTaskId());
        if (CollectionUtils.isEmpty(skuInfoList)) {
            log.info("没有已完成的出库单");
            jsonResult.setMsg("没有已完成的出库单");
            return jsonResult;
        }

        //同一个任务下
        for (SkuInfoWithDestinationBean info : skuInfoList) {
            //创建入库通知单
            InNoticeCreateVO inNoticeCreateVO = this.createInNoticeByOutWithOne(info, bean);
            if (null == inNoticeCreateVO) {
                log.info("生成入库通知单失败,目的地仓库：" + info.getDestinationId() + "任务号：" + bean.getTaskId());
                throw new ChainStockException("生成入库通知单失败");
            }
            createVOS.add(inNoticeCreateVO);
        }

        //返回值
        jsonResult.setData(createVOS);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


    @Override
    public JsonResult<InNoticeManageListVO> queryByInInNoticeQo(InNoticeQO inNoticeQo) {
        JsonResult<InNoticeManageListVO> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        InNoticeManageListVO inNoticeListVo = new InNoticeManageListVO();
        List<InNoticeManageVO> noticeList = wmsInInNoticeMapper.selectManageByQoPage(inNoticeQo);
        //复制分页信息
        BeanUtils.copyProperties(inNoticeQo, inNoticeListVo);
        if (null == noticeList || noticeList.isEmpty()) {
            jsonResult.setData(inNoticeListVo);
            return jsonResult;
        }
        //构建返回的中文 以及仓库信息
        for (InNoticeManageVO notice : noticeList) {
            this.getChineseByInNotice(notice);
        }
        inNoticeListVo.setVoList(noticeList);
        jsonResult.setData(inNoticeListVo);
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<Boolean> cancelInInNotice(CancelInNoticeBean cancelInNoticeBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //根据入库通知单号 获取旧的通知单数据
        InNoticeVo inNoticeVo = this.getInInNoticeByNoticeId(cancelInNoticeBean.getInNoticeId());
        if(null == inNoticeVo || null == inNoticeVo.getInInNotice() || CollectionUtils.isEmpty(inNoticeVo.getInInNoticeDetails())){
            jsonResult.setMsg("根据id查找入库通知单失败");
            return jsonResult;
        }
        //判断入库通知单状态
        jsonResult = this.checkInNoticeModify(inNoticeVo);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        //判断是否生成了对应的入库单 只要生成了 就不能取消
        List<WmsInIn> wmsInIns = inInService.getByInNoticeId(cancelInNoticeBean.getInNoticeId());
        if (null == wmsInIns || wmsInIns.isEmpty()) {
            //取消入库通知单
            this.cancelInNotice(cancelInNoticeBean, inNoticeVo);
            jsonResult.setSuccess(true);
            return jsonResult;
        } else {
            jsonResult.setMsg("该入库通知单已经生成入库单，不能取消");
            return jsonResult;
        }

    }

    @Override
    public InNoticeVo getByInInNoticeId(Long noticeId) {
        return wmsInInNoticeMapper.selectDetailByNoticeId(noticeId);
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<Boolean> updateInInNotice(ModifyInNoticeBean modifyBean) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //根据入库通知单号 获取旧的通知单数据
        InNoticeVo inNoticeVo = this.getInInNoticeByNoticeId(modifyBean.getInNoticeId());
        if(null == inNoticeVo || null == inNoticeVo.getInInNotice() || CollectionUtils.isEmpty(inNoticeVo.getInInNoticeDetails())){
            jsonResult.setMsg("根据id查找入库通知单失败");
            return jsonResult;
        }
        //判断入库通知单状态
        jsonResult = this.checkInNoticeModify(inNoticeVo);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        //修改入库通知单
        this.updateInNotice(modifyBean, inNoticeVo);

        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 判断入库通知单状态
     *
     * @param inNoticeVo 入库通知单包括详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午3:41
     */
    @Override
    public JsonResult<Boolean> checkInInNotice(InNoticeVo inNoticeVo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //判断获取到的参数是否为空
        if (null == inNoticeVo || null == inNoticeVo.getInInNotice() || CollectionUtils.isEmpty(inNoticeVo.getInInNoticeDetails())) {
            log.info("该入库通知单不存在");
            jsonResult.setMsg("该入库通知单不存在");
            return jsonResult;
        }
        //判断入库通知主单的状态
        if (StockOperStatusEnum.INVAILD.getType().equals(inNoticeVo.getInInNotice().getOperStatus())) {
            log.info("该通知单状态不正确，单号：" + inNoticeVo.getInInNotice().getInNoticeNo());
            jsonResult.setMsg("该通知单状态不正确");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<InNoticeDetailListVO> getDetailByNoticeId(InNoticeQO qo) {
        JsonResult<InNoticeDetailListVO> jsonResult = new JsonResult<>();
        InNoticeDetailListVO listVO = new InNoticeDetailListVO();
        if (null == qo || null == qo.getId()) {
            jsonResult.setMsg("参数不能为空");
            log.warn("参数不能为空，请求参数入库通知单Id不能为空");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        //根据入库通知单编号获取入库通知单详情
        List<WmsInNoticeDetailDTO> noticeDetailList = wmsInInNoticeDetailMapper.selectManageByQOPage(qo);
        BeanUtils.copyProperties(qo, listVO);
        if (null == noticeDetailList || noticeDetailList.isEmpty()) {
            jsonResult.setData(listVO);
            return jsonResult;
        }
        noticeDetailList.forEach(this::setDetailParam);
        listVO.setDetailDTOS(noticeDetailList);
        jsonResult.setData(listVO);
        return jsonResult;
    }

    /**
     * 设置一些参数
     *
     * @author xwcai
     * @date 2018/11/1 上午11:34
     * @param detail
     * @return void
     */
    private void setDetailParam(WmsInNoticeDetailDTO detail){
        detail.setSkuNo(detail.getSkuNo().replaceAll("^(0+)", ""));
        //构建返回的净重 毛重
        //设置净重 毛重
        if (null == detail.getGrossWeight() || detail.getGrossWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(detail.getWeightUnit())) {
            detail.setGrossWeightStr("");
        } else {
            detail.setGrossWeightStr(detail.getGrossWeight().toString() + detail.getWeightUnit());
        }
        if (null == detail.getNetWeight() || detail.getNetWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(detail.getWeightUnit())) {
            detail.setNetWeightStr("");
        } else {
            detail.setNetWeightStr(detail.getNetWeight().toString() + detail.getWeightUnit());
        }

        //获取owner 名称
        String ownerName = dataCenterService.findOwnerNameByIdAndType(detail.getOwner(),detail.getOwnerType());
        if(org.apache.commons.lang.StringUtils.isNotEmpty(ownerName)){
            detail.setOwnerName(ownerName);
        }

        //获取库存的状态
        StockStatusEnums stockEnum = StockStatusEnums.getEnum(detail.getStatus());
        if(null != stockEnum){
            detail.setStatusDesc(stockEnum.getName());
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void updateInNoticeDetailWithRealAmount(List<WmsInInNoticeDetail> detailList, StockInBean inBean) {

        //修改所有详情单的真实入库数量状态
        wmsInInNoticeDetailMapper.updateRealAmountBatch(detailList);
        //修改入库通知单的状态
        WmsInInNotice inInNotice = new WmsInInNotice();
        inInNotice.setId(inBean.getInNoticeId());
        inInNotice.setModifiedUser(inBean.getUserId());
        inInNotice.setModifiedDate(new Date());
        this.updateNoticeStatus(inInNotice);
    }

    @Override
    public List<InNoticeVo> getDetailByTaskIdAndRepoId(ConfirmBO confirmBO) {
        return wmsInInNoticeMapper.selectDetailByTaskIdAndRepoId(confirmBO.getTaskId(), confirmBO.getRepoId());
    }

    @Override
    public List<BillDTO> queryNOsByTaskId(Long taskId) {
        return wmsInInNoticeMapper.queryBillNOsByQO(taskId);
    }

    @Override
    public JsonResult<Boolean> checkInNoticeFinish(Long taskId) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        List<WmsInInNotice> list = this.getInNoticeByTaskId(taskId);
        if (null == list || list.isEmpty()) {
            log.info("该任务没有对应入库通知单");
            jsonResult.setMsg("该任务没有对应入库通知单");
            jsonResult.setData(false);
            return jsonResult;
        }
        //根据任务号 查询是否有未完成入库通知单
        List<WmsInInNotice> unFinishList = this.getInUnFinishNotice(taskId);
        //如果查询出来不是空 就认为false
        if (null == unFinishList || unFinishList.isEmpty()) {
            jsonResult.setSuccess(Boolean.TRUE);
            jsonResult.setData(Boolean.TRUE);
        } else {
            jsonResult.setSuccess(Boolean.TRUE);
            jsonResult.setData(Boolean.FALSE);
        }
        return jsonResult;
    }

    /**
     * 根据任务单号查询 所有未完成的入库通知单 并且不是取消
     *
     * @param taskId 任务id
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice>
     * @author xwcai
     * @date 2018/5/21 下午5:26
     */
    @Override
    public List<WmsInInNotice> getInUnFinishNotice(Long taskId) {
        return wmsInInNoticeMapper.selectUnFinishByTaskId(taskId);
    }

    @Override
    public List<WmsInInNotice> getInNoticeByTaskId(Long taskId) {
        return wmsInInNoticeMapper.selectByTaskId(taskId);
    }

    @Override
    public WmsInInNotice getInNoticeById(Long id) {
        return wmsInInNoticeMapper.selectByPrimaryKey(id);
    }

    @Override
    public JsonResult<InNoticeManageVO> selectManageWithDetailByQO(InNoticeQO qo) {
        JsonResult<InNoticeManageVO> jsonResult = new JsonResult<>();
        if (null == qo || StringUtils.isEmpty(qo.getInNoticeNo())) {
            jsonResult.setMsg("参数不合法");
            log.warn("参数不合法，入库通知单号不能为空");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        InNoticeManageVO vo = wmsInInNoticeMapper.selectManageByNO(qo.getInNoticeNo());
        if (null == vo) {
            return jsonResult;
        }
        //获取详情单
        qo.setId(vo.getId());
        this.getNoticeDetail(vo,qo);
        //翻译一些参数
        this.getChineseByInNotice(vo);
        jsonResult.setData(vo);
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> selectAllNOs() {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        List<String> inNOs = wmsInInNoticeMapper.selectAllNOs();
        if(!CollectionUtils.isEmpty(inNOs)){
            jsonResult.setData(inNOs);
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public Integer queryInNoticeCount(BillCountQO qo) {
        return wmsInInNoticeMapper.selectCountByQO(qo);
    }

    @Override
    public Integer manageQueryProcessCount(StockInTypeEnum inTypeEnum) {
        Integer num = wmsInInNoticeMapper.selectCountProcessing(inTypeEnum.getType());
        return num == null ? 0 : num;
    }

    /**
     * 管理页面查询到 的主单 查询详情单
     *
     * @param notice 入库通知单主体
     *
     * @return void
     * @author xwcai
     * @date 2018/7/21 上午11:39
     */
    private void getNoticeDetail(InNoticeManageVO notice,InNoticeQO qo) {
        JsonResult<InNoticeDetailListVO> jsonResult = this.getDetailByNoticeId(qo);
        if (null == jsonResult || !jsonResult.isSuccess() || null == jsonResult.getData()) {
            log.error("获取入库通知单详情失败，通知单ID：{}", notice.getId());
            throw new ChainStockException("获取入库通知单详情失败");
        }
        notice.setDetailListVO(jsonResult.getData());
    }

    /**
     * 翻译一些参数 构建一些返回值
     *
     * @param notice 入库通知单主体
     *
     * @return void
     * @author xwcai
     * @date 2018/7/21 上午11:27
     */
    private void getChineseByInNotice(InNoticeManageVO notice) {
        //状态的中文
        StockOperStatusEnum statusEnum = StockOperStatusEnum.getByType(notice.getOperStatus());
        if (null != statusEnum) {
            notice.setOperStatusDesc(statusEnum.getTypeDes());
        }
        //收获仓库名称 联系人 联系电话
        Repo repo = dataCenterService.findRepoDetailWithOutException(notice.getRepoId());
        if(null != repo){
            notice.setRepoName(repo.getRepoName());
            notice.setContactName(repo.getConsignee());
            notice.setPhoneNum(repo.getTel());
        }
    }

    /**
     * 根据任务创建入库通知单 构建创建bean
     *
     * @param info 查询出来的出库单的sku信息
     * @param bean 任务创建入库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.InNoticeCreateVO>
     * @author xwcai
     * @date 2018/6/21 上午11:29
     */
    private InNoticeCreateVO createInNoticeByOutWithOne(SkuInfoWithDestinationBean info, InNoticeByOutBean bean) {

        StockOutTypeEnum outTypeEnum = StockOutTypeEnum.getByType(info.getOutType());
        //先判断获取到的出库类型
        if(null == outTypeEnum){
            log.info("该出库通知单类型不正确");
            return null;
        }
        //然后获取对应的入库类型 如果为空  就不生成入库通知单
        StockInTypeEnum inTypeEnum = InAndOutTypeRelationEnum.getInTypeEnum(outTypeEnum);
        if(null == inTypeEnum){
            log.info("该出库通知单没有对应的入库通知单类型");
            return null;
        }
        InNoticeCreateVO createVO = new InNoticeCreateVO();
        //创建入库通知单
        InNoticeBean inNoticeBean = new InNoticeBean();
        inNoticeBean.setType(inTypeEnum.getType());
        inNoticeBean.setInSourceId(bean.getTaskId());
        inNoticeBean.setRepoId(info.getDestinationId());
        //设置创建人
        inNoticeBean.setModifyId(bean.getModifyId());
        //设置预计入库时间
        inNoticeBean.setPlanInDate(bean.getPlanInDate());

        //不扣减库存
        inNoticeBean.setIsDeduction(IsDeductionEnums.NOT_DEDUCTION.getCode());
        //显示
        inNoticeBean.setIsDisplay(IsDisplayEnums.DISPLAY.getCode());
        //上级code
        inNoticeBean.setUpperCode(UpperCodeEnums.TOP_CODE.getCode());
        //系统类型
        inNoticeBean.setSystemType(bean.getSystemType());

        //判断是否已经生成过了入库通知单 根据任务单号 入库类型 仓库ID
        WmsInInNotice wmsInInNotice = wmsInInNoticeMapper.selectByRepoIdAndSourceIdAndType(inNoticeBean.getRepoId(), inNoticeBean.getInSourceId(), inNoticeBean.getType());
        if (null != wmsInInNotice) {
            createVO.setRepoId(wmsInInNotice.getRepoId());
            createVO.setNoticeId(wmsInInNotice.getId());
            createVO.setNoticeNo(wmsInInNotice.getInNoticeNo());
            log.info("该任务单" + inNoticeBean.getInSourceId() + "该目的地", wmsInInNotice.getRepoId() + "该类型" + wmsInInNotice.getType() + " 已经生成入库通知单");
            return createVO;
        }
        //默认都是非扫码
        info.getInfoBeans().forEach(skuInfoBean -> skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType()));
        //设置sku
        inNoticeBean.setSkuInfoList(info.getInfoBeans());

        //事务问题
        JsonResult<InNoticeCreateVO> jsonResult  = ((InNoticeServiceImpl) AopContext.currentProxy()).createInInNotice(inNoticeBean);
        if(null == jsonResult || !jsonResult.isSuccess() || null == jsonResult.getData()){
            log.error("生成入库通知单失败：{}",JsonUtils.toJson(jsonResult));
            return createVO;
        }
        //修改之前的上级code
        this.updateUpperCode(inNoticeBean,jsonResult.getData().getNoticeId());
        return jsonResult.getData();
    }

    /**
     * 修改之前生成的不显示的入库通知单的上级code
     *
     * @author xwcai
     * @date 2018/7/24 下午3:43
     * @param bean 必要 入库来源ID 入库类型 仓库Id 修改人
     * @param noticeId 显示的入库通知单ID
     * @return void
     */
    private void updateUpperCode(InNoticeBean bean,Long noticeId){
        if(null == noticeId){
            throw new ChainStockException("生成入库通知单，修改不显示的code失败，生成的Id不能为空");
        }
        WmsInInNotice inInNotice = new WmsInInNotice();
        inInNotice.setUpperCode(noticeId);
        inInNotice.setModifiedDate(new Date());
        inInNotice.setModifiedUser(bean.getModifyId());
        inInNotice.setInSourceId(bean.getInSourceId());
        inInNotice.setRepoId(bean.getRepoId());
        inInNotice.setType(bean.getType());
        wmsInInNoticeMapper.updateUpperCode(inInNotice);
    }

    /**
     * 修改入库通知单状态
     *
     * @param inInNotice  入库通知单
     *
     * @return void
     * @author xwcai
     * @date 2018/4/21 下午3:08
     */
    private void updateNoticeStatus(WmsInInNotice inInNotice) {
        //查询所有通知详情单的状态
        List<Integer> noticeOperStatus = wmsInInNoticeDetailMapper.selectOperStatusGroupByInNoticeId(inInNotice.getId());
        //判断查询到的所有状态list 是否正确
        if (null == noticeOperStatus || noticeOperStatus.isEmpty()) {
            throw new ChainStockException("查询到的入库通知详情单状态不正确" + inInNotice.getInNoticeNo());
        }
        StockOperStatusEnum operStatusEnum = baseInfoService.getStockOperStatus(noticeOperStatus);
        if (null == operStatusEnum) {
            throw new ChainStockException("查询到的入库通知详情单状态不正确ID" + inInNotice.getId());
        }
        inInNotice.setOperStatus(operStatusEnum.getType());
        wmsInInNoticeMapper.updateByNoticeNoSelective(inInNotice);
    }

    /**
     * 创建入库通知单前的参数校验
     *
     * @param inNoticeBean 创建入库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/17 上午10:11
     */
    private JsonResult<Long> checkBeforeCreate(InNoticeBean inNoticeBean) {
        JsonResult<Long> jsonResult = new JsonResult<>();
        //根据入库类型，来源ID 查询是否有入库通知单 如果已经生成 就直接返回
        List<WmsInInNotice> inInNotices = this.queryInNotice(inNoticeBean.getType(), inNoticeBean.getInSourceId(),inNoticeBean.getRepoId());
        if (null == inInNotices || inInNotices.isEmpty()) {
            jsonResult.setSuccess(true);
            return jsonResult;
        } else {
            log.info("该订单已经生成过入库通知单单，请勿重复生成：" + inNoticeBean.getInSourceId());
            jsonResult.setMsg("该订单已经生成过入库通知单单，请勿重复生成");
            return jsonResult;
        }
    }

    /**
     * 根据type inSourceId 查询是否已经生成了入库通知单 且状态为有效 同一个仓库
     *
     * @param type       入库类型
     * @param inSourceId 入库来源id  == 任务id
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice>
     * @author xwcai
     * @date 2018/4/17 上午10:01
     */
    private List<WmsInInNotice> queryInNotice(Integer type, Long inSourceId,Long repoId) {
        return wmsInInNoticeMapper.selectByTypeAndInSourceId(type, inSourceId,repoId);
    }

    /**
     * 修改入库通知单
     *
     * @param modifyBean 修改bean
     * @param inNoticeVo 入库通知单 包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午2:13
     */
    private void updateInNotice(ModifyInNoticeBean modifyBean, InNoticeVo inNoticeVo) {

        //先修改旧的通知单 构建新的通知单
        WmsInInNotice updateNotice = new WmsInInNotice();
        updateNotice.setModifiedDate(new Date());
        updateNotice.setModifiedUser(modifyBean.getUserId());
        updateNotice.setId(modifyBean.getInNoticeId());
        //修改入库通知单
        wmsInInNoticeMapper.updateByPrimaryKeySelective(updateNotice);

        //构建需要修改的入库通知详情单
        List<WmsInInNoticeDetail> noticeDetails = this.buildUpdateNoticeDetails(modifyBean, inNoticeVo);

        //修改入库通知单 详单  如果删除 就是将数量修改为0
        this.updateDetailByModifyNotice(noticeDetails);

    }

    /**
     * 构建修改入库通知详情单的list
     *
     * @param modifyBean 修改bean
     * @param inNoticeVo 入库通知单包含详情
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail>
     * @author xwcai
     * @date 2018/4/26 上午9:57
     */
    private List<WmsInInNoticeDetail> buildUpdateNoticeDetails(ModifyInNoticeBean modifyBean, InNoticeVo inNoticeVo) {
        List<WmsInInNoticeDetail> noticeDetails = new ArrayList<>();
        Date now = new Date();
        for (ModifyNoticeSkuBean skuBean : modifyBean.getSkuInfoList()) {
            WmsInInNoticeDetail updateDetail = new WmsInInNoticeDetail();
            for (WmsInInNoticeDetail detail : inNoticeVo.getInInNoticeDetails()) {
                if (detail.getId().equals(skuBean.getId())) {
                    //先设置需要修改的入库通知详情单的id
                    updateDetail.setId(detail.getId());
                    //设置修改人修改时间
                    updateDetail.setModifiedDate(now);
                    updateDetail.setModifiedUser(modifyBean.getUserId());
                    //比对已经入库的和想要入库的
                    Integer flag = detail.getRealAmount().compareTo(skuBean.getAmount());
                    if (flag == 0) {
                        //如果已经入库的数量跟修改后的数量一样大 那么就修改数量 修改状态
                        updateDetail.setAmount(skuBean.getAmount());
                        updateDetail.setStatus(StockOperStatusEnum.FINISH.getType());
                        noticeDetails.add(updateDetail);
                    } else if (flag < 0) {
                        //如果已经入库的数量比修改后的数量小 那么直接修改数量 不用修改状体啊
                        updateDetail.setAmount(skuBean.getAmount());
                        noticeDetails.add(updateDetail);
                    } else {
                        //如果已经入库的数量比修改后的数量大 那么抛出异常 回滚之前的操作
                        throw new ChainStockException("修改的数量不能小于已经入库的数量");
                    }
                }
            }
        }
        return noticeDetails;
    }

    /**
     * 入库通知单修改-入库通知详情单修改逻辑
     *
     * @param updateDetails 入库通知单修改的bean
     *
     * @return void
     * @author xwcai
     * @date 2018/4/21 下午6:15
     */
    private void updateDetailByModifyNotice(List<WmsInInNoticeDetail> updateDetails) {
        //判断传入的更新list 是否为空
        if (null == updateDetails || updateDetails.isEmpty()) {
            return;
        }
        wmsInInNoticeDetailMapper.updateByInNoticeIdAndSkuIdAndBatchNoAndStatus(updateDetails);
        //库存变更
    }

    /**
     * 将入库通知主单 设置为无效
     *
     * @param cancelInNoticeBean 入库通知单  取消bean
     * @param inNoticeVo         入库通知单包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午1:48
     */
    private void cancelMainInNotice(CancelInNoticeBean cancelInNoticeBean, InNoticeVo inNoticeVo) {
        //先将入库通知单状态 改为无效
        WmsInInNotice wmsInInNotice = new WmsInInNotice();
        wmsInInNotice.setId(inNoticeVo.getInInNotice().getId());
        wmsInInNotice.setOperStatus(StockOperStatusEnum.INVAILD.getType());
        //修改人 修改时间
        wmsInInNotice.setModifiedDate(new Date());
        wmsInInNotice.setModifiedUser(cancelInNoticeBean.getUserId());
        //逻辑删除
        wmsInInNoticeMapper.updateByPrimaryKeySelective(wmsInInNotice);

    }


    /**
     * 取消入库通知单
     *
     * @param cancelInNoticeBean 入库通知单 取消的bean
     * @param inNoticeVo         入库通知单包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/14 下午2:02
     */
    private void cancelInNotice(CancelInNoticeBean cancelInNoticeBean, InNoticeVo inNoticeVo) {
        //先将入库通知单状态 改为无效
        this.cancelMainInNotice(cancelInNoticeBean, inNoticeVo);

        //再删除入库通知单详情
        this.cancelNoticeDetail(cancelInNoticeBean.getUserId(), inNoticeVo);
    }

    /**
     * 取消入库通知单详情
     *
     * @param modifiedUser 修改人
     * @param inNoticeVo   入库通知单包含详情
     *
     * @return java.lang.Boolean
     * @author xwcai
     * @date 2018/4/12 下午7:25
     */
    private void cancelNoticeDetail(String modifiedUser, InNoticeVo inNoticeVo) {
        WmsInInNoticeDetail cancelDetail = new WmsInInNoticeDetail();
        //将入库通知单  取消状态
        cancelDetail.setOperStatus(StockOperStatusEnum.INVAILD.getType());
        cancelDetail.setInNoticeId(inNoticeVo.getInInNotice().getId());
        //修改人 修改时间
        cancelDetail.setModifiedDate(new Date());
        cancelDetail.setModifiedUser(modifiedUser);
        wmsInInNoticeDetailMapper.updateByInNoticeId(cancelDetail);

        //库存变化
    }

    /**
     * 判断入库通知单单号是否存在 如果存在 返回入库通知单 不存在 返回null
     *
     * @param inNoticeId 入库通知单id
     *
     * @return java.lang.Boolean
     * @author xwcai
     * @date 2018/4/12 下午7:18
     */
    private InNoticeVo getInInNoticeByNoticeId(Long inNoticeId) {
        //根据入库通知单号 获取旧的通知单数据
        InNoticeVo inNoticeVo = this.getByInInNoticeId(inNoticeId);
        //判断获取到的参数是否为空
        if (null == inNoticeVo || null == inNoticeVo.getInInNotice() || null == inNoticeVo.getInInNoticeDetails() || inNoticeVo.getInInNoticeDetails().isEmpty()) {
            return null;
        }
        return inNoticeVo;
    }

    /**
     * 根据入库通知单 批量插入入库通知单详情  并且库存变化
     *
     * @param noticeDetails 入库通知单详情
     * @param wmsInInNotice 入库通知单
     *
     * @return
     * @author xwcai
     * @date 2018/4/12 下午7:11
     */
    private void insertBatchNoticeDetail(List<WmsInInNoticeDetail> noticeDetails, WmsInInNotice wmsInInNotice) {
        if (null == noticeDetails || noticeDetails.isEmpty()) {
            log.info("批量插入入库通知详情单时，详情单不能为空" + "入库通知单单号：" + wmsInInNotice.getId());
            throw new ChainStockException("批量插入入库通知详情单时，详情单不能为空");
        }
        //批量插入详情
        wmsInInNoticeDetailMapper.insertBatch(noticeDetails);
        //库存变化  根据是否扣减库存的字段，来确定是否调用
        if(IsDeductionEnums.DEDUCTION.getCode().equals(wmsInInNotice.getIsDeduction())){
            List<WmsStockBean> wmsStockBeans = this.buildByInNoticeDetails(noticeDetails, wmsInInNotice);
            wmsInventoryStockService.updateStock(wmsStockBeans);
        }
    }

    /**
     * 根据入库通知详情单列表创建变更库存实体
     *
     * @param noticeDetails 入库通知单详情
     * @param wmsInInNotice 入库通知单
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean>
     * @author xwcai
     * @date 2018/5/14 下午4:36
     */
    private List<WmsStockBean> buildByInNoticeDetails(List<WmsInInNoticeDetail> noticeDetails, WmsInInNotice wmsInInNotice) {
        List<WmsStockBean> wmsStockBeans = new ArrayList<>();
        Date now = new Date();
        for (WmsInInNoticeDetail detail : noticeDetails) {
            WmsStockBean bean = new WmsStockBean();
            //创建时间 修改时间 创建人 修改人
            bean.setCreatedDate(now);
            bean.setModifiedDate(now);
            bean.setCreatedUser(detail.getCreateUser());
            bean.setModifiedUser(detail.getModifiedUser());

            //类型 入库
            bean.setType(InOutTypeEnums.IN_NOTICE_STOCK.getCode());
            //放入详情单Id
            bean.setOrderDetailId(detail.getId());
            //目标状态
            bean.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());

            //构建sku相关 还要加上用途
            bean.setSkuId(detail.getSkuId());
            bean.setNums(detail.getAmount());
            bean.setRepoId(wmsInInNotice.getRepoId());
            bean.setBatchNo(detail.getBatchNo());

            bean.setOwner(detail.getOwner());
            bean.setOwnerType(detail.getOwnerType());

            //用途为通用
            //构建用途 入库都是通用
            bean.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
            bean.setRuleType(RuleTypeEnums.COMMON.getCode());
            //系统类型
            bean.setSystemType(wmsInInNotice.getSystemType());
            wmsStockBeans.add(bean);
        }
        return wmsStockBeans;
    }

    /**
     * 构建通知单详情list
     *
     * @param inNoticeBean  入库通知单的bean
     * @param wmsInInNotice 入库通知单
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail>
     * @author xwcai
     * @date 2018/4/12 下午6:59
     */
    private List<WmsInInNoticeDetail> buildNoticeDetailList(InNoticeBean inNoticeBean, WmsInInNotice wmsInInNotice) {
        Set<Long> skuIdSet = new HashSet<>();
        //根据skuId获取sku详细信息
        for (SkuInfoBean skuInfo : inNoticeBean.getSkuInfoList()) {
            skuIdSet.add(skuInfo.getSkuId());
        }
        //根据skuIdList获取sku的详细信息
        List<Sku> skus = baseInfoService.getSkuDetailBySkuId(new ArrayList<>(skuIdSet));

        List<WmsInInNoticeDetail> noticeDetails = new ArrayList<>();
        //构建WmsInInNoticeDetail
        for (SkuInfoBean skuInfo : inNoticeBean.getSkuInfoList()) {
            Sku sku = baseInfoService.getBeanBySkuId(skus, skuInfo.getSkuId());
            if (null == sku) {
                log.info("该sku信息没有获取到：" + skuInfo.getSkuId());
                continue;
            }
            WmsInInNoticeDetail wmsInInNoticeDetail = this.buildNoticeDetail(wmsInInNotice, sku, skuInfo);
            noticeDetails.add(wmsInInNoticeDetail);
        }
        return noticeDetails;
    }

    /**
     * 生成入库通知单单号
     *
     * @return java.lang.String
     * @author xwcai
     * @date 2018/4/12 上午10:24
     */
    private String getInNoticeId() {
        return baseInfoService.getNOByCharacter(InOutConstants.IN_NOTICE_NO_STR);
    }

    /**
     * 构建入库通知单
     *
     * @param inNoticeBean 入库通知单 创建bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice
     * @author xwcai
     * @date 2018/4/12 上午10:22
     */
    private WmsInInNotice buildInNotice(InNoticeBean inNoticeBean) {
        Date now = new Date();
        WmsInInNotice wmsInInNotice = new WmsInInNotice();
        //初始化 修改时间  修改人
        wmsInInNotice.setModifiedDate(now);
        wmsInInNotice.setModifiedUser(inNoticeBean.getUserId());

        //构建参数
        wmsInInNotice.setInSourceId(inNoticeBean.getInSourceId());
        wmsInInNotice.setPlanInDate(inNoticeBean.getPlanInDate());
        //构建仓库ID
        wmsInInNotice.setRepoId(inNoticeBean.getRepoId());
        wmsInInNotice.setType(inNoticeBean.getType());
        wmsInInNotice.setTypeDes(StockInTypeEnum.getByType(inNoticeBean.getType()).getTypeDesc());
        wmsInInNotice.setOperStatus(StockOperStatusEnum.UN_START.getType());

        //设置 是否扣减库存
        wmsInInNotice.setIsDeduction(inNoticeBean.getIsDeduction());
        //设置是否显示
        wmsInInNotice.setIsDisplay(inNoticeBean.getIsDisplay());
        //设置上层code
        wmsInInNotice.setUpperCode(inNoticeBean.getUpperCode());
        return wmsInInNotice;
    }

    /**
     * 构建InNoticeDetailBean
     *
     * @param wmsInInNotice 入库通知单
     * @param sku           sku信息
     * @param bean          创建入库通知单中的sku信息
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail
     * @author xwcai
     * @date 2018/4/12 上午9:14
     */
    private WmsInInNoticeDetail buildNoticeDetail(WmsInInNotice wmsInInNotice, Sku sku, SkuInfoBean bean) {
        WmsInInNoticeDetail noticeDetail = new WmsInInNoticeDetail();
        Date now = new Date();
        //初始化创建时间 修改时间 创建人 修改人
        noticeDetail.setModifiedUser(wmsInInNotice.getModifiedUser());
        noticeDetail.setModifiedDate(now);
        noticeDetail.setCreateDate(now);
        noticeDetail.setCreateUser(wmsInInNotice.getModifiedUser());
        //设置skuId
        noticeDetail.setSkuId(sku.getId());
        noticeDetail.setSkuNo(sku.getSkuNo());
        //设置批次信息
        noticeDetail.setBatchNo(bean.getBatchNo());
        //设置资源拥有者 如果传入skuBean 中 TargetOwner 不为空 就用目标的
        if (null == bean.getTargetOwner() || null == bean.getTargetOwnerType() || USE_SOURCE_OWMNER.equals(bean.getTargetOwner())) {
            noticeDetail.setOwner(bean.getOwner());
            noticeDetail.setOwnerType(bean.getOwnerType());
        } else {
            noticeDetail.setOwner(bean.getTargetOwner());
            noticeDetail.setOwnerType(bean.getTargetOwnerType());
        }

        //设置sku信息
        noticeDetail.setSkuName(sku.getSkuName());
        noticeDetail.setSkuUnit(sku.getSkuUnit());
        //设置sku数量
        noticeDetail.setAmount(bean.getAmount());
        //自动生成默认为非扫码入库
        if (null == bean.getScanType()) {
            noticeDetail.setScanType(ScanerType.NOT_SCAN.getType());
        } else {
            noticeDetail.setScanType(bean.getScanType());
        }

        //设置重量
        if (null != sku.getGrossWeight() && null != sku.getNetWeight() && StringUtils.isNotEmpty(sku.getWeightUnit())) {
            noticeDetail.setGrossWeight(BigDecimalUtils.mul(sku.getGrossWeight(),bean.getAmount(),3));
            noticeDetail.setNetWeight(BigDecimalUtils.mul(sku.getNetWeight(),bean.getAmount(),3));
            noticeDetail.setWeightUnit(sku.getWeightUnit());
        }

        noticeDetail.setInNoticeId(wmsInInNotice.getId());
        //设置库存状态
        noticeDetail.setStatus(bean.getStatus());
        //刚开始进入数量为0
        noticeDetail.setRealAmount(0);

        noticeDetail.setOperStatus(StockOperStatusEnum.UN_START.getType());
        return noticeDetail;
    }

    /**
     * 修改 删除之前检查
     *
     * @param inNoticeVo 入库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/24 上午9:29
     */
    private JsonResult<Boolean> checkInNoticeModify(InNoticeVo inNoticeVo) {
        JsonResult<Boolean> jsonResult = ((InNoticeServiceImpl) AopContext.currentProxy()).checkInInNotice(inNoticeVo);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        if (StockOperStatusEnum.FINISH.getType().equals(inNoticeVo.getInInNotice().getOperStatus())) {
            log.info("该通知单状态不正确，单号：" + inNoticeVo.getInInNotice().getInNoticeNo());
            jsonResult.setMsg("该通知单状态不正确");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public JsonResult<Set<String>> selectAllBatchNoStockIn(InNoticeQO qo) {
        JsonResult<Set<String>> jsonResult = new JsonResult<>();
        if(StringUtils.isEmpty(qo.getInNoticeNo())){
           jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(),StockResultCode.PARAM_EMPTY.getDesc());
           return jsonResult;
        }
        Set<String> batchNos = wmsInInNoticeMapper.selectAllBatchNoStockIn(qo.getInNoticeNo());
        if(!CollectionUtils.isEmpty(batchNos)){
            jsonResult.setData(batchNos);
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }

    @Override
    public JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO stockOutSkuInfoQO) {
        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult = new JsonResult<>();

        //查询后 返回inNoticeNo 并分组
        List<StockOutSkuInfoVO> voList = inInService.selectStockOutSkuInfoByMap(stockOutSkuInfoQO.getNoticeNoList());
        if(!CollectionUtils.isEmpty(voList)){
            jsonResult.setData(voList.stream().collect(Collectors.groupingBy(StockOutSkuInfoVO::getNoticeNo)));
        }
        jsonResult.setSuccess(Boolean.TRUE);
        return jsonResult;
    }
}
