package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockOutBean;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageVO;
import com.nfsq.supply.chain.stock.api.model.vo.OutOutSapDetailVO;
import com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithSkuNoBean;
import com.nfsq.supply.chain.stock.microservice.out.model.qo.OutOutQo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutListVo;
import com.nfsq.supply.chain.utils.common.JsonResult;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/13 下午5:08
 */
public interface OutOutService {

    /**
     * 创建出库单 包括去扣减sap库存
     *
     * @param outBean 创建出库单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午2:38
     */
    JsonResult<String> createOutOutWithSap(StockOutBean outBean);

    /**
     * 创建出库单
     *
     * @param outBean 创建出库单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午2:38
     */
    JsonResult<String> createOutOut(StockOutBean outBean);

    /**
     * 创建出库单 不发送msg
     *
     * @author xwcai
     * @date 2018/9/7 上午9:36
     * @param outBean
     * @param detailList
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.String>
     */
    JsonResult<String> createOutOutWithOutSendMsg(StockOutBean outBean,List<WmsOutOutDetail> detailList);

    /**
     * 根据出库通知单单号查询出库单
     *
     * @param outNoticeId 出库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut
     * @author xwcai
     * @date 2018/4/14 下午3:45
     */
    List<WmsOutOut> getByOutNoticeId(Long outNoticeId);

    /**
     * 根据条件查询所有出库单
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutListVo>
     * @author xwcai
     * @date 2018/4/14 下午6:14
     */
    JsonResult<OutOutListVo> queryOutByQo(OutOutQo qo);

    /**
     * 根据出库单号查询出库单详情
     *
     * @param outId 出库单id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List   <   com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail>>
     * @author xwcai
     * @date 2018/4/14 下午6:16
     */
    JsonResult<List<WmsOutOutDetail>> getDetailByOutId(Long outId);

    /**
     * 查询
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutManageListVO>
     * @author xwcai
     * @date 2018/5/23 上午10:35
     */
    JsonResult<OutManageListVO> manageQueryByQo(OutOutPageQO qo);

    /**
     * 根据NO 查询详情
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List   <   com.nfsq.supply.chain.stock.api.model.vo.OutDetailManageVO>>
     * @author xwcai
     * @date 2018/5/23 上午10:40
     */
    JsonResult<OutManageVO> manageDetailQueryByQo(OutOutPageQO qo);

    /**
     * 查询单号 通过taskId
     *
     * @param taskId 任务id
     *
     * @return java.util.List<java.lang.String>
     * @author xwcai
     * @date 2018/5/31 上午9:59
     */
    List<BillDTO> queryNOsByTaskId(Long taskId);

    /**
     * 根据时间段  查询出库单数量
     *
     * @param qo 查询qo
     *
     * @return java.lang.Integer
     * @author xwcai
     * @date 2018/6/21 下午2:42
     */
    Integer queryOutOutCount(BillCountQO qo);

    /**
     * 查询所有出库单号
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<java.lang.String>>
     * @author xwcai
     * @date 2018/6/22 上午10:23
     */
    JsonResult<List<String>> queryAllOutNos();

    /**
     * 已发货根据sku统计 (sql 写死 只查询门店)
     *
     * @author xwcai
     * @date 2018/7/1 下午3:47
     * @param
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO>
     */
    List<SkuManageBO> selectSkuSnedCount();

    /**
     * 根据出库通知单id 查询所有出库单的sku信息
     *
     * @author xwcai
     * @date 2018/8/7 下午6:34
     * @param outNoticeId
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithDestinationBean>
     */
    List<SkuInfoWithSkuNoBean> selectSkuInfoByOutNoticeId(Long outNoticeId);

    /**
     * 根据出库通知单单号 查询出库的sku
     *
     * @author xwcai
     * @date 2018/9/4 下午4:26
     * @param outNoticeNos
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    List<StockOutSkuInfoVO> selectStockOutSkuInfo(List<String> outNoticeNos);

    /**
     * 根据出库通知单单号 查询出库的sku
     *
     * @author xwcai
     * @date 2018/9/4 下午4:26
     * @param outNoticeNos
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(List<String> outNoticeNos);

    /**
     * 根据出库通知单单号 查询出库的sku
     *
     * @author xwcai
     * @date 2018/9/4 下午4:26
     * @param outNoticeNos
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    List<OutOutSapDetailVO> selectStockOutSapInfo(List<String> outNoticeNos);

    /**
     * 根据出库单号查询出库单
     *
     * @author xwcai
     * @date 2018/10/10 下午2:52
     * @param outNo
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut
     */
    WmsOutOut selectOutBillByOutNo(String outNo) ;

    /**
     * 通过出库单Id查询出库单详情
     *
     * @author xwcai
     * @date 2018/10/10 下午2:53
     * @param outId
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail>
     */
    List<WmsOutOutDetail> selectOutDetailByOutId(Long outId);

    /**
     * @Author: xwcai
     * @Date: 2019-05-08 16:20
     * @Description: 通过出库通知单ID查询 实际出库方式
     * @Param: [outNoticeId]
     * @return: java.util.List<java.lang.Integer>
     */
    List<Integer> selectScanTypeByNoticeId(Long outNoticeId);
}

