package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InDetailManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageListVO;
import com.nfsq.supply.chain.stock.api.service.InInProviderApi;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.in.service.InInService;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/5/21 下午3:21
 */
@RestController
@Slf4j
public class InInController implements InInProviderApi {

    @Autowired
    InInService inInService;

    @Override
    public JsonResult<String> createInIn(@RequestBody StockInBean stockInBean) {
        JsonResult<String> jsonResult = new JsonResult<>();
        try {
            jsonResult = inInService.createInInWithSap(stockInBean);
        } catch (Exception e) {
            if (e instanceof ChainStockException) {
                log.warn("创建入库单失败：");
                log.warn(e.getMessage(), e);
                jsonResult.setMsg(e.getMessage());
            } else {
                log.error("创建入库单失败：");
                log.error(e.getMessage(), e);
                jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            }
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<Boolean> confirmInIn(@RequestBody ConfirmBO confirmBO) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try {
            jsonResult = inInService.confirmInIn(confirmBO);
        } catch (Exception e) {

            if (e instanceof ChainStockException) {
                log.warn("收获接口失败：", confirmBO.getTaskId());
                log.warn(e.getMessage(), e);
                jsonResult.setMsg(e.getMessage());
            } else {
                log.error("收获接口失败：", confirmBO.getTaskId());
                log.error(e.getMessage(), e);
                jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            }
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<InManageListVO> queryInInByQo(@RequestBody InInQO qo) {
        JsonResult<InManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inInService.queryInInByQo(qo);
        } catch (Exception e) {
            log.error("管理页面查询入库单失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<InDetailManageListVO> getDetailByQO(@RequestBody InInQO qo) {
        JsonResult<InDetailManageListVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = inInService.getDetailByQO(qo);
        } catch (Exception e) {
            log.error("管理页面查询入库单详情失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> selectAllNOs(@RequestBody DummyBean bean) {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        try {
            jsonResult = inInService.selectAllNOs();
        } catch (Exception e) {
            log.error("管理页面查询入库单编号失败：");
            log.error(e.getMessage(), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            jsonResult.setSuccess(false);
            return jsonResult;
        }
        return jsonResult;
    }
}
