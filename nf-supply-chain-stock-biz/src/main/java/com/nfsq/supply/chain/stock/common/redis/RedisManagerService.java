package com.nfsq.supply.chain.stock.common.redis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.nfsq.supply.chain.dataAuthority.api.model.qo.UserDataRoleMapQO;
import com.nfsq.supply.chain.dataAuthority.api.model.vo.UserDataRoleRelationMapVO;
import com.nfsq.supply.chain.dataAuthority.api.service.UserDataRoleRelationProviderApi;
import com.nfsq.supply.chain.stock.api.model.enums.CacheType;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.SapSyncStock;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.SapSyncStockMapper;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.WmsStockPersistService;
import com.nfsq.supply.chain.utils.common.JsonResult;

@Component
public class RedisManagerService
{
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private SapSyncStockMapper sapSyncStockMapper;
    
    @Autowired
    private WmsStockPersistService wmsStockPersistService;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    @Autowired
    private UserDataRoleRelationProviderApi userDataRoleRelationProviderApi;
    
    @Cacheable(value = {
        CacheType.ExpireTime.ONE_HOUR}, key = "'commonStock'+T(String).valueOf(#skuId).concat('-').concat(#status).concat('-').concat(#repoId)")
    public String queryCommonStock(Long skuId, Integer status, Long repoId)
    {
        logger.info("此时无缓存，查询数据库，插入缓存，key=commonStock{}-{}-{}", skuId, status, repoId);
        Long nums = wmsStockPersistService.queryCommonStock(skuId, status, repoId);
        String commonNums = "0";
        if (null != nums)
        {
            commonNums = String.valueOf(nums);
        }
        return commonNums;
    }
    
    @CacheEvict(value = {
        CacheType.ExpireTime.ONE_HOUR}, key = "'commonStock'+T(String).valueOf(#skuId).concat('-').concat(#status).concat('-').concat(#repoId)")
    public void deleteCommonStockRedis(Long skuId, Integer status, Long repoId)
    {
        logger.info("删除缓存，key=commonStock{}-{}-{}", skuId, status, repoId);
    }
    
    @Cacheable(value = {
        CacheType.ExpireTime.ONE_HOUR}, key = "'ruleStock'+T(String).valueOf(#skuId).concat('-').concat(#status).concat('-').concat(#repoId).concat('-').concat(#ruleType).concat('-').concat(#ruleGroup).concat('-').concat(#dealerIds)")
    public String queryRuleStock(Long skuId, Integer status, Long repoId, Integer ruleType, Integer ruleGroup,
        List<Long> dealerIds)
    {
        logger.info("此时无缓存，查询数据库，插入缓存，key=ruleStock{}-{}-{}-{}-{}-{}",
            skuId,
            status,
            repoId,
            ruleType,
            ruleGroup,
            dealerIds);
        Long nums = wmsStockPersistService.queryRuleStock(skuId, status, repoId, ruleType, ruleGroup, dealerIds);
        String ruleNums = "0";
        if (null != nums)
        {
            ruleNums = String.valueOf(nums);
        }
        return ruleNums;
    }
    
    @Cacheable(value = {CacheType.ExpireTime.ONE_HOUR}, key = "'sapStock'+#skuNo+#factoryNo+#repoNo")
    public SapSyncStock querySapStock(String skuNo, String factoryNo, String repoNo)
    {
        logger.info("此时无缓存，查询数据库，插入缓存，key=sapStock{}{}{}", skuNo, factoryNo, repoNo);
        SapSyncStock querySapStock = sapSyncStockMapper.querySapStock(skuNo, factoryNo, repoNo);
        return querySapStock;
    }
    
    @CachePut(value = {CacheType.ExpireTime.ONE_HOUR}, key = "'sapStock'+#skuNo+#factoryNo+#repoNo")
    public SapSyncStock updateSapStockRedis(String skuNo, String factoryNo, String repoNo)
    {
        logger.info("更新缓存，key=sapStock{}{}{}", skuNo, factoryNo, repoNo);
        SapSyncStock querySapStock = sapSyncStockMapper.querySapStock(skuNo, factoryNo, repoNo);
        return querySapStock;
    }
    
    @CacheEvict(value = {CacheType.ExpireTime.ONE_HOUR}, key = "'sapStock'+#skuNo+#factoryNo+#repoNo")
    public void deleteSapStockRedis(String skuNo, String factoryNo, String repoNo)
    {
        logger.info("删除缓存，key=sapStock{}{}{}", skuNo, factoryNo, repoNo);
    }
    
    @Cacheable(value = {
        CacheType.ExpireTime.ONE_HOUR}, key = "'storeStock'+T(String).valueOf(#repoId).concat('-').concat(#storeId)")
    public List<WmsInventoryStock> queryStoreStock(Long repoId, Long storeId)
    {
        logger.info("此时无缓存，查询数据库，插入缓存，key=storeStock{}-{}", repoId, storeId);
        List<WmsInventoryStock> stockList =
            wmsStockPersistService.queryStoreStock(repoId, StockStatusEnums.getStoreStatus(), storeId);
        return stockList;
    }
    
    @CacheEvict(value = {
        CacheType.ExpireTime.ONE_HOUR}, key = "'storeStock'+T(String).valueOf(#repoId).concat('-').concat(#storeId)")
    public void deleteStoreStockRedis(Long repoId, Long storeId)
    {
        logger.info("删除缓存，key=storeStock{}-{}", repoId, storeId);
    }
    
    /**
     * 
     * 清除所有sap缓存
     *
     * @author sundi
     */
    public void deleteRuleStockRedis(Long skuId, Integer status, Long repoId, Integer ruleType, Integer ruleGroup)
    {
        RedisConnection connection = null;
        try
        {
            String pattern = new StringBuffer().append("ruleStock")
                .append(skuId)
                .append("-")
                .append(status)
                .append("-")
                .append(repoId)
                .append("-")
                .append(ruleType)
                .append("-")
                .append(ruleGroup)
                .append("*")
                .toString();
            connection = redisTemplate.getConnectionFactory().getConnection();
            Set<byte[]> caches = connection.keys(pattern.getBytes());
            for(byte[] b:caches)
            {
                logger.info("删除缓存，key="+new String(b));
            }
            if (!caches.isEmpty())
            {
                connection.del(caches.toArray(new byte[][] {}));
            }
        }
        catch (Exception e)
        {
            logger.error("清除规则库存缓存失败");
        }
        finally
        {
            if (null != connection)
            {
                connection.close();
            }
        }
    }
    
    /**
     * 
     * 获取用户数据权限取值
     *
     * @author sundi
     * @param loginId
     * @param loginId
     * @return
     */
    @Cacheable(value = {CacheType.ExpireTime.HALF_HOUR}, key = "'userdatarolemap-userId'+T(String).valueOf(#loginId)")
    public Map<String,List<String>> getDataAuthorityValues(String loginId){
        logger.info("此时无缓存，key={}","userdatarolemap-userId"+loginId);
        UserDataRoleMapQO paramUserDataRoleMapQO=new UserDataRoleMapQO();
        paramUserDataRoleMapQO.setLoginId(loginId);
        JsonResult<UserDataRoleRelationMapVO> result=userDataRoleRelationProviderApi.queryDataRoleRelationMap(paramUserDataRoleMapQO); 
        if(null!=result&&result.isSuccess()&&null!=result.getData())
        {
            return result.getData().getUserRoleMap();
        }
        return null;
    }
}
