package com.nfsq.supply.chain.stock.microservice.out.dal.domain;

import java.util.Date;

public class WmsOutRule {
    private Long id;

    private Integer systemType;

    private Integer checkRule;

    private Integer isAutoCommit;

    private Integer isInBatch;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public Integer getCheckRule() {
        return checkRule;
    }

    public void setCheckRule(Integer checkRule) {
        this.checkRule = checkRule;
    }

    public Integer getIsAutoCommit() {
        return isAutoCommit;
    }

    public void setIsAutoCommit(Integer isAutoCommit) {
        this.isAutoCommit = isAutoCommit;
    }

    public Integer getIsInBatch() {
        return isInBatch;
    }

    public void setIsInBatch(Integer isInBatch) {
        this.isInBatch = isInBatch;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}