package com.nfsq.supply.chain.stock.microservice.manage.service.impl;

import com.google.common.collect.Lists;
import com.nfsq.supply.chain.datacenter.api.RepoAPI;
import com.nfsq.supply.chain.datacenter.api.SkuApi;
import com.nfsq.supply.chain.datacenter.api.common.enums.OwnerTypeEnum;
import com.nfsq.supply.chain.datacenter.api.common.enums.RepoExtendTypeEnum;
import com.nfsq.supply.chain.datacenter.api.common.enums.SkuTypeEnums;
import com.nfsq.supply.chain.datacenter.api.common.enums.StoreTypeEnums;
import com.nfsq.supply.chain.datacenter.api.parameter.RepoParam;
import com.nfsq.supply.chain.datacenter.api.response.NcpStore;
import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.datacenter.api.response.SaleMachine;
import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.enums.RepoOwnerEnum;
import com.nfsq.supply.chain.stock.api.model.enums.RuleGroupEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BatchNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.RepoQO;
import com.nfsq.supply.chain.stock.api.model.qo.StoreStockQO;
import com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.FanAndStoreStockInfoVO;
import com.nfsq.supply.chain.stock.api.model.vo.RepoOrStockVO;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.DataCenterService;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.SapSyncStockService;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.WmsStockPersistService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 
 * @author xfeng6
 * @created 2018年4月11日 上午10:43:58
 */
@Component
@Slf4j
public class WmsInventoryStockServiceImpl implements WmsInventoryStockService
{
    
    @Autowired
    private WmsStockPersistService wmsStockPersistService;
  
    @Autowired
    private DataCenterService dataCenterService;
    
    @Autowired
    private RepoAPI repoAPI;
    
    @Autowired
    private SkuApi skuApi;
    
    @Autowired SapSyncStockService sapSyncStockService;
    
    @Autowired
    private RedisManagerService redisManagerService;
    
    private static final String COMMA = ",";
    
    private static final String RAIL = "-";
    
    @Override
    public List<WmsInventoryStockDTO> queryStock(WmsStockQO stock)
    {
        Long id = stock.getId();
        Integer type = stock.getType();
        if(null==id&&null==type)
        {
            return build(stock, null);
        }
        List<Repo> repoList=getQueryRepos(stock);
        if(CollectionUtils.isEmpty(repoList))
        {
            return null;
        }
        return build(stock, repoList);
    }
    
    private List<Repo> getQueryRepos(WmsStockQO stock)
    {
        Long id = stock.getId();
        Long repoId = stock.getRepoId();
        String machineId=stock.getMachineId();
        Integer type = stock.getType();
        List<Repo> repos=new ArrayList<>();
        if (RepoOwnerEnum.STORE.getType().equals(type)||RepoOwnerEnum.MACHINE.getType().equals(type))
        {
            if (repoId == null&&machineId==null)
            {
                if(RepoOwnerEnum.STORE.getType().equals(type))
                {
                    // 通过经销商id获取经销商下所有门店id，通过门店id获取所有仓库信息
                    List<NcpStore> storeList=dataCenterService.findStoreByFranchiserId(id);          
                    if(!CollectionUtils.isEmpty(storeList))
                    {
                        for(NcpStore store:storeList)
                        {
                            repos.addAll(dataCenterService.getRepoByExtendsNo(store.getEcode(), type));
                        }
                    }
                }
                else
                {
                  //获取经销商下所有机器信息
                    List<SaleMachine> machineList=dataCenterService.findMachineByFranchiserId(id);
                    if(!CollectionUtils.isEmpty(machineList))
                    {
                        for(SaleMachine machine:machineList)
                        {
                            repos.addAll(dataCenterService.getRepoByExtendsNo(machine.getId(), type));
                        }
                    }
                }        
            }
            else
            {
                if(RepoOwnerEnum.STORE.getType().equals(type))
                {
                    NcpStore ncpStore=dataCenterService.findStoreById(repoId);
                    if(ncpStore==null){
                        return null;
                    }
                    // 通过门店id获取所有仓库信息
                    repos = dataCenterService.getRepoByExtendsNo(ncpStore.getEcode(), type);
                }
                else
                {
                   // 通过门店id获取所有仓库信息
                  repos = dataCenterService.getRepoByExtendsNo(machineId, type);
                }
            }
        }
        else
        {
            if (repoId == null)
            {
                repos = dataCenterService.getRepoList(id, type);
            }
            else
            {
                RepoParam param = new RepoParam();
                param.setId(repoId);
                JsonResult<Repo> result = repoAPI.getRepoDetail(param);
                if (result == null || !result.isSuccess()||null==result.getData())
                {
                    throw new ChainStockException("查询仓库失败");
                }
                Repo repo = result.getData();
                if(null==repo)
                {
                    return null;
                }
                repos=Lists.newArrayList(repo);
            }
        }
        return repos;
    }

    private Map<Long, Sku> skuCache(Set<Long> skuIds)
    {
        JsonResult<List<Sku>> skusResult = skuApi.getBySkuIdList(new ArrayList<>(skuIds));
        if (skusResult == null || !skusResult.isSuccess())
        {
            throw new ChainStockException("批量查询sku失败：" + JsonUtils.toJson(skuIds));
        }
        List<Sku> skus = skusResult.getData();
        Map<Long, Sku> skuMap = new HashMap<>(skus.size());
        for (Sku sku : skus)
        {
            skuMap.putIfAbsent(sku.getId(), sku);
        }
        return skuMap;
    }
    
    private Map<Long, Repo> repoCache(List<Repo> repos)
    {
        if (CollectionUtils.isEmpty(repos))
        {
            return new HashMap<>();
        }
        Map<Long, Repo> repoMap = new HashMap<>(repos.size());
        for (Repo repo : repos)
        {
            repoMap.putIfAbsent(repo.getId(), repo);
        }
        return repoMap;
    }
    
    private List<WmsInventoryStockDTO> build(WmsStockQO stock, List<Repo> repos)
    {   
        if (!CollectionUtils.isEmpty(repos))
        {
            List<Long> repoIds = repos.stream().map(Repo::getId).collect(Collectors.toList());     
            stock.setRepoIds(repoIds);
        }
        List<WmsInventoryStockDTO> dtos = wmsStockPersistService.queryStock(stock);
        if (CollectionUtils.isEmpty(dtos))
        {
            return null;
        }
        Set<Long> skuIds = dtos.stream().map(WmsInventoryStockDTO::getSkuId).collect(Collectors.toSet());
        Map<Long, Sku> skuMap = skuCache(skuIds);
        if (skuMap.size() == 0)
        {
            return null;
        }
        Map<Long, Repo> repoMap = repoCache(repos);
        Map<String, String> ownerNameMap=new HashMap<>();
        for (WmsInventoryStockDTO dto : dtos)
        {
            Repo repo = repoMap.get(dto.getRepoId());
            if (repo != null)
            {
                dto.setRepoName(repo.getRepoName());  
            }
            else
            {
                Repo newRepo=dataCenterService.findRepoDetailWithOutException(dto.getRepoId());
                if(null!=newRepo)
                {
                    repoMap.putIfAbsent(dto.getRepoId(), newRepo);
                    dto.setRepoName(newRepo.getRepoName());
                }  
            }
            //设置ownerName
            if(null!=dto.getOwner()&&null!=dto.getOwnerType())
            {
                String ownerName=getOwnerName(dto.getOwner(),dto.getOwnerType(),ownerNameMap);
                dto.setRepoOwnerName(ownerName);
            }        
            Sku sku = skuMap.get(dto.getSkuId());
            if (sku != null)
            {
                dto.setSkuTypeName(SkuTypeEnums.getByType(sku.getSkuType()) == null ? ""
                    : SkuTypeEnums.getByType(sku.getSkuType()).getDesc());
                dto.setSkuName(sku.getSkuName());
                dto.setSkuNo(sku.getSkuNo()==null?"":sku.getSkuNo().replaceAll("^(0+)", ""));
                dto.setSkuType(sku.getSkuType());
                dto.setSkuUnit(sku.getSkuUnit());
            }
            String[] statusNum = dto.getStatusNums().split(COMMA);
            if (statusNum != null && statusNum.length > 0)
            {
                for (String temp : statusNum)
                {
                    String[] t = temp.split(RAIL);
                    Integer statusCode = Integer.valueOf(t[0]);
                    Integer num = Integer.valueOf(t[1]);
                    switch (StockStatusEnums.getEnum(statusCode))
                    {
                        case WMSINVENTORYSTOCK_QUALITY:
                            dto.setQuality(num);
                            break;
                        case WMSINVENTORYSTOCK_FROZEN:
                            dto.setFrozen(num);
                            break;
                        case WMSINVENTORYSTOCK_NOT_LIMIT:
                            dto.setUnlimit(num);
                            break;
                        case WMSINVENTORYSTOCK_ON_THE_WAY:
                            dto.setOnTheWay(num);
                            break;
                        case WMSINVENTORYSTOCK_SCRAP:
                            dto.setScrap(num);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return dtos;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    @Override
    public void updateStock(List<WmsStockBean> wmsStockBeans)
    {
        if (CollectionUtils.isEmpty(wmsStockBeans))
        {
            return;
        }
        for (WmsStockBean wmsStockBean : wmsStockBeans)
        {
            Integer status = wmsStockBean.getStatus();
            Integer targetStatus = wmsStockBean.getTargetStatus();
            if (status != null && targetStatus != null)
            {
                wmsStockPersistService.updateStock(wmsStockBean);           
            }
            else if (targetStatus != null)
            {
                wmsStockPersistService.addStock(wmsStockBean);
            }
            else if (status != null)
            {
                if(wmsStockPersistService.isSpecify(wmsStockBean))
                {
                    wmsStockPersistService.specifyReduceStock(wmsStockBean);
                }
                else
                {
                    wmsStockPersistService.reduceStock(wmsStockBean);
                }
            }
            else
            {
                throw new ChainStockException("库存更改失败");
            }
        }
    }
    
    @Override
    public List<WmsAvaliableStockBO> queryAvaliableStock(List<WmsAvaliableStockBO> wmsAvaliableStockBOs)
    {
        for (WmsAvaliableStockBO bean : wmsAvaliableStockBOs)
        {
            Long totalNums=0L;
            for(Integer status:bean.getStatuses())
            {
                Long commonNums=0L;
                Long ruleNums=0L;   
                String commonNumsRedis=redisManagerService.queryCommonStock(bean.getSkuId(),status,bean.getRepoId());
                if(!StringUtils.isEmpty(commonNumsRedis))
                {
                    commonNums=Long.parseLong(commonNumsRedis);
                }
                if(bean.getRuleType()!=RuleTypeEnums.COMMON.getCode()||bean.getRuleGroup()!=RuleGroupEnums.CURRENCY.getCode())
                {
                    List<Long> dealerIds=new ArrayList<>();
                    if(!CollectionUtils.isEmpty(bean.getDealerIds()))
                    {
                        dealerIds=bean.getDealerIds();
                    }
                    String ruleNumsRedis=redisManagerService.queryRuleStock(bean.getSkuId(),status,bean.getRepoId(),bean.getRuleType(),bean.getRuleGroup(),dealerIds);
                    if(!StringUtils.isEmpty(ruleNumsRedis))
                    {
                        ruleNums=Long.parseLong(ruleNumsRedis);
                    }       
                }         
                Long nums=commonNums+ruleNums;
                totalNums=totalNums+nums;
            }
            bean.setNums(totalNums);
        }
        return wmsAvaliableStockBOs;
    }
    
    @Override
    public List<String> queryBatchNos(BatchNoQO qo)
    {
        return wmsStockPersistService.queryBatchNos(qo);
    }
    
    @Override
    public RepoOrStockVO queryRepos(RepoQO bean)
    {
        RepoOrStockVO vo = new RepoOrStockVO();
        Long id = bean.getId();
        Integer type = bean.getType();
        if (RepoOwnerEnum.STORE.getType().equals(type)||RepoOwnerEnum.MACHINE.getType().equals(type))
        {
            if(RepoOwnerEnum.STORE.getType().equals(type))
            {
                // 获取经销商下所有门店信息
                List<NcpStore> storeList=dataCenterService.findStoreByFranchiserId(id); 
                vo.setStoreList(storeList);
                return vo;
            }
            else
            {
                //获取经销商下所有机器信息
                List<SaleMachine> machineList=dataCenterService.findMachineByFranchiserId(id);
                if(!CollectionUtils.isEmpty(machineList))
                {
                    vo.setSaleMachineList(machineList);
                }
                return vo;
            }
        }
        else
        {
            List<Repo> repos = dataCenterService.getRepoList(id, type);
            vo.setRepoList(repos);
            return vo;
        }
    }
    
    @Override
    public Map<Long, Integer> queryStoreStock(StoreStockQO storeStockQO)
    {
        Map<Long, Integer> skuNumsMap = new HashMap<>();
        List<WmsInventoryStock> stockList = redisManagerService.queryStoreStock(storeStockQO.getRepoId(),storeStockQO.getStoreId());
        if (!CollectionUtils.isEmpty(stockList))
        {
            for(WmsInventoryStock stock:stockList)
            {
                if(null!=stock)
                {
                    skuNumsMap.put(stock.getSkuId(), stock.getNums());
                }    
            }
        }
        return skuNumsMap;
    }

    @Override
    public List<SkuManageBO> selectUnLimitStockByRepoId(Long repoId) {
        return wmsStockPersistService.selectUnLimitStockByRepoId(repoId);
    }
    
    @Override
    public List<SkuNumsBO> selectUnlimitStockByCondition(Long skuId, Long repoId, Integer status,String batchNo,Long owner,Integer ownerType)
    {
        if(null!=batchNo&&StringUtils.isEmpty(batchNo.trim()))
        {
            batchNo=null;
        }
        List<SkuNumsBO> skuNumsBoList=wmsStockPersistService.selectUnlimitStockByCondition(skuId,repoId,status,batchNo,owner,ownerType);
        List<SkuNumsBO> returnList=new ArrayList<>();
        if(CollectionUtils.isEmpty(skuNumsBoList)) 
        {
            return returnList;
        }
        Map<String,String> ownerNameMap=new HashMap<>();  
        skuNumsBoList.forEach(bo->{
            if(null!=bo.getOwner()&&null!=bo.getOwnerType())
            {   
                String ownerName=getOwnerName(bo.getOwner(),bo.getOwnerType(),ownerNameMap);
                bo.setOwnerName(ownerName);
            }  
            if(bo.getTotalNums()>0)
            {
                returnList.add(bo);
            }
        });
        return returnList;
    }

    @Override
    public List<FanAndStoreStockInfoVO> queryStockByFanAndStore(WmsStockQO wmsStockQO) {
        //查询repoId
        RepoParam param = new RepoParam();
        param.setExtendsId(wmsStockQO.getId());
        param.setStorageType(StoreTypeEnums.FINISHED.getCode());
        if (OwnerTypeEnum.FRANCHISER == OwnerTypeEnum.getEnum(wmsStockQO.getType())){
            param.setExtendsType(RepoExtendTypeEnum.FRANCHISER.getCode());
        } else if (OwnerTypeEnum.STORE == OwnerTypeEnum.getEnum(wmsStockQO.getType())) {
            param.setExtendsType(RepoExtendTypeEnum.STORE.getCode());
        }else {
            log.warn("其他类型暂不支持。。。");
            return null;
        }
        JsonResult<List<Repo>> dataCenterResult = repoAPI.getByConditions(param);
        if (!dataCenterResult.isSuccess() || CollectionUtils.isEmpty(dataCenterResult.getData())) {
            log.warn("查询仓库失败 {}",JsonUtils.toJson(dataCenterResult));
            return Lists.newArrayList();
        }
        Repo repo = dataCenterResult.getData().get(0);
        wmsStockQO.setRepoId(repo.getId());
        log.info("参数 {}",JsonUtils.toJson(wmsStockQO));

        List<FanAndStoreStockInfoVO> fanAndStoreStockInfoVOS = wmsStockPersistService.queryFranAndStoreStockInfo(wmsStockQO);
        fanAndStoreStockInfoVOS.forEach(vo ->{
            Sku sku = dataCenterService.findBySkuId(vo.getSkuId());
            vo.setSkuName(sku.getSkuName());
            vo.setSkuNo(sku.getSkuNo());
            vo.setSkuUnit(sku.getSkuUnit());
            vo.setSkuTypeName(sku.getSkuType()+"");
        });
        return fanAndStoreStockInfoVOS;
    }

    public String getOwnerName(Long owner,Integer ownerType,Map<String,String> ownerNameMap)
    {
        String key= new StringBuffer().append(owner).append("_").append(ownerType).toString();
        String ownerName = ownerNameMap.get(key);
        if(StringUtils.isEmpty(ownerName))
        {
            ownerName=dataCenterService.findOwnerNameByIdAndType(owner, ownerType);
            if(!StringUtils.isEmpty(ownerName))
            {
                ownerNameMap.putIfAbsent(key, ownerName);
            }
        }  
        return ownerName;
    }
}
