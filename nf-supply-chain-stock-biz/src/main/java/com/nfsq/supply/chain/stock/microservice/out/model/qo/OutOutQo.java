package com.nfsq.supply.chain.stock.microservice.out.model.qo;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * @author xwcai
 * @date 2018/4/14 下午6:09
 */
@ApiModel(value = "出库单查询bean",description = "出库单查询bean")
@Getter
@Setter
public class OutOutQo extends PageBaseBean{

    @ApiModelProperty(value = "outNo",name = "出库单号",dataType = "String",required =true)
    private String outNo;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        if(StringUtils.isBlank(outNo.trim())){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
