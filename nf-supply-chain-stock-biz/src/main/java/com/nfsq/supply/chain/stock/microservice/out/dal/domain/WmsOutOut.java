package com.nfsq.supply.chain.stock.microservice.out.dal.domain;

import java.util.Date;

public class WmsOutOut {
    private Long id;

    private String outNo;

    private Long outNoticeId;

    private Long repoId;

    private Date outDate;

    private Integer operStatus;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    private Integer systemType;

    private String wholeRepoNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOutNo() {
        return outNo;
    }

    public void setOutNo(String outNo) {
        this.outNo = outNo == null ? null : outNo.trim();
    }

    public Long getOutNoticeId() {
        return outNoticeId;
    }

    public void setOutNoticeId(Long outNoticeId) {
        this.outNoticeId = outNoticeId;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public Integer getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(Integer operStatus) {
        this.operStatus = operStatus;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public String getWholeRepoNo() {
        return wholeRepoNo;
    }

    public void setWholeRepoNo(String wholeRepoNo) {
        this.wholeRepoNo = wholeRepoNo;
    }
}