package com.nfsq.supply.chain.stock.microservice.out.dal.domain;

import java.util.Date;

public class WmsOutOutNotice {
    private Long id;

    private String outNoticeNo;

    private Integer type;

    private String typeDes;

    private Long outSourceId;

    private Long repoId;

    private Date planOutDate;

    private Long destinationId;

    private Integer destinationType;

    private Integer operStatus;

    private String bind;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    private Integer systemType;

    private String wholeRepoNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOutNoticeNo() {
        return outNoticeNo;
    }

    public void setOutNoticeNo(String outNoticeNo) {
        this.outNoticeNo = outNoticeNo == null ? null : outNoticeNo.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeDes() {
        return typeDes;
    }

    public void setTypeDes(String typeDes) {
        this.typeDes = typeDes == null ? null : typeDes.trim();
    }

    public Long getOutSourceId() {
        return outSourceId;
    }

    public void setOutSourceId(Long outSourceId) {
        this.outSourceId = outSourceId;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    public Date getPlanOutDate() {
        return planOutDate;
    }

    public void setPlanOutDate(Date planOutDate) {
        this.planOutDate = planOutDate;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public Integer getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(Integer destinationType) {
        this.destinationType = destinationType;
    }

    public Integer getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(Integer operStatus) {
        this.operStatus = operStatus;
    }

    public String getBind() {
        return bind;
    }

    public void setBind(String bind) {
        this.bind = bind == null ? null : bind.trim();
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public String getWholeRepoNo() {
        return wholeRepoNo;
    }

    public void setWholeRepoNo(String wholeRepoNo) {
        this.wholeRepoNo = wholeRepoNo;
    }
}