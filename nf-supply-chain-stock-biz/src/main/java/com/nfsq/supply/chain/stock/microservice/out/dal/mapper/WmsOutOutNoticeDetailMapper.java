package com.nfsq.supply.chain.stock.microservice.out.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.dto.ScanerNoticeDetailDTO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDetailDTO;
import com.nfsq.supply.chain.stock.api.model.qo.OutNoticeQo;
import com.nfsq.supply.chain.stock.api.model.qo.ScanerUnLimitStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityConstants;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface WmsOutOutNoticeDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsOutOutNoticeDetail record);

    int insertSelective(WmsOutOutNoticeDetail record);

    WmsOutOutNoticeDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsOutOutNoticeDetail record);

    int updateByPrimaryKey(WmsOutOutNoticeDetail record);

    void insertBatch(@Param("list") List<WmsOutOutNoticeDetail> list);

    List<WmsOutOutNoticeDetail> selectByNoticeId(@Param("outNoticeId") Long outNoticeId);

    List<WmsOutOutNoticeDetailDTO> selectManageByQOPage(OutNoticeQo qo);

    List<WmsOutOutNoticeDetailDTO> selectManageByQO(OutNoticeQo qo);

    List<WmsOutOutNoticeDetailDTO> selectManageByNoticeIds(@Param("noticeIds") Set<Long> noticeIds);

    int updateRealAmountBatch(@Param("list") List<WmsOutOutNoticeDetail> list);

    List<Integer> selectOperStatusGroupByInNoticeNo(@Param("outNoticeId") Long outNoticeId);

    Long selectOccpyStock(WmsAvaliableStockBO bean);

    @DataAuthority(tableAlias="o",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<SkuManageBO> selectSkuUnSnedCount(@Param("list") List<Long> list);

    /**
     * 保留批次 owner targerOwner
     * @param noticeId
     * @return
     */
    List<ScanerNoticeDetailDTO> selectScanerDetailByNoticeId(@Param("noticeId") Long noticeId);

    /**
     * 不考虑批次 owner targetowner
     * @param noticeId
     * @return
     */
    List<ScanerNoticeDetailDTO> selectScanerByNoticeId(@Param("noticeId") Long noticeId);
    /**
     * 考虑批次 owner targetowner
     * @param noticeId
     * @return
     */
    List<ScanerNoticeDetailDTO> selectScanerByNoticeIdWithWater(@Param("noticeId") Long noticeId);

    List<ScanerNotLimitStockVO> selectScanerSku(ScanerUnLimitStockQO qo);

    int updateByFinishOutNotice(@Param("operStatus") Integer operStatus,@Param("modifiedUser") String modifiedUser,@Param("outNoticeId") Long outNoticeId);

    int deleteByNoticeIds(@Param("outNoticeIds") List<Long> outNoticeIds);
}