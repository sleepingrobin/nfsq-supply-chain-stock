package com.nfsq.supply.chain.stock.microservice.out.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/4/14 下午1:25
 */
@ApiModel(value = "ReqSwagger2",description = "取消出库通知单")
@Getter
@Setter
public class CancelOutNoticeBean extends BaseBean{

    @ApiModelProperty(value = "outNoticeId",name = "出库通知单号",dataType = "Long",required =true)
    private Long outNoticeId;

    @Override
    public JsonResult<?> validate() {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        if(null == outNoticeId || outNoticeId < 0){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(),StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
