package com.nfsq.supply.chain.stock.microservice.in.model.vo;

import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 上午11:22
 */
@ApiModel(value = "ReqSwagger2",description = "入库单")
@Getter
@Setter
public class InInVo {

    @ApiModelProperty(value = "wmsInIn",name = "入库单",dataType = "Object")
    private WmsInIn wmsInIn;

    @ApiModelProperty(value = "wmsInInDetails",name = "入库详情单",dataType = "List")
    private List<WmsInInDetail> wmsInInDetails;
}
