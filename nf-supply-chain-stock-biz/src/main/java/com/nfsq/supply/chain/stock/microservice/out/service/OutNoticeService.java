package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.stock.api.model.bean.*;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.enums.StockOutTypeEnum;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutRule;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.CancelOutNoticeBean;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeListVo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo;
import com.nfsq.supply.chain.utils.common.JsonResult;
import java.util.List;
import java.util.Map;

/**
 * @author xwcai
 * @date 2018/4/13 下午5:08
 */
public interface OutNoticeService {

    /**
     * 批量创建创建出库通知单
     *
     * @param bean 批量创建出库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 下午5:17
     */
    JsonResult<Map<String, String>> listCreateOutOutNotice(OutNoticeListBean bean);

    /**
     * 创建出库通知单
     *
     * @param bean 出库通知单的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/5/18 下午2:09
     */
    JsonResult<String> outOutNoticeCreate(OutNoticeBean bean);

    /**
     * 根据出库通知单单号查询出库通知单详情
     *
     * @param outNoticeId 出库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo
     * @author xwcai
     * @date 2018/4/14 下午1:12
     */
    OutOutNoticeVo getByOutOutNoticeId(Long outNoticeId);

    /**
     * 根据出库通知单单号查询出库通知单详情
     *
     * @param outNoticeId 出库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo
     * @author xwcai
     * @date 2018/4/14 下午1:12
     */
    OutOutNoticeVo selectDetailAndNotFinishByOutNoticeId(Long outNoticeId);

    /**
     * 取消出库通知单
     *
     * @param cancelBean 取消的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午1:27
     */
    JsonResult<Boolean> cancelOutOutNotice(CancelOutNoticeBean cancelBean);

    /**
     * 修改出库通知单
     *
     * @param outNoticeBean 修改的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午2:05
     */
    JsonResult<Boolean> updateOutOutNotice(OutNoticeBean outNoticeBean);

    /**
     * 检查出库通知单状态
     *
     * @param outNoticeVo 出库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午3:48
     */
    JsonResult<Boolean> checkOutOutNotice(OutOutNoticeVo outNoticeVo);

    /**
     * 根据qo 分页查询所有出库通知单
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:56
     */
    JsonResult<OutOutNoticeListVo> queryByQoPage(OutNoticeQo qo);

    /**
     * 根据qo 查询所有出库通知单
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/4/14 下午5:56
     */
    JsonResult<OutNoticeVOList> queryByQo(OutNoticeQo qo);

    /**
     * 根据出库通知单单号查询出库通知单详情
     *
     * @param noticeId 通知单编号
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List               <               com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail>>
     * @author xwcai
     * @date 2018/4/14 下午6:02
     */
    JsonResult<List<WmsOutOutNoticeDetail>> getDetailByNoticeNo(Long noticeId);

    /**
     * 批量更新数量
     *
     * @param detailList  出库通知单详情
     * @param outNoticeVo 出库通知单 包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/18 下午6:57
     */
    void updateOutNoticeDetailWithRealAmount(List<WmsOutOutNoticeDetail> detailList, OutOutNoticeVo outNoticeVo, String modifyUser, WmsOutRule outRule);

    /**
     * 根据条件查询所有可用库存 可用库存的skuId 数量
     *
     * @param bo 查询可用库存的bean
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.model.vo.SkuOccupyVo>
     * @author xwcai
     * @date 2018/5/11 上午10:00
     */
    Long queryOccpyStock(WmsAvaliableStockBO bo);


    /**
     * 查询出库通知单 包含详情
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.out.model.vo.OutNoticeListVo>
     * @author xwcai
     * @date 2018/5/13 下午3:31
     */
    JsonResult<OutNoticeVOList> queryWithDetailByQo(OutNoticeQo qo);

    /**
     * 绑定出库通知单
     *
     * @param bindOutNoticeBean 绑定的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author lyao
     * @date 2018/5/16 下午3:16
     */
    JsonResult<Boolean> bindOutOutNotice(BindOutNoticeBean bindOutNoticeBean);

    /**
     * 根据单号查询出库通知单详情
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    JsonResult<ScanerNoticeVO> queryWithScaner(ScanerNoticeQo qo);

    /**
     * 根据机器码分页查询出库通知单详情（任务：绑定了机器码的就是任务）
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    JsonResult<ScanerNoticePageByScanTypeVO> queryWithScanersPage(ScanerNoticePageQo qo);

    /**
     * 根据单号查询出库通知单详情
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeVo>
     * @author xwcai
     * @date 2018/5/19 下午3:21
     */
    JsonResult<ScanerNoticeVO> queryWithScanerQO(ScanerNoticeQo qo);

    /**
     * 根据qo查询  给管理页面用的分页
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     * @author xwcai
     * @date 2018/5/22 下午7:04
     */
    JsonResult<OutNoticeManageListVO> manageQueryByQO(OutNoticeQo qo);

    /**
     * 根据qo查询  给管理页面用的分页
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     * @author xwcai
     * @date 2018/5/22 下午7:04
     */
    JsonResult<OutNoticeManageListVO> manageQueryWaitByQO(OutNoticeQo qo);

    /**
     * 根据单号Id获取详情list
     *
     * @param qo 查询qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List               <               com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDetailDTO>>
     * @author xwcai
     * @date 2018/5/23 上午9:45
     */
    JsonResult<OutNoticeDetailManagePageVO> manageDetailQuery(OutNoticeQo qo);

    /**
     * 查询未完成
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Long>
     * @author xwcai
     * @date 2018/5/23 上午10:42
     */
    JsonResult<Long> manageQueryUnFinishCount();

    /**
     * 查询未完成
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Long>
     * @author xwcai
     * @date 2018/5/23 上午10:42
     */
    Integer manageQueryProcessCount(StockOutTypeEnum outTypeEnum);

    /**
     * 根据任务号查询出库通知单是否全部完成
     *
     * @param taskId 任务id
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/6/19 下午3:12
     */
    JsonResult<Boolean> checkOutNoticeFinish(Long taskId);

    /**
     * 根据任务查询出库单信息
     *
     * @param taskId 任务id
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.dto.BillDTO>
     * @author xwcai
     * @date 2018/6/20 下午5:01
     */
    List<BillDTO> queryBillNOsByQO(Long taskId);

    /**
     * 根据时间段  查询出库通知单数量
     *
     * @param qo 查询qo
     *
     * @return java.lang.Integer
     * @author xwcai
     * @date 2018/6/21 下午2:42
     */
    Integer queryOutNoticeCount(BillCountQO qo);

    /**
     * 查询所有出库通知单单号
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List                                                               <                                                               java.lang.String>>
     * @author xwcai
     * @date 2018/6/22 上午10:23
     */
    JsonResult<List<String>> queryAllOutNoticeNos();

    /**
     * 根据出库单号查询详情 以及主体
     *
     * @param qo 查询 qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     * @author xwcai
     * @date 2018/6/25 下午6:37
     */
    JsonResult<OutNoticeManageVO> queryWithDetailByNO(OutNoticeQo qo);

    /**
     * 根据主键查询出库通知单详情
     *
     * @param id 出库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice
     * @author xwcai
     * @date 2018/6/28 下午2:27
     */
    WmsOutOutNotice getOutNoticeById(Long id);

    /**
     * 未发货根据sku统计 (sql 写死 只查询门店)
     *
     * @param
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO>
     * @author xwcai
     * @date 2018/7/1 下午3:48
     */
    List<SkuManageBO> selectSkuUnSnedCount();

    /**
     * 查询可用库存
     *
     * @param qo 根据单号，机器码查询
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNotLimitStockVO>
     * @author xwcai
     * @date 2018/7/27 下午1:40
     */
    JsonResult<List<ScanerNotLimitStockVO>> queryNotLimitStock(ScanerUnLimitStockQO qo);

    /**
     * 更新打印次数
     *
     * @param outNoticeNo 通知单号
     *
     * @return int
     * @author hwchen
     */
    int updatePrintTimesByOutNoticeNo(String outNoticeNo);

    /**
     * 根据出库通知单单号list  查询对应所有的出库产品信息
     *
     * @param stockOutSkuInfoQO 查询的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     * @author xwcai
     * @date 2018/9/4 下午4:00
     */
    JsonResult<List<StockOutSkuInfoVO>> selectStockOutSkuInfo(StockOutSkuInfoQO stockOutSkuInfoQO);

    /**
     * 根据出库通知单单号list  查询对应所有的出库产品信息
     *
     * @param stockOutSkuInfoQO 查询的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     * @author xwcai
     * @date 2018/9/4 下午4:00
     */
    JsonResult<Map<String, List<StockOutSkuInfoVO>>> selectStockOutSkuInfoByMap(StockOutSkuInfoQO stockOutSkuInfoQO);

    /**
     * 根据出库通知单单号 查询出库通知单明细的出库方式 true 就是扫码  false 非扫码
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/9/17 下午1:54
     */
    JsonResult<Boolean> checkOutNoticeStockOutScan(OutNoticeQo qo);

    /**
     * 结单操作
     *
     * @param bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/9/24 上午11:00
     */
    JsonResult<Long> manageFinishOutNotice(FinishOutNoticeBean bean);

    /**
     * 根据出库通知单单号list  查询对应所有的出库产品信息
     *
     * @param stockOutSkuInfoQO 查询的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     * @author xwcai
     * @date 2018/9/4 下午4:00
     */
    JsonResult<List<OutOutSapDetailVO>> selectStockOutSapInfo(StockOutSkuInfoQO stockOutSkuInfoQO);

    /**
     * 发送出库消息
     *
     * @param sendOutNoticeBean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/10/10 下午2:48
     */
    JsonResult<Boolean> sendStockOutMsg(SendOutNoticeBean sendOutNoticeBean);

    /**
     * 根据通知单NO 获取通知单信息
     *
     * @param qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ScanerNoticeByScanTypeVO>
     * @author xwcai
     * @date 2019/1/8 上午8:58
     */
    JsonResult<ScanerNoticeByScanTypeVO> queryWithScanerShow(ScanerNoticeQo qo);

    /**
     * 批量根据出库单号查询详情 以及主体
     *
     * @param qo 查询 qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.OutNoticeManageVO>
     * @author xwcai
     * @date 2018/6/25 下午6:37
     */
    JsonResult<List<OutNoticeManageVO>> batchQueryWithDetailByNOs(OutNoticeBatchQO qo);

    /**
     * 批量根据出库单号查询是否已被接单
     * @param qo
     * @return
     */
    JsonResult<Boolean> checkAcceptOrderStatus(OutNoticeBatchQO qo);

    /**
     * 批量根据出库单号删除出库通知单和出库通知详情单
     * @param qo
     * @return
     */
    JsonResult<Boolean> deleteNoticeAndDetail(OutNoticeBatchQO qo);
}
