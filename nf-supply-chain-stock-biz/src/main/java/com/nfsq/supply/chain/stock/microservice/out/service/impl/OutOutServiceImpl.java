package com.nfsq.supply.chain.stock.microservice.out.service.impl;

import com.nfsq.supply.chain.datacenter.api.RepoAPI;
import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.datacenter.api.response.RepoAndExtendInfo;
import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockOutBean;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.IsPageEnum;
import com.nfsq.supply.chain.stock.api.model.enums.ScanerType;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.common.config.StockConfig;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.DataCenterService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.*;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutDetailMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.mapper.WmsOutOutNoticeDetailMapper;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.SkuInfoWithSkuNoBean;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.AutoCommitEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.CheckRuleEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.enums.OutBatchEnums;
import com.nfsq.supply.chain.stock.microservice.out.model.qo.OutOutQo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutListVo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutOutService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutRuleService;
import com.nfsq.supply.chain.utils.common.BigDecimalUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xwcai
 * @date 2018/4/13 下午5:09
 */
@Component
@Slf4j
public class OutOutServiceImpl implements OutOutService {

    @Autowired
    WmsOutOutMapper wmsOutOutMapper;

    @Autowired
    WmsOutOutDetailMapper wmsOutOutDetailMapper;

    @Autowired
    OutNoticeService outNoticeService;

    @Autowired
    WmsInventoryStockService wmsInventoryStockService;

    @Autowired
    BaseInfoService baseInfoService;

    @Autowired
    WmsOutOutNoticeDetailMapper wmsOutOutNoticeDetailMapper;

    @Autowired
    RepoAPI repoAPI;

    @Autowired
    StockConfig stockConfig;

    @Autowired
    DataCenterService dataCenterService;

    @Autowired
    AsyncServiceImpl asyncService;

    @Autowired
    OutRuleService outRuleService;

    @Override
    public JsonResult<String> createOutOutWithSap(StockOutBean outBean) {
        JsonResult<String> jsonResult = ((OutOutServiceImpl) AopContext.currentProxy()).createOutOut(outBean);
        return jsonResult;
    }

    @Override
    public JsonResult<String> createOutOut(StockOutBean outBean) {
        List<WmsOutOutDetail> detailList = new ArrayList<>();
        JsonResult<String> jsonResult = ((OutOutServiceImpl) AopContext.currentProxy()).createOutOutWithOutSendMsg(outBean,detailList);
        return jsonResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public JsonResult<String> createOutOutWithOutSendMsg(StockOutBean outBean,List<WmsOutOutDetail> detailList){
        JsonResult<String> jsonResult = new JsonResult<>();
        //检查出库通知单单号是否正确
        OutOutNoticeVo outNoticeVo = outNoticeService.selectDetailAndNotFinishByOutNoticeId(outBean.getOutNoticeId());
        JsonResult<Boolean> checkResult = outNoticeService.checkOutOutNotice(outNoticeVo);
        if (!checkResult.isSuccess()) {
            jsonResult.setMsg(checkResult.getMsg());
            return jsonResult;
        }
        //根据系统类型获取对应的出库规则
        WmsOutRule outRule = outRuleService.selectBySystemType(outNoticeVo.getWmsOutOutNotice().getSystemType());
        if (null == outRule) {
            log.error("查询不到对应的出库规则,系统类型：{}", outNoticeVo.getWmsOutOutNotice().getSystemType());
            jsonResult.setMsg("查询不到对应的出库规则");
            return jsonResult;
        }
        //校验出库通知单 并构建需要修改的出库通知详情单
        JsonResult<Map<String, Object>> detailResult = this.getOutDetails(outBean, outNoticeVo, outRule);
        if (!detailResult.isSuccess()) {
            jsonResult.generateCodeAndMsgInfo(detailResult.getCode(), detailResult.getMsg());
            return jsonResult;
        }
        //判断出库详情单
        if (null == detailResult.getData() || detailResult.getData().isEmpty()) {
            log.info("生成出库详情单失败,出库通知单单号：" + outBean.getOutNoticeId());
            jsonResult.setMsg("生成出库详情单失败,出库通知单单号：" + outBean.getOutNoticeId());
            return jsonResult;
        }
        //生成出库单
        String noticeNo = this.createOutOutOrder(outBean, detailResult.getData(), outNoticeVo, outRule,detailList);

        jsonResult.setSuccess(true);
        jsonResult.setData(noticeNo);
        return jsonResult;
    }

    @Override
    public List<WmsOutOut> getByOutNoticeId(Long outNoticeId) {
        return wmsOutOutMapper.selectByOutNoticeId(outNoticeId);
    }

    @Override
    public JsonResult<OutOutListVo> queryOutByQo(OutOutQo qo) {
        JsonResult<OutOutListVo> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        OutOutListVo outOutListVo = new OutOutListVo();
        List<WmsOutOut> outOutList = wmsOutOutMapper.selectByQoPage(qo);
        BeanUtils.copyProperties(qo, outOutListVo);
        //如果查询出来没有记录 直接返回
        if (null == outOutList || outOutList.isEmpty()) {
            return jsonResult;
        }
        outOutListVo.setOutOutList(outOutList);
        jsonResult.setData(outOutListVo);
        return jsonResult;
    }

    @Override
    public JsonResult<List<WmsOutOutDetail>> getDetailByOutId(Long outId) {
        JsonResult<List<WmsOutOutDetail>> jsonResult = new JsonResult<>();
        jsonResult.setSuccess(true);
        List<WmsOutOutDetail> outOutDetails = wmsOutOutDetailMapper.selectByOutId(outId);
        //如果查询出来没有记录 直接返回
        if (null == outOutDetails || outOutDetails.isEmpty()) {
            return jsonResult;
        }
        jsonResult.setData(outOutDetails);
        return jsonResult;
    }

    @Override
    public JsonResult<OutManageListVO> manageQueryByQo(OutOutPageQO qo) {
        JsonResult<OutManageListVO> jsonResult = new JsonResult<>();
        OutManageListVO manageListVO = new OutManageListVO();
        List<OutManageVO> manageList = wmsOutOutMapper.selectManageByQoPage(qo);
        BeanUtils.copyProperties(qo, manageListVO);
        if (null == manageList || manageList.isEmpty()) {
            jsonResult.setSuccess(true);
            jsonResult.setData(manageListVO);
            return jsonResult;
        }

        for (OutManageVO vo : manageList) {
            //获取状态名成
            this.getSupplyNameAndOutRepo(vo);
        }
        manageListVO.setVoList(manageList);
        jsonResult.setData(manageListVO);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 获取仓库名称
     *
     * @author xwcai
     * @date 2018/9/29 下午1:49
     * @param vo
     * @return void
     */
    private void getSupplyNameAndOutRepo(OutManageVO vo){
        //获取状态名成
        StockOperStatusEnum statusEnum = StockOperStatusEnum.getByType(vo.getOperStatus());
        vo.setOperStatusDesc(statusEnum.getTypeDes());
        //获取目的地 供货方名称 联系电话
        JsonResult<Map<Long, RepoAndExtendInfo>> repoResult = repoAPI.getExtendsInfoById(vo.getSupplyStockId(), vo.getDestinationStockId());
        //如果请求不成功
        if (!repoResult.isSuccess() || null == repoResult.getData() || repoResult.getData().isEmpty()) {
            log.info("获取供货方 送达方 名称信息失败" + repoResult.getMsg());
        } else {
            Map<Long, RepoAndExtendInfo> map = repoResult.getData();
            //供货方仓库
            RepoAndExtendInfo suppluStock = map.get(vo.getSupplyStockId());
            if(null != suppluStock){
                vo.setSupplyName(suppluStock.getRepoName());
            }
            //目的地仓库
            RepoAndExtendInfo destionationStock = map.get(vo.getDestinationStockId());
            if(null != destionationStock){
                vo.setDestinationName(destionationStock.getRepoName());
                //目的地 供货方名称 联系电话
                vo.setAddress(destionationStock.getRepoAddr());
                vo.setContactName(destionationStock.getRepoConsignee());
                vo.setPhoneNum(destionationStock.getRepoConsigneeTel());
            }

        }
    }

    @Override
    public JsonResult<OutManageVO> manageDetailQueryByQo(OutOutPageQO qo) {
        JsonResult<OutManageVO> jsonResult = new JsonResult<>();
        if(StringUtils.isEmpty(qo.getOutNo())){
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(),StockResultCode.PARAM_EMPTY.getDesc());
            return jsonResult;
        }
        OutManageVO vo = wmsOutOutMapper.selectManageByNo(qo.getOutNo());
        if(null == vo){
            jsonResult.setSuccess(true);
            return jsonResult;
        }
        OutDetailManagePageVO voPage = new OutDetailManagePageVO();
        List<OutDetailManageVO> voList = new ArrayList<>();
        qo.setId(vo.getId());
        if(IsPageEnum.IS_PAGE.getCode().equals(qo.getIsPage())){
            voList = wmsOutOutDetailMapper.selectManageByQoPage(qo);
        }else{
            voList = wmsOutOutDetailMapper.selectManageByQo(qo);
        }

        jsonResult.setSuccess(true);
        BeanUtils.copyProperties(qo, voPage);
        if(CollectionUtils.isEmpty(voList)){
            return jsonResult;
        }
        this.getSupplyNameAndOutRepo(vo);
        //翻译中文
        for (OutDetailManageVO detailManageVO : voList) {
            this.getChineseByOutBill(detailManageVO);
        }

        voPage.setVoList(voList);
        vo.setDetailManageVO(voPage);
        jsonResult.setData(vo);
        return jsonResult;
    }

    /**
     * 将一些字段 翻译为中文
     *
     * @author xwcai
     * @date 2018/9/6 下午1:14
     * @param vo
     * @return void
     */
    private void getChineseByOutBill(OutDetailManageVO vo){
        //扫码方式
        ScanerType scanerType = ScanerType.getByType(vo.getScanType());
        if (null != scanerType) {
            vo.setScanTypeDesc(scanerType.getDesc());
        }
        //去除0
        vo.setSkuNo(vo.getSkuNo().replaceAll("^(0+)", ""));

        //设置净重 毛重
        if (null == vo.getGrossWeight() || vo.getGrossWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(vo.getWeightUnit())) {
            vo.setGrossWeightStr("");
        } else {
            vo.setGrossWeightStr(vo.getGrossWeight().toString() + vo.getWeightUnit());
        }
        if (null == vo.getNetWeight() || vo.getNetWeight().compareTo(new BigDecimal("0")) == 0 || StringUtils.isEmpty(vo.getWeightUnit())) {
            vo.setNetWeightStr("");
        } else {
            vo.setNetWeightStr(vo.getNetWeight().toString() + vo.getWeightUnit());
        }
        //获取owner 名称
        String ownerName = dataCenterService.findOwnerNameByIdAndType(vo.getOwner(), vo.getOwnerType());
        if (StringUtils.isNotEmpty(ownerName)) {
            vo.setOwnerName(ownerName);
        }
    }

    @Override
    public List<BillDTO> queryNOsByTaskId(Long taskId) {
        return wmsOutOutMapper.queryBillNOsByQO(taskId);
    }

    @Override
    public Integer queryOutOutCount(BillCountQO qo) {
        return wmsOutOutMapper.selectCountByQO(qo);
    }

    @Override
    public JsonResult<List<String>> queryAllOutNos() {
        JsonResult<List<String>> jsonResult = new JsonResult<>();
        List<String> outNoticeNos = wmsOutOutMapper.selectAllOutNos();
        if (null != outNoticeNos && !outNoticeNos.isEmpty()) {
            jsonResult.setData(outNoticeNos);
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public List<SkuManageBO> selectSkuSnedCount() {
        return wmsOutOutDetailMapper.selectSkuSnedCount(stockConfig.getUnLimitSkuIds());
    }

    @Override
    public List<SkuInfoWithSkuNoBean> selectSkuInfoByOutNoticeId(Long outNoticeId) {
        return wmsOutOutDetailMapper.selectSkuInfoByOutNoticeId(outNoticeId);
    }

    @Override
    public List<StockOutSkuInfoVO> selectStockOutSkuInfo(List<String> outNoticeNos) {
        return wmsOutOutDetailMapper.selectStockOutSkuInfo(outNoticeNos);
    }

    @Override
    public List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(List<String> outNoticeNos) {
        return wmsOutOutDetailMapper.selectStockOutSkuInfoByMap(outNoticeNos);
    }

    @Override
    public List<OutOutSapDetailVO> selectStockOutSapInfo(List<String> outNoticeNos) {
        return wmsOutOutDetailMapper.selectStockOutSapInfo(outNoticeNos);
    }

    @Override
    public WmsOutOut selectOutBillByOutNo(String outNo) {
        return wmsOutOutMapper.selectByNo(outNo);
    }

    @Override
    public List<WmsOutOutDetail> selectOutDetailByOutId(Long outId) {
        return wmsOutOutDetailMapper.selectByOutId(outId);
    }

    @Override
    public List<Integer> selectScanTypeByNoticeId(Long outNoticeId) {
        return wmsOutOutDetailMapper.selectScanTypeByNoticeId(outNoticeId);
    }

    /**
     * 创建出库通知详情单
     *
     * @param outBean     创建 出库单的bean
     * @param map         构建好的 出库单详情 更新的bean
     * @param outNoticeVo 出库通知单 包含详情
     *
     * @return void
     * @author xwcai
     * @date 2018/4/24 下午3:12
     */
    @SuppressWarnings("unchecked")
    private String createOutOutOrder(StockOutBean outBean, Map<String, Object> map, OutOutNoticeVo outNoticeVo, WmsOutRule outRule,List<WmsOutOutDetail> outOutDetails) {

        //创建出库单
        WmsOutOut wmsOutOut = this.buildWmsOutOut(outBean, outNoticeVo.getWmsOutOutNotice());
        wmsOutOutMapper.insert(wmsOutOut);

        //创建出库详情单
        outOutDetails.addAll((List<WmsOutOutDetail>) map.get(InOutConstants.INSERT_ORDER));
        wmsOutOutDetailMapper.insertBatch(outOutDetails, wmsOutOut);

        //出库通知详情 修改
        List<WmsOutOutNoticeDetail> noticeDetails = (List<WmsOutOutNoticeDetail>) map.get(InOutConstants.UPDATE_NOTICE);
        Map<Long, WmsOutOutNoticeDetail> noticeDetailMap = noticeDetails.stream().collect(Collectors.toMap(WmsOutOutNoticeDetail::getId, a -> a, (k1, k2) -> k1));

        //库存变化
        List<WmsStockBean> wmsStockBeans = this.buildWithOutList(outOutDetails, outBean, noticeDetailMap, wmsOutOut);
        wmsInventoryStockService.updateStock(wmsStockBeans);

        //修改出库通知详情单状态
        outNoticeService.updateOutNoticeDetailWithRealAmount(noticeDetails, outNoticeVo, outBean.getUserId(), outRule);

        return wmsOutOut.getOutNo();
    }

    /**
     * 构建出库单
     *
     * @param outBean 创建 出库单的bean
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut
     * @author xwcai
     * @date 2018/4/14 下午3:08
     */
    private WmsOutOut buildWmsOutOut(StockOutBean outBean, WmsOutOutNotice wmsOutOutNotice) {
        WmsOutOut wmsOutOut = new WmsOutOut();
        Date now = new Date();
        //构建创建人 创建时间 修改人 修改时间
        wmsOutOut.setCreateDate(now);
        wmsOutOut.setModifiedDate(now);
        wmsOutOut.setCreateUser(outBean.getUserId());
        wmsOutOut.setModifiedUser(outBean.getUserId());

        //构建参数 出库时间就是当前时间
        wmsOutOut.setOutDate(now);
        wmsOutOut.setOutNoticeId(outBean.getOutNoticeId());
        wmsOutOut.setRepoId(outBean.getRepoId());
        wmsOutOut.setOperStatus(StockOperStatusEnum.FINISH.getType());
        //出库单单号
        wmsOutOut.setOutNo(this.getOutOutId());
        //系统类型
        wmsOutOut.setSystemType(wmsOutOutNotice.getSystemType());

        //设置数据权限
        wmsOutOut.setWholeRepoNo(wmsOutOutNotice.getWholeRepoNo());
        return wmsOutOut;
    }

    /**
     * 出库单ID 生成
     *
     * @return java.lang.String
     * @author xwcai
     * @date 2018/4/13 下午3:40
     */
    private String getOutOutId() {
        return baseInfoService.getNOByCharacter(InOutConstants.OUT_OUT_NO_STR);
    }

    /**
     * 获取出库单详情
     *
     * @param outBean     创建出库单的bean
     * @param outNoticeVo 出库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.Listcom.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail>>
     * @author xwcai
     * @date 2018/4/14 下午3:04
     */
    private JsonResult<Map<String, Object>> getOutDetails(StockOutBean outBean, OutOutNoticeVo outNoticeVo, WmsOutRule outRule) {
        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();

        //先校验出库单信息
        JsonResult<Boolean> outResult = this.checkOutNoticeInfo(outBean, outNoticeVo);
        if (!outResult.isSuccess()) {
            jsonResult.generateCodeAndMsgInfo(outResult.getCode(), outResult.getMsg());
            return jsonResult;
        }

        //判断出库单详细sku信息
        jsonResult = this.getSku(outBean, outNoticeVo, outRule);
        if (!jsonResult.isSuccess()) {
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 出库信息 和 出库通知单信息  校验
     *
     * @param outBean     创建出库单的bean
     * @param outNoticeVo 出库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/14 下午2:50
     */
    private JsonResult<Boolean> checkOutNoticeInfo(StockOutBean outBean, OutOutNoticeVo outNoticeVo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        //先判断获取到的出库单是否正确
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNotice() || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty()) {
            log.info("该出库通知单不存在 出库通知单ID：" + outBean.getOutNoticeId());
            jsonResult.setMsg("该出库通知单不存在");
            return jsonResult;
        }
        //判断是否是同一个仓库
        if (!outBean.getRepoId().equals(outNoticeVo.getWmsOutOutNotice().getRepoId())) {
            log.info("该出库通知单 仓库 不正确 仓库ID：" + outBean.getRepoId());
            jsonResult.setMsg("该出库通知单 仓库 不正确");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 出库单 和 出库单详情的对比
     *
     * @param outBean     创建出库单的bean
     * @param outNoticeVo 出库通知单包含详情
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail>>
     * @author xwcai
     * @date 2018/4/14 下午3:04
     */
    private JsonResult<Map<String, Object>> getSku(StockOutBean outBean, OutOutNoticeVo outNoticeVo, WmsOutRule outRule) {
        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();
        List<SkuInfoBean> scanList = outBean.getSkuInfoList();
        if (null == outNoticeVo || null == outNoticeVo.getWmsOutOutNoticeDetails() || outNoticeVo.getWmsOutOutNoticeDetails().isEmpty()) {
            log.info("出库通知单详情不存在，该出库通知单编号：" + outBean.getOutNoticeId());
            jsonResult.setMsg("出库通知单详情不存在");
            return jsonResult;
        }
        //获取参数
        return this.getParam(outBean, scanList, outNoticeVo.getWmsOutOutNoticeDetails(), outRule);
    }

    /**
     * 出库时候 sku校验 并构建需要修改的出库详情单
     *
     * @param outBean     创建出库单的bean
     * @param scanList    扫码的sku信息
     * @param detailsList 出库通知单中的sku
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail>>
     * @author xwcai
     * @date 2018/4/23 上午11:54
     */
    private JsonResult<Map<String, Object>> getParam(StockOutBean outBean, List<SkuInfoBean> scanList, List<WmsOutOutNoticeDetail> detailsList, WmsOutRule outRule) {

        JsonResult<Map<String, Object>> jsonResult = new JsonResult<>();

        Map<String, Object> map = new HashMap<>();
        //返回错误的信息 只要有值 就必须 返回失败 判断是否有多扫
        StringBuilder returnStr = new StringBuilder();
        List<WmsOutOutDetail> outDetailList = this.getOutOutDetailByStockOut(scanList, detailsList, returnStr);
        //构建更新的出库通知详情单
        //是否所有sku都有出库 化妆品 ： 必须全部有值
        StringBuilder mayOutStr = new StringBuilder();
        //对比出库通知单中的 是否有出库，并且构建update的List  判断是否有少扫的情况
        List<WmsOutOutNoticeDetail> updateOutNoticeList = this.getUpdateOutNotceByStockOut(scanList, detailsList, outBean.getModifyId(), mayOutStr, outRule);

        //最后 检查 出库满足不满足 业务要求  化妆品：必须所有商品全部出库 判断是否允许大于 小于 等于
        //出库的通知单 区分 扫码 不扫码的
        StringBuilder scanStr = this.checkOutNoticeAllOutGroupScanType(updateOutNoticeList, outRule);

        //根据业务类型 判断是否能够继续出库流程 目前只有化妆品
        if (mayOutStr.length() > 0) {
            returnStr.append(mayOutStr);
        }
        if (scanStr.length() > 0) {
            returnStr.append(scanStr);
        }
        //如果返回的Str 不为null  就认为失败，返回
        if (returnStr.length() > 0) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg(returnStr.toString());
            return jsonResult;
        }

        //分组两个扫码类型 分别比较 有一个不通过  就是不通过 通过出库通知单的 去找 出库的
        map.put(InOutConstants.UPDATE_NOTICE, updateOutNoticeList);
        map.put(InOutConstants.INSERT_ORDER, outDetailList);
        jsonResult.setData(map);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 根据是否扫码分类 检查是否全部完成出库
     *
     * @param updateOutNoticeList 更新的出库通知单列表
     *
     * @return java.lang.StringBuilder
     * @author xwcai
     * @date 2018/8/1 上午11:03
     */
    private StringBuilder checkOutNoticeAllOutGroupScanType(List<WmsOutOutNoticeDetail> updateOutNoticeList, WmsOutRule outRule) {
        StringBuilder scanStr = new StringBuilder();
        Map<Integer, List<WmsOutOutNoticeDetail>> scanTypeWmsMap = updateOutNoticeList.stream().collect(Collectors.groupingBy(WmsOutOutNoticeDetail::getScanType, Collectors.toList()));
        scanTypeWmsMap.forEach((scanType, detail) -> detail.forEach(wms -> this.checkStockOutNum(wms, scanStr, outRule)));
        return scanStr;
    }

    /**
     * 出库数量检查，根据出库规则
     *
     * @param wms     出库的
     * @param scanStr 返回的错误信息
     * @param outRule 出库规则
     *
     * @return void
     * @author xwcai
     * @date 2018/9/3 上午11:32
     */
    private void checkStockOutNum(WmsOutOutNoticeDetail wms, StringBuilder scanStr, WmsOutRule outRule) {
        if (wms.getOperStatus().equals(StockOperStatusEnum.STARTING.getType()) && !CheckRuleEnums.checkAllowOutLessThan(outRule.getCheckRule())) {
            this.containBatchStr(wms,scanStr,outRule);
            scanStr.append("出库数量小于需求数量\n");
            log.info("出库数量小于需求数量,{}", wms.getSkuName());
        }
        if (wms.getOperStatus().equals(StockOperStatusEnum.MORE.getType()) && !CheckRuleEnums.checkAllowOutMoreThan(outRule.getCheckRule())) {
            this.containBatchStr(wms,scanStr,outRule);
            scanStr.append("出库数量超出需求数量\n");
            log.info("出库数量超出需求数量,{}", wms.getSkuName());
        }
        if (wms.getOperStatus().equals(StockOperStatusEnum.FINISH.getType()) && !CheckRuleEnums.checkAllowOutEqual(outRule.getCheckRule())) {
            this.containBatchStr(wms,scanStr,outRule);
            scanStr.append("出库数量等于需求数量\n");
            log.info("出库数量等于需求数量,{}", wms.getSkuName());
        }
        //如果是自动结单的 就要将超出的设置为进行中 如果不是自动结单的 就全部都是进行中
        if(AutoCommitEnums.COMMIT.getCode().equals(outRule.getIsAutoCommit())){
            if(wms.getOperStatus().equals(StockOperStatusEnum.MORE.getType())){
                wms.setOperStatus(StockOperStatusEnum.STARTING.getType());
            }
        }else{
            wms.setOperStatus(StockOperStatusEnum.STARTING.getType());
        }
    }

    /**
     * 是否拼接批次
     *
     * @author xwcai
     * @date 2018/9/18 下午2:53
     * @param scanStr
     * @param outRule
     * @return void
     */
    private void containBatchStr(WmsOutOutNoticeDetail wms,StringBuilder scanStr, WmsOutRule outRule){
        scanStr.append(wms.getSkuName());
        if(SystemTypeEnum.WATER.getCode().equals(outRule.getSystemType()) && StringUtils.isNotEmpty(wms.getBatchNo())){
            if(InOutConstants.DEFAULT_BATCH_NO.equals(wms.getBatchNo())){
                scanStr.append("，批次：").append(InOutConstants.DEFAULT_BATCH_NO_CHINESE).append("，");
            }else{
                scanStr.append("，批次：").append(wms.getBatchNo()).append("，");
            }
        }
    }

    /**
     * 构建更新的出库通知单  并计算出库的总数量 出库通知单的状态设置 检查是否有出库未出的sku
     *
     * @param skuList     出库的skuList
     * @param detailsList 需要出库的skuList
     * @param modifyUser  修改人
     * @param mayOutStr   返回的提示信息
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail>
     * @author xwcai
     * @date 2018/8/1 上午11:05
     */
    private List<WmsOutOutNoticeDetail> getUpdateOutNotceByStockOut(List<SkuInfoBean> skuList, List<WmsOutOutNoticeDetail> detailsList, String modifyUser, StringBuilder mayOutStr, WmsOutRule outRule) {
        List<WmsOutOutNoticeDetail> updateOutNoticeList = new ArrayList<>();
        //先判断本次出库的sku中 扫码类型有几种
        Map<Integer, List<SkuInfoBean>> scanTypeMap = skuList.stream().collect(Collectors.groupingBy(sku -> {
            return ScanerType.FILL_A_VACANCY.getType().equals(sku.getScanType()) ? ScanerType.SCAN.getType() : sku.getScanType();
        }));
        Map<Integer, List<WmsOutOutNoticeDetail>> scanTypeWmsMap = detailsList.stream().collect(Collectors.groupingBy(WmsOutOutNoticeDetail::getScanType));
        for (Map.Entry<Integer, List<SkuInfoBean>> skuScanMap : scanTypeMap.entrySet()) {
            //获取对应扫码方式的出库通知单
            List<WmsOutOutNoticeDetail> scanTypeWmsList = scanTypeWmsMap.get(skuScanMap.getKey());
            if (CollectionUtils.isEmpty(scanTypeWmsList)) {
                log.info("扫码方式,没有出库需求,{}", skuScanMap.getKey());
                mayOutStr.append("扫码方式").append("没有出库需求\n");
                continue;
            }
            for (WmsOutOutNoticeDetail detail : scanTypeWmsList) {
                //计算实际出库数量 扫码方式 不用加入判断 Long exami
                BigDecimal amount = skuScanMap.getValue().stream()
                        .filter(sku -> this.stockOutCheck(detail, sku))
                        .map(sku -> BigDecimal.valueOf(sku.getAmount()))
                        .reduce(new BigDecimal("0"), BigDecimal::add);
                //判断是否有出库
                if (null == amount || amount.compareTo(BigDecimal.valueOf(0)) <= 0) {
                    log.info("本次出库不包括,{}", detail.getSkuName());
                    //如果不允许分批出库 就把提示信息写入
                    if (OutBatchEnums.NOT_BATCH.getCode().equals(outRule.getIsInBatch())) {
                        this.containBatchStr(detail,mayOutStr,outRule);
                        mayOutStr.append("出库数量小于需求数量\n");
                    }
                    continue;
                }
                //构建修改的bean
                this.buildUpdateWmsOutNoticeDetail(detail, amount, modifyUser);
                //设置更新的detail内容
                updateOutNoticeList.add(detail);
            }
        }
        return updateOutNoticeList;
    }

    /**
     * 构建需要更新的出库通知单数据
     *
     * @param detail     出库通知单
     * @param amount     实际出库数量
     * @param modifyUser 修改人
     *
     * @return void
     * @author xwcai
     * @date 2018/8/1 下午2:18
     */
    private void buildUpdateWmsOutNoticeDetail(WmsOutOutNoticeDetail detail, BigDecimal amount, String modifyUser) {
        //更新实际出库的数量
        Integer compareFlag = amount.compareTo(BigDecimalUtils.sub(detail.getAmount(), detail.getRealAmount()));
        //出库数量的比对 如果小于等于 证明 可以出库 ；大于不能出库
        //设置状态
        if (compareFlag < 0) {
            detail.setOperStatus(StockOperStatusEnum.STARTING.getType());
        } else if (compareFlag == 0) {
            detail.setOperStatus(StockOperStatusEnum.FINISH.getType());
        } else {
            detail.setOperStatus(StockOperStatusEnum.MORE.getType());
        }
        //构建修改的bean
        detail.setRealAmount(amount.intValue());
        detail.setModifiedUser(modifyUser);
        detail.setModifiedDate(new Date());
    }

    /**
     * 检查本次出库的sku 是否符合要求  状态 skuId 扫码类型 并构建出库详情单List
     *
     * @param scanList    出库的skuList
     * @param detailsList 需要出库的skuList
     * @param returnStr   返回的提示信息
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail>
     * @author xwcai
     * @date 2018/8/1 上午11:06
     */
    private List<WmsOutOutDetail> getOutOutDetailByStockOut(List<SkuInfoBean> scanList, List<WmsOutOutNoticeDetail> detailsList, StringBuilder returnStr) {
        List<WmsOutOutDetail> outDetailList = new ArrayList<>();
        //对比出库的传入的sku  是否符合出库通知单中
        for (SkuInfoBean skuInfoBean : scanList) {
            //根据传入的参数 判断是否有对应的出库通知单
            Optional<WmsOutOutNoticeDetail> detailOptional = detailsList.stream()
                    .filter(wmsDetail -> this.stockOutCheck(wmsDetail, skuInfoBean))
                    .findFirst();
            WmsOutOutNoticeDetail detail = null;
            //如果为空 就记录错误等待返回
            if (detailOptional.isPresent()) {
                detail = detailOptional.get();
            }
            if (null == detail) {
                log.warn("物料ID：" + skuInfoBean.getSkuId() + "物料批次：" + skuInfoBean.getBatchNo() + "不包含在此次出库通知单中");
                returnStr.append("物料ID：").append(skuInfoBean.getSkuId().toString()).append("物料批次：").append(skuInfoBean.getBatchNo()).append("不包含在此次出库通知单中\n");
                continue;
            }
            //构建的时候 先计算本次出库单的重量
            WmsOutOutDetail outDetail = this.stockOutBuildOutDetail(detail, skuInfoBean);
            outDetailList.add(outDetail);
        }
        return outDetailList;
    }

    /**
     * 出库校验 从扫码结果检查 出库通知单详情
     *
     * @param wmsDetail   出库通知单详情
     * @param skuInfoBean 扫码结果
     *
     * @return java.lang.Boolean
     * @author xwcai
     * @date 2018/9/3 上午11:07
     */
    private Boolean stockOutCheck(WmsOutOutNoticeDetail wmsDetail, SkuInfoBean skuInfoBean) {
        //先检查 skuId 状态 扫码方式 是否匹配
        if (wmsDetail.getSkuId().equals(skuInfoBean.getSkuId()) && wmsDetail.getStatus().equals(skuInfoBean.getStatus()) && ScanerType.scanEquals(wmsDetail.getScanType(),skuInfoBean.getScanType())) {
            Boolean flag = Boolean.TRUE;
            //再检查owner 如果没有 就不检查
            if (null != wmsDetail.getOwner() && null != wmsDetail.getOwnerType()) {
                flag = wmsDetail.getOwner().equals(skuInfoBean.getOwner()) && wmsDetail.getOwnerType().equals(skuInfoBean.getOwnerType());
                if(flag && StringUtils.isNotEmpty(wmsDetail.getBatchNo())){
                    //最后检查批次 如果没有就不检查
                    flag = wmsDetail.getBatchNo().equals(skuInfoBean.getBatchNo());
                }
            }else{
                //最后检查批次 如果没有就不检查
                if (StringUtils.isNotEmpty(wmsDetail.getBatchNo())) {
                    flag = wmsDetail.getBatchNo().equals(skuInfoBean.getBatchNo());
                }
            }
            return flag;
        }
        return Boolean.FALSE;
    }

    /**
     * 出库时构建出库详情
     *
     * @param detail      出库通知单详情
     * @param skuInfoBean sku详情
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail
     * @author xwcai
     * @date 2018/9/3 上午10:56
     */
    private WmsOutOutDetail stockOutBuildOutDetail(WmsOutOutNoticeDetail detail, SkuInfoBean skuInfoBean) {
        //构建的时候 先计算本次出库单的重量
        WmsOutOutDetail outDetail = this.buildOutOutDetail(detail, skuInfoBean);

        if (null != detail.getGrossWeight() && null != detail.getNetWeight() && StringUtils.isNotEmpty(detail.getWeightUnit())) {
            outDetail.setWeightUnit(detail.getWeightUnit());
            //设置重量 要先计算单位重量 然后给出库单详情计算重量 然后再计算出库通知单本次增加的重量
            BigDecimal grossWeightPro = BigDecimalUtils.div(detail.getGrossWeight(), detail.getAmount(), 3);
            BigDecimal netWeightPro = BigDecimalUtils.div(detail.getNetWeight(), detail.getAmount(), 3);
            //设置出库通知单中的重量
            outDetail.setGrossWeight(BigDecimalUtils.mul(grossWeightPro, skuInfoBean.getAmount(), 3));
            outDetail.setNetWeight(BigDecimalUtils.mul(netWeightPro, skuInfoBean.getAmount(), 3));

        }
        return outDetail;
    }

    /**
     * 构建修改库存的bean
     *
     * @param detail          出库通知详情
     * @param outBean         出库单的bean
     * @param noticeDetailMap 出库通知单详情
     *
     * @return com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean
     * @author xwcai
     * @date 2018/6/6 上午9:48
     */
    private WmsStockBean buildWithOut(WmsOutOutDetail detail, StockOutBean outBean, Map<Long, WmsOutOutNoticeDetail> noticeDetailMap, WmsOutOut wmsOutOut) {
        WmsStockBean bean = new WmsStockBean();
        //构建sku相关
        bean.setStatus(detail.getStatus());
        bean.setBatchNo(detail.getBatchNo());
        bean.setNums(detail.getAmount());
        bean.setSkuId(detail.getSkuId());

        //构建创建时间 修改时间 创建人 修改人
        bean.setModifiedUser(detail.getModifiedUser());
        bean.setCreatedUser(detail.getModifiedUser());
        bean.setModifiedDate(detail.getModifiedDate());
        bean.setCreatedDate(detail.getModifiedDate());

        //根据出库通知单id 获取用途信息
        WmsOutOutNoticeDetail noticeDetail = noticeDetailMap.get(detail.getOutNoticeDetailId());
        //构建用途
        bean.setRuleGroup(noticeDetail.getRuleGroup());
        bean.setRuleType(noticeDetail.getRuleType());
        bean.setDealerId(noticeDetail.getRuleDetail());

        //构建仓库
        bean.setRepoId(outBean.getRepoId());
        //设置出库单Id
        bean.setOrderDetailId(detail.getId());
        //设置操作类型
        bean.setType(InOutTypeEnums.OUT_STOCK.getCode());
        bean.setOwner(detail.getOwner());
        bean.setOwnerType(detail.getOwnerType());

        //系统类型
        bean.setSystemType(wmsOutOut.getSystemType());
        return bean;
    }

    /**
     * 构建修改库存list
     *
     * @param outOutDetails   出库单详情
     * @param outBean         创建的出库单的bean
     * @param noticeDetailMap 出库通知单详情
     *
     * @return java.util.List<com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean>
     * @author xwcai
     * @date 2018/6/6 上午9:48
     */
    private List<WmsStockBean> buildWithOutList(List<WmsOutOutDetail> outOutDetails, StockOutBean outBean, Map<Long, WmsOutOutNoticeDetail> noticeDetailMap, WmsOutOut wmsOutOut) {
        List<WmsStockBean> wmsStockBeans = new ArrayList<>();
        for (WmsOutOutDetail detail : outOutDetails) {
            WmsStockBean bean = this.buildWithOut(detail, outBean, noticeDetailMap, wmsOutOut);
            wmsStockBeans.add(bean);
        }
        return wmsStockBeans;
    }

    /**
     * 构建出库详情单
     *
     * @param detail 出库通知单详情
     * @param bean   sku信息
     *
     * @return com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail
     * @author xwcai
     * @date 2018/5/3 上午9:18
     */
    private WmsOutOutDetail buildOutOutDetail(WmsOutOutNoticeDetail detail, SkuInfoBean bean) {
        WmsOutOutDetail outOutDetail = new WmsOutOutDetail();
        Date now = new Date();
        //创建人 创建时间 修改人 修改时间
        outOutDetail.setCreateDate(now);
        outOutDetail.setModifiedDate(now);
        outOutDetail.setCreateUser(detail.getModifiedUser());
        outOutDetail.setModifiedUser(detail.getModifiedUser());

        //设置扫码方式
        outOutDetail.setScanType(bean.getScanType());
        //数量
        outOutDetail.setAmount(bean.getAmount());

        //资产拥有者
        outOutDetail.setOwner(bean.getOwner());
        outOutDetail.setOwnerType(bean.getOwnerType());

        //sku相关信息
        outOutDetail.setSkuId(bean.getSkuId());
        outOutDetail.setSkuNo(detail.getSkuNo());
        outOutDetail.setBatchNo(bean.getBatchNo());

        //构建基础的数据
        outOutDetail.setSkuName(detail.getSkuName());
        outOutDetail.setSkuUnit(detail.getSkuUnit());
        outOutDetail.setStatus(detail.getStatus());
        //设置出库详情单 跟出库通知详情单的关系
        outOutDetail.setOutNoticeDetailId(detail.getId());

        //目标状态
        if (detail.getTargetOwner() == -1 || detail.getTargetOwnerType() == -1) {
            outOutDetail.setTargetOwnerType(bean.getOwnerType());
            outOutDetail.setTargetOwner(bean.getOwner());
        } else {
            outOutDetail.setTargetOwnerType(detail.getTargetOwnerType());
            outOutDetail.setTargetOwner(detail.getTargetOwner());
        }

        return outOutDetail;
    }
}
