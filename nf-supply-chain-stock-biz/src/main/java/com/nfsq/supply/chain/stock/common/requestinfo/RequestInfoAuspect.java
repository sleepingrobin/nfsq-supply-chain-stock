package com.nfsq.supply.chain.stock.common.requestinfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author xwcai
 * @date 2018/3/29 下午4:37
 */
@Aspect
@Component
public class RequestInfoAuspect {

    private Logger logger = LoggerFactory.getLogger("requestInfo");

    private static final Integer END_INDEX = 5000;
    private static final Integer START_INDEX = 0;

    @Around("execution(* com.nfsq.supply.chain.*.controller..*.*(..))")
    public Object around(final ProceedingJoinPoint joinPoint) throws Throwable{
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String url = request.getRequestURL().toString();
            ObjectMapper mapper = new ObjectMapper();
            logger.info("请求url,{}" ,url);
            String param = this.printLogLength(Arrays.toString(joinPoint.getArgs()));
            logger.info("请求参数,{}", param);

            Object result = joinPoint.proceed();
            String resultStr = this.printLogLength(mapper.writeValueAsString(result));
            logger.info("返回参数,{}", resultStr);
            return result;
        }catch (Throwable t){
            logger.error("RequestInfoAuspect error"+t);
            throw t;
        }
    }

    /**
     * 打印日志长度设置
     *
     * @author xwcai
     * @date 2018/9/12 上午11:30
     * @param log
     * @return java.lang.String
     */
    private String printLogLength(String log){
        String logStr = log;
        if(logStr.length() > END_INDEX){
            return logStr.substring(START_INDEX,END_INDEX);
        }else{
            return logStr;
        }
    }
}
