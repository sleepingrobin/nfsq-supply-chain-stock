package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.BillNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.NoticeFinishQO;
import com.nfsq.supply.chain.stock.api.model.vo.BillCountVO;
import com.nfsq.supply.chain.stock.api.model.vo.BillVO;
import com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO;
import com.nfsq.supply.chain.stock.api.service.NoticeProviderApi;
import com.nfsq.supply.chain.stock.microservice.notice.service.NoticeService;
import com.nfsq.supply.chain.utils.common.DummyBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwcai
 * @date 2018/6/19 下午3:24
 */
@RestController
@Slf4j
public class NoticeController implements NoticeProviderApi {

    @Autowired
    NoticeService noticeService;

    @Override
    public JsonResult<Boolean> checkNoticeFinish(@RequestBody NoticeFinishQO qo) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        try {
            jsonResult = noticeService.checkNoticeFinish(qo);
        } catch (Exception e) {
            log.error("检查通知单是否完成失败");
            log.error(e.getMessage(),e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<BillVO> queryBillNOByQO(@RequestBody BillNoQO billNoQO) {
        JsonResult<BillVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = noticeService.queryBillNOByQO(billNoQO);
        } catch (Exception e) {
            log.error("查询任务号，查询对应的出入库单子信息失败");
            log.error(e.getMessage(),e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<BillCountVO> queryBillCountByQO(@RequestBody BillCountQO billCountQO) {
        JsonResult<BillCountVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = noticeService.queryBillCountByQO(billCountQO);
        } catch (Exception e) {
            log.error("看板，查询单子统计信息失败");
            log.error(e.getMessage(),e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<ProcessCountVO> queryProcessBillCountByQO(@RequestBody DummyBean dummyBean) {
        JsonResult<ProcessCountVO> jsonResult = new JsonResult<>();
        try {
            jsonResult = noticeService.queryProcessCount();
        } catch (Exception e) {
            log.error("待办任务查询失败");
            log.error(e.getMessage(),e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }


}
