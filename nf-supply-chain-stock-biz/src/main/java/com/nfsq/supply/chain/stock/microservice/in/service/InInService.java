package com.nfsq.supply.chain.stock.microservice.in.service;

import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InDetailManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InInVo;
import com.nfsq.supply.chain.utils.common.JsonResult;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/11 下午4:02
 */
public interface InInService {

    /**
     * 创建实际入库单
     *
     * @param inBean 入库的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 上午10:19
     */
    JsonResult<String> createInInWithSap(StockInBean inBean);

    /**
     * 创建实际入库单
     *
     * @param inBean 入库的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/4/13 上午10:19
     */
    JsonResult<String> createInIn(StockInBean inBean);

    /**
     * 不发送消息msg
     *
     * @author xwcai
     * @date 2018/9/7 上午9:37
     * @param inBean
     * @param inInDetails
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.String>
     */
    JsonResult<String> createInInWithOutSendMsg(StockInBean inBean);

    /**
     * 根据管理页面qo查询入库单
     *
     * @param inInQo 入库单查询的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.microservice.in.model.vo.InInListVo>
     * @author xwcai
     * @date 2018/4/14 上午11:27
     */
    JsonResult<InManageListVO> queryInInByQo(InInQO inInQo);

    /**
     * 根据入库单单号查询入库单信息
     *
     * @param inInNo 入库单编号
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.model.vo.InInVo
     * @author xwcai
     * @date 2018/4/14 下午1:07
     */
    InInVo getByInInNO(String inInNo);

    /**
     * 根据入库通知单单号 查询入库单
     *
     * @param inNoticeId 入库通知单id
     *
     * @return com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn
     * @author xwcai
     * @date 2018/4/14 下午3:34
     */
    List<WmsInIn> getByInNoticeId(Long inNoticeId);

    /**
     * 管理页面 根据入库单单号 查询入库单详情
     *
     * @param inNo 入库单编号
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List       <       com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail>>
     * @author xwcai
     * @date 2018/4/14 下午6:05
     */
    JsonResult<InDetailManageListVO> getDetailByQO(InInQO qo);

    /**
     * 根据任务Id生成入库单
     *
     * @param confirmBO 确认收获的bean
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Long>
     * @author xwcai
     * @date 2018/5/21 下午4:02
     */
    JsonResult<Boolean> confirmInIn(ConfirmBO confirmBO);

    /**
     * 批量确认收货
     *
     * @author xwcai
     * @date 2018/9/13 上午11:35
     * @param confirmBO
     * @param beans
     * @return void
     */
    void listConfirmInIn(ConfirmBO confirmBO,List<StockInBean> beans);

    /**
     * 查询单号 通过taskId
     *
     * @param taskId 任务id
     *
     * @return java.util.List<java.lang.String>
     * @author xwcai
     * @date 2018/5/31 上午9:59
     */
    List<BillDTO> queryNOsByTaskId(Long taskId);

    /**
     * 查询所有的入库单编号
     *
     * @author xwcai
     * @date 2018/7/21 下午2:04
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.util.List<java.lang.String>>
     */
    JsonResult<List<String>> selectAllNOs() ;

    /**
     * 查询入库单数量
     *
     * @author xwcai
     * @date 2018/7/23 上午11:03
     * @param qo 查询qo
     * @return java.lang.Integer
     */
    Integer queryInInCount(BillCountQO qo) ;

    /**
     * 根据通知单单号 查询出库的sku
     *
     * @author xwcai
     * @date 2018/9/4 下午4:26
     * @param outNoticeNos
     * @return java.util.List<com.nfsq.supply.chain.stock.api.model.vo.StockOutSkuInfoVO>
     */
    List<StockOutSkuInfoVO> selectStockOutSkuInfoByMap(List<String> inNoticeNos);
}
