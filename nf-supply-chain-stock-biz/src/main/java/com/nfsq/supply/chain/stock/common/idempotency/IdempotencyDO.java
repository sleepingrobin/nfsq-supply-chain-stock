package com.nfsq.supply.chain.stock.common.idempotency;

import java.util.Date;

public class IdempotencyDO {
    /**
     * 主键
     */
    private int id;
    /**
     *唯一标示字段
     */
    private String bizUniqueKey;
    /**
     *方法执行结果
     * 序列化数据
     */
    private String result;
    /**
     *初次操作人
     */
    private String createdUser;
    /**
     *初次操作时间
     */
    private Date createdDate;
    /**
     *最新更改人
     */
    private String modifiedUser;
    /**
     *最新更改时间
     */
    private Date modifiedDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBizUniqueKey() {
        return bizUniqueKey;
    }

    public void setBizUniqueKey(String bizUniqueKey) {
        this.bizUniqueKey = bizUniqueKey;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
