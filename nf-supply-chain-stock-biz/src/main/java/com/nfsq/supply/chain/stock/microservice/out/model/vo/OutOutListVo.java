package com.nfsq.supply.chain.stock.microservice.out.model.vo;

import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/14 下午6:10
 */
@ApiModel(value = "ReqSwagger2",description = "出库单列表")
@Getter
@Setter
public class OutOutListVo {
    @ApiModelProperty(value = "outOutList",name = "出库单列表",dataType = "List")
    private List<WmsOutOut> outOutList;
}
