package com.nfsq.supply.chain.stock.common.runtimecount;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.nfsq.supply.chain.stock.common.ChainStockException;

import java.lang.reflect.Method;


/**
 * Created by handepei on 15/3/10.
 */
@Aspect
@Component
public class MethodTimeAdvice {
	private Logger logger = LoggerFactory.getLogger("runTimeCount");
    /**
     *   性能监控开关可以在运行时动态设置开关
     */
    private volatile boolean switchOn = true;
    /**
     * 是否打印方法执行时间的阈值，执行时间超过该值则写入日志
     */
    private volatile int threshold = -1;
    /**
     * 线程变量，存储AOP拦截的每个方法的开始时间及结束时间
     */
    private  ThreadLocal<StackData> dataHolder = new ThreadLocal<StackData>();
    
    @Around("execution(* com.nfsq.supply.chain..*.*(..))")
	public Object around(final ProceedingJoinPoint joinPoint) throws Throwable {
        if(switchOn){
        	try {
	            Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
	            String name = joinPoint.getThis().getClass().getName() + "." + method.getName();
                startCount(name);
                return joinPoint.proceed();
            } catch(Throwable e){
                if(!(e instanceof ChainStockException))
                {
                    logger.error("MethodTimeAdive.ivoke() error");
                }             	
                throw e;
            }finally {
                stopCount(threshold);
            }
        }
        else {
            try {
				return joinPoint.proceed();
			} catch (Throwable e) {
				logger.error("MethodTimeAdive.ivoke() error",e);
				throw e;
			}
        }
    }

    /**
     * 统计方法执行时间开始，该方法放置于MethodInvocation的proceed之前。
     * @param logName  纪录被代理累的方法名（即被拦截的被代理类方法名）
     */
    public  void startCount(String logName) {
        //从dataHolder中获取AOP拦截的方法构建的StackData结构数据
        StackData data = dataHolder.get();
        //获取当前AOP拦截的方法构建的StackData结构数据
        StackEntry currentEntry = new StackEntry(logName, System.currentTimeMillis());
        if (data == null || data.level ==0 || data.currentEntry ==null) {
            data = new StackData();
            data.root = currentEntry;
            data.level = 1;
            dataHolder.set(data);
        } else {
            StackEntry parent = data.currentEntry;
            currentEntry.parent=parent;
            try {
                //被引用的子方法纪录到child中
                parent.child.add(currentEntry);
            } catch (Exception e) {
              logger.error("MethodTimeAdvice Error ---",e);
            }

        }
        //纪录的结点下移，深度加一。
        data.currentEntry = currentEntry;
        currentEntry.level=data.level;
        data.level++;

    }

    /**
     * 统计方法执行时间结束，该方法放置于MethodInvocation的proceed之后
     * @param threshold  是否打印方法执行时间的阈值，统计结束时将执行时间超过该值则写入日志
     */
    public  void stopCount(int threshold) {
        StackData data = dataHolder.get();
        if(data!=null) {
            StackEntry self = data.currentEntry;
            self.endTime = System.currentTimeMillis();
            data.currentEntry = self.parent;
            data.level--;
            if(data.root == self && (self.endTime -self.beginTime) > threshold){
                //调用堆栈退出前，打印日志。
                printStack(data);
            }
        }


    }

    /**
     * 此处还可以进行改进，可以将超时的数据放入一个有界队列
     * 里，在另一个线程进行打印。
     * @param data
     */
    private  void printStack(StackData data) {
        StringBuilder sb = new StringBuilder("\r\n");
        StackEntry root = data.root;
        //构建树形结构
        appendNode(root, sb);
        logger.warn(sb.toString());

        //LoggerRun.infoLogger.info(sb.toString());

    }

    /**
     * 遍历一次请求所有被拦截的方法（迭代），构建树形结构图。
     * @param entry  起始跟节点数据
     * @param sb  存放树形结构图
     */
    private static void appendNode(StackEntry entry, StringBuilder sb) {
        long totalTime = entry.endTime-entry.beginTime ;
        if(entry.level ==1){
            sb.append("统计方法运行时间"+"\r\n");
            sb.append("|-");
        }
        sb.append(totalTime);
        sb.append(" ms; [");
        sb.append(entry.logName);
        sb.append("]");
        //遍历当前结点所有子结点即被引用函数
        for(StackEntry cnode : entry.child){
            sb.append("\r\n|");
            for(int i=0,l=entry.level;i<l;i++){
                sb.append("+---");
            }
            //迭代遍历
            appendNode(cnode,sb);
        }

    }
}
