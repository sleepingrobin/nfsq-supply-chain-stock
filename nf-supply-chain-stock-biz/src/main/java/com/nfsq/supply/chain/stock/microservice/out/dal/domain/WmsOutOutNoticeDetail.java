package com.nfsq.supply.chain.stock.microservice.out.dal.domain;

import java.math.BigDecimal;
import java.util.Date;

public class WmsOutOutNoticeDetail {
    private Long id;

    private Long outNoticeId;

    private Long skuId;

    private String skuNo;

    private String skuName;

    private String skuUnit;

    private String batchNo;

    private Long batchId;

    private Integer ownerType;

    private Long owner;

    private Integer amount;

    private Integer realAmount;

    private BigDecimal grossWeight;

    private BigDecimal netWeight;

    private String weightUnit;

    private Long targetOwner;

    private Integer targetOwnerType;

    private Integer status;

    private Integer scanType;

    private Integer ruleGroup;

    private Integer ruleType;

    private Long ruleDetail;

    private Integer operStatus;

    private String createUser;

    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOutNoticeId() {
        return outNoticeId;
    }

    public void setOutNoticeId(Long outNoticeId) {
        this.outNoticeId = outNoticeId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getSkuNo() {
        return skuNo;
    }

    public void setSkuNo(String skuNo) {
        this.skuNo = skuNo == null ? null : skuNo.trim();
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName == null ? null : skuName.trim();
    }

    public String getSkuUnit() {
        return skuUnit;
    }

    public void setSkuUnit(String skuUnit) {
        this.skuUnit = skuUnit == null ? null : skuUnit.trim();
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo == null ? null : batchNo.trim();
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public Integer getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType) {
        this.ownerType = ownerType;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Integer realAmount) {
        this.realAmount = realAmount;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit == null ? null : weightUnit.trim();
    }

    public Long getTargetOwner() {
        return targetOwner;
    }

    public void setTargetOwner(Long targetOwner) {
        this.targetOwner = targetOwner;
    }

    public Integer getTargetOwnerType() {
        return targetOwnerType;
    }

    public void setTargetOwnerType(Integer targetOwnerType) {
        this.targetOwnerType = targetOwnerType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getScanType() {
        return scanType;
    }

    public void setScanType(Integer scanType) {
        this.scanType = scanType;
    }

    public Integer getRuleGroup() {
        return ruleGroup;
    }

    public void setRuleGroup(Integer ruleGroup) {
        this.ruleGroup = ruleGroup;
    }

    public Integer getRuleType() {
        return ruleType;
    }

    public void setRuleType(Integer ruleType) {
        this.ruleType = ruleType;
    }

    public Long getRuleDetail() {
        return ruleDetail;
    }

    public void setRuleDetail(Long ruleDetail) {
        this.ruleDetail = ruleDetail;
    }

    public Integer getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(Integer operStatus) {
        this.operStatus = operStatus;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}