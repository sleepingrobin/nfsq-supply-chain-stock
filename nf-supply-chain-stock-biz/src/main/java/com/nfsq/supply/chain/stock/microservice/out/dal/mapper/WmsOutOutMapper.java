package com.nfsq.supply.chain.stock.microservice.out.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.BillDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.OutManageVO;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut;
import com.nfsq.supply.chain.stock.microservice.out.model.qo.OutOutQo;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityConstants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsOutOutMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsOutOut record);

    int insertSelective(WmsOutOut record);

    WmsOutOut selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsOutOut record);

    int updateByPrimaryKey(WmsOutOut record);

    List<WmsOutOut> selectByOutNoticeId(@Param("outNoticeId") Long outNoticeId);

    List<WmsOutOut> selectByQoPage(OutOutQo qo);

    @DataAuthority(tableAlias="o",fields= {DataAuthorityConstants.SYSTEM_TYPE_FEILD,DataAuthorityConstants.WHOLE_REPO_NO_FEILD})
    List<OutManageVO> selectManageByQoPage(OutOutPageQO qo);

    OutManageVO selectManageByNo(@Param("outNo") String outNo);

    WmsOutOut selectByNo(@Param("outNo") String outNo);

    List<BillDTO> queryBillNOsByQO(@Param("outSourceId") Long outSourceId);

    Integer selectCountByQO(BillCountQO qo);

    List<String> selectAllOutNos();
}