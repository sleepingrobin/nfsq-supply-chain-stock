/**
 * 
 */
package com.nfsq.supply.chain.stock.privilege.privilege.authaspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.PageBaseBean;
import com.nfsq.supply.chain.utils.common.enums.PrivilegeResultEnums;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityUtils;

/**
 * 权限切面
 * 
 * @date 2015年9月22日 下午6:58:21
 * @author wangk
 * @Description:
 * @project claim_web
 */
@Aspect
@Component
@Order(0)
public class AuthAspect
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthAspect.class);
   
    
    /**
     * 权限验证环绕通知
     * 
     * @Date:2015年9月24日
     * @author wangk
     * @param joinPoint 切点方法
     * @return
     * @Description:
     */
    @Around("execution(* com.nfsq.supply.chain.stock.controller..*.*(..))")
    public Object around(final ProceedingJoinPoint joinPoint)
    {
        try
        {
            Object obj = argsCheck(joinPoint);
            if (obj != null)
            {
                return obj;
            }
            if (joinPoint.getArgs()[0] instanceof DataAuthorityBean)
            {
                // ******************************数据权限相关处理****************************//
                DataAuthorityBean bean = (DataAuthorityBean)joinPoint.getArgs()[0];
                DataAuthorityUtils.setCurrentDataAuthorityBean(bean);
                // ******************************数据权限相关处理****************************//
            }
            return proceed(joinPoint);
        }
        catch (Exception e)
        {
            LOGGER.error("拦截异常！！", e);
        }
        finally
        {
            LOGGER.warn("CurrentDataAuthorityBean.get()=" + DataAuthorityUtils.getCurrentDataAuthorityBean());
            DataAuthorityUtils.removeCurrentDataAuthorityBean();
            LOGGER.warn("CurrentDataAuthorityBean.get()=" + DataAuthorityUtils.getCurrentDataAuthorityBean());
        }
        return null;
    }
    
    private Object argsCheck(final ProceedingJoinPoint joinPoint)
    {
        if (joinPoint.getArgs().length > 1)
        {
            JsonResult<?> jsonResult = new JsonResult<>();
            PrivilegeResultEnums resultCode = PrivilegeResultEnums.INVALIDARGS;
            return jsonResult.generateCodeAndMsgInfo(resultCode.getErrCode(), resultCode.getErrReason());
        }
        
        Object obj = joinPoint.getArgs()[0];
        
        if (obj instanceof BaseBean)
        {
            BaseBean bean = (BaseBean)obj;
            JsonResult<?> checkResult = bean.validate();
            if (!checkResult.isSuccess())
            {
                return checkResult;
            }
        }
        else if (obj instanceof PageBaseBean)
        {
            PageBaseBean bean = (PageBaseBean)obj;
            JsonResult<?> checkResult = bean.validate();
            if (!checkResult.isSuccess())
            {
                return checkResult;
            }
        }
        
        return null;
    }
    
    /**
     * 异常捕获
     * 
     * @Date:2015年11月17日
     * @author wangk
     * @param joinPoint
     * @return
     * @Description:
     */
    private Object proceed(ProceedingJoinPoint joinPoint)
    {
        try
        {
            return joinPoint.proceed();
        }
        catch (Throwable e)
        {
            LOGGER.error("方法执行异常", e);
            Class<?> returnType = ((MethodSignature)joinPoint.getSignature()).getMethod().getReturnType();
            try
            {
                return returnType.newInstance();
            }
            catch (Exception e1)
            {
                LOGGER.error("类型错误", e1);
                return null;
            }
        }
    }
    
}
