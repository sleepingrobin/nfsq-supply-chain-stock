package com.nfsq.supply.chain.stock.microservice.out.service.impl;

import com.nfsq.supply.chain.datacenter.api.RepoAPI;
import com.nfsq.supply.chain.datacenter.api.common.enums.OwnerTypeEnum;
import com.nfsq.supply.chain.datacenter.api.parameter.SkuParam;
import com.nfsq.supply.chain.stock.api.model.bean.FinishNoticeBO;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.common.kafka.ProduceMessageService;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.stock.microservice.common.constant.InOutConstants;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNotice;
import com.nfsq.supply.chain.stock.microservice.in.service.InNoticeService;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.DataCenterService;
import com.nfsq.supply.chain.stock.microservice.out.service.AsyncService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutNoticeService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutOutService;
import com.nfsq.supply.chain.stock.microservice.out.service.OutRuleService;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/28 下午1:41
 */
@Component
@Slf4j
public class AsyncServiceImpl implements AsyncService {

    @Autowired
    private RepoAPI repoAPI;
    
    @Autowired
    private DataCenterService dataCenterService;

    @Autowired
    ProduceMessageService produceMessageService;

    @Lazy
    @Autowired
    InNoticeService inNoticeService;

    @Lazy
    @Autowired
    OutNoticeService outNoticeService;

    @Autowired
    RedisManagerService redisManagerService;

    @Autowired
    @Lazy
    OutOutService outOutService;

    @Autowired
    @Lazy
    OutRuleService outRuleService;

    @Override
    @Async
    public void sendInNoticeMessage(StockInBean inBean) {
        //所有kafka消息 不影响流程
        try {
            log.info("入库通知单开始收货,{}",inBean.getInNoticeId());
            //判断该任务下是否所有入库通知单都已经完成
            WmsInInNotice inInNotice = inNoticeService.getInNoticeById(inBean.getInNoticeId());
            //如果判断里面有值 就证明还不是全部完成
            if (null == inInNotice) {
                log.info("该入库通知单查询不到，ID为：" + inBean.getInNoticeId());
                return;
            }
            //判断该入库通知单是否完成  如果已经完成 就发送异步消息 否则 不发
            if (!StockOperStatusEnum.FINISH.getType().equals(inInNotice.getOperStatus())) {
                log.info("该入库通知单尚未完成，ID为：,{},{}" , inBean.getInNoticeId(),inInNotice.getOperStatus());
                return;
            }
            FinishNoticeBO noticeBO = new FinishNoticeBO();
            noticeBO.setTaskId(inInNotice.getInSourceId());
            noticeBO.setNoticeId(inInNotice.getId());
            noticeBO.setNoticeNO(inInNotice.getInNoticeNo());
            //最后给调度中心发送入库通知单完成消息
            produceMessageService.sendMessage(InOutConstants.FINISH_IN_NOTICE, JsonUtils.toJson(noticeBO));
        } catch (Exception e) {
            log.error("异步消息 入库通知单号：" + inBean.getInNoticeId());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 扣减通用库存缓存
     *
     * @param wmsStockBean
     *
     * @author sundi
     */
    @Async
    @Override
    public void deleteCommonStockRedis(WmsStockBean wmsStockBean) {
        try {
            if (wmsStockBean.getOwnerType() == OwnerTypeEnum.STORE.getCode()) {
                redisManagerService.deleteStoreStockRedis(wmsStockBean.getRepoId(), wmsStockBean.getOwner());
            }
            if(wmsStockBean.getOwnerType() == OwnerTypeEnum.COMPANY.getCode())
            {
                //删除可用库存缓存
                redisManagerService.deleteCommonStockRedis(wmsStockBean.getSkuId(), wmsStockBean.getStatus(), wmsStockBean.getRepoId());
            } 
        } catch (Exception e) {
            log.error("删除通用缓存失败:" + JsonUtils.toJson(wmsStockBean), e);
        }

    }

    /**
     * 扣减规则库存缓存
     *
     * @param wmsStockBean
     *
     * @author sundi
     */
    @Async
    @Override
    public void deleteRuleStockRedis(WmsStockBean wmsStockBean) {
        try {
            if (wmsStockBean.getOwnerType() == OwnerTypeEnum.STORE.getCode()) {
                redisManagerService.deleteStoreStockRedis(wmsStockBean.getRepoId(), wmsStockBean.getOwner());
            }
            if(wmsStockBean.getOwnerType() == OwnerTypeEnum.COMPANY.getCode())
            {
                //删除规则库存缓存
                redisManagerService.deleteRuleStockRedis(wmsStockBean.getSkuId(), wmsStockBean.getStatus(), wmsStockBean.getRepoId(), wmsStockBean.getRuleType(), wmsStockBean.getRuleGroup());
            } 
        } catch (Exception e) {
            log.error("删除规则缓存失败:" + JsonUtils.toJson(wmsStockBean), e);
        }
    }

    @Override
    @Async
    public void prepareSkuByParams(List<SkuParam> skuParams)
    {
        dataCenterService.prepareSkuByParams(skuParams);
    }
}
