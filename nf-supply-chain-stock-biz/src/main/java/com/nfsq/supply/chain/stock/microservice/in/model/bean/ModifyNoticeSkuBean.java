package com.nfsq.supply.chain.stock.microservice.in.model.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xwcai
 * @date 2018/4/26 上午9:33
 */
@ApiModel(value = "ReqSwagger2",description = "修改的sku信息")
@Getter
@Setter
public class ModifyNoticeSkuBean {

    @ApiModelProperty(value = "id",name = "通知详情单的ID",dataType = "Long",required =true)
    private Long id;

    @ApiModelProperty(value = "amount",name = "数量",dataType = "Integer",required =true)
    private Integer amount;
}
