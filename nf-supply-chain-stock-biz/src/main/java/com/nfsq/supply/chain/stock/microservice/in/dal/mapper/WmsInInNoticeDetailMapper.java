package com.nfsq.supply.chain.stock.microservice.in.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.dto.WmsInNoticeDetailDTO;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInNoticeDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsInInNoticeDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInInNoticeDetail record);

    int insertSelective(WmsInInNoticeDetail record);

    WmsInInNoticeDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInInNoticeDetail record);

    int updateByPrimaryKey(WmsInInNoticeDetail record);

    void insertBatch(@Param("list") List<WmsInInNoticeDetail> list);

    int updateByInNoticeId(WmsInInNoticeDetail record);

    List<WmsInNoticeDetailDTO> selectManageByQOPage(InNoticeQO qo);

    int updateRealAmountBatch(@Param("list") List<WmsInInNoticeDetail> list);

    List<Integer> selectOperStatusGroupByInNoticeId(@Param("inNoticeId") Long inNoticeId);

    int updateOperStatusByInNoticeIdAndSkuIdAndBatchNoAndStatus(@Param("list") List<WmsInInNoticeDetail> list);

    int updateByInNoticeIdAndSkuIdAndBatchNoAndStatus(@Param("list") List<WmsInInNoticeDetail> list);

}