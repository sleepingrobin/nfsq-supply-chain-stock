package com.nfsq.supply.chain.stock.microservice.manage.enums;

import java.util.ArrayList;
import java.util.List;

public enum OperStatusEnums {
	NORMAL(0, "正常"),
	MODIFY(1,"修改"),
	CANCLE(2,"取消");
	private Integer code;
    private String name;
    private OperStatusEnums(Integer code, String name) {
        this.code = code;
        this.name = name;
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static List<Integer> getCodes(){
    	List<Integer> codes = new ArrayList<>();
    	for(OperStatusEnums enums:OperStatusEnums.values()){
    		codes.add(enums.getCode());
    	}
    	return codes;
    }
}
