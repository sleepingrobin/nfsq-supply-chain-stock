package com.nfsq.supply.chain.stock.microservice.manage.service;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BatchNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.RepoQO;
import com.nfsq.supply.chain.stock.api.model.qo.StoreStockQO;
import com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.FanAndStoreStockInfoVO;
import com.nfsq.supply.chain.stock.api.model.vo.RepoOrStockVO;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;

import java.util.List;
import java.util.Map;


/**
 * 
 * @author  xfeng6
 * @created 2018年4月11日 上午10:44:09
 */
public interface WmsInventoryStockService {
	
	/**
	 * 查询库存
	 * @author  wangk
	 * @date 2018年4月27日 上午9:42:39
	 * @param stock
	 * @return
	 * @return  List<WmsInventoryStock>
	 */
	public List<WmsInventoryStockDTO> queryStock(WmsStockQO stock);

	/**
	 * 更新库存
	 * @author  wangk
	 * @date 2018年4月27日 上午9:43:58
	 * @param wmsStockBeans
	 * @throws ChainStockException
	 * @return  void
	 */
	public void updateStock(List<WmsStockBean> wmsStockBeans);
	
	/**
	 * 查询可用库存
	 * @author  wangk
	 * @date 2018年5月11日 下午1:54:23
	 * @param wmsAvaliableStockBOs
	 * @return
	 * @return  List<WmsAvaliableStockBO>
	 */
	public List<WmsAvaliableStockBO> queryAvaliableStock(List<WmsAvaliableStockBO> wmsAvaliableStockBOs);

	/**
	 * 查询库存中所有批次号
	 * @author  wangk
	 * @date 2018年5月24日 下午7:23:17
	 * @return
	 * @return  List<String>
	 */
	public List<String> queryBatchNos(BatchNoQO qo);
	
	/**
	 * 查询门店或仓库
	 * @author  wangk
	 * @date 2018年5月28日 上午11:20:12
	 * @param bean
	 * @return
	 * @return  JsonResult<List<Repo>>
	 */
	public RepoOrStockVO queryRepos(RepoQO bean);

	/**
	 * 查询门店库存
	 * @param storeStockBOs
	 * @return
	 */
	public Map<Long, Integer> queryStoreStock(StoreStockQO storeStockQO);

	/**
	 * 根据仓库id 查询非限制的sku 数量 百分比
	 *
	 * @author xwcai
	 * @date 2018/7/1 下午3:37
	 * @param repoId
	 * @return java.util.List<com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO>
	 */
	List<SkuManageBO> selectUnLimitStockByRepoId(Long repoId);
	
	/**
     * 
     * 根据物料id,仓库id和状态查询sku总数量列表
     *
     * @author sundi
     * @param skuId
     * @param repoId
     * @param status
     * @return
     */
    List<SkuNumsBO> selectUnlimitStockByCondition(Long skuId,Long repoId,Integer status,String batchNo,Long owner,Integer ownerType);

	/**
	 * 经销商直销查询
	 * @param wmsStockQO
	 * @return
	 */
	List<FanAndStoreStockInfoVO> queryStockByFanAndStore(WmsStockQO wmsStockQO);

}
