package com.nfsq.supply.chain.stock.microservice.manage.dal.domain;

import java.util.Date;

import com.nfsq.supply.chain.utils.common.PageParameter;

public class WmsInventoryStock extends PageParameter {
    private Long id;

    private Long skuId;

    private Long batchId;

    private String batchNo;

    private Integer nums;

    private Integer status;

    private Long repoId;

    private Long owner;
    
    private Integer ownerType;

    private Integer ruleGroup;

    private Integer ruleType;

    private Date createdDate;

    private String createdUser;

    private Date modifiedDate;

    private String modifiedUser;
    
    private Integer systemType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo == null ? null : batchNo.trim();
    }

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public Integer getRuleGroup() {
        return ruleGroup;
    }

    public void setRuleGroup(Integer ruleGroup) {
        this.ruleGroup = ruleGroup;
    }

    public Integer getRuleType() {
        return ruleType;
    }

    public void setRuleType(Integer ruleType) {
        this.ruleType = ruleType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser == null ? null : createdUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

    public Integer getSystemType()
    {
        return systemType;
    }

    public void setSystemType(Integer systemType)
    {
        this.systemType = systemType;
    }
}