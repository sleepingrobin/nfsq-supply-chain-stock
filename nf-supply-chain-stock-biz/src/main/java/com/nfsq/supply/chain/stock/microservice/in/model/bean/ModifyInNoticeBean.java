package com.nfsq.supply.chain.stock.microservice.in.model.bean;

import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.utils.common.BaseBean;
import com.nfsq.supply.chain.utils.common.JsonResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/26 上午9:24
 */
@ApiModel(value = "ReqSwagger2", description = "修改入库通知单的入参")
@Getter
@Setter
public class ModifyInNoticeBean extends BaseBean {

    @ApiModelProperty(value = "inNoticeId", name = "入库通知单号", dataType = "Long", required = false)
    private Long inNoticeId;

    @ApiModelProperty(value = "skuInfoList", name = "入库sku详情", dataType = "List", required = true)
    private List<ModifyNoticeSkuBean> skuInfoList;

    @Override
    public JsonResult<?> validate() {
        JsonResult<?> jsonResult = new JsonResult<>();
        //参数是否为空检查
        if (null == inNoticeId || inNoticeId < 0 || null == skuInfoList || skuInfoList.isEmpty()) {
            jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
            return jsonResult;
        }
        //检查所有sku内参数是否正确
        for (ModifyNoticeSkuBean skuInfo : skuInfoList) {
            //检查内部sku参数是否为空
            if (null == skuInfo.getAmount() || null == skuInfo.getId()) {
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
            //检查内部sku数量、重量不能小于0
            if (skuInfo.getAmount() < 0 || skuInfo.getId() <= 0){
                jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_ILLEGALITY.getCode(), StockResultCode.PARAM_ILLEGALITY.getDesc());
                return jsonResult;
            }
        }
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
