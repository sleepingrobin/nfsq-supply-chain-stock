package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryRuleDetail;

public interface WmsInventoryRuleDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WmsInventoryRuleDetail record);

    int insertSelective(WmsInventoryRuleDetail record);

    WmsInventoryRuleDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WmsInventoryRuleDetail record);

    int updateByPrimaryKey(WmsInventoryRuleDetail record);
}