package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import com.nfsq.supply.chain.stock.api.model.bean.SkuManageBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.qo.BatchNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO;
import com.nfsq.supply.chain.stock.api.model.vo.FanAndStoreStockInfoVO;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsInventoryStockMapper {

    int insert(WmsInventoryStock record);

	List<WmsInventoryStockDTO> queryStockPage(WmsStockQO stock);

	int addStock(WmsStockBean wmsStockBean);

	Long queryActualStock(@Param("skuId") Long skuId, @Param("statuses") List<Integer> statuses, @Param("repoId") Long repoId,@Param("ruleGroup") Integer ruleGroup, @Param("ruleType") Integer ruleType,@Param("dealerIds") List<Long> dealerIds);
	
	Long queryCommonStock(@Param("skuId") Long skuId, @Param("status") Integer status, @Param("repoId") Long repoId);
	
	Long queryRuleStock(@Param("skuId") Long skuId, @Param("status") Integer status, @Param("repoId") Long repoId, @Param("ruleType") Integer ruleType,@Param("ruleGroup") Integer ruleGroup,@Param("dealerIds") List<Long> dealerIds);
	
	Long checkStock(WmsStockBean wmsStockBean);
	
	List<WmsInventoryStock> queryStocks(@Param("bean")WmsStockBean wmsStockBean,@Param("skuIdList") List<Long> skuIdList);

	int reduceRuleStock(WmsStockBean wmsStockBean);

	int queryRemainNum(WmsStockBean wmsStockBean);

	int checkRuleStock(WmsStockBean wmsStockBean);

	int reduceCommonStock(WmsStockBean wmsStockBean);

	int updateStock(WmsStockBean wmsStockBean);

	Long queryStockId(WmsStockBean wmsStockBean);

	List<Long> queryDealerIdsByStockIds(@Param("skuId") Long skuId, @Param("statuses") List<Integer> statuses, @Param("repoId") Long repoId,@Param("ruleGroup") Integer ruleGroup, @Param("ruleType") Integer ruleType,@Param("dealerIds") List<Long> dealerIds,@Param("owner") Long owner, @Param("ownerType") Integer ownerType);

	int queryStock(WmsInventoryStock stock);

	List<String> queryBatchNos(BatchNoQO qo);

	List<WmsInventoryStock>  queryStoreStock( @Param("repoId") Long repoId, @Param("statuses")List<Integer> statuses,@Param("owner") Long owner);

	@DataAuthority(tableAlias = "ws")
	List<SkuManageBO> selectUnLimitStockByRepoId(@Param("repoId") Long repoId);

	List<SkuNumsBO> selectUnlimitStockByCondition(@Param("skuId") Long skuId, @Param("repoId") Long repoId,@Param("status") Integer status,@Param("batchNo")String batchNo,@Param("owner")Long owner,@Param("ownerType")Integer ownerType);

	List<Long> queryDealerIdsByStockIds(@Param("skuId") Long skuId, @Param("statuses") List<Integer> statuses, @Param("repoId") Long repoId,@Param("ruleGroup") Integer ruleGroup, @Param("ruleType") Integer ruleType,@Param("dealerIds") List<Long> dealerIds);

	List<FanAndStoreStockInfoVO> queryFranAndStoreStoreInfo(WmsStockQO wmsStockQO);
	
}