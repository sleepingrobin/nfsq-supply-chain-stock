package com.nfsq.supply.chain.stock.microservice.notice.service;

import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.BillNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.NoticeFinishQO;
import com.nfsq.supply.chain.stock.api.model.vo.BillCountVO;
import com.nfsq.supply.chain.stock.api.model.vo.BillVO;
import com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO;
import com.nfsq.supply.chain.utils.common.JsonResult;

/**
 * @author xwcai
 * @date 2018/6/19 下午3:03
 */
public interface NoticeService {

    /**
     * 查询对应的通知单是否全部完成
     *
     * @param qo 通知单是否完成 查询的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<java.lang.Boolean>
     * @author xwcai
     * @date 2018/6/19 下午3:04
     */
    JsonResult<Boolean> checkNoticeFinish(NoticeFinishQO qo);

    /**
     * 根据任务id 查询任务下所有出库通知单 出库单 入库通知单 入库单  单号
     *
     * @param qo 查询任务下各种单据单号的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.BillNoVO>
     * @author xwcai
     * @date 2018/5/31 上午9:45
     */
    JsonResult<BillVO> queryBillNOByQO(BillNoQO qo);

    /**
     * 根据时间段 查询各种单据数量
     *
     * @param qo 查询各种单据数量的qo
     *
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.BillCountVO>
     * @author xwcai
     * @date 2018/6/21 下午2:40
     */
    JsonResult<BillCountVO> queryBillCountByQO(BillCountQO qo);

    /**
     * 查询所有待办数量
     *
     * @author xwcai
     * @date 2018/7/26 下午3:34
     * @param
     * @return com.nfsq.supply.chain.utils.common.JsonResult<com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO>
     */
    JsonResult<ProcessCountVO> queryProcessCount();
}
