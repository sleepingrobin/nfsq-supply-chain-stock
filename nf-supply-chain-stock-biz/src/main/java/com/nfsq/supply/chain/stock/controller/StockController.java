package com.nfsq.supply.chain.stock.controller;

import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.bean.WmsStockBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.enums.StockResultCode;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.api.service.StockProviderApi;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.SapSyncStockService;
import com.nfsq.supply.chain.stock.microservice.manage.service.WmsInventoryStockService;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author wangk
 * @date 2018年5月16日 下午2:04:17
 */
@RestController
@Slf4j
public class StockController implements StockProviderApi {
    @Autowired
    private WmsInventoryStockService wmsInventoryStockService;
    @Autowired
    private SapSyncStockService sapSyncStockService;

    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.stock.api.service.StockProviderApi#queryStock(com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO)
     */
    @Override
    public JsonResult<WmsInventoryStockVO> queryStock(@RequestBody WmsStockQO stock) {
        JsonResult<WmsInventoryStockVO> jsonResult = new JsonResult<>();
        try {
            List<WmsInventoryStockDTO> stocks = wmsInventoryStockService.queryStock(stock);
            WmsInventoryStockVO vo = new WmsInventoryStockVO();
            vo.setWmsInventoryStockDTOs(stocks);
            BeanUtils.copyProperties(stock, vo);
            jsonResult.setData(vo);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询库存失败：" + JsonUtils.toJson(stock), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<FanAndStoreStockInfoVO>> queryStockByFanAndStore(@RequestBody WmsStockQO wmsStockQO) {
        try {
            List<FanAndStoreStockInfoVO> fanAndStoreStockInfoVOS = wmsInventoryStockService.queryStockByFanAndStore(wmsStockQO);
            return JsonResult.ok(fanAndStoreStockInfoVOS);
        } catch (Exception e) {
            log.error("经销商直销查询异常",e);
            JsonResult jsonResult = new JsonResult<>();
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
            return jsonResult;
        }

    }

    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.stock.api.service.StockProviderApi#updateStock(java.util.List)
     */
    @Override
    public JsonResult<Boolean> updateStock(@RequestBody ModifyStockQO modifyStockQO) {
        JsonResult<Boolean> jsonResult = new JsonResult<>();
        List<WmsStockBO> wmsStockBOs = modifyStockQO.getWmsStockBOs();
        try {
            if (CollectionUtils.isEmpty(wmsStockBOs)) {
                return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
            }
            Date date=new Date();
            List<WmsStockBean> wmsStockBeans = new ArrayList<>(wmsStockBOs.size());
            for (WmsStockBO wmsStockBO : wmsStockBOs) {
                WmsStockBean bean = new WmsStockBean();
                BeanUtils.copyProperties(wmsStockBO, bean);
                bean.setCreatedUser("system");
                bean.setCreatedDate(date);
                bean.setModifiedDate(date);
                bean.setModifiedUser("system");
                bean.setSystemType(wmsStockBO.getCurrentSystemType());
                wmsStockBeans.add(bean);
            }
            wmsInventoryStockService.updateStock(wmsStockBeans);
            jsonResult.setData(Boolean.TRUE);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("批量更改库存失败：" + JsonUtils.toJson(wmsStockBOs), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(),e.getMessage());
        }
        return jsonResult;
    }

    /* (non-Javadoc)
     * @see com.nfsq.supply.chain.stock.api.service.StockProviderApi#queryAvaliableStock(java.util.List)
     */
    @Override
    public JsonResult<WmsAvaliableStockVO> queryAvaliableStock(@RequestBody AvaliableStockQO avaliableStockQO) {
        JsonResult<WmsAvaliableStockVO> jsonResult = new JsonResult<>();
        List<WmsAvaliableStockBO> wmsAvaliableStockBOs = avaliableStockQO.getWmsAvaliableStockBOs();
        try {
            if (CollectionUtils.isEmpty(wmsAvaliableStockBOs)) {
                return jsonResult.generateCodeAndMsgInfo(StockResultCode.PARAM_EMPTY.getCode(), StockResultCode.PARAM_EMPTY.getDesc());
            }
            List<WmsAvaliableStockBO> result = wmsInventoryStockService.queryAvaliableStock(wmsAvaliableStockBOs);
            WmsAvaliableStockVO vo = new WmsAvaliableStockVO();
            vo.setWmsAvaliableStockBOs(result);
            jsonResult.setData(vo);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询可用库存失败：" + JsonUtils.toJson(wmsAvaliableStockBOs), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<RepoOrStockVO> queryRepos(@RequestBody RepoQO bean){
    	JsonResult<RepoOrStockVO> jsonResult = new JsonResult<>();
        try {
            RepoOrStockVO repos = wmsInventoryStockService.queryRepos(bean);
            jsonResult.setData(repos);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询仓库失败", e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

    @Override
    public JsonResult<List<String>> queryBatchNos(@RequestBody BatchNoQO qo){
    	JsonResult<List<String>> jsonResult = new JsonResult<>();
        try {
        	List<String> batchNos = wmsInventoryStockService.queryBatchNos(qo);
            jsonResult.setData(batchNos);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询批次号失败", e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }

	@Override
	public JsonResult<StoreStockVO> queryStoreStock(@RequestBody StoreStockQO storeStockQO) {
		JsonResult<StoreStockVO> jsonResult = new JsonResult<>();
        try {
            Map<Long,Integer> skuNumMap = wmsInventoryStockService.queryStoreStock(storeStockQO);
            StoreStockVO vo = new StoreStockVO();    
            vo.setSkuNumMap(skuNumMap);
            jsonResult.setData(vo);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询门店库存失败：" + JsonUtils.toJson(storeStockQO), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
	}

	@Override
    public JsonResult<SapSyncStockVO> querySapSyncStock(@RequestBody SapSyncStockQO sapSyncStockQO)
    {      
        JsonResult<SapSyncStockVO> jsonResult = new JsonResult<>();
        try {
            Map<String, Integer> skuNumsMap = new HashMap<>();
            skuNumsMap=sapSyncStockService.querySapStock(sapSyncStockQO.getSkuNoList(),sapSyncStockQO.getRepoId());
            SapSyncStockVO vo = new SapSyncStockVO();
            vo.setSkuNumMap(skuNumsMap);
            jsonResult.setData(vo);
            jsonResult.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            log.error("查询sap同步库存失败：" + JsonUtils.toJson(sapSyncStockQO), e);
            jsonResult.generateCodeAndMsgInfo(StockResultCode.SYSTEM_ERROR.getCode(), StockResultCode.SYSTEM_ERROR.getDesc());
        }
        return jsonResult;
    }
}
