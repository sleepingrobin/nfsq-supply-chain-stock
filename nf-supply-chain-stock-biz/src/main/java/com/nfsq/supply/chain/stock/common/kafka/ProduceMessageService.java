package com.nfsq.supply.chain.stock.common.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author xwcai
 * @date 2018/1/8 下午4:23
 */
@Component
public class ProduceMessageService {
    private static final Logger logger = LoggerFactory.getLogger(ProduceMessageService.class);

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    public void sendMessage(String topicId,String json){
        logger.info("放入kafka ：topicId:" + topicId + "放入的json：" + json);
        kafkaTemplate.send(topicId, json);
    }
}
