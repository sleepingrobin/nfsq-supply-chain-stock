package com.nfsq.supply.chain.stock.microservice.manage.dal.domain;

import java.util.Date;

public class SapSyncStock {
    private Long id;
    
    private String skuNo;

    private String factoryNo;

    private String repoNo;
    
    private String batchNo;

    private Integer unlimitNums;

    private Integer qualityNums;

    private Integer frozenNums;

    private String createUser;
    
    private Date createDate;

    private String modifiedUser;

    private Date modifiedDate;
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public String getModifiedUser()
    {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser)
    {
        this.modifiedUser = modifiedUser;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    public String getSkuNo() {
        return skuNo;
    }

    public void setSkuNo(String skuNo) {
        this.skuNo = skuNo == null ? null : skuNo.trim();
    }
    
    public String getBatchNo()
    {
        return batchNo;
    }

    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getFactoryNo() {
        return factoryNo;
    }

    public void setFactoryNo(String factoryNo) {
        this.factoryNo = factoryNo == null ? null : factoryNo.trim();
    }

    public String getRepoNo() {
        return repoNo;
    }

    public void setRepoNo(String repoNo) {
        this.repoNo = repoNo == null ? null : repoNo.trim();
    }

    public Integer getUnlimitNums() {
        return unlimitNums;
    }

    public void setUnlimitNums(Integer unlimitNums) {
        this.unlimitNums = unlimitNums;
    }

    public Integer getQualityNums() {
        return qualityNums;
    }

    public void setQualityNums(Integer qualityNums) {
        this.qualityNums = qualityNums;
    }

    public Integer getFrozenNums() {
        return frozenNums;
    }

    public void setFrozenNums(Integer frozenNums) {
        this.frozenNums = frozenNums;
    }

}