package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.datacenter.api.parameter.SkuParam;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;

import java.util.List;

/**
 * @author xwcai
 * @date 2018/6/28 下午1:40
 */
public interface AsyncService {

    /**
     * 入库通知单完成发送kafka消息
     *
     * @param inBean 入库通知单id
     *
     * @return void
     * @author xwcai
     * @date 2018/5/21 下午5:33
     */
    void sendInNoticeMessage(StockInBean inBean);
    
    void deleteCommonStockRedis(WmsStockBean wmsStockBean);
    
    void deleteRuleStockRedis(WmsStockBean wmsStockBean);
    
    void prepareSkuByParams(List<SkuParam> skuParams);
}
