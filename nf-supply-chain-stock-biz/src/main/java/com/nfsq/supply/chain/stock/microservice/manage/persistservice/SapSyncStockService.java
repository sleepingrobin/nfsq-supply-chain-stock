package com.nfsq.supply.chain.stock.microservice.manage.persistservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.nfsq.supply.chain.datacenter.api.response.Repo;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.common.config.StockConfig;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.SapSyncStock;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.SapSyncStockMapper;

/**
 * 
 * sap同步库存表
 *
 * @author sundi
 * @version 2018年6月27日
 * @since SCM
 */
@Component
public class SapSyncStockService
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private SapSyncStockMapper sapSyncStockMapper;
    
    @Autowired
    private DataCenterService dataCenterService;
    
    @Autowired
    private RedisManagerService redisManagerService;
    
    @Autowired
    private StockConfig stockConfig;
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void addSapStock(List<SapSyncStock> sapSyncStockList)
    {
        sapSyncStockList.forEach(sapSyncStock -> {
            int queryResult=sapSyncStockMapper.queryStock(sapSyncStock);
            if(queryResult>1)
            {
                logger.debug("多条sap同步库存记录，增加sap同步库存失败，导致失败的实体:{},本次操作所有实体:{}",sapSyncStock,sapSyncStockList);
                throw new ChainStockException("多条sap同步库存记录，增加sap同步库存失败");
            }
            else if(queryResult==1)
            {
                int addResult = sapSyncStockMapper.addSapStock(sapSyncStock);
                if (addResult != 1)
                {
                    logger.debug("添加sap同步库存失败,导致失败的实体:{},本次操作所有实体:{}",sapSyncStock,sapSyncStockList);
                    throw new ChainStockException("添加sap同步库存失败");
                }
                //删除缓存
                redisManagerService.deleteSapStockRedis(sapSyncStock.getSkuNo(), sapSyncStock.getFactoryNo(), sapSyncStock.getRepoNo());
            }
            else 
            {
                int insertResult =sapSyncStockMapper.insertSapStock(sapSyncStock);
                if (insertResult != 1)
                {
                    logger.debug("插入sap同步库存失败,导致失败的实体:{},本次操作所有实体:{}",sapSyncStock,sapSyncStockList);
                    throw new ChainStockException("插入sap同步库存失败");
                }
                //删除缓存
                redisManagerService.deleteSapStockRedis(sapSyncStock.getSkuNo(), sapSyncStock.getFactoryNo(), sapSyncStock.getRepoNo());
            }  
        });
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void reduceSapStock(List<SapSyncStock> sapSyncStockList)
    {
        sapSyncStockList.forEach(sapSyncStock -> {
            int result = sapSyncStockMapper.reduceSapStock(sapSyncStock);
            if (result != 1)
            {
                logger.debug("减少sap同步库存失败,导致失败的实体:{},本次操作所有实体:{}",sapSyncStock,sapSyncStockList);
                throw new ChainStockException("减少sap同步库存失败");
            }
            //删除缓存
            redisManagerService.deleteSapStockRedis(sapSyncStock.getSkuNo(), sapSyncStock.getFactoryNo(), sapSyncStock.getRepoNo());
        });
    }
    
    public Map<String, Integer> querySapStock(List<String> skuNoList, Long repoId)
    {
        Map<String, Integer> skuNumsMap = new HashMap<>();
        String factoryNo = stockConfig.getFactoryNo();
        String repoNo = stockConfig.getRepoNo();
        // 如果指定了仓库id则获取对应的仓库编号和工厂编号
        if (null != repoId)
        {
            Repo repo = dataCenterService.findRepoDetail(repoId);
            factoryNo = repo.getExtendsNo();
            repoNo = repo.getRepoNo();
        }
        // 查询指定skuNo库存
        List<SapSyncStock> stockList = new ArrayList<>();
        for (String skuNo : skuNoList)
        {
            SapSyncStock querySapStock = redisManagerService.querySapStock(skuNo, factoryNo, repoNo);
            if(null!=querySapStock&&!StringUtils.isEmpty(querySapStock.getSkuNo()))
            {
                stockList.add(querySapStock);
            }         
        }
        // 设置到map中
        if (!CollectionUtils.isEmpty(stockList))
        {
            skuNumsMap =
                stockList.stream().collect(Collectors.toMap(SapSyncStock::getSkuNo, SapSyncStock::getUnlimitNums));
        }
        return skuNumsMap;
    }
}
