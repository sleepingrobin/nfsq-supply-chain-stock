package com.nfsq.supply.chain.stock.common.idempotency.mapper;


import com.nfsq.supply.chain.stock.common.idempotency.IdempotencyDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IdempotencyMapper {

    int insert(IdempotencyDO idempotencyDO);

    int deleteByUniqueKey(IdempotencyDO idempotencyDO);

    int updateByUniqueKey(IdempotencyDO example);

    IdempotencyDO selectByUniqueKey(IdempotencyDO idempotencyDO);


}