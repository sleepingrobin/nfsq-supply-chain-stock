package com.nfsq.supply.chain.stock.microservice.in.service;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeBean;
import com.nfsq.supply.chain.stock.api.model.bean.InNoticeByOutBean;
import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import com.nfsq.supply.chain.stock.api.model.enums.ScanerType;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.InNoticeQO;
import com.nfsq.supply.chain.stock.api.model.qo.StockOutSkuInfoQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoServiceTest;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.CancelInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.ModifyInNoticeBean;
import com.nfsq.supply.chain.stock.microservice.in.model.bean.ModifyNoticeSkuBean;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InNoticeVo;
import com.nfsq.supply.chain.utils.common.DateUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * @author xwcai
 * @date 2018/4/23 下午1:52
 */
public class InNoticeServiceTest extends BaseTest{



    @Autowired
    InNoticeService inNoticeService;

    @Test
    //成功创建
    public void createInInNotice() throws Exception {
        InNoticeBean inNoticeBean = new InNoticeBean();
        inNoticeBean.setInSourceId(30L);
        inNoticeBean.setPlanInDate(DateUtils.addDay(new Date(),1));
        inNoticeBean.setRepoId(1012L);
        inNoticeBean.setType(1);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean = new SkuInfoBean();
        bean.setAmount(50);
        bean.setBatchNo("1");
        bean.setSkuId(890L);
        bean.setScanType(1);
        bean.setOwner(123L);
        bean.setOwnerType(1);
        //0 非限制
        bean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(bean);
        inNoticeBean.setSkuInfoList(skuInfoBeanList);
        inNoticeBean.setModifyId("test");
        inNoticeBean.setSystemType(SystemTypeEnum.MAQUILLAGE.getCode());
        JsonResult<InNoticeCreateVO> jsonResult = inNoticeService.createInInNotice(inNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

    }

    @Test
    public void createInInNoticeByOut() throws Exception {
        InNoticeByOutBean bean = new InNoticeByOutBean();
        bean.setTaskId(22985l);
        bean.setPlanInDate(new Date());
        bean.setModifyId("test");
        JsonResult<List<InNoticeCreateVO>> jsonResult = inNoticeService.createInInNoticeByOut(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createInInNoticeByOutFail() throws Exception {
        InNoticeByOutBean bean = new InNoticeByOutBean();
        bean.setTaskId(329l);
        bean.setPlanInDate(new Date());
        bean.setModifyId("test");
        JsonResult<List<InNoticeCreateVO>> jsonResult = inNoticeService.createInInNoticeByOut(bean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createInInNoticeByOutFail2() throws Exception {
        InNoticeByOutBean bean = new InNoticeByOutBean();
        bean.setTaskId(22L);
        bean.setPlanInDate(new Date());
        bean.setModifyId("test");
        JsonResult<List<InNoticeCreateVO>> jsonResult = inNoticeService.createInInNoticeByOut(bean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    //重复创建
    public void createInInNoticeDouble() throws Exception {
        InNoticeBean inNoticeBean = new InNoticeBean();
        inNoticeBean.setInSourceId(29L);
        inNoticeBean.setPlanInDate(DateUtils.addDay(new Date(),1));
        inNoticeBean.setRepoId(1012l);
        inNoticeBean.setType(1);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean = new SkuInfoBean();
        bean.setAmount(50);
        bean.setBatchNo("1");
        bean.setSkuId(890L);
        bean.setOwnerType(1);
        bean.setOwner(123L);
        bean.setScanType(ScanerType.SCAN.getType());
        //0 非限制
        bean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(bean);
        inNoticeBean.setSystemType(SystemTypeEnum.MAQUILLAGE.getCode());
        inNoticeBean.setSkuInfoList(skuInfoBeanList);
        inNoticeBean.setModifyId("test");
        inNoticeBean.setSystemType(SystemTypeEnum.MAQUILLAGE.getCode());
        JsonResult<InNoticeCreateVO> jsonResult = inNoticeService.createInInNotice(inNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        jsonResult = inNoticeService.createInInNotice(inNoticeBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());

    }

    @Test
    public void queryByInInNoticeQo() throws Exception {
        InNoticeQO qo = new InNoticeQO();
//        List<Integer> statuses = new ArrayList<>();
////        statuses.add(2);
//        qo.setOperStatuses(statuses);
//        qo.setExpressNo("123");
        JsonResult<InNoticeManageListVO> jsonResult = inNoticeService.queryByInInNoticeQo(qo);
        Assert.assertNotNull(jsonResult.getData());


    }

    @Test
    public void queryByInInNoticeQoNull() throws Exception {
        InNoticeQO qo = new InNoticeQO();
//        List<Integer> statuses = new ArrayList<>();
////        statuses.add(2);
//        qo.setOperStatuses(statuses);
//        qo.setExpressNo("123");
        qo.setInNoticeNo("1212313123133");
        JsonResult<InNoticeManageListVO> jsonResult = inNoticeService.queryByInInNoticeQo(qo);
        Assert.assertNull(jsonResult.getData().getVoList());

    }

    @Test
    public void cancelInInNotice() throws Exception {
        CancelInNoticeBean cancelInNoticeBean = new CancelInNoticeBean();
        cancelInNoticeBean.setModifyId("test");
//        cancelInNoticeBean.setFrom("7a7eaa8d82f843299cfee1f80b927d58");
        cancelInNoticeBean.setInNoticeId(230l);
        JsonResult<Boolean> jsonResult = inNoticeService.cancelInInNotice(cancelInNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void cancelInInNoticeFail() throws Exception {
        CancelInNoticeBean cancelInNoticeBean = new CancelInNoticeBean();
        cancelInNoticeBean.setModifyId("test");
//        cancelInNoticeBean.setFrom("7a7eaa8d82f843299cfee1f80b927d58");
        cancelInNoticeBean.setInNoticeId(1232301l);
        JsonResult<Boolean> jsonResult = inNoticeService.cancelInInNotice(cancelInNoticeBean);
        Assert.assertEquals("根据id查找入库通知单失败",jsonResult.getMsg());
    }

    @Test
    public void cancelInInNoticeCanNot() throws Exception {
        CancelInNoticeBean cancelInNoticeBean = new CancelInNoticeBean();
        cancelInNoticeBean.setModifyId("test");
//        cancelInNoticeBean.setFrom("7a7eaa8d82f843299cfee1f80b927d58");
        cancelInNoticeBean.setInNoticeId(153l);
        JsonResult<Boolean> jsonResult = inNoticeService.cancelInInNotice(cancelInNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void getByInInNoticeId() throws Exception {
        InNoticeVo inNoticeVo = inNoticeService.getByInInNoticeId(329L);
        Assert.assertNotNull(inNoticeVo);
    }

    @Test
    public void updateInInNotice() throws Exception {
        ModifyInNoticeBean modifyBean = new ModifyInNoticeBean();
//        inNoticeBean.setInNoticeNo("7a7eaa8d82f843299cfee1f80b927d58");
        modifyBean.setInNoticeId(153L);
        List<ModifyNoticeSkuBean> skuInfoBeanList = new ArrayList<>();
        ModifyNoticeSkuBean bean = new ModifyNoticeSkuBean();
        bean.setAmount(60);
        bean.setId(106l);
        skuInfoBeanList.add(bean);
        modifyBean.setSkuInfoList(skuInfoBeanList);
        modifyBean.setModifyId("test");

        JsonResult<Boolean> jsonResult = inNoticeService.updateInInNotice(modifyBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void updateInInNoticeFAIL() throws Exception {
        ModifyInNoticeBean modifyBean = new ModifyInNoticeBean();
//        inNoticeBean.setInNoticeNo("7a7eaa8d82f843299cfee1f80b927d58");
        modifyBean.setInNoticeId(112312353L);
        List<ModifyNoticeSkuBean> skuInfoBeanList = new ArrayList<>();
        ModifyNoticeSkuBean bean = new ModifyNoticeSkuBean();
        bean.setAmount(60);
        bean.setId(106l);
        skuInfoBeanList.add(bean);
        modifyBean.setSkuInfoList(skuInfoBeanList);
        modifyBean.setModifyId("test");

        JsonResult<Boolean> jsonResult = inNoticeService.updateInInNotice(modifyBean);
        Assert.assertEquals("根据id查找入库通知单失败",jsonResult.getMsg());

    }

    @Test
    public void checkInInNotice() throws Exception {

        InNoticeVo inNoticeVo = inNoticeService.getByInInNoticeId(153L);
        JsonResult<Boolean> jsonResult = inNoticeService.checkInInNotice(inNoticeVo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void checkInInNoticeFail() throws Exception {

        InNoticeVo inNoticeVo = inNoticeService.getByInInNoticeId(152l);
        JsonResult<Boolean> jsonResult = inNoticeService.checkInInNotice(inNoticeVo);
        Assert.assertEquals("该通知单状态不正确",jsonResult.getMsg());
    }

    @Test
    public void getDetailByNoticeNo() throws Exception {
        InNoticeQO qo = new InNoticeQO();
        qo.setId(152L);
        JsonResult<InNoticeDetailListVO> jsonResult = inNoticeService.getDetailByNoticeId(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void getDetailByNoticeNoFail() throws Exception {
        InNoticeQO qo = new InNoticeQO();
        JsonResult<InNoticeDetailListVO> jsonResult = inNoticeService.getDetailByNoticeId(qo);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void updateInNoticeDetailWithRealAmount() throws Exception {

    }

    @Test
    public void getDetailByTaskId() throws Exception {
        ConfirmBO bo = new ConfirmBO();
        bo.setTaskId(2l);
        bo.setRepoId(84l);
        List<InNoticeVo> vos = inNoticeService.getDetailByTaskIdAndRepoId(bo);
        Assert.assertNotNull(vos);
    }

    @Test
    public void selectManageWithDetailByQO() {
        InNoticeQO qo = new InNoticeQO();
        qo.setInNoticeNo("rktzd2018091310503398682");
        JsonResult<InNoticeManageVO> jsonResult = inNoticeService.selectManageWithDetailByQO(qo);
        Assert.assertNotNull(jsonResult.getData());

    }

    @Test
    public void selectManageWithDetailByQOFail() {
        InNoticeQO qo = new InNoticeQO();
        qo.setInNoticeNo("123");
        qo = new InNoticeQO();
        JsonResult jsonResult = inNoticeService.selectManageWithDetailByQO(qo);
        Assert.assertNull(jsonResult.getData());

    }

    @Test
    public void selectAllNOs() {
        JsonResult<List<String>> jsonResult = inNoticeService.selectAllNOs();
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

    }

    @Test
    public void selectAllBatchNoStockIn() {
        InNoticeQO qo = new InNoticeQO();
        qo.setInNoticeNo("rktzd2018091313260970485");
        JsonResult<Set<String>> jsonResult = inNoticeService.selectAllBatchNoStockIn(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void selectAllBatchNoStockInFail() {
        InNoticeQO qo = new InNoticeQO();
        JsonResult<Set<String>> jsonResult = inNoticeService.selectAllBatchNoStockIn(qo);
        Assert.assertEquals("参数为空",jsonResult.getMsg());
    }

    @Test
    public void selectStockOutSkuInfoByMap() {
        StockOutSkuInfoQO qo = new StockOutSkuInfoQO();
        List<String> inNoticeNos  = new ArrayList<>();
        inNoticeNos.add("rktzd2018091313260970485");
        inNoticeNos.add("rktzd2018081616153459875");
        qo.setNoticeNoList(inNoticeNos);
        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult =  inNoticeService.selectStockOutSkuInfoByMap(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void checkInNoticeFinish() {
        JsonResult jsonResult = inNoticeService.checkInNoticeFinish(1235353l);
        Assert.assertEquals("该任务没有对应入库通知单",jsonResult.getMsg());
    }

}