package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryRuleDetail;

public class WmsInventoryRuleDetailMapperTest extends BaseTest{
	
	@Autowired
	private WmsInventoryRuleDetailMapper wmsInventoryRuleDetailMapper;

	@Test
	public void testDeleteByPrimaryKey() {
		wmsInventoryRuleDetailMapper.deleteByPrimaryKey(2l);
	}

	@Test
	public void testInsert() {
		WmsInventoryRuleDetail record = new WmsInventoryRuleDetail(); 
		record.setCreatedDate(new Date());
		record.setCreatedUser("xfeng6Test2");
		record.setDealerId(1236L);
		record.setStockId(23600L);
		wmsInventoryRuleDetailMapper.insert(record);
	}

	@Test
	public void testInsertSelective() {
		WmsInventoryRuleDetail record = new WmsInventoryRuleDetail(); 
		record.setCreatedDate(new Date());
		record.setCreatedUser("xfeng6Test1InsertSeletive");
		record.setDealerId(2236L);
		record.setStockId(43600L);
		wmsInventoryRuleDetailMapper.insertSelective(record);
	}

	@Test
	public void testUpdateByPrimaryKeySelective() {
		WmsInventoryRuleDetail record = new WmsInventoryRuleDetail();
		record.setId(30L);
		record.setCreatedUser("wangkangshabi");
		wmsInventoryRuleDetailMapper.updateByPrimaryKeySelective(record);
	}

}
