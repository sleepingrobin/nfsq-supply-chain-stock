package com.nfsq.supply.chain.stock.microservice.manage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.common.redis.RedisManagerService;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.SapSyncStock;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.SapSyncStockMapper;
import com.nfsq.supply.chain.stock.microservice.manage.persistservice.SapSyncStockService;


/**
 * @author  wangk
 * @date 2018年5月14日 下午4:45:56
 */
public class SapSyncStockTest extends BaseTest {
    @Autowired
    private SapSyncStockService sapSyncStockService;
    @Autowired
    private SapSyncStockMapper sapSyncStockMapper;
    @Autowired
    private RedisManagerService redisManagerService;
	@Test
	public void testQueryStock() {
	    List<String> skuNoList=new ArrayList<String>();
	    skuNoList.add("000000011150200001");
	    skuNoList.add("000000012230800143");
	    Map<String, Integer> map=sapSyncStockService.querySapStock(skuNoList,null);
	   System.out.println(map);
	   
	   SapSyncStock sapSyncStock=sapSyncStockMapper.querySapStock("000000011150200001", null, null);
	   System.out.println(sapSyncStock);
	}
	
	@Test
    public void testReduceSapStock() {
	    try
	    {
	        List<SapSyncStock> list=new ArrayList<>();
	        SapSyncStock sapSyncStock=new SapSyncStock();
	        sapSyncStock.setSkuNo("000000011150200001");
	        sapSyncStock.setBatchNo("20180322");
	        sapSyncStock.setFactoryNo("1610");
	        sapSyncStock.setRepoNo("0001");
	        sapSyncStock.setUnlimitNums(1);
	        list.add(sapSyncStock);
	        SapSyncStock sapSyncStock2=new SapSyncStock();
	        sapSyncStock2.setSkuNo("000000012230800143");
	        sapSyncStock2.setBatchNo("20180317");
	        sapSyncStock2.setFactoryNo("1610");
	        sapSyncStock2.setRepoNo("0001");
	        sapSyncStock2.setUnlimitNums(1);
	        list.add(sapSyncStock2);
	        sapSyncStockService.reduceSapStock(list);
	    }
	    catch(Exception e)
	    {
	        
	    }
	    
    }
	
	@Test
    public void testAddSapStock() {
	    List<SapSyncStock> list=new ArrayList<>();
        SapSyncStock sapSyncStock=new SapSyncStock();
        sapSyncStock.setSkuNo("000000011150200001");
        sapSyncStock.setBatchNo("20180322");
        sapSyncStock.setFactoryNo("1610");
        sapSyncStock.setRepoNo("0001");
        sapSyncStock.setUnlimitNums(100);
        list.add(sapSyncStock);
        sapSyncStockService.addSapStock(list);
    }
	
	@Test
    public void testInsertSapStock() {
        SapSyncStock sapSyncStock=new SapSyncStock();
        sapSyncStock.setSkuNo("000000011150200002");
        sapSyncStock.setBatchNo("2018032200");
        sapSyncStock.setFactoryNo("1610");
        sapSyncStock.setRepoNo("0001");
        sapSyncStock.setUnlimitNums(100);
        sapSyncStockMapper.insertSapStock(sapSyncStock);
    }
	
	@Test
	public void testSapStockRedies() {
	        String skuNo="000000011150200002";
	        String factoryNo="20180322";
	        String repoNo="0001";                
            redisManagerService.querySapStock(skuNo, factoryNo, repoNo);
	        redisManagerService.deleteSapStockRedis(skuNo, factoryNo, repoNo);
	        Long repoId=468L;
	        Long storeId=1L;
	        Long repoId2=468L;
            Long storeId2=103L;           
	        redisManagerService.queryStoreStock(repoId2, storeId2);
	        redisManagerService.deleteStoreStockRedis(repoId, storeId);
	        redisManagerService.queryStoreStock(repoId, storeId);    
	        redisManagerService.queryStoreStock(repoId2, storeId2);
	        redisManagerService.deleteStoreStockRedis(repoId, storeId);
	        redisManagerService.deleteStoreStockRedis(repoId2, storeId2);
	}
	
	@Test
	public void queryAvaliableStockTest() {
	    try
	    {
	        String commonNums=redisManagerService.queryCommonStock(890L, 3, 84L);
	        redisManagerService.deleteCommonStockRedis(890L, 3, 84L);
	        String ruleNums=redisManagerService.queryRuleStock(890L, 3, 84L,2,0,Lists.newArrayList(1L,2L));
	        List<Long> dealerIds=new ArrayList<>();
	        String ruleNums3=redisManagerService.queryRuleStock(890L, 3, 84L,2,0,dealerIds);
	        String ruleNums4=redisManagerService.queryRuleStock(890L, 3, 84L,1,0,dealerIds);
	        String ruleNums2=redisManagerService.queryRuleStock(890L, 3, 84L,1,0,Lists.newArrayList(2L,3L));
	        System.err.println(ruleNums);
            redisManagerService.deleteRuleStockRedis(890L, 3, 84L,2,0);
	    }
	    catch(Exception e)
	    {
	        System.err.println(e);
	    }
	}
}
