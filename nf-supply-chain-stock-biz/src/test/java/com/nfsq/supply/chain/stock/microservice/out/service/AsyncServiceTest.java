package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.microservice.out.service.impl.AsyncServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * com.nfsq.supply.chain.stock.microservice.out.service
 *
 * @Author : Gaohf
 * @Description :
 * @Date : 2018/8/7
 */
public class AsyncServiceTest extends BaseTest{

    @Autowired
    private AsyncServiceImpl asyncService;

    @Test
    public void test(){
//        asyncService.sendSapMessage(162L, InOutTypeEnums.IN_NOTICE_STOCK);
    }

    @Test
    public void sendSapInNotice() {
//        asyncService.sendSapInNotice(57L);
    }

    @Test
    public void sendInNoticeMessageFail() {
        StockInBean bean = new StockInBean();
        bean.setInNoticeId(123412313L);
        asyncService.sendInNoticeMessage(bean);
    }

    @Test
    public void sendInNoticeMessage() {
        StockInBean bean = new StockInBean();
        bean.setInNoticeId(172L);
        asyncService.sendInNoticeMessage(bean);
    }
}
