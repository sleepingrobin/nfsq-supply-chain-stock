package com.nfsq.supply.chain.stock.microservice.notice.service;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.BillNoQO;
import com.nfsq.supply.chain.stock.api.model.qo.NoticeFinishQO;
import com.nfsq.supply.chain.stock.api.model.vo.BillCountVO;
import com.nfsq.supply.chain.stock.api.model.vo.BillVO;
import com.nfsq.supply.chain.stock.api.model.vo.ProcessCountVO;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author xwcai
 * @date 2018/6/20 下午4:19
 */
public class NoticeServiceTest extends BaseTest {

    @Autowired
    NoticeService noticeService;


    @Test
    public void checkNoticeFinish() {
        NoticeFinishQO qo = new NoticeFinishQO();
        qo.setQueryType(InOutTypeEnums.IN_NOTICE_STOCK.getCode());
        qo.setTaskId(437l);
        JsonResult<Boolean> jsonResult = noticeService.checkNoticeFinish(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void checkNoticeFinishFail() {
        NoticeFinishQO qo = new NoticeFinishQO();
        qo.setQueryType(7);
        qo.setTaskId(437l);
        JsonResult<Boolean> jsonResult = noticeService.checkNoticeFinish(qo);
        Assert.assertEquals("输入的类型id不正确",jsonResult.getMsg());
    }

    @Test
    public void checkNoticeFinish2() {
        NoticeFinishQO qo = new NoticeFinishQO();
        qo.setQueryType(InOutTypeEnums.OUT_NOTICE_STOCK.getCode());
        qo.setTaskId(308l);
        JsonResult<Boolean> jsonResult = noticeService.checkNoticeFinish(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryBillNOByQO()  {
        BillNoQO qo = new BillNoQO();
        qo.setTaskId(22973l);
        JsonResult<BillVO> jsonResult = noticeService.queryBillNOByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryBillCountByQO() {

        BillCountQO qo = new BillCountQO();
        qo.setCurrentSystemType(SystemTypeEnum.MAQUILLAGE.getCode());
        JsonResult<BillCountVO> jsonResult = noticeService.queryBillCountByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

        qo.setCurrentSystemType(SystemTypeEnum.APPLE.getCode());
        jsonResult = noticeService.queryBillCountByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

        qo.setCurrentSystemType(SystemTypeEnum.RICE.getCode());
        jsonResult = noticeService.queryBillCountByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

        qo.setCurrentSystemType(SystemTypeEnum.WATER.getCode());
        jsonResult = noticeService.queryBillCountByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryProcessCount() {
        JsonResult<ProcessCountVO> jsonResult = noticeService.queryProcessCount();
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }
}