package com.nfsq.supply.chain.stock.microservice.baseinfo.service;

import com.nfsq.supply.chain.datacenter.api.response.Sku;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.microservice.common.enums.StockOperStatusEnum;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author xwcai
 * @date 2018/5/28 上午8:58
 */
public class BaseInfoServiceTest extends BaseTest{

    @Autowired
    BaseInfoService baseInfoService;

    @Test
    public void getNOByCharacter() throws Exception {

        System.out.println(baseInfoService.getNOByCharacter("abc"));
    }

    @Test
    public void getSkuDetailBySkuId() {
        try {
            List<Long> skuIds = new ArrayList<>();
            List<Sku> list = baseInfoService.getSkuDetailBySkuId(skuIds);
        }catch (ChainStockException e) {
            Assert.assertEquals("获取sku详情失败，skuId 不能为空：",e.getMessage());
        }try{
            List<Long> skuIds = new ArrayList<>();
            skuIds.add(3808L);
            List<Sku> list = baseInfoService.getSkuDetailBySkuId(skuIds);
        }catch (Exception e){
            Assert.assertEquals("获取sku详情失败：null",e.getMessage());
        }
    }

    @Test
    public void getStockOperStatus() {
        List<Integer> operStatus = new ArrayList<>();
        operStatus.add(StockOperStatusEnum.STARTING.getType());
        StockOperStatusEnum statusEnum = baseInfoService.getStockOperStatus(operStatus);
        Assert.assertSame(StockOperStatusEnum.STARTING,statusEnum);

        operStatus.clear();
        operStatus.add(StockOperStatusEnum.UN_START.getType());
        statusEnum = baseInfoService.getStockOperStatus(operStatus);
        Assert.assertSame(StockOperStatusEnum.STARTING,statusEnum);

        operStatus.clear();
        operStatus.add(StockOperStatusEnum.MORE.getType());
        statusEnum = baseInfoService.getStockOperStatus(operStatus);
        Assert.assertSame(StockOperStatusEnum.STARTING,statusEnum);

        operStatus.clear();
        operStatus.add(6);
        statusEnum = baseInfoService.getStockOperStatus(operStatus);
        Assert.assertNull(statusEnum);
    }

    public static void  setSystemType(){
        DataAuthorityBean bean=new DataAuthorityBean();
        bean.setLoginId("xwcai");
        bean.setCurrentSystemType(2);
        DataAuthorityUtils.setCurrentDataAuthorityBean(bean);
    }
}