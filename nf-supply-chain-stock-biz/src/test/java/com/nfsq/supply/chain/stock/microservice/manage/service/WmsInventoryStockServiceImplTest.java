package com.nfsq.supply.chain.stock.microservice.manage.service;

import com.google.common.collect.Lists;
import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.datacenter.api.common.enums.OwnerTypeEnum;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.bean.WmsAvaliableStockBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.enums.InOutTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleGroupEnums;
import com.nfsq.supply.chain.stock.api.model.enums.RuleTypeEnums;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.FanAndStoreStockInfoVO;
import com.nfsq.supply.chain.stock.api.model.vo.RepoOrStockVO;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author  wangk
 * @date 2018年5月14日 下午4:45:56
 */
public class WmsInventoryStockServiceImplTest extends BaseTest {
	@Autowired
	private WmsInventoryStockService service;

	/**
	 * Test method for {@link com.nfsq.supply.chain.stock.microservice.manage.service.impl.WmsInventoryStockServiceImpl#(com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock)}.
	 */
	@Test
	public void testQueryStock() {
		WmsStockQO stock = new WmsStockQO();
		stock.setBatchNo("2018061310");
		List<WmsInventoryStockDTO> list=service.queryStock(stock);
		WmsStockQO stock2 = new WmsStockQO();
        stock2.setId(177L);
        stock2.setRepoId(11L);
        stock2.setType(1);
        List<WmsInventoryStockDTO> list2=service.queryStock(stock2);
        WmsStockQO stock3 = new WmsStockQO();
        stock3.setId(97563L);
        stock3.setType(3);
        List<WmsInventoryStockDTO> list3=service.queryStock(stock3);
        WmsStockQO stock4 = new WmsStockQO();
        stock4.setId(97563L);
        stock4.setRepoId(11L);
        stock4.setType(3);
        try {
			List<WmsInventoryStockDTO> list4 = service.queryStock(stock4);
		}catch (Exception e){
			Assert.assertEquals("门店不存在",e.getMessage());
		}
	}
	
	   @Test
	    public void testQueryRepos() {
	       RepoQO qo = new RepoQO();
	       qo.setId(177L);
	       qo.setType(1);
	       RepoOrStockVO vo=service.queryRepos(qo);
	       RepoQO qo2 = new RepoQO();
           qo2.setId(97563L);
           qo2.setType(3);
           RepoOrStockVO vo2=service.queryRepos(qo2);
	        System.out.println(vo);
	    }

	
	/**
	 * Test method for {@link com.nfsq.supply.chain.stock.microservice.manage.service.impl.WmsInventoryStockServiceImpl#updateStock(java.util.List)}.
	 */
	@Test
	public void testUpdateStock() {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				List<WmsStockBean> wmsStockBeans = new ArrayList<>();
				WmsStockBean out = new WmsStockBean();
				WmsStockBean inNotice = new WmsStockBean();
				WmsStockBean in = new WmsStockBean();
				out.setBatchNo("A0478");
				out.setCreatedDate(new Date());
				out.setCreatedUser("王康");
				out.setNums(1);
				out.setOrderDetailId(1L);
				out.setOwner(101L);
				out.setOwnerType(OwnerTypeEnum.COMPANY.getCode());
				out.setRepoId(84L);
				out.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
				out.setRuleType(RuleTypeEnums.COMMON.getCode());
				out.setSkuId(890L);
				out.setType(InOutTypeEnums.OUT_STOCK.getCode());
				out.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
				out.setDealerId(123L);
				wmsStockBeans.add(out);
				BeanUtils.copyProperties(out, inNotice);
				inNotice.setOrderDetailId(2L);
				inNotice.setRepoId(234L);
				inNotice.setRuleType(RuleTypeEnums.COMMON.getCode());
				inNotice.setType(InOutTypeEnums.IN_NOTICE_STOCK.getCode());
				inNotice.setStatus(null);
				inNotice.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());
				wmsStockBeans.add(inNotice);
				BeanUtils.copyProperties(inNotice, in);
				in.setOrderDetailId(3L);
				in.setType(InOutTypeEnums.IN_STOCK.getCode());
				in.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());
				in.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
				wmsStockBeans.add(in);
				service.updateStock(wmsStockBeans);
			}
		});
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				List<WmsStockBean> wmsStockBeans = new ArrayList<>();
				WmsStockBean out = new WmsStockBean();
				WmsStockBean inNotice = new WmsStockBean();
				WmsStockBean in = new WmsStockBean();
				out.setBatchNo("201804028");
				out.setCreatedDate(new Date());
				out.setCreatedUser("王康");
				out.setNums(10);
				out.setOrderDetailId(1L);
				out.setOwner(1334L);
				out.setRepoId(233L);
				out.setRuleGroup(RuleGroupEnums.DUMP.getCode());
				out.setRuleType(RuleTypeEnums.EXCLUSIVE_DISTRIBUTOR.getCode());
				out.setSkuId(2L);
				out.setType(InOutTypeEnums.OUT_STOCK.getCode());
				out.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
				out.setDealerId(123L);
				wmsStockBeans.add(out);
				BeanUtils.copyProperties(out, inNotice);
				inNotice.setOrderDetailId(2L);
				inNotice.setRepoId(234L);
				inNotice.setRuleType(RuleTypeEnums.COMMON.getCode());
				inNotice.setType(InOutTypeEnums.IN_NOTICE_STOCK.getCode());
				inNotice.setStatus(null);
				inNotice.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());
				wmsStockBeans.add(inNotice);
				BeanUtils.copyProperties(inNotice, in);
				in.setOrderDetailId(3L);
				in.setType(InOutTypeEnums.IN_STOCK.getCode());
				in.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_ON_THE_WAY.getCode());
				in.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
				wmsStockBeans.add(in);
				service.updateStock(wmsStockBeans);
			}
		});
		t1.start();
		t2.start();
		try {
			Thread.sleep(3000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link com.nfsq.supply.chain.stock.microservice.manage.service.impl.WmsInventoryStockServiceImpl#queryAvaliableStock(java.util.List)}.
	 */
	@Test
	public void testQueryAvaliableStock() {
		List<WmsAvaliableStockBO> wmsAvaliableStockBOs = new ArrayList<>();
		WmsAvaliableStockBO bean = new WmsAvaliableStockBO();
		bean.setSkuId(890L);
		bean.setRepoId(84L);
		bean.setStatuses(StockStatusEnums.getAvaliableStatus());
		bean.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
		bean.setRuleType(RuleTypeEnums.EXCLUSIVE_DISTRIBUTOR.getCode());
		bean.setDealerIds(Lists.newArrayList(2233L));
		wmsAvaliableStockBOs.add(bean);
		AvaliableStockQO qo=new AvaliableStockQO();
		qo.setWmsAvaliableStockBOs(wmsAvaliableStockBOs);
		service.queryAvaliableStock(wmsAvaliableStockBOs);
	}

	@Test
	public void testQueryStoreStock() {
		StoreStockQO storeStockQO=new StoreStockQO();
		storeStockQO.setRepoId(84L);
		storeStockQO.setStoreId(11L);
		Map<Long, Integer> skuNumsMap=service.queryStoreStock(storeStockQO);
		System.out.println(skuNumsMap);
	}
	
	@Test
    public void testQueryStockNums() {
        Long skuId=890L;
        Long repoId=84L;
        List<SkuNumsBO> list= service.selectUnlimitStockByCondition(skuId, repoId, (3),"0000011839",101L,1);
        System.out.println(list);
    }
	
	@Test
    public void testUpdateStock2() {
	    List<WmsStockBean> wmsStockBeans = new ArrayList<>();
        WmsStockBean out = new WmsStockBean();
        out.setBatchNo("2018061310");
        out.setNums(10);
        out.setOrderDetailId(1L);
        out.setOwner(101L);
        out.setOwnerType(OwnerTypeEnum.COMPANY.getCode());
        out.setRepoId(84L);
        out.setRuleGroup(RuleGroupEnums.SALE.getCode());
        out.setRuleType(RuleTypeEnums.KA.getCode());
        out.setSkuId(892L);
        out.setType(InOutTypeEnums.OUT_STOCK.getCode());
        out.setSystemType(SystemTypeEnum.MAQUILLAGE.getCode());
        out.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        //out.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        wmsStockBeans.add(out);
        service.updateStock(wmsStockBeans);
	}
	
	@Test
    public void testSpecifyReduceStock() {
        List<WmsStockBean> wmsStockBeans = new ArrayList<>();
        WmsStockBean out = new WmsStockBean();
        out.setBatchNo("20181001");
        out.setNums(11);
        //out.setOrderDetailId(1L);
        out.setOwner(168L);
        out.setOwnerType(OwnerTypeEnum.COMPANY.getCode());
        out.setRepoId(11L);
        out.setRuleGroup(RuleGroupEnums.CURRENCY.getCode());
        out.setRuleType(RuleTypeEnums.COMMON.getCode());
        out.setSkuId(25514L);
        out.setType(InOutTypeEnums.MACHINE_REPLENISH_STOCK.getCode());
        out.setSystemType(SystemTypeEnum.RICE.getCode());
        out.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        //out.setTargetStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        wmsStockBeans.add(out);
        service.updateStock(wmsStockBeans);
    }
	
	@Test
    public void testQueryBatchNo() {
	    BatchNoQO qo=new BatchNoQO();
	    //qo.setBatchNo("2018");
	    List<String> list=service.queryBatchNos(qo);
	    System.err.println(list);
    }

    @Test
	public void testQueryFranAndStore() {
		WmsStockQO qo = new WmsStockQO();
		qo.setId(26733l);
		qo.setType(3);
		//sku 可选
		List<FanAndStoreStockInfoVO> fanAndStoreStockInfoVOS = service.queryStockByFanAndStore(qo);
		for (FanAndStoreStockInfoVO fanAndStoreStockInfoVO : fanAndStoreStockInfoVOS) {
			System.out.println(JsonUtils.toJson(fanAndStoreStockInfoVO));
		}

		System.out.println("--------------------------------");
		WmsStockQO qo1 = new WmsStockQO();
		qo1.setId(26733l);
		qo1.setType(3);
		qo1.setSkuId(939l);
		//sku 可选
		List<FanAndStoreStockInfoVO> fanAndStoreStockInfoVOS2 = service.queryStockByFanAndStore(qo1);
		for (FanAndStoreStockInfoVO fanAndStoreStockInfoVO : fanAndStoreStockInfoVOS2) {
			System.out.println(JsonUtils.toJson(fanAndStoreStockInfoVO));
		}
	}
}
