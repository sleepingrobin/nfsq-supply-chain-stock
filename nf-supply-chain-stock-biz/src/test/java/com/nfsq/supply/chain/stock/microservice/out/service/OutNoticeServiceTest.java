package com.nfsq.supply.chain.stock.microservice.out.service;

import com.google.common.collect.Lists;
import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.*;
import com.nfsq.supply.chain.stock.api.model.dto.WmsOutOutNoticeDTO;
import com.nfsq.supply.chain.stock.api.model.enums.DestinationEnum;
import com.nfsq.supply.chain.stock.api.model.enums.ScanerType;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.*;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.common.ChainStockException;
import com.nfsq.supply.chain.stock.common.kafka.ProduceMessageService;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.mapper.WmsInventoryStockMapper;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNotice;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutNoticeDetail;
import com.nfsq.supply.chain.stock.microservice.out.model.bean.CancelOutNoticeBean;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeListVo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutNoticeVo;
import com.nfsq.supply.chain.utils.common.DateUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.common.JsonUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xwcai
 * @date 2018/4/23 下午5:01
 */
public class OutNoticeServiceTest extends BaseTest{


    private static final Logger logger = LoggerFactory.getLogger(OutNoticeServiceTest.class);


    @Autowired
    OutNoticeService outNoticeService;

    @Autowired
    ProduceMessageService produceMessageService;

    @Autowired
    WmsInventoryStockMapper mapper;

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void listCreateOutOutNotice() throws Exception {
        OutNoticeListBean listBean = new OutNoticeListBean();
        listBean.setModifyId("test");
        List<OutNoticeBean> beans = new ArrayList<>();
        for(int i = 1;i<=2;i++){
            OutNoticeBean outNoticeBean = new OutNoticeBean();
            outNoticeBean.setModifyId("test");
            outNoticeBean.setType(1);
            outNoticeBean.setDestinationId(1002l);
            outNoticeBean.setDestinationType(DestinationEnum.FACTORY.getType());
            outNoticeBean.setOutSourceId(100003L + i);
            outNoticeBean.setPlanOutDate(DateUtils.addDay(new Date(),1));
            outNoticeBean.setRepoId(84l);
            outNoticeBean.setSystemType(SystemTypeEnum.WATER.getCode());

            List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
            SkuInfoBean bean1 = new SkuInfoBean();
            bean1.setAmount(50);
            bean1.setSkuId(890L);
            //0 非限制
            bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
            bean1.setRuleGroup(1);
            bean1.setRuleType(3);
            bean1.setRuleDetail((long) 10);
            bean1.setScanType(ScanerType.SCAN.getType());
            bean1.setTargetOwner(123L);
            bean1.setTargetOwnerType(1);
            skuInfoBeanList.add(bean1);

            SkuInfoBean bean2 = new SkuInfoBean();
            bean2.setAmount(50);
            bean2.setSkuId(891L);
            //0 非限制
            bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
            bean2.setRuleGroup(1);
            bean2.setRuleType(3);
            bean2.setRuleDetail((long) 10);
            bean2.setScanType(ScanerType.SCAN.getType());
            bean2.setTargetOwner(123L);
            bean2.setTargetOwnerType(1);
            skuInfoBeanList.add(bean2);

            SkuInfoBean bean3 = new SkuInfoBean();
            bean3.setAmount(50);
            bean3.setSkuId(892L);
            //0 非限制
            bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
            bean3.setRuleGroup(1);
            bean3.setRuleType(3);
            bean3.setRuleDetail((long) 10);
            bean3.setScanType(ScanerType.SCAN.getType());
            bean3.setTargetOwner(123L);
            bean3.setTargetOwnerType(1);
            skuInfoBeanList.add(bean3);

            outNoticeBean.setSkuInfoList(skuInfoBeanList);
            beans.add(outNoticeBean);
        }
        listBean.setNoticeBeanList(beans);
        System.out.println(JsonUtils.toJson(listBean));
        JsonResult<Map<String,String>> jsonResult = outNoticeService.listCreateOutOutNotice(listBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryWithDetailByQo() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setRepoId(84l);
        JsonResult<OutNoticeVOList> jsonResult = outNoticeService.queryWithDetailByQo(qo);
        Assert.assertNotNull(jsonResult.getData().getOutNoticeVos());
    }

    @Test
    public void queryWithDetailByQoFail() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setRepoId(81234l);
        JsonResult<OutNoticeVOList> jsonResult = outNoticeService.queryWithDetailByQo(qo);
        Assert.assertNull(jsonResult.getData().getOutNoticeVos());
    }

    @Test
    public void bindOutOutNotice() throws Exception {
        BindOutNoticeBean bean = new BindOutNoticeBean();
        bean.setMachineCode("862951031027353");
        bean.setNoticeId(110619950L);
        JsonResult<Boolean> jsonResult = outNoticeService.bindOutOutNotice(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

        ScanerNoticePageQo qo = new ScanerNoticePageQo();
        qo.setMachineCode("862951031027353");
        JsonResult<ScanerNoticePageByScanTypeVO> result = outNoticeService.queryWithScanersPage(qo);
        Assert.assertNotNull(result.getData().getNoticeList());
//        bean.setNoticeId(12L);
//        JsonResult<Boolean> jsonResult2 = outNoticeService.bindOutOutNotice(bean);
//        Assert.assertEquals("该通知单不存在",jsonResult2.getMsg());
//
//        bean.setNoticeId(11226L);
//        JsonResult<Boolean> jsonResult3 = outNoticeService.bindOutOutNotice(bean);
//        Assert.assertEquals("该通知单已被绑定",jsonResult3.getMsg());
    }

    @Test
    public void queryWithScaner() throws Exception {
        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeNo("cktzd2018092717352632102");
        qo.setLoginId("xwcai");
        qo.setMachineCode("359832070237733");
        qo.setCurrentSystemType(2);
        JsonResult<ScanerNoticeVO> jsonResult = outNoticeService.queryWithScaner(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryWithScanerFail() throws Exception {
        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeNo("cktzd201809271735dasd2632102");
        qo.setLoginId("xwcai");
        qo.setMachineCode("359832070237733");
        qo.setCurrentSystemType(2);
        JsonResult<ScanerNoticeVO> jsonResult = outNoticeService.queryWithScaner(qo);
        Assert.assertEquals("该出库通知单不存在或已经被接单，请查证后再试",jsonResult.getMsg());
    }


    @Test
    public void queryWithScanerQO() {
        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeId(11237L);
        qo.setLoginId("xwcai");
        qo.setCurrentSystemType(2);
        qo.setMachineCode("359832070237733");
        JsonResult<ScanerNoticeVO> jsonResult = outNoticeService.queryWithScanerQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryWithScanerQOFail() {
        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeId(112373L);
        qo.setLoginId("xwcai");
        qo.setCurrentSystemType(2);
        qo.setMachineCode("359832070237733");
        JsonResult<ScanerNoticeVO> jsonResult = outNoticeService.queryWithScanerQO(qo);
        Assert.assertEquals("该出库通知单不存在或已经被接单，请查证后再试",jsonResult.getMsg());
    }

    @Test
    public void queryWithScanerPage() throws Exception {
        ScanerNoticePageQo qo = new ScanerNoticePageQo();
        qo.setMachineCode("862951031027353");
        qo.setLoginId("xwcai");
        qo.setCurrentSystemType(2);
//        JsonResult<ScanerNoticePageVO> jsonResult = outNoticeService.queryWithScanerPage(qo);
        JsonResult<ScanerNoticePageByScanTypeVO> jsonResult = outNoticeService.queryWithScanersPage(qo);
        Assert.assertNotNull(jsonResult.getData().getNoticeList());

    }

    @Test
    public void queryWithScanerPageFail() throws Exception {
        ScanerNoticePageQo qo = new ScanerNoticePageQo();
        qo.setMachineCode("862951031027353123");
//        JsonResult<ScanerNoticePageVO> jsonResult = outNoticeService.queryWithScanerPage(qo);
        JsonResult<ScanerNoticePageByScanTypeVO> jsonResult = outNoticeService.queryWithScanersPage(qo);
        Assert.assertNull(jsonResult.getMsg(),jsonResult.getData().getNoticeList());

    }

    @Test
    public void outOutNoticeCreate() throws Exception {
        OutNoticeBean outNoticeBean = new OutNoticeBean();
        outNoticeBean.setModifyId("test");
        outNoticeBean.setType(1);
        outNoticeBean.setDestinationId(82l);
        outNoticeBean.setDestinationType(DestinationEnum.FACTORY.getType());
        outNoticeBean.setOutSourceId(22L);
        outNoticeBean.setPlanOutDate(DateUtils.addDay(new Date(),1));
        outNoticeBean.setRepoId(84l);


        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(50);
        bean1.setSkuId(890L);
        //0 非限制
        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean1.setRuleGroup(1);
        bean1.setRuleType(3);
        bean1.setRuleDetail((long) 10);
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setTargetOwner(123L);
        bean1.setTargetOwnerType(1);
        skuInfoBeanList.add(bean1);
        
        SkuInfoBean bean2 = new SkuInfoBean();
        bean2.setAmount(50);
        bean2.setSkuId(891L);
        //0 非限制
        bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean2.setRuleGroup(1);
        bean2.setRuleType(3);
        bean2.setRuleDetail((long) 10);
        bean2.setScanType(ScanerType.SCAN.getType());
        bean2.setTargetOwner(123L);
        bean2.setTargetOwnerType(1);
        skuInfoBeanList.add(bean2);
        
        SkuInfoBean bean3 = new SkuInfoBean();
        bean3.setAmount(50);
        bean3.setSkuId(892L);
        //0 非限制
        bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean3.setRuleGroup(1);
        bean3.setRuleType(3);
        bean3.setRuleDetail((long) 10);
        bean3.setScanType(ScanerType.SCAN.getType());
        bean3.setTargetOwner(123L);
        bean3.setTargetOwnerType(1);
        skuInfoBeanList.add(bean3);

        outNoticeBean.setSystemType(SystemTypeEnum.RICE.getCode());
        outNoticeBean.setSkuInfoList(skuInfoBeanList);

        JsonResult<String> jsonResult = outNoticeService.outOutNoticeCreate(outNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void outOutNoticeCreateFail() throws Exception {
        OutNoticeBean outNoticeBean = new OutNoticeBean();
        outNoticeBean.setModifyId("test");
        outNoticeBean.setType(1);
        outNoticeBean.setDestinationId(82l);
        outNoticeBean.setDestinationType(DestinationEnum.FACTORY.getType());
        outNoticeBean.setPlanOutDate(DateUtils.addDay(new Date(),1));
        outNoticeBean.setRepoId(84l);


        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(50);
        bean1.setSkuId(890L);
        //0 非限制
        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean1.setRuleGroup(1);
        bean1.setRuleType(3);
        bean1.setRuleDetail((long) 10);
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setTargetOwner(123L);
        bean1.setTargetOwnerType(1);
        skuInfoBeanList.add(bean1);

        SkuInfoBean bean2 = new SkuInfoBean();
        bean2.setAmount(50);
        bean2.setSkuId(891L);
        //0 非限制
        bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean2.setRuleGroup(1);
        bean2.setRuleType(3);
        bean2.setRuleDetail((long) 10);
        bean2.setScanType(ScanerType.SCAN.getType());
        bean2.setTargetOwner(123L);
        bean2.setTargetOwnerType(1);
        skuInfoBeanList.add(bean2);

        SkuInfoBean bean3 = new SkuInfoBean();
        bean3.setAmount(50);
        bean3.setSkuId(892L);
        //0 非限制
        bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean3.setRuleGroup(1);
        bean3.setRuleType(3);
        bean3.setRuleDetail((long) 10);
        bean3.setScanType(ScanerType.SCAN.getType());
        bean3.setTargetOwner(123L);
        bean3.setTargetOwnerType(1);
        skuInfoBeanList.add(bean3);

        outNoticeBean.setSystemType(SystemTypeEnum.RICE.getCode());
        outNoticeBean.setSkuInfoList(skuInfoBeanList);

        JsonResult<String> jsonResult = outNoticeService.outOutNoticeCreate(outNoticeBean);
        Assert.assertFalse(jsonResult.isSuccess());

        outNoticeBean.setOutSourceId(29l);
        outNoticeBean.setRepoId(123123123l);
        try{
            jsonResult = outNoticeService.outOutNoticeCreate(outNoticeBean);
        }catch (ChainStockException e){
            Assert.assertEquals("创建出库通知单时，获取仓库信息失败",e.getMessage());
        }
    }

    @Test
    public void getByOutOutNoticeNO() throws Exception {

        OutOutNoticeVo outNoticeVo = outNoticeService.getByOutOutNoticeId(12L);
        Assert.assertNull(outNoticeVo);
    }

    @Test
    @Transactional
    public void cancelOutOutNotice() throws Exception {
        CancelOutNoticeBean cancelBean = new CancelOutNoticeBean();
        cancelBean.setModifyId("test");
        cancelBean.setOutNoticeId(1578L);
        JsonResult<Boolean> jsonResult = outNoticeService.cancelOutOutNotice(cancelBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    @Transactional
    public void cancelOutOutNoticeFail() throws Exception {
        CancelOutNoticeBean cancelBean = new CancelOutNoticeBean();
        cancelBean.setModifyId("test");
        cancelBean.setOutNoticeId(854L);
        JsonResult<Boolean> jsonResult = outNoticeService.cancelOutOutNotice(cancelBean);
        Assert.assertEquals("该出库通知单正在出库中，不能取消",jsonResult.getMsg());
    }

    @Test
    @Transactional
    public void cancelOutOutNoticeFail2() throws Exception {
        CancelOutNoticeBean cancelBean = new CancelOutNoticeBean();
        cancelBean.setModifyId("test");
        cancelBean.setOutNoticeId(344L);
        JsonResult<Boolean> jsonResult = outNoticeService.cancelOutOutNotice(cancelBean);
        Assert.assertEquals("该出库通知单已经取消",jsonResult.getMsg());
    }

    @Test
    @Transactional
    public void cancelOutOutNoticeFail3() throws Exception {
        CancelOutNoticeBean cancelBean = new CancelOutNoticeBean();
        cancelBean.setModifyId("test");
        cancelBean.setOutNoticeId(851234L);
        JsonResult<Boolean> jsonResult = outNoticeService.cancelOutOutNotice(cancelBean);
        Assert.assertEquals("该出库通知单不存在",jsonResult.getMsg());
    }

    @Test
    @Transactional
    public void updateOutOutNoticeFail() throws Exception {
        OutNoticeBean outNoticeBean = new OutNoticeBean();
        outNoticeBean.setModifyId("test");
        outNoticeBean.setOutNoticeId(854L);

        JsonResult<Boolean> jsonResult = outNoticeService.updateOutOutNotice(outNoticeBean);
        Assert.assertEquals("参数不合法",jsonResult.getMsg());
    }

    @Transactional
    @Test
    public void updateOutOutNotice() throws Exception {
        OutNoticeBean outNoticeBean = new OutNoticeBean();
        outNoticeBean.setModifyId("test");
        outNoticeBean.setOutNoticeId(11216L);
        outNoticeBean.setOutSourceId(22862L);
        outNoticeBean.setModifyId("test");
        outNoticeBean.setType(1);
        outNoticeBean.setDestinationId(82l);
        outNoticeBean.setDestinationType(DestinationEnum.FACTORY.getType());
        outNoticeBean.setOutSourceId(22L);
        outNoticeBean.setPlanOutDate(DateUtils.addDay(new Date(),1));
        outNoticeBean.setRepoId(84l);


        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(50);
        bean1.setSkuId(890L);
        //0 非限制
        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean1.setRuleGroup(1);
        bean1.setRuleType(3);
        bean1.setRuleDetail((long) 10);
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setScanType(ScanerType.SCAN.getType());
        bean1.setTargetOwner(123L);
        bean1.setTargetOwnerType(1);
        skuInfoBeanList.add(bean1);

        SkuInfoBean bean2 = new SkuInfoBean();
        bean2.setAmount(50);
        bean2.setSkuId(891L);
        //0 非限制
        bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean2.setRuleGroup(1);
        bean2.setRuleType(3);
        bean2.setRuleDetail((long) 10);
        bean2.setScanType(ScanerType.SCAN.getType());
        bean2.setTargetOwner(123L);
        bean2.setTargetOwnerType(1);
        skuInfoBeanList.add(bean2);

        SkuInfoBean bean3 = new SkuInfoBean();
        bean3.setAmount(50);
        bean3.setSkuId(892L);
        //0 非限制
        bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean3.setRuleGroup(1);
        bean3.setRuleType(3);
        bean3.setRuleDetail((long) 10);
        bean3.setScanType(ScanerType.SCAN.getType());
        bean3.setTargetOwner(123L);
        bean3.setTargetOwnerType(1);
        skuInfoBeanList.add(bean3);

        outNoticeBean.setSystemType(SystemTypeEnum.RICE.getCode());
        outNoticeBean.setSkuInfoList(skuInfoBeanList);
        JsonResult<Boolean> jsonResult = outNoticeService.updateOutOutNotice(outNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void checkOutOutNotice() throws Exception {
        OutOutNoticeVo outNoticeVo = outNoticeService.getByOutOutNoticeId(12L);
        JsonResult<Boolean> jsonResult = outNoticeService.checkOutOutNotice(outNoticeVo);
        Assert.assertEquals("该通知单不存在",jsonResult.getMsg());
    }

    @Test
    public void queryByQo() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutSourceId(301l);
        JsonResult<OutNoticeVOList> jsonResult = outNoticeService.queryByQo(qo);
        Assert.assertNotNull(jsonResult.getMsg(),jsonResult.getData().getOutNoticeVos());
    }

    @Test
    public void queryByQoNukl() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutSourceId(301231l);
        JsonResult<OutNoticeVOList> jsonResult = outNoticeService.queryByQo(qo);
        Assert.assertNull(jsonResult.getMsg(),jsonResult.getData().getOutNoticeVos());
    }

    @Test
    public void queryByQoPage() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        JsonResult<OutOutNoticeListVo> jsonResult = outNoticeService.queryByQoPage(qo);
        Assert.assertNotNull(jsonResult.getData().getOutOutNotices());
    }

    @Test
    public void getDetailByNoticeNo() throws Exception {

        JsonResult<List<WmsOutOutNoticeDetail>> jsonResult = outNoticeService.getDetailByNoticeNo(11379L);
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void getDetailByNoticeNoNull() throws Exception {

        JsonResult<List<WmsOutOutNoticeDetail>> jsonResult = outNoticeService.getDetailByNoticeNo(12L);
        Assert.assertNull(jsonResult.getMsg(),jsonResult.getData());
    }

    @Test
    public void updateInNoticeDetailWithRealAmount() throws Exception {
        OutNoticeVO dto = new OutNoticeVO();
        OutOutNoticeVo vo = new OutOutNoticeVo();
        WmsOutOutNoticeDTO noticeDTO = new WmsOutOutNoticeDTO();
        WmsOutOutNotice outOutNotice = new WmsOutOutNotice();
        WmsOutOutNoticeDetail detail = new WmsOutOutNoticeDetail();
        List<WmsOutOutNoticeDetail> details = new ArrayList<>();
        outOutNotice.setId(1L);
        outOutNotice.setDestinationId(23L);
        detail.setId(2L);
        details.add(detail);
        vo.setWmsOutOutNotice(outOutNotice);
        vo.setWmsOutOutNoticeDetails(details);
        BeanUtils.copyProperties(vo,dto);
        BeanUtils.copyProperties(outOutNotice,noticeDTO);
    }

    @Test
    public void queryOccpyStock(){
        WmsAvaliableStockBO bo = new WmsAvaliableStockBO();
        Long dealerId = 23l;
        bo.setDealerIds(Lists.newArrayList(dealerId));
//        statues.add(1);
        bo.setStatuses(StockStatusEnums.getAvaliableStatus());
        bo.setRepoId(1012l);
        bo.setRuleGroup(2);
        bo.setRuleType(3);
        bo.setSkuId(1l);
        Long num = outNoticeService.queryOccpyStock(bo);
        Assert.assertNotNull(num);
    }

    @Test
    public void manageQueryByQO() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
//        qo.setOutNoticeNo("20180529");
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(0);
//        qo.setOperStatus(list);
//        qo.setOutSourceId(106l);
//        qo.setType(1);
        JsonResult<OutNoticeManageListVO> jsonResult = outNoticeService.manageQueryByQO(qo);
        Assert.assertNotNull(jsonResult.getMsg(),jsonResult.getData().getNoticeManageVOS());
    }

    @Test
    public void manageQueryWaitByQO() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
//        qo.setOutNoticeNo("20180529");
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(0);
//        qo.setOperStatus(list);
//        qo.setOutSourceId(106l);
//        qo.setType(1);
//        qo.setExpressNo("123");
        JsonResult<OutNoticeManageListVO> jsonResult = outNoticeService.manageQueryWaitByQO(qo);
        Assert.assertNotNull(jsonResult.getData().getNoticeManageVOS());
    }

    @Test
    public void manageQuery() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setId(854l);
        qo.setIsPage(1);
        JsonResult<OutNoticeDetailManagePageVO> jsonResult = outNoticeService.manageDetailQuery(qo);
        Assert.assertNotNull(jsonResult.getData().getDetailDTOS());
    }

    @Test
    public void manageQueryUnFinishCount() throws Exception {
        OutNoticeQo qo = new OutNoticeQo();
        JsonResult<Long> jsonResult = outNoticeService.manageQueryUnFinishCount();
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void sendMessage(){
        produceMessageService.sendMessage("outOutFinish","{\"outNoticeNo\":\"cktzd2018062560099768\",\"taskId\":106,\"skuAmountMap\":{\"890\":1}}");
    }


    @Test
    public void queryOutNoticeCount() {
        BillCountQO qo = new BillCountQO();
        Integer count = outNoticeService.queryOutNoticeCount(qo);
        Assert.assertNotNull(count);
    }

    @Test
    public void queryAllOutNoticeNos() {

        JsonResult<List<String>> jsonResult = outNoticeService.queryAllOutNoticeNos();
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void queryWithDetailByNONull() {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutNoticeNo("cktzd2018091218423119201");
        JsonResult<OutNoticeManageVO> jsonResult = outNoticeService.queryWithDetailByNO(qo);
        Assert.assertNull(jsonResult.getData());
    }

    @Test
    public void queryWithDetailByNO() {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutNoticeNo("cktzd2018092511150110918");
        qo.setLoginId("xwcai");
        qo.setCurrentSystemType(2);
        JsonResult<OutNoticeManageVO> jsonResult = outNoticeService.queryWithDetailByNO(qo);
        Assert.assertNotNull(jsonResult.getData());
    }


    @Test
    public void queryNotLimitStock() {
        //添加库存
        WmsStockBean wmsStockBean = new WmsStockBean();
        wmsStockBean.setNums(1000);
        wmsStockBean.setSkuId(1298L);
        wmsStockBean.setBatchNo("180410G10");
        wmsStockBean.setRepoId(10011L);
        wmsStockBean.setStatus(3);
        wmsStockBean.setOwner(131L);
        wmsStockBean.setOwnerType(1);
        wmsStockBean.setRuleGroup(0);
        wmsStockBean.setRuleType(0);

        mapper.updateStock(wmsStockBean);

        ScanerUnLimitStockQO qo = new ScanerUnLimitStockQO();
        qo.setNoticeId(110619943L);
        qo.setMachineCode("359832070237733");
        JsonResult<List<ScanerNotLimitStockVO>> jsonResult = outNoticeService.queryNotLimitStock(qo);
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void queryNotLimitStockFail() {
        ScanerUnLimitStockQO qo = new ScanerUnLimitStockQO();
        qo.setNoticeId(1112312L);
        qo.setMachineCode("359832070237733");
        JsonResult<List<ScanerNotLimitStockVO>> jsonResult = outNoticeService.queryNotLimitStock(qo);
        Assert.assertEquals("该出库通知单已完成或不存在",jsonResult.getMsg());
    }

    @Test
    public void queryNotLimitStockFail2() {
        ScanerUnLimitStockQO qo = new ScanerUnLimitStockQO();
        qo.setNoticeId(11231L);
        qo.setMachineCode("359832070237733");
        JsonResult<List<ScanerNotLimitStockVO>> jsonResult = outNoticeService.queryNotLimitStock(qo);
        Assert.assertEquals("该出库通知单已完成或不存在",jsonResult.getMsg());
    }

    @Test
    public void updatePrintTimesByOutNoticeNo() {

        int i = outNoticeService.updatePrintTimesByOutNoticeNo("cktzd2018070600934250");
        System.out.println("----------------" + i);
        int n = outNoticeService.updatePrintTimesByOutNoticeNo("cktzd2018070600934250");
        System.out.println("----------------" + n);
    }

    @Test
    public void selectStockOutSkuInfo() {
        StockOutSkuInfoQO qo = new StockOutSkuInfoQO();
        List<String> outNoticeNos  = new ArrayList<>();
        outNoticeNos.add("cktzd2018071616728611");
        outNoticeNos.add("cktzd2018071237168262");
        qo.setNoticeNoList(outNoticeNos);
        JsonResult<List<StockOutSkuInfoVO>> jsonResult =  outNoticeService.selectStockOutSkuInfo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void selectStockOutSkuInfoByMap() {
        StockOutSkuInfoQO qo = new StockOutSkuInfoQO();
        List<String> outNoticeNos  = new ArrayList<>();
        outNoticeNos.add("cktzd2018071616728611");
        outNoticeNos.add("cktzd2018071237168262");
        qo.setNoticeNoList(outNoticeNos);
        JsonResult<Map<String, List<StockOutSkuInfoVO>>> jsonResult =  outNoticeService.selectStockOutSkuInfoByMap(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void checkOutNoticeStockOutScan() {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutNoticeNo("cktzd2018071237168262");
        JsonResult<Boolean> jsonResult = outNoticeService.checkOutNoticeStockOutScan(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void checkOutNoticeStockOutScanFail() {
        OutNoticeQo qo = new OutNoticeQo();
        qo.setOutNoticeNo("cktzd2018071237168261231232");
        JsonResult<Boolean> jsonResult = outNoticeService.checkOutNoticeStockOutScan(qo);
        Assert.assertEquals("该出库通知单不存在",jsonResult.getMsg());
    }

    @Test
    public void selectStockOutSapInfo() {
        StockOutSkuInfoQO qo = new StockOutSkuInfoQO();
        List<String> outNoticeNos  = new ArrayList<>();
        outNoticeNos.add("cktzd2018071616728611");
        outNoticeNos.add("cktzd2018071237168262");
        qo.setNoticeNoList(outNoticeNos);
        JsonResult<List<OutOutSapDetailVO>>  jsonResult = outNoticeService.selectStockOutSapInfo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void manageFinishOutNotice() {
        FinishOutNoticeBean bean = new FinishOutNoticeBean();
        bean.setNoticeId(11263L);
        JsonResult<Long> jsonResult = outNoticeService.manageFinishOutNotice(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void manageFinishOutNoticeFail() {
        FinishOutNoticeBean bean = new FinishOutNoticeBean();
        bean.setNoticeId(333312L);
        JsonResult<Long> jsonResult = outNoticeService.manageFinishOutNotice(bean);
        Assert.assertEquals("出库通知单不存在或已经完成",jsonResult.getMsg());
    }

    @Test
    public void manageFinishOutNoticeFail2() {
        FinishOutNoticeBean bean = new FinishOutNoticeBean();
        bean.setNoticeId(11112L);
        JsonResult<Long> jsonResult = outNoticeService.manageFinishOutNotice(bean);
        Assert.assertEquals("该业务类型不允许手动出库",jsonResult.getMsg());
    }

    @Test
    public void manageFinishOutNoticeFail3() {
        FinishOutNoticeBean bean = new FinishOutNoticeBean();
        bean.setNoticeId(333312L);
        JsonResult<Long> jsonResult = outNoticeService.manageFinishOutNotice(bean);
        Assert.assertEquals("出库通知单不存在或已经完成",jsonResult.getMsg());
    }

    @Test
    public void sendStockOutMsg() {
        SendOutNoticeBean sendOutNoticeBean = new SendOutNoticeBean();
        sendOutNoticeBean.setOutNo("ckd2018101610192627881");
        sendOutNoticeBean.setModifyId("test");
        JsonResult jsonResult = outNoticeService.sendStockOutMsg(sendOutNoticeBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

    }

    @Test
    public void sendStockOutMsgFail() {
        SendOutNoticeBean sendOutNoticeBean = new SendOutNoticeBean();
        sendOutNoticeBean.setOutNo("ckd2018101611231230192627881");
        sendOutNoticeBean.setModifyId("test");
        JsonResult jsonResult = outNoticeService.sendStockOutMsg(sendOutNoticeBean);
        Assert.assertEquals("查询不到出库单",jsonResult.getMsg());

    }

    @Test
    public void sendStockOutMsgFail2() {
        SendOutNoticeBean sendOutNoticeBean = new SendOutNoticeBean();
        sendOutNoticeBean.setOutNo("ckd2018101615071454200");
        sendOutNoticeBean.setModifyId("test");
        JsonResult jsonResult = outNoticeService.sendStockOutMsg(sendOutNoticeBean);
        Assert.assertEquals("查询不到出库通知单单",jsonResult.getMsg());

    }

    @Test
    public void queryWithScanerShow() {

        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeNo("cktzd2018091714440232431");
        qo.setLoginId("xwcai");
        qo.setMachineCode("359832070237733");
        qo.setCurrentSystemType(2);
        JsonResult jsonResult = outNoticeService.queryWithScanerShow(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryWithScanerShowEmpty() {

        ScanerNoticeQo qo = new ScanerNoticeQo();
        qo.setNoticeNo("cktzd20180917004604337813");
        qo.setLoginId("xwcai");
        qo.setMachineCode("359832070237733");
        qo.setCurrentSystemType(4);
        JsonResult jsonResult = outNoticeService.queryWithScanerShow(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        Assert.assertNull(jsonResult.getData());
    }

    @Test
    public void batchQueryWithDetailByNOs() {
        OutNoticeBatchQO qo = new OutNoticeBatchQO();
        List<String> list = new ArrayList<>();
        list.add("cktzd2018091714440232431");
        qo.setOutNoticeNos(list);
        JsonResult jsonResult = outNoticeService.batchQueryWithDetailByNOs(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void checkAcceptOrderStatus() {
        OutNoticeBatchQO qo = new OutNoticeBatchQO();
        List<String> list = new ArrayList<>();
        //list.add("cktzd2018091714440232431");
        list.add("cktzd2018103010100112630");
        list.add("cktzd2018103010100112634");
        list.add("cktzd2018103010100112639");
        qo.setOutNoticeNos(list);
        JsonResult jsonResult = outNoticeService.checkAcceptOrderStatus(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void deleteNoticeAndDetail() {
        OutNoticeBatchQO qo = new OutNoticeBatchQO();
        List<String> list = new ArrayList<>();
        list.add("cktzd2018092717352632102");
        qo.setOutNoticeNos(list);
        qo.setModifyId("xwcai");
        qo.setCurrentSystemType(2);
        JsonResult jsonResult = outNoticeService.deleteNoticeAndDetail(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }
}