package com.nfsq.supply.chain.stock.microservice.out.service;

import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.RollBackBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import com.nfsq.supply.chain.stock.api.model.bean.StockOutBean;
import com.nfsq.supply.chain.stock.api.model.enums.IsPageEnum;
import com.nfsq.supply.chain.stock.api.model.enums.ScanerType;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.OutOutPageQO;
import com.nfsq.supply.chain.stock.api.model.vo.*;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOut;
import com.nfsq.supply.chain.stock.microservice.out.dal.domain.WmsOutOutDetail;
import com.nfsq.supply.chain.stock.microservice.out.model.qo.OutOutQo;
import com.nfsq.supply.chain.stock.microservice.out.model.vo.OutOutListVo;
import com.nfsq.supply.chain.utils.common.DateUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * @author xwcai
 * @date 2018/4/23 下午5:31
 */
public class OutOutServiceTest extends BaseTest{



    @Autowired
    OutOutService outOutService;

    @Test
    public void createOutOut() throws Exception {
        StockOutBean outBean = new StockOutBean();
        outBean.setOutDate(DateUtils.addDay(new Date(),1));
        outBean.setRepoId(84l);
        outBean.setOutNoticeId(854l);
        outBean.setModifyId("test");
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(1);
        bean1.setBatchNo("0713300");
        bean1.setSkuId(892l);
        bean1.setOwner(101L);
        bean1.setOwnerType(1);
        bean1.setScanType(ScanerType.NOT_SCAN.getType());
        bean1.setRuleGroup(0);
        bean1.setRuleType(0);
        SkuInfoBean bean2 = new SkuInfoBean();
        bean2.setAmount(71);
        bean2.setBatchNo("0713300");
        bean2.setSkuId(892l);
        bean2.setOwner(101L);
        bean2.setOwnerType(1);
        bean2.setScanType(ScanerType.NOT_SCAN.getType());
        bean2.setRuleGroup(0);
        bean2.setRuleType(0);
        SkuInfoBean bean3 = new SkuInfoBean();
        bean3.setAmount(1);
        bean3.setBatchNo("A0478");
        bean3.setSkuId(890l);
        bean3.setOwner(101L);
        bean3.setOwnerType(1);
        bean3.setScanType(ScanerType.NOT_SCAN.getType());
        bean3.setRuleGroup(0);
        bean3.setRuleType(0);
        SkuInfoBean bean4 = new SkuInfoBean();
        bean4.setAmount(71);
        bean4.setBatchNo("E0668");
        bean4.setSkuId(890l);
        bean4.setOwner(101L);
        bean4.setOwnerType(1);
        bean4.setScanType(ScanerType.NOT_SCAN.getType());
        bean4.setRuleGroup(0);
        bean4.setRuleType(0);

        SkuInfoBean bean5 = new SkuInfoBean();
        bean5.setAmount(4);
        bean5.setBatchNo("A0478");
        bean5.setSkuId(894l);
        bean5.setOwner(101L);
        bean5.setOwnerType(1);
        bean5.setScanType(ScanerType.SCAN.getType());
        bean5.setRuleGroup(0);
        bean5.setRuleType(0);

        SkuInfoBean bean6 = new SkuInfoBean();
        bean6.setAmount(3);
        bean6.setBatchNo("20180726");
        bean6.setSkuId(893l);
        bean6.setOwner(101L);
        bean6.setOwnerType(1);
        bean6.setScanType(ScanerType.FILL_A_VACANCY.getType());
        bean6.setRuleGroup(0);
        bean6.setRuleType(0);
        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());

        //0 非限制
        bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean4.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean5.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean6.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(bean1);
        skuInfoBeanList.add(bean2);
        skuInfoBeanList.add(bean3);
        skuInfoBeanList.add(bean4);
        skuInfoBeanList.add(bean5);
        skuInfoBeanList.add(bean6);
        outBean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult = outOutService.createOutOut(outBean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createOutOutFail() throws Exception {
        StockOutBean outBean = new StockOutBean();
        outBean.setOutDate(DateUtils.addDay(new Date(),1));
        outBean.setRepoId(84l);
        outBean.setOutNoticeId(851231234l);
        outBean.setModifyId("test");
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(1);
        bean1.setBatchNo("0713300");
        bean1.setSkuId(892l);
        bean1.setOwner(101L);
        bean1.setOwnerType(1);
        bean1.setScanType(ScanerType.NOT_SCAN.getType());
        bean1.setRuleGroup(0);
        bean1.setRuleType(0);

        skuInfoBeanList.add(bean1);
        outBean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult = outOutService.createOutOut(outBean);
        Assert.assertEquals("该通知单不存在",jsonResult.getMsg());
    }

    @Test
    public void createOutOutFail2() throws Exception {
        StockOutBean outBean = new StockOutBean();
        outBean.setOutDate(DateUtils.addDay(new Date(),1));
        outBean.setRepoId(84l);
        outBean.setOutNoticeId(1441l);
        outBean.setModifyId("test");
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(1);
        bean1.setBatchNo("0713300");
        bean1.setSkuId(892l);
        bean1.setOwner(101L);
        bean1.setOwnerType(1);
        bean1.setScanType(ScanerType.NOT_SCAN.getType());
        bean1.setRuleGroup(0);
        bean1.setRuleType(0);

        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());

        //0 非限制
        skuInfoBeanList.add(bean1);
        outBean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult = outOutService.createOutOut(outBean);
        Assert.assertEquals("查询不到对应的出库规则",jsonResult.getMsg());
    }

    @Test
    public void createOutOutFail3() throws Exception {
        StockOutBean outBean = new StockOutBean();
        outBean.setOutDate(DateUtils.addDay(new Date(),1));
        outBean.setRepoId(84l);
        outBean.setOutNoticeId(854l);
        outBean.setModifyId("test");
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean bean1 = new SkuInfoBean();
        bean1.setAmount(1);
        bean1.setBatchNo("0713300");
        bean1.setSkuId(892l);
        bean1.setOwner(101L);
        bean1.setOwnerType(1);
        bean1.setScanType(ScanerType.NOT_SCAN.getType());
        bean1.setRuleGroup(0);
        bean1.setRuleType(0);
        SkuInfoBean bean2 = new SkuInfoBean();
        bean2.setAmount(70);
        bean2.setBatchNo("0713300");
        bean2.setSkuId(892l);
        bean2.setOwner(101L);
        bean2.setOwnerType(1);
        bean2.setScanType(ScanerType.NOT_SCAN.getType());
        bean2.setRuleGroup(0);
        bean2.setRuleType(0);
        SkuInfoBean bean3 = new SkuInfoBean();
        bean3.setAmount(1);
        bean3.setBatchNo("A0478");
        bean3.setSkuId(890l);
        bean3.setOwner(101L);
        bean3.setOwnerType(1);
        bean3.setScanType(ScanerType.NOT_SCAN.getType());
        bean3.setRuleGroup(0);
        bean3.setRuleType(0);
        SkuInfoBean bean4 = new SkuInfoBean();
        bean4.setAmount(71);
        bean4.setBatchNo("E0668");
        bean4.setSkuId(890l);
        bean4.setOwner(101L);
        bean4.setOwnerType(1);
        bean4.setScanType(ScanerType.NOT_SCAN.getType());
        bean4.setRuleGroup(0);
        bean4.setRuleType(0);

        SkuInfoBean bean5 = new SkuInfoBean();
        bean5.setAmount(40);
        bean5.setBatchNo("A0478");
        bean5.setSkuId(894l);
        bean5.setOwner(101L);
        bean5.setOwnerType(1);
        bean5.setScanType(ScanerType.SCAN.getType());
        bean5.setRuleGroup(0);
        bean5.setRuleType(0);

        SkuInfoBean bean6 = new SkuInfoBean();
        bean6.setAmount(3);
        bean6.setBatchNo("2018072612");
        bean6.setSkuId(893l);
        bean6.setOwner(101L);
        bean6.setOwnerType(1);
        bean6.setScanType(ScanerType.SCAN.getType());
        bean6.setRuleGroup(0);
        bean6.setRuleType(0);
        bean1.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());

        //0 非限制
        bean3.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean2.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean4.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean5.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        bean6.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(bean1);
        outBean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult = outOutService.createOutOutWithSap(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        skuInfoBeanList.add(bean2);
        outBean.setSkuInfoList(skuInfoBeanList);
         jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        skuInfoBeanList.add(bean3);
        outBean.setSkuInfoList(skuInfoBeanList);
        jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        skuInfoBeanList.add(bean4);
        outBean.setSkuInfoList(skuInfoBeanList);
        jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        skuInfoBeanList.add(bean5);
        outBean.setSkuInfoList(skuInfoBeanList);
        jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        skuInfoBeanList.add(bean6);
        outBean.setSkuInfoList(skuInfoBeanList);
        jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
        outBean.setSkuInfoList(skuInfoBeanList);
         jsonResult = outOutService.createOutOut(outBean);
        Assert.assertFalse(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void getByOutNoticeNo() throws Exception {

        List<WmsOutOut> outOut = outOutService.getByOutNoticeId(1901l);
        Assert.assertNotNull(outOut);
    }

    @Test
    public void queryOutByQo() throws Exception {
        OutOutQo qo = new OutOutQo();
        JsonResult<OutOutListVo> jsonResult = outOutService.queryOutByQo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryOutByQoNull() throws Exception {
        OutOutQo qo = new OutOutQo();
        qo.setOutNo("123123");
        JsonResult<OutOutListVo> jsonResult = outOutService.queryOutByQo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void getDetailByOutNo() throws Exception {

        JsonResult<List<WmsOutOutDetail>> jsonResult = outOutService.getDetailByOutId(1L);
        Assert.assertNull(jsonResult.getData());
    }

    @Test
    public void getDetailByOutNoNotNull() throws Exception {

        JsonResult<List<WmsOutOutDetail>> jsonResult = outOutService.getDetailByOutId(88L);
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void manageQueryByQo() throws Exception {
        OutOutPageQO qo = new OutOutPageQO();
        JsonResult<OutManageListVO> jsonResult = outOutService.manageQueryByQo(qo);
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void manageQueryByQoNull() throws Exception {
        OutOutPageQO qo = new OutOutPageQO();
        qo.setType(1);
        qo.setOutNo("123123123");
        JsonResult<OutManageListVO> jsonResult = outOutService.manageQueryByQo(qo);
        Assert.assertNull(jsonResult.getData().getVoList());
    }

    @Test
    public void manageDetailQueryByQo() throws Exception {
        OutOutPageQO qo = new OutOutPageQO();
        JsonResult<OutManageVO> jsonResult0 = outOutService.manageDetailQueryByQo(qo);
        Assert.assertEquals("参数为空",jsonResult0.getMsg());
        qo.setOutNo("ckd2018070907488241");
        JsonResult<OutManageVO> jsonResult1 = outOutService.manageDetailQueryByQo(qo);
        Assert.assertTrue(jsonResult1.getMsg(),jsonResult1.isSuccess());
        qo.setIsPage(IsPageEnum.IS_NOT_PAGE.getCode());
        JsonResult<OutManageVO> jsonResult = outOutService.manageDetailQueryByQo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
        qo.setOutNo("ckd2018070907481231238241");
        jsonResult = outOutService.manageDetailQueryByQo(qo);
        Assert.assertNull(jsonResult.getData());
    }

    @Test
    public void queryOutOutCount() {
        BillCountQO qo = new BillCountQO();

        Integer count = outOutService.queryOutOutCount(qo);
        Assert.assertTrue(count >= 0);
    }

    @Test
    public void queryAllOutNos() {

        JsonResult<List<String>> jsonResult = outOutService.queryAllOutNos();
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void selectStockOutSkuInfo() {
        List<String> outNoticeNos  = new ArrayList<>();
        outNoticeNos.add("cktzd2018071616728611");
        outNoticeNos.add("cktzd2018071237168262");
        List<StockOutSkuInfoVO> list =  outOutService.selectStockOutSkuInfo(outNoticeNos);
        Assert.assertNotNull(list);
    }

    @Test
    public void selectStockOutSkuInfoByMap() {
        List<String> outNoticeNos  = new ArrayList<>();
        outNoticeNos.add("cktzd2018071616728611");
        outNoticeNos.add("cktzd2018071237168262");
        List<StockOutSkuInfoVO> list =  outOutService.selectStockOutSkuInfoByMap(outNoticeNos);
        Assert.assertNotNull(list);
    }
}