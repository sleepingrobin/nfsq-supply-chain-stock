package com.nfsq.supply.chain.stock.microservice.in.service;

import com.nfsq.supply.chain.dataAuthority.api.model.enums.SystemTypeEnum;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.ConfirmBO;
import com.nfsq.supply.chain.stock.api.model.bean.SkuInfoBean;
import com.nfsq.supply.chain.stock.api.model.bean.StockInBean;
import com.nfsq.supply.chain.stock.api.model.enums.ScanerType;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.BillCountQO;
import com.nfsq.supply.chain.stock.api.model.qo.InInQO;
import com.nfsq.supply.chain.stock.api.model.vo.InDetailManageListVO;
import com.nfsq.supply.chain.stock.api.model.vo.InManageListVO;
import com.nfsq.supply.chain.stock.microservice.baseinfo.service.BaseInfoServiceTest;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInIn;
import com.nfsq.supply.chain.stock.microservice.in.dal.domain.WmsInInDetail;
import com.nfsq.supply.chain.stock.microservice.in.model.vo.InInVo;
import com.nfsq.supply.chain.utils.common.DateUtils;
import com.nfsq.supply.chain.utils.common.JsonResult;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xwcai
 * @date 2018/4/23 下午3:53
 */
public class InInServiceTest extends BaseTest{

    @Autowired
    InInService inInService;

    @Test
    public void createInIn() throws Exception {
        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(329l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(6);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(892l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInIn(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryInInByQo() throws Exception {

        InInQO qo = new InInQO();
        BaseInfoServiceTest.setSystemType();
        JsonResult jsonResult = inInService.queryInInByQo(qo);
        Assert.assertNotNull(jsonResult.getData());
    }

    @Test
    public void queryInInByQoFail() throws Exception {
        InInQO qo = new InInQO();
        JsonResult<InManageListVO> jsonResult = inInService.queryInInByQo(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());

    }

    @Test
    public void getByInInNO() throws Exception {

        InInVo inInVo = inInService.getByInInNO("rkd2018070609393277");
        Assert.assertNotNull(inInVo);
    }

    @Test
    public void getByInNoticeId() throws Exception {

        List<WmsInIn> inIns = inInService.getByInNoticeId(152L);
        Assert.assertNotNull(inIns);
    }

    @Test
    public void getDetailByInId() throws Exception {

        InInQO qo = new InInQO();
        qo.setId(57L);
        JsonResult<InDetailManageListVO> jsonResult = inInService.getDetailByQO(qo);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void confirmInInFail() throws Exception {
        ConfirmBO confirmBO = new ConfirmBO();
        confirmBO.setTaskId(510l);
        confirmBO.setRepoId(84l);
        confirmBO.setModifyId("test");
        JsonResult<Boolean> jsonResult = inInService.confirmInIn(confirmBO);

        Assert.assertEquals("根据任务单号查询不到入库通知单",jsonResult.getMsg());
    }

    @Test
    public void confirmInIn() throws Exception {
        ConfirmBO confirmBO = new ConfirmBO();
        confirmBO.setTaskId(22L);
        confirmBO.setRepoId(1012l);
        confirmBO.setModifyId("test");
        JsonResult<Boolean> jsonResult = inInService.confirmInIn(confirmBO);

        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void queryProctInInCount() {
        BillCountQO qo = new BillCountQO();

//        Integer count = inInService.queryProctInInCount(qo);

        System.out.println("123");
    }

    @Test
    public void selectAllNOs() {

        JsonResult<List<String>> jsonResult = inInService.selectAllNOs();
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createInInWithSap() {

        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(329l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(6);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(892l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInInWithSap(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createInInWithSapFail() {

        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(1l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(6);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(892l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInInWithSap(bean);
        Assert.assertEquals("该入库通知单不存在",jsonResult.getMsg());
    }

    @Test
    public void createInInMore() throws Exception {
        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(329l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(7);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(892l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInIn(bean);
        Assert.assertEquals("入库数量超出限制",jsonResult.getMsg());
    }

    @Test
    public void createInInLess() throws Exception {
        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(329l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(3);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(892l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInIn(bean);
        Assert.assertTrue(jsonResult.getMsg(),jsonResult.isSuccess());
    }

    @Test
    public void createInInMoreSku() throws Exception {
        StockInBean bean = new StockInBean();
        bean.setModifyId("test");
        bean.setInDate(DateUtils.addDay(new Date(),2));
//        bean.set("a79eeeae949741d2ac9e42ab0f6ba7d4");
        bean.setRepoId(91l);
        bean.setInNoticeId(329l);
        List<SkuInfoBean> skuInfoBeanList = new ArrayList<>();
        SkuInfoBean skuInfoBean = new SkuInfoBean();
        skuInfoBean.setAmount(6);
        skuInfoBean.setBatchNo("CAH3");
        skuInfoBean.setSkuId(8992l);
        skuInfoBean.setOwner(1l);
        skuInfoBean.setOwnerType(3);
        //0 非限制
        skuInfoBean.setStatus(StockStatusEnums.WMSINVENTORYSTOCK_NOT_LIMIT.getCode());
        skuInfoBeanList.add(skuInfoBean);
        skuInfoBean.setScanType(ScanerType.NOT_SCAN.getType());
        bean.setSkuInfoList(skuInfoBeanList);
        JsonResult<String> jsonResult =  inInService.createInIn(bean);
        Assert.assertEquals("入库扫码出现不匹配的sku：{}8992批次信息CAH3",jsonResult.getMsg());
    }
}