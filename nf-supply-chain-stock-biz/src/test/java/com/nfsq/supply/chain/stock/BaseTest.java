package com.nfsq.supply.chain.stock;

import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityBean;
import com.nfsq.supply.chain.utils.dataAuthority.DataAuthorityUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {NfSupplyChainStockBizApplication.class},webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("dev")
@Transactional
@Slf4j
public abstract class BaseTest {

    @Before
    public void setBaseSystemType(){
        log.info("开始权限赋值");
        DataAuthorityBean bean=new DataAuthorityBean();
        bean.setLoginId("xwcai");

        bean.setCurrentSystemType(2);
        DataAuthorityUtils.setCurrentDataAuthorityBean(bean);
    }

}

####