package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStockDetail;


public class WmsInventoryStockDetailMapperTest extends BaseTest{
	
	@Autowired
	private WmsInventoryStockDetailMapper wmsInventoryStockDetailMapper;
	
	@Test
	public void testDeleteByPrimaryKey() {
		wmsInventoryStockDetailMapper.deleteByPrimaryKey(103L); 
	}

	@Test
	public void testInsert() {
		WmsInventoryStockDetail record = new WmsInventoryStockDetail();
		record.setBatchNo("233");
		record.setBatchId(233l);
		record.setCreatedDate(new Date());
		record.setCreatedUser("发as");
		record.setType(3);
		record.setNums(23);
		record.setOperStatus(2);
		record.setOrderDetailId(2332L);
		record.setSkuId(334L);
		record.setStatus(2);
		record.setStatus(2);
		record.setStockId(2343L);
		wmsInventoryStockDetailMapper.insert(record);
	}

	@Test
	public void testInsertSelective() {
		WmsInventoryStockDetail record = new WmsInventoryStockDetail();
		record.setBatchNo("233");
		record.setBatchId(2445l);
		record.setCreatedDate(new Date());
		record.setCreatedUser("发as2");
		record.setType(3);
		record.setNums(23);
		record.setOperStatus(2);
		record.setOrderDetailId(2332L);
		record.setOwner(1L);
		record.setOwnerType(1);
		record.setRuleType(1);
		record.setRuleGroup(1);
		record.setSkuId(334L);
		record.setStatus(2);
		record.setStockId(2343L);
		wmsInventoryStockDetailMapper.insertSelective(record);
	}

	@Test
	public void testSelectByPrimaryKey() {
		WmsInventoryStockDetail wmsInventoryStockDetail = wmsInventoryStockDetailMapper.selectByPrimaryKey(104L);
	}

	@Test
	public void testUpdateByPrimaryKeySelective() {
		WmsInventoryStockDetail record = new WmsInventoryStockDetail();
		record.setBatchNo("1992");
		record.setId(103L);
		wmsInventoryStockDetailMapper.updateByPrimaryKey(record);
	}

	@Test
	public void testUpdateByPrimaryKey() {
		WmsInventoryStockDetail record = new WmsInventoryStockDetail();
		record.setBatchNo("1993");
		record.setId(103L);
		wmsInventoryStockDetailMapper.updateByPrimaryKeySelective(record);
	}

}
