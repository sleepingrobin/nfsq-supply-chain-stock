package com.nfsq.supply.chain.stock.microservice.manage.dal.mapper;


import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.nfsq.supply.chain.stock.BaseTest;
import com.nfsq.supply.chain.stock.api.model.bean.SkuNumsBO;
import com.nfsq.supply.chain.stock.api.model.dto.WmsInventoryStockDTO;
import com.nfsq.supply.chain.stock.api.model.enums.StockStatusEnums;
import com.nfsq.supply.chain.stock.api.model.qo.WmsStockQO;
import com.nfsq.supply.chain.stock.microservice.manage.bean.WmsStockBean;
import com.nfsq.supply.chain.stock.microservice.manage.dal.domain.WmsInventoryStock;

public class WmsInventoryStockMapperTest extends BaseTest{
	
	@Autowired
	private WmsInventoryStockMapper wmsInventoryStockMapper;
	
	@Test
	public void testQueryStoreStock() {
		Long repoId=84L;
		List<Integer> statuses=StockStatusEnums.getStoreStatus();
		Long owner=11L;
		List<WmsInventoryStock> list=wmsInventoryStockMapper.queryStoreStock(repoId,statuses,owner);
		System.out.println(list);
	}
	
	@Test
    public void testQueryStock() {
        Long repoId=84L;
        WmsStockQO stock=new WmsStockQO();
        List<Long> repoIds=new ArrayList<Long>();
        repoIds.add(repoId);
        repoIds.add(84L);
        stock.setRepoIds(repoIds);
        //stock.setCurrentSystemType(1);
        List<WmsInventoryStockDTO> list=wmsInventoryStockMapper.queryStockPage(stock);
        System.out.println(list);
    }
	
	@Test
    public void testQueryStockNums() {
        Long repoId=84L;
        Long skuId=890L;
        List<SkuNumsBO> list=wmsInventoryStockMapper.selectUnlimitStockByCondition(skuId,repoId,3,"0000011839",101L,1);
        System.out.println(list);
    }
	
	@Test
    public void testQueryActualStockNums() {
        Long repoId=84L;
        Long skuId=890L;
        Long nums=wmsInventoryStockMapper.queryActualStock(skuId, StockStatusEnums.getAvaliableStatus(), repoId, 0, 3, Lists.newArrayList(2233L));
        System.out.println(nums);
    }
	
	@Test
    public void testQueryStocks() {
	    WmsStockBean wmsStockBean=new WmsStockBean();
	    wmsStockBean.setBatchNo("20181001");
	    wmsStockBean.setRepoId(11L);
	    wmsStockBean.setStatus(3);
	    wmsStockBean.setOwner(168L);
	    wmsStockBean.setOwnerType(3);
	    List<Long> skuIdList=new ArrayList<>();
	    skuIdList.add(25514L);
	    skuIdList.add(1392L);
	    List<WmsInventoryStock> list=wmsInventoryStockMapper.queryStocks(wmsStockBean,skuIdList);
        System.out.println(list);
    }	
}
